package com.apt.readers;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

import com.apt.data.Domain;
import com.apt.data.Mesh;
import com.apt.data.Node;
import com.apt.data.RegularSquareMesh;
import com.apt.data.Spacing;

public class RegularGridBathymetryReader {

	/**
	 * Read and parse a regular square grid bathymetry file.
	 * 
	 * @param meshFile
	 * @throws Exception
	 */
	//public Mesh readFile(File meshDescriptionFile, File bathymetryFile) throws Exception {
		public Mesh readFile(File bathymetryFile) throws Exception {
			
		/** The mesh.*/
		RegularSquareMesh mesh;
		
		/** Minimum x value of domain.*/
		double xmin = 0.0;
		
		/** Node spacing x.*/
		double xstep = 0.0;
		
		/** Number of nodes in x.*/
		int nx = 0;
		
		/** Minimum y value of domain.*/
		double ymin = 0.0;
		
		/** Node spacing y.*/
		double ystep = 0.0;
		
		/** Number of nodes in y.*/
		int ny = 0;

		//double[][] nodePoints = new double[ny][nx];
		
		List<Node> nodes = new ArrayList<Node>();
		
		try (FileReader fr = new FileReader(bathymetryFile); BufferedReader br = new BufferedReader(fr)) {

			// this is a square messh so we expect data for nx*ny nodes
		
			int dataLineCount = 0;
			String line = null;
			while ((line = br.readLine()) != null) {

				// skip comments
				if (line.startsWith("#"))
					continue;
				
				// skip empty lines
				if (line.isEmpty())
					continue;
				
				// extract metadata
				if (line.startsWith("grid:")) {
					String part = line.substring(5).trim();
					System.err.printf("RGR::Parsing regular grid: type: %s\n", part);
				} else if (line.startsWith("xmin:")) {
					String part = line.substring(5).trim();
					xmin = Double.parseDouble(part);
					System.err.printf("RGR::Parsing regular grid: xmin: %f\n", xmin);
				} else if (line.startsWith("xstep:")) {
					String part = line.substring(6).trim();
					xstep = Double.parseDouble(part);
					System.err.printf("RGR::Parsing regular grid: xstep: %f\n", xstep);
				} else if (line.startsWith("nx:")) {
					String part = line.substring(3).trim();
					nx = Integer.parseInt(part);
					System.err.printf("RGR::Parsing regular grid: nx: %d\n", nx);
				} else if (line.startsWith("ymin:")) {
					String part = line.substring(5).trim();
					ymin = Double.parseDouble(part);
					System.err.printf("RGR::Parsing regular grid: ymin: %f\n", ymin);
				} else if (line.startsWith("ystep:")) {
					String part = line.substring(6).trim();
					ystep = Double.parseDouble(part);
					System.err.printf("RGR::Parsing regular grid: ystep: %f\n", ystep);
				} else if (line.startsWith("ny:")) {
					String part = line.substring(3).trim();
					ny = Integer.parseInt(part);
					System.err.printf("RGR::Parsing regular grid: ny: %d\n", ny);
				} else {
					// a line of data... x,y,z
					dataLineCount++; // line count starting 1
					String[] parts = line.split(","); // check there are 3 items
					if (parts.length < 3)
						throw new Exception(String.format("RGR::Line: %d : contains wrong number of items: %d :: %s", dataLineCount, parts.length, line));
					
					double x = Double.parseDouble(parts[0].trim());
					double y = Double.parseDouble(parts[1].trim());
					double z = Double.parseDouble(parts[2].trim());
					//int ref = Integer.parseInt();
					// identify cell number 
					//int i = (int)Math.floor((x - xmin)/xstep);
					//int j = (int)Math.floor((y - ymin)/ystep);
					
					//nodePoints[j][i] = z; // effectively a cell surrounding the node
					
					// create an actual node
					Node node = new Node(dataLineCount, x, y);
					node.depth = z;
					
					nodes.add(node);
				}

			} // next line
		
			
			// create the domain bounds
			Domain domain = new Domain(xmin, xmin+xstep*(nx-1), ymin, ymin+ystep*(ny-1));
			Spacing spacing = new Spacing(xstep, ystep);
			System.err.printf("RGR::Reading regular grid: found: %d lines of data expected %d lines \n", dataLineCount, nx*ny);
			System.err.printf("RGR::Expecting total of %d cells \n", (nx-1)*(ny-1));
			mesh = new RegularSquareMesh(domain, spacing, nodes); // need new params

		}

		return mesh;
		
	}

}
