package com.apt.readers;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.apt.data.Cell;
import com.apt.data.Domain;
import com.apt.data.Node;
import com.apt.data.NodeType;
import com.apt.data.TriangularMesh;

import ucar.ma2.ArrayFloat;
import ucar.ma2.ArrayInt;
import ucar.nc2.Dimension;
import ucar.nc2.NetcdfFile;
import ucar.nc2.Variable;
import ucar.nc2.dataset.NetcdfDataset;

/** Read a mesh file into an internal data structure containing the mesh and flow data and later seabed data.*/
public class HydroModelReader {

	TriangularMesh mesh;
	
	private ArrayFloat.D1 lat;
	private ArrayFloat.D1 lon;

	private ArrayFloat.D1 latc;
	private ArrayFloat.D1 lonc;

	private ArrayFloat.D1 x;
	private ArrayFloat.D1 y;

	private ArrayFloat.D1 xc;
	private ArrayFloat.D1 yc;

	private ArrayFloat.D1 h; // depth below geoid
	private ArrayFloat.D2 zeta; // tidal height above geoid

	private ArrayFloat.D2 siglay;
	private ArrayFloat.D2 siglev;
	
	private ArrayFloat.D3 u; 
	private ArrayFloat.D3 v;
	
	private ArrayFloat.D1 times; 

	//private ArrayInt.D1 ntve; // number of elements round each node
	//private ArrayInt.D2 nbve; // the elements round each node (using ntve cumulative sum!)
	private ArrayInt.D2 nv; // nodes round element

	private ArrayInt.D1 bnodes;
	private ArrayInt.D1 obnodes;
	
	/**
	 * Create a reader to process  
	 * @param flowFile
	 * @throws Exception
	 */
	public HydroModelReader(File flowFile) throws Exception {
		
		readFile(flowFile);
		
	}
	
	private void readFile(File flowFile) throws Exception {
	
		NetcdfFile file = NetcdfFile.open(flowFile.getPath());
		NetcdfDataset data = new NetcdfDataset(file);

		List<Dimension> sdim = data.getDimensions();
		System.err.println("Overall dimensions...");
		for (Dimension d : sdim) {
			System.err.printf("Dim: %s : %d \n", d.getFullName(), d.getLength());
		}
		System.err.println();

		List<Variable> vars = data.getVariables();
		System.err.println("All variables...");
		for (Variable v : vars) {
			System.err.printf("Var: %s (%s): [", v.getFullName(), v.getDataType().toString());
			List<Dimension> dds = v.getDimensions();
			for (Dimension d : dds) {
				System.err.printf(" %s ", d.getFullName());
			}
			System.err.printf("]\n");
		}
		System.err.println();

		Variable vlat = data.findVariable("lat"); // y
		lat = (ArrayFloat.D1) vlat.read();

		Variable vlon = data.findVariable("lon"); // x
		lon = (ArrayFloat.D1) vlon.read();

		Variable vlatc = data.findVariable("latc"); // yc
		latc = (ArrayFloat.D1) vlatc.read();

		Variable vlonc = data.findVariable("lonc"); // xc
		lonc = (ArrayFloat.D1) vlonc.read();

		double xmin = 999999.9;
		double ymin = 999999.9;
		double xmax = -999999.9;
		double ymax = -999999.9;
		
		for (int i = 0; i < lat.getSize(); i++) {
			if (lat.get(i) < ymin)
				ymin = lat.get(i);
			if (lat.get(i) > ymax)
				ymax = lat.get(i);
			if (lon.get(i) < xmin)
				xmin = lon.get(i);
			if (lon.get(i) > xmax)
				xmax = lon.get(i);
		}
		Domain domain = new Domain(xmin, xmax, ymin, ymax);

		System.err.printf("Range: (%f, %f) -> (%f, %f) \n", xmin, ymin, xmax, ymax);

		Variable vh = data.findVariable("bathymetry");
		h = (ArrayFloat.D1) vh.read();

		Variable vu = data.findVariable("u");
		u = (ArrayFloat.D3) vu.read();
		int[] us = u.getShape(); // us#0 is the number of time steps
		for (Integer i : us) {System.err.printf("us: %d ", i);}
		System.err.println();
		
		Variable vv = data.findVariable("v");
		v = (ArrayFloat.D3) vv.read();
		int[] vs = v.getShape();
		for (Integer i : vs) {System.err.printf("vs: %d ", i);}
		System.err.println();
		
		//Variable var_ntve = data.findVariable("ntve");
		//ntve = (ArrayInt.D1) var_ntve.read();

		//Variable var_nbve = data.findVariable("nbve");
		//nbve = (ArrayInt.D2) var_nbve.read();

		Variable var_nv = data.findVariable("nv");
		nv = (ArrayInt.D2) var_nv.read();

		//Variable var_siglay = data.findVariable("siglay_center");
		//siglay = (ArrayFloat.D2) var_siglay.read();

		//Variable var_zeta = data.findVariable("zeta");
		//zeta = (ArrayFloat.D2) var_zeta.read();

		// mesh data
		Variable var_bnodes = data.findVariable("bnodes");
		bnodes = (ArrayInt.D1) var_bnodes.read();

		Variable var_obnodes = data.findVariable("obnodes");
		obnodes = (ArrayInt.D1) var_obnodes.read();

		Variable var_time = data.findVariable("time");
		times = (ArrayFloat.D1)var_time.read();
		int ntimes = times.getShape()[0];
		// save this so we can fill the flowtimes array with them....
		int[] flowTimes = new int[ntimes];
		
		
		// extract node infos
		Map<Integer, Node> nodes = new HashMap<Integer, Node>();
		int[] shapeN = lat.getShape();
		for (int j = 0; j < shapeN[0]; j++) {
			double flat = lat.getDouble(j);
			double flon = lon.getDouble(j);
			Node n = new Node(j+1, flon, flat);
			n.type = NodeType.SEA; // we need to check bnodes and obnodes to find out this info
			n.depth = h.getDouble(j);
			nodes.put(j+1, n);
			System.err.printf("Extract node: %s \n", n);
		}
		
		// check cells
		Map<Integer, Cell> cells = new HashMap<Integer, Cell>();
		int[] shapeNv = nv.getShape(); // numsurrounding x num cells
		System.err.printf("Extracting upto: %d x %d cells...", shapeNv[0], shapeNv[1]);
		for (int j = 0; j < shapeNv[1]; j++) { // cell number ref
			Node[] mynodes = new Node[3];
			System.err.printf("Cell: %d ", j+1);
			for (int i = 0; i < shapeNv[0]; i++) {
				// nv(i,j) i : 0,1,2, j: cells
				int inode = nv.get(i, j);
				mynodes[i] = nodes.get(inode);
				System.err.printf(" %d ", inode);
			}
			System.err.println();
			double flat = latc.getFloat(j);//??
			double flon = lonc.getFloat(j);//??
			Cell c = new Cell(j+1, mynodes); // recalc the xc,yc values!!
			c.xc = flat;
			c.yc = flon;
			
			// TODO redesign this in Cell as a 2D var (time, layer)
			// should extract u.getDouble(time, *, j););
			//c.times = new double[ntimes];
			
			//c.u = new double[us[0]]; // one entry per timestep
			//c.v = new double[vs[0]]; // one entry per timestep
			for (int t = 0; t < us[0]; t++) {
				//c.u[t] = u.get(t, 1, j); // NOTE: using level 1 only of siglevs
				//c.v[t] = v.get(t, 1, j); // NOTE: using level 1 only of siglevs
				double atime = times.getDouble(t);
				//c.times[t] = atime;
				flowTimes[t] = (int)atime; // keep overwriting this - a waste but for now....
			}
			System.err.printf("Extract cell: %s \n", c);
			cells.put(c.ref, c);
		} // next cell ref
		
		
		 mesh = new TriangularMesh(domain, cells, nodes);
		 //mesh.setFlowTimes(flowTimes);
	}
	
	public TriangularMesh getMesh() { return mesh; }
	
	
	
}
