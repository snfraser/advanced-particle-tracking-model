/**
 * 
 */
package com.apt.readers;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

import com.apt.data.Cell;
import com.apt.data.Domain;
import com.apt.data.Mesh;
import com.apt.data.Node;
import com.apt.data.NodeType;
import com.apt.data.TriangularMesh;

/**
 * 
 */
public class MikeMeshReader {

	public Mesh readFile(File bathymetryFile) throws Exception {

		Map<Integer, Node> nodes = new HashMap<Integer, Node>();
		Map<Integer, Cell> cells = new HashMap<Integer, Cell>();

		double xmin = 9999999999.9;
		double xmax = -9999999999.9;
		double ymin = 9999999999.9;
		double ymax = -9999999999.9;
		
		try (FileReader fr = new FileReader(bathymetryFile); BufferedReader br = new BufferedReader(fr)) {

			int dataLineCount = 0;
			String line = null;

			line = br.readLine();
			StringTokenizer st = new StringTokenizer(line);
			st.nextToken(); // skip 100079
			st.nextToken(); // skip 1000
			int numberOfVertices = Integer.parseInt(st.nextToken());

			for (int i = 0; i < numberOfVertices; i++) {

				// index x y z type
				line = br.readLine();
				st = new StringTokenizer(line);
				int index = Integer.parseInt(st.nextToken());
				double x = Double.parseDouble(st.nextToken());
				double y = Double.parseDouble(st.nextToken());
				double z = Double.parseDouble(st.nextToken());
				int code = Integer.parseInt(st.nextToken());
				NodeType type = getType(code);
				Node n = new Node(index, x, y);
				n.depth = z;
				n.type = type;
				nodes.put(index, n);
				
				if (x < xmin)
					xmin = x;
				if (y < ymin)
					ymin = y;
				if (x > xmax)
					xmax = x;
				if (y > ymax)
					ymax = y;
				
			}

			line = br.readLine();
			st = new StringTokenizer(line);

			int numberOfCells = Integer.parseInt(st.nextToken());

			for (int i = 0; i < numberOfCells; i++) {

				line = br.readLine();
				st = new StringTokenizer(line);

				int index = Integer.parseInt(st.nextToken());
				Node[] narray = new Node[3];
				int v1 = Integer.parseInt(st.nextToken());
				int v2 = Integer.parseInt(st.nextToken());
				int v3 = Integer.parseInt(st.nextToken());
				narray[0] = nodes.get(v1);
				narray[1] = nodes.get(v1);
				narray[2] = nodes.get(v1);
				Cell c = new Cell(index, narray);
				cells.put(index, c);
			}

		}

		Domain  domain = new Domain(xmin, xmax, ymin, ymax);
		TriangularMesh mesh = new TriangularMesh(null, cells, nodes);

		return mesh;
	}

	private NodeType getType(int code) {
		switch (code) {
		case 0:
			return NodeType.SEA;
		case 1:
			return NodeType.LAND_BOUNDARY;
		case 2:
			return NodeType.OPEN_BOUNDARY;
		}
		return null;
	}

}
