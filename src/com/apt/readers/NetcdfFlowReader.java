/**
 * 
 */
package com.apt.readers;

import java.awt.geom.Point2D;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.apt.data.Cell;
import com.apt.data.Domain;
import com.apt.data.FlowData;
import com.apt.data.FlowLayer;
import com.apt.data.FlowProfile;
import com.apt.data.FlowTimeSeries;
import com.apt.data.FlowVector;
import com.apt.data.IndexingSearchStrategy;
import com.apt.data.Node;
import com.apt.data.NodeType;
import com.apt.data.RingSearchStrategy;
import com.apt.data.TriangularMesh;
import com.apt.models.flow.NetcdfFlowModel;

import ucar.ma2.ArrayFloat;
import ucar.ma2.ArrayInt;
import ucar.nc2.Dimension;
import ucar.nc2.NetcdfFile;
import ucar.nc2.Variable;
import ucar.nc2.dataset.NetcdfDataset;
import ucar.unidata.geoloc.LatLonPoint;
import ucar.unidata.geoloc.LatLonPointImpl;
import ucar.unidata.geoloc.ProjectionPoint;
import ucar.unidata.geoloc.projection.UtmProjection;

/**
 * Reads hydr-model NetCDF data.
 * 
 * This version v2 uses the new FlowData and FlowLayers instead of the implicit
 * u,v,w,t arrays.
 * 
 * @author SA05SF
 *
 */
public class NetcdfFlowReader {

	/**
	 * CORRECTION TERM where the uv values are excessively high - remove asap......
	 */
	static final double CORRECTION = 10.0;

	private UtmProjection projection;

	TriangularMesh mesh;

	Map<Integer, FlowProfile> profiles;

	private ArrayFloat.D1 lat;
	private ArrayFloat.D1 lon;

	private ArrayFloat.D1 latc;
	private ArrayFloat.D1 lonc;

	private ArrayFloat.D1 x;
	private ArrayFloat.D1 y;

	private ArrayFloat.D1 xc;
	private ArrayFloat.D1 yc;

	private ArrayFloat.D1 h; // depth node
	private ArrayFloat.D1 hc; // depth cell
	private ArrayFloat.D2 zeta; // tidal height

	// siglay and siglev are for the whole domain and are 1D.
	private ArrayFloat.D2 siglay;
	private ArrayFloat.D1 siglev;

	private ArrayFloat.D3 u;
	private ArrayFloat.D3 v;

	private ArrayFloat.D1 times;

	// private ArrayInt.D1 ntve; // number of elements round each node
	// private ArrayInt.D2 nbve; // the elements round each node (using ntve
	// cumulative sum!)
	private ArrayInt.D2 nv; // nodes round element
	private ArrayInt.D1 ntve; // number elements round a node
	private ArrayInt.D2 nbve; // elements surrounding a node

	private ArrayInt.D1 bnodes;
	private ArrayInt.D1 obnodes;

	/**
	 * Create a reader to process
	 * 
	 * @param flowFile
	 * @throws Exception
	 */
	public NetcdfFlowReader() throws Exception {

	}

	public NetcdfFlowModel readFile(File meshFile, File flowFile) throws Exception {

		NetcdfFile meshNetfile = NetcdfFile.open(meshFile.getPath());
		NetcdfDataset meshData = new NetcdfDataset(meshNetfile);

		NetcdfFile flowNetfile = NetcdfFile.open(flowFile.getPath());
		NetcdfDataset flowData = new NetcdfDataset(flowNetfile);
		
		// Process MESH
		List<Dimension> sdim = meshData.getDimensions();
		System.err.println("Overall mesh dimensions...");
		for (Dimension d : sdim) {
			System.err.printf("Dim: %s : %d \n", d.getFullName(), d.getLength());
		}
		System.err.println();

		List<Variable> meshVars = meshData.getVariables();
		System.err.println("All MESH variables...");
		for (Variable v : meshVars) {
			System.err.printf("Var: %s (%s): [", v.getFullName(), v.getDataType().toString());
			List<Dimension> dds = v.getDimensions();
			for (Dimension d : dds) {
				System.err.printf(" %s ", d.getFullName());
			}
			System.err.printf("]\n");
		}
		System.err.println();

		List<Dimension> fdim = flowData.getDimensions();
		System.err.println("Overall flow dimensions...");
		for (Dimension d : fdim) {
			System.err.printf("Dim: %s : %d \n", d.getFullName(), d.getLength());
		}
		System.err.println();
		
		List<Variable> flowVars = flowData.getVariables();
		System.err.println("All FLOW variables...");
		for (Variable v : flowVars) {
			System.err.printf("Var: %s (%s): [", v.getFullName(), v.getDataType().toString());
			List<Dimension> dds = v.getDimensions();
			for (Dimension d : dds) {
				System.err.printf(" %s ", d.getFullName());
			}
			System.err.printf("]\n");
		}
		System.err.println();
		
		
		
		Variable vlat = flowData.findVariable("lat"); // y
		lat = (ArrayFloat.D1) vlat.read();

		Variable vlon = flowData.findVariable("lon"); // x
		lon = (ArrayFloat.D1) vlon.read();

		Variable vlatc = flowData.findVariable("latc"); // yc
		latc = (ArrayFloat.D1) vlatc.read();

		Variable vlonc = flowData.findVariable("lonc"); // xc
		lonc = (ArrayFloat.D1) vlonc.read();

		double xmin = 999999.9;
		double ymin = 999999.9;
		double xmax = -999999.9;
		double ymax = -999999.9;

		for (int i = 0; i < lat.getSize(); i++) {
			if (lat.get(i) < ymin)
				ymin = lat.get(i);
			if (lat.get(i) > ymax)
				ymax = lat.get(i);
			if (lon.get(i) < xmin)
				xmin = lon.get(i);
			if (lon.get(i) > xmax)
				xmax = lon.get(i);
		}
	
		double midlon = 0.5*(xmin + xmax);
		double midlat = 0.5*(ymin + ymax);
		System.err.printf("Range (lon/lat): (%f, %f) -> (%f, %f) MidPoint: (%f, %f) \n", xmin, ymin, xmax, ymax, midlon, midlat);

		int zone = (int) Math.floor((midlon + 180.0) / 6.0) + 1;
		boolean north = (midlat > 0.0);
		projection = new UtmProjection(zone, north);
		
		Point2D.Double min = convertToUtm(ymin, xmin); // these are lon/lat -> xy
		Point2D.Double max = convertToUtm(ymax, xmax); // these are lon/lat -> xy
		Domain domain = new Domain(min.x, max.x, min.y, max.y);
		
		System.err.printf("Domain bounds: %s \n", domain);
		
		Variable vh = flowData.findVariable("h");
		h = (ArrayFloat.D1) vh.read();

		Variable vhc = flowData.findVariable("h_center");
		hc = (ArrayFloat.D1) vhc.read();
		
		Variable vu = flowData.findVariable("u");
		u = (ArrayFloat.D3) vu.read();
		int[] us = u.getShape(); // us#0 is the number of time steps
		for (Integer i : us) {
			System.err.printf("us: %d ", i);
		}
		System.err.println();

		Variable vv = flowData.findVariable("v");
		v = (ArrayFloat.D3) vv.read();
		int[] vs = v.getShape();
		for (Integer i : vs) {
			System.err.printf("vs: %d ", i);
		}
		System.err.println();

		 // int ntve(node) number elements surrounding a node
	     // int nbve(maxelem, node) actual elements surrounding a node
		
		Variable var_ntve = flowData.findVariable("ntve"); // should really be meshdata
		ntve = (ArrayInt.D1) var_ntve.read();// should really be meshdata

		Variable var_nbve = flowData.findVariable("nbve");
		nbve = (ArrayInt.D2) var_nbve.read();

		Variable var_nv = flowData.findVariable("nv");
		nv = (ArrayInt.D2) var_nv.read();
		int[] ns = nv.getShape();
		for (Integer i : ns) {
			System.err.printf("nv: %d ", i);
		}
		System.err.println();
		
		
		// sigma layer centers for this element [layer, nele]
		//Variable var_siglay = data.findVariable("siglay_center");
		Variable var_siglay = flowData.findVariable("siglay_center");
		siglay = (ArrayFloat.D2) var_siglay.read();
		int[] nss = siglay.getShape();
		System.err.printf("Sigma layers:: ");
		for (int il = 0; il < nss[0]; il++) {
			System.err.printf(" %f ", siglay.get(il, 1)); // just pick cell 1 for now
		}
		System.err.println();

		Variable var_zeta = flowData.findVariable("zeta");
		zeta = (ArrayFloat.D2) var_zeta.read();
		int[] nz = zeta.getShape();
		for (Integer i : nz) {
			System.err.printf("zeta: %d ", i);
		}
		System.err.println();
		
		// mesh bounds: bnodes or boundaryNodesAll
		//Variable var_bnodes = data.findVariable("bnodes");
		 Variable var_bnodes = meshData.findVariable("boundaryNodesAll");
		bnodes = (ArrayInt.D1) var_bnodes.read();
		System.err.printf("All boundary nodes: %d\n", bnodes.getSize());

		// mesh bounds: obnodes or boundaryNodesOpen
		//Variable var_obnodes = data.findVariable("obnodes");
		Variable var_obnodes = meshData.findVariable("boundaryNodesOpen");
		obnodes = (ArrayInt.D1) var_obnodes.read();
		System.err.printf("Open boundary nodes: %d\n", obnodes.getSize());
		
		Variable var_time = flowData.findVariable("time");
		times = (ArrayFloat.D1) var_time.read();
		int ntimes = times.getShape()[0];
		
		long ntt = times.getSize();
		for (int t = 0; t< ntt; t++) {
			double atime = times.getDouble(t);
			System.err.printf("Time: %f \n", atime);
		}
		// work out time step and start
		double MJD2MS = 86400*1000.0;
		long time0 = (long)(times.getDouble(0)*MJD2MS);
		long timestep = (long)((times.getDouble(1) - times.getDouble(0))*MJD2MS); // what units??? mjd to ms
		
		System.err.printf("Checking timing info: time0: %d (%f) tstep: %d (%f)\n", 
				time0, (double)time0/1000.0, timestep, (double)timestep/1000.0);
		
		// save this so we can fill the flowtimes array with them....
		//int[] flowTimes = new int[ntimes];

		
		
		int nlayers = siglay.getShape()[0];
		//int[] layers = new int[nlayers];
		
		// Create the profile to cell mapping
		profiles = new HashMap<Integer, FlowProfile>();
		
		// extract node infos
		Map<Integer, Node> nodes = new HashMap<Integer, Node>();
		int nnode = lat.getShape()[0];
		for (int j = 0; j < nnode; j++) {
			double flat = lat.getDouble(j);
			double flon = lon.getDouble(j);
			
			//LatLonPoint point = new LatLonPointImpl(flat, flon);
			//ProjectionPoint xypoint = projection.latLonToProj(point);
			//double x = 1000.0 * xypoint.getX(); // km -> m
			//double y = 1000.0 * xypoint.getY(); // km -> m         
			
			Point2D.Double pos = convertToUtm(flat, flon);
			
			Node n = new Node(j + 1, pos.x, pos.y);
			n.type = NodeType.SEA; // for now
			n.depth = -h.getDouble(j);
			nodes.put(j + 1, n);
			//System.err.printf("Extract node: %s \n", n);
		}

		
		// check cells
		Map<Integer, Cell> cells = new HashMap<Integer, Cell>();
		int[] shapeNv = nv.getShape(); // numsurrounding x num cells
		System.err.printf("Extracting upto: %d x %d cells...", shapeNv[0], shapeNv[1]);
		for (int j = 0; j < shapeNv[1]; j++) { // cell number ref
			
			Node[] mynodes = new Node[3];
			//System.err.printf("Cell: %d ", j + 1);
			for (int i = 0; i < shapeNv[0]; i++) {
				// nv(i,j) i : 0,1,2, j: cells
				int inode = nv.get(i, j);
				mynodes[i] = nodes.get(inode);
				//System.err.printf(" %d ", inode);
			}
			//System.err.println();
			double flat = latc.getFloat(j);// ??
			double flon = lonc.getFloat(j);// ??
			Cell c = new Cell(j + 1, mynodes); // recalc the xc,yc values!!
			
			//LatLonPoint point = new LatLonPointImpl(flat, flon);
			//ProjectionPoint xypoint = projection.latLonToProj(point);
			//double x = 1000.0 * xypoint.getX(); // km -> m
			//double y = 1000.0 * xypoint.getY(); // km -> m   
			
			Point2D.Double pos = convertToUtm(flat, flon);
			
			c.xc = pos.x;
			c.yc = pos.y;
			c.zc = -hc.getDouble(j); // depth -ve below surface
			
			// A flow data set for the cell
			//FlowData flowData = new FlowData();
			
			FlowProfile flowprofile = new FlowProfile(); // holds all the timeseries at each sigma layer
			
			// create a set of layers for this cells
			// we want the layers starting at lowest and working to surface may need to reverse
			for (int il = nlayers-1; il >= 0; il--) {

				// Another layer for the flowdata.
				//double sigmaDepth = siglay.get(il, j);
				double sigmaDepth = siglay.getDouble(il);
				//FlowLayer layer = new FlowLayer(sigmaDepth);
				
				FlowTimeSeries timeseries = new FlowTimeSeries(time0, timestep);
				//System.err.printf("Create layer at depth: %f \n", sigmaDepth);
				
				// TODO redesign this in Cell as a 2D var (time, layer)
				// should extract u.getDouble(time, *, j););
				

				for (int t = 0; t < ntt; t++) {
					// t is time, il is layer, j is cellref
					double ut = u.get(t, il, j); // u at: time, layer, cell#j
					double vt = v.get(t, il, j); // v at: time, layer, cell#j
					
					double atime = times.getDouble(t);
			
					FlowVector fv = new FlowVector(ut, vt, 0.0);
					timeseries.append(fv);
				
				}
				flowprofile.addFlow(sigmaDepth, timeseries);// addflow(sigma, ts) !!!!!
				// set all the times and flow vectors for this layer
				
				//flowData.addLayer(layer);
				
			}

			// associate the flow profile with the cell
			profiles.put(c.ref, flowprofile);
			
			//System.err.printf("Extract cell: %s \n", c);
			cells.put(c.ref, c);
			
		} // next cell ref
		
		// optional create mapping from cell to surrounding cells 
		// this cells ring-mapping to surrounding cells
		Map<Integer, List<Cell>> ringMap = new HashMap<Integer, List<Cell>>();
					
		System.err.println("checking surrounding cells mapping...");
		for (int j = 1; j <= cells.size(); j++) { 
			
			Cell thisCell = cells.get(j);
			//System.err.printf("  check cell: %s ...\n", thisCell);
			
			List<Cell> mylist = new ArrayList<Cell>();
			
			Node[] mynodes = thisCell.nodes;
			
			for (int i = 0; i < mynodes.length; i++) { // usually 3
				
				Node theNode = mynodes[i];
				//System.err.printf("    processing %dth node of %d ref: %d \n", i, mynodes.length, theNode.ref);
				
				int nse = ntve.get(theNode.ref-1); // how many surrounding nodes
				//System.err.printf("    node %d has %d surrounding cells \n", theNode.ref, nse);
				
				// loop round those nodes and find the surrounding elements
				for (int k = 0; k < nse; k++) {
					//System.err.printf("     get nbve(%d, %d) \n", k, theNodeRef -1);
					int ocellref = nbve.get(k, theNode.ref-1);
					//System.err.printf("     processing %d th cell ref %d \n", k, ocellref);
					if (ocellref == 0)
						continue; // skip any zero refs????
					
					Cell other = cells.get(ocellref); // could be a duffer
					
					// add this to the mapping for this cell if not already there adn exclude ourself
					if (other.ref != thisCell.ref && !mylist.contains(other))
						mylist.add(other);
				}
				
			}
			System.err.printf("  Cell %d has %d surrounding cells in ring#1 \n", j, mylist.size());
			ringMap.put(j, mylist);
		}

		System.err.printf("checking all boundary nodes: size: %d \n", bnodes.getSize());
		for (int i = 0; i < bnodes.getSize(); i++) {
			int inode = bnodes.get(i); // this is a node ref
			Node n = nodes.get(inode);
			n.type = NodeType.LAND_BOUNDARY;
			//System.err.printf("Bnode: %d %d (%.1f %.1f) \n", inode, n.ref, n.x, n.y);
			// set to land boundary for now
		}
		System.err.printf("checking open boundary nodes: size: %d \n", obnodes.getSize());
		for (int i = 0; i < obnodes.getSize(); i++) {
			int inode = obnodes.get(i); // find ith obnode
			Node n = nodes.get(inode);
			n.type = NodeType.OPEN_BOUNDARY; // set to open boundary
			//System.err.printf("OBnode: %d %d (%.1f %.1f) \n", inode, n.ref, n.x, n.y);
		}

		mesh = new TriangularMesh(domain, cells, nodes);
		//mesh.setSearchStrategy(new IndexingSearchStrategy());
		mesh.setSearchStrategy(new RingSearchStrategy(ringMap)); // too much setup time for day2day testing 45m for 176K mesh
		
		NetcdfFlowModel model = new NetcdfFlowModel(mesh, profiles);
		return model;
	}

	public TriangularMesh getMesh() {
		return mesh;
	}

	private Point2D.Double convertToUtm(double lat, double lon) {
		LatLonPoint point = new LatLonPointImpl(lat, lon);
		ProjectionPoint xypoint = projection.latLonToProj(point);
		double x = 1000.0 * xypoint.getX(); // km -> m
		double y = 1000.0 * xypoint.getY(); // km -> m
		return new Point2D.Double(x, y);
	}

}
