/**
 * 
 */
package com.apt.readers;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import com.apt.data.Mesh;
import com.apt.models.IFlowModel;
import com.apt.models.flow.NetcdfFlowModel;

/**
 * Read a NewDEPOMOD binary flowmetry file and convert to a mesh flow model. NDM
 * binary flow files hold the flow at the nodes so need to interpolate onto
 * cells.
 * 
 */
public class BinaryFlowReader {

	private long dataPos = 0L;

	/**
	 * Read a file in NDM Binary flow format based on the supplied mesh.
	 * 
	 * @param mesh     A mesh (usually triangular).
	 * @param flowFile The file in NDM binary flow format.
	 * @return An instance of IFlowmetry.
	 * @throws Exception
	 */
	public IFlowModel readFile(Mesh mesh, File flowFile) throws Exception {

		DataInputStream stream = new DataInputStream(new FileInputStream(flowFile));

		readTag(stream, BinaryDictionary.headerstart);
		readTag(stream, BinaryDictionary.version);
		long version = readLong(stream);

		readTag(stream, BinaryDictionary.timestart);
		readTag(stream, BinaryDictionary.origintime);
		long origintime = readLong(stream);

		readTag(stream, BinaryDictionary.starttime);
		long starttime = readLong(stream);
		readTag(stream, BinaryDictionary.deltat);
		double deltaT = readDouble(stream);
		readTag(stream, BinaryDictionary.ntimesteps);
		int nTimeSteps = readInt(stream);
		System.err.printf("BFR::read(): start: %d ntimesteps: %d delta: %f duration: %f \n", starttime, nTimeSteps,
				deltaT, nTimeSteps * deltaT);
		readTag(stream, BinaryDictionary.timeend);
		readTag(stream, BinaryDictionary.elestart);
		readTag(stream, BinaryDictionary.nbnodes);
		int nBNodes = readInt(stream);

		int[] nodeReferences = new int[nBNodes]; // we should have this from the mesh!!!
		readTag(stream, BinaryDictionary.bnoderefs);
		for (int i = 0; i < nBNodes; i++) {
			nodeReferences[i] = readInt(stream);
			System.err.printf("Node: %d is %d \n", i, nodeReferences[i]);
		}
		
	
		
		readTag(stream, BinaryDictionary.eleend);
		readTag(stream, BinaryDictionary.layersstart);
		readTag(stream, BinaryDictionary.nlayers);
		int nLayers = readInt(stream);
		System.err.println("BFR:: read: nlayers: " + nLayers);
		double[] sigmaDepths = new double[(int) nLayers];
		readTag(stream, BinaryDictionary.sigmadepths);
		for (int i = 0; i < nLayers; i++) {
			sigmaDepths[i] = readDouble(stream);
			System.err.printf(" %d -> %f ,", i, sigmaDepths[i]);
		}

		readTag(stream, BinaryDictionary.layersend);
		readTag(stream, BinaryDictionary.valsstart);
		readTag(stream, BinaryDictionary.valsperbnode);
		int valsPerBNode = readInt(stream);
		System.err.println("BFR:: read: valsperbnode: " + valsPerBNode); // should be 1 (depth)
		long[] bNodeValNames = new long[(int) valsPerBNode];
		for (int i = 0; i < valsPerBNode; i++) {
			readLong(stream);
		}

		readTag(stream, BinaryDictionary.valsperfnode);
		int valsPerFNode = readInt(stream);
		System.err.println("BFR:: read: valsperfnode: " + valsPerFNode); // should be 3 (u, v, w)
		long[] fNodeValNames = new long[(int) valsPerFNode];
		for (int i = 0; i < valsPerFNode; i++) {
			readLong(stream);
		}

		readTag(stream, BinaryDictionary.valsend);
		readTag(stream, BinaryDictionary.headerend);
		readTag(stream, BinaryDictionary.datastart);
		// bathData = new BasicNDCubeDouble(new int[]{(int) nTimeSteps, (int) nBNodes},
		// valsPerBNode);
		// flowData = new BasicNDCubeDouble(new int[]{(int) nTimeSteps, (int) nBNodes,
		// (int) nLayers}, valsPerFNode);

		if (true)
			return null;
		
		
		for (int timeIndex = 0; timeIndex < nTimeSteps; timeIndex++) {
			System.err.printf("BFR:: read: timeindex: %d of: %d \n", timeIndex + 1, nTimeSteps);

			readTag(stream, BinaryDictionary.framestart);
			for (int nodeIndex = 0; nodeIndex < nBNodes; nodeIndex++) {
				System.err.printf("BFR:: read: nodeindex: %d of: %d \n", nodeIndex + 1, nBNodes);

				for (int valIndex = 0; valIndex < valsPerBNode; valIndex++) {
					try {
						// bathData.setMember(new int[]{timeIndex, nodeIndex}, valIndex,
						// readDouble(stream));
						readDouble(stream); // skip this depth data
					} catch (Exception exp) {
						int available = stream.available();
						System.err.println("error reading value: " + timeIndex + " " + nodeIndex + " " + valIndex);
					}
				}
				for (int layerIndex = 0; layerIndex < nLayers; layerIndex++) {
					for (int valIndex = 0; valIndex < valsPerFNode; valIndex++) { // 0:u, 1:v, 2:w
						try {
							double val = readDouble(stream); // flow u,v or w

							// flowData.setMember(new int[]{timeIndex, nodeIndex, layerIndex}, valIndex,
							// val);

							// flow(t, node, layer, uvw)

						} catch (Exception exp) {
							int available = stream.available();
							System.err.println("error reading value: " + timeIndex + " " + nodeIndex + " " + layerIndex
									+ " " + valIndex);
						}
					}
				}
			}
			readTag(stream, BinaryDictionary.frameend);

		}
		return null;
	}

	private void readTag(DataInputStream stream, long tag) throws IOException {
		if (stream.readLong() == tag) {
			System.err.printf("BFR::readTag: %s at %d\n", Long.toString(tag, 36), dataPos);
			dataPos++;
			return;
		}
		throw new IOException("Expected Tag " + Long.toString(tag, 36) + " (" + tag + ") at data position " + dataPos);
	}

	private long readLong(DataInputStream stream) throws IOException {
		try {
			long val = stream.readLong();
			dataPos++;
			return val;
		} catch (IOException Ioe) {
			throw new IOException("Error Reading Value at data position " + dataPos, Ioe);
		}
	}

	private double readDouble(DataInputStream stream) throws IOException {
		try {
			double val = stream.readDouble();
			dataPos++;
			return val;
		} catch (IOException Ioe) {
			throw new IOException("Error Reading Value at data position " + dataPos, Ioe);
		}
	}

	private int readInt(DataInputStream stream) throws IOException {
		try {
			int val = stream.readInt();
			dataPos++;
			return val;
		} catch (IOException Ioe) {
			throw new IOException("Error Reading Value at data position " + dataPos, Ioe);
		}
	}

	private class BinaryDictionary {
		public static final long bnoderefs = 32887638385624L;
		public static final long bnodevals = 32887638567280L;
		public static final long datastart = 37521850183625L;
		public static final long dataend = 28952026537L;
		public static final long deltat = 810592661L;
		public static final long eleend = 882470569L;
		public static final long elestart = 1143705649097L;
		public static final long fnodevals = 44172078197104L;
		public static final long frameend = 1234877869993L;
		public static final long framestart = 1600401743302601L;
		public static final long headerend = 49078545401065L;
		public static final long headerstart = 63605794863571913L;
		public static final long layersend = 60101853498409L;
		public static final long layersstart = 77892002157729737L;
		public static final long nbnodes = 50770889956L;
		public static final long nlayers = 51354185032L;
		public static final long ntimesteps = 2419150529782720L;
		public static final long origintime = 2515055439413894L;
		public static final long sigmadepths = 104247399890527120L;
		public static final long starttime = 81287088663686L;
		public static final long timeend = 64252702633L;
		public static final long timestart = 83271526404041L;
		public static final long valsend = 68121511465L;
		public static final long valsperbnode = 4119048144909578786L;
		public static final long valsperfnode = 4119048144916297250L;
		public static final long valsstart = 88285502650313L;
		public static final long version = 68373459095L;

		// BNODEVALUES
		public static final long elevation = 41173553727959L;

		// FNODEVALS
		public static final long density = 29184658774L;
		public static final long salinity = 2217265538326L;
		public static final long termprature = 107514489802979066L;
		public static final long uvelocity = 87094371948118L;
		public static final long vvelocity = 89915481855574L;
		public static final long wvelocity = 92736591763030L;
	}

}
