/**
 * 
 */
package com.apt.readers;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import com.apt.data.FlowProfile;
import com.apt.data.FlowTimeSeries;
import com.apt.data.FlowVector;
import com.apt.data.IFlowTimeSeries;
import com.apt.data.Mesh;
import com.apt.models.IFlowModel;
import com.apt.models.flow.SinglePointFlowModel;
import com.apt.models.readers.FlowmetryReader;

/**
 * Read data from a single point flowmetry data set. Data is in the format:
 * xmeter: <x>, ymeter: <y>, step: <tstep s>, ndepth: <n>
 * 
 * 
 * @author SA05SF
 *
 */
public class SinglePointFlowmetryReader {

	public IFlowModel readFile(File flowmetryFile) throws Exception {

		double meterx = 0.0;
		double metery = 0.0;
	
		
		double sitez = 0.0;
		
		int nsigma = 0;
		double step = 0.0;
		double[] sigmas;
		long time0 = 0L;
		
		boolean gotSiteDepth = false;
	

		FlowProfile profile = new FlowProfile();
	
		int dataLineCount = 0;
		
		try (FileReader fr = new FileReader(flowmetryFile); BufferedReader br = new BufferedReader(fr)) {

			String line = null;
			while ((line = br.readLine()) != null) {
				dataLineCount++; // line count starting 1
				
				// skip comments
				if (line.startsWith("#"))
					continue;

				// skip empty lines
				if (line.isEmpty())
					continue;

				// extract metadata
				if (line.startsWith("meterx:")) {
					String part = line.substring(7).trim();
					meterx = Double.parseDouble(part);
				} else if (line.startsWith("metery:")) {
					String part = line.substring(7).trim();
					metery = Double.parseDouble(part);
				} else if (line.startsWith("sitez:")) {
					String part = line.substring(6).trim();
					sitez = Double.parseDouble(part);	
					gotSiteDepth = true;
				} else if (line.startsWith("step:")) {
					String part = line.substring(5).trim();
					step = Double.parseDouble(part);
				} else if (line.startsWith("depths:")) {
					String part = line.substring(7).trim();
					StringTokenizer st = new StringTokenizer(part, ",");
					nsigma = st.countTokens();
					sigmas = new double[nsigma];
					// check we have the site depth
					if (!gotSiteDepth)
						throw new Exception(String
								.format("SPtFlowRdr:: cannot process sigma depths, site depth not read at line: %d in flow file: %s", 
										dataLineCount, flowmetryFile.getName()));
					int id = 0;
					while (st.hasMoreTokens()) {
						double depth = Double.parseDouble(st.nextToken());
						sigmas[id] = -depth/sitez;
						id++;
					}
					
					// we can now create the various TS for each sigma level
					for (int i = 0; i < nsigma; i++) {
						FlowTimeSeries ts = new FlowTimeSeries(time0, (long)step);
						profile.addFlow(sigmas[i], ts); // at sigma starting at lowest depth
					}
					
				} else {
					// a line of data: stepno, u0, v0, u1, v1, ... un, vn
					
					// expect (nsigma*2 + 1) entries

					String[] parts = line.split(","); // check there are 3 items
					if (parts.length < nsigma * 2 + 1)
						throw new Exception(String.format("SPtFlowRdr::Line: %d : contains wrong number of items: %d (%d) :: %s",
								dataLineCount, parts.length, nsigma * 2 + 1, line));

					//System.err.printf("Line: %d ", dataLineCount);
					
					
					//int istep = Integer.parseInt(parts[0].trim());
					for (int i = 0; i < nsigma; i++) {
						double u = Double.parseDouble(parts[2 * i + 1]);
						double v = Double.parseDouble(parts[2 * i + 2]);
						FlowVector flow = new FlowVector(u, v, 0.0);
						IFlowTimeSeries ts = profile.getTimeSeries(i);
						((FlowTimeSeries)ts).append(flow); // get the ith ts value
						//System.err.printf(" (%f, %f) ", u, v);
					}
					//System.err.println();
				
				}
			} // next line

		}
		
		SinglePointFlowModel spf = new SinglePointFlowModel(profile, 0L, (long)step, nsigma, sitez);
		//System.err.printf("SPFR:: completed after %d lines with: %s \n", dataLineCount, spf);
		return spf;
	}
}
