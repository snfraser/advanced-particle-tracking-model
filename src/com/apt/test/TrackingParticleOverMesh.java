/**
 * 
 */
package com.apt.test;

import java.util.ArrayList;
import java.util.List;

import com.apt.data.Cell;
import com.apt.data.CrossingType;
import com.apt.data.Domain;
import com.apt.data.Node;
import com.apt.data.NodeType;
import com.apt.data.Particle;
import com.apt.data.ParticlesState;
import com.apt.data.PositionVector;
import com.apt.data.RegularSquareMesh;
import com.apt.data.Spacing;

/**
 * Track a particle over a mesh looking to detect cell edge crossings and jumps.
 * 
 * @author SA05SF
 *
 */
public class TrackingParticleOverMesh {
	static final int NX = 8;
	static final int NY = 8;
	static final double X0 = 0.0;
	static final double Y0 = 0.0;
	static final double DX = 10.0;
	static final double DY = 10.0;

	static final double DT = 60.0; // time step [s]

	RegularSquareMesh mesh;

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		TrackingParticleOverMesh tt = new TrackingParticleOverMesh();

		// 1. create a mesh
		tt.createMesh(X0, Y0, DX, DY, NX, NY);

		// 2. Start particles near centre of mesh and send off with specified speed

		int nlost = 0;
		int nbed = 0;
		int nrun = 5000;
		for (int i = 0; i < nrun; i++) {
			Particle p = new Particle();
			p.setStartPosition(55.0, 45.0, 0.0);

			double vxcms = (Math.random() - 0.5) * 20.0; // speed [cm/s]
			double vycms = (Math.random() - 0.5) * 20.0; // speed [cm/s]
			double vzcms = -Math.random() * 10.0; // settling speed 9.2 cms
			FinalPoint fp = tt.trackParticleViaTransport(p, vxcms, vycms, vzcms, DT);
			System.err.printf("Final state: %s \n", fp.crossing.name());
			if (fp.crossing == CrossingType.LAND)
				nbed++;
			else
				nlost++;
			try {
				Thread.sleep(1000L);
			} catch (Exception ix) {
			}
			System.err.println();
		}
		System.err.printf("Total runs: %d Lost: %d  Landed: %d \n", nrun, nlost, nbed);
	}

	private void createMesh(double x0, double y0, double dx, double dy, int nx, int ny) {
		Spacing spacing = new Spacing(dx, dy);
		Domain domain = new Domain(x0, (nx - 1) * dx, y0, (ny - 1) * dy);
		System.err.printf("Create square mesh with domain: %s \n", domain);
		List<Node> nodes = new ArrayList<Node>();

		// create nodes...
		int index = 0;
		double maxdepth = 30.0; // max at top/right corner of grid
		for (int j = 0; j < ny; j++) {
			double y = y0 + j * dy;
			for (int i = 0; i < nx; i++) {
				double x = x0 + i * dx;
				double z = maxdepth*(i + j)/ (nx + ny) + (Math.random() - 0.5)* 2.0;
				
				Node n = new Node(++index, x, y);
				if (i < nx / 2 - j)
					n.depth = 10.0;
				else
					n.depth = -z;
				System.err.printf("Create Node: %4d [%d, %d] at: (%f %f %f) \n", index, i, j, x, y, n.depth);
				nodes.add(n);
			}
		}

		mesh = new RegularSquareMesh(domain, spacing, nodes);
	}

	/**
	 * Create a transportMechanism using supplied flow speed [cm/s].
	 * 
	 * @param p     The particle to track.
	 * @param vxcms U flow [cm/s].
	 * @param vycms V flow [cm/s].
	 * @param vzcms W flow [cm/s] usually represents a settling speed so generally
	 *              negative.
	 * @param dt    Time-stpe [s].
	 */
	private FinalPoint trackParticleViaTransport(Particle p, double vxcms, double vycms, double vzcms, double dt) {

		TransportMechanism trans = new TransportMechanism(vxcms / 100.0, vycms / 100.0, vzcms / 100.0);

		System.err.printf("Track particle via transport mechanism: Start: (%f, %f, %f) flow: (%f, %f, %f) \n", p.getX(),
				p.getY(), p.getZ(), vxcms, vycms, vzcms);

		double t = 0.0;
		while (t < 5000 && p.getState() == ParticlesState.SUSPEND) {
			trans.move(p, t, dt);
			t += dt;
		}

		Cell c = mesh.findCell(p.getX(), p.getY(), null);
		System.err.printf("Tracking:: final position of particle is: (%f,%f,%f) in cell: %d  state: %s\n", p.getX(),
				p.getY(), p.getZ(), c.ref, p.getState().name());
		
		CrossingType type = p.getState() == ParticlesState.ONBED ? CrossingType.LAND: CrossingType.OPEN;
		FinalPoint fp = new FinalPoint(p.getX(), p.getY(), c, type);
		return fp;
	}

	// NOT USED
	private static CrossingType crossingType(Node n1, Node n2) {
		if (n1.type == NodeType.OPEN_BOUNDARY && n2.type == NodeType.LAND_BOUNDARY)
			return CrossingType.OPEN;
		if (n2.type == NodeType.OPEN_BOUNDARY && n1.type == NodeType.LAND_BOUNDARY)
			return CrossingType.OPEN;
		if (n1.type == NodeType.OPEN_BOUNDARY && n2.type == NodeType.OPEN_BOUNDARY)
			return CrossingType.OPEN;
		if (n1.type == NodeType.LAND_BOUNDARY && n2.type == NodeType.LAND_BOUNDARY)
			return CrossingType.LAND;
		return CrossingType.SEA;
	}

	private class FinalPoint {
		double x;
		double y;
		Cell cell;
		CrossingType crossing;

		/**
		 * @param x
		 * @param y
		 * @param cell
		 * @param crossing
		 */
		public FinalPoint(double x, double y, Cell cell, CrossingType crossing) {
			this.x = x;
			this.y = y;
			this.cell = cell;
			this.crossing = crossing;
		}

	}

	private class TransportMechanism {

		private double vx;
		private double vy;
		private double vz;

		/**
		 * @param vx U flow [m/s].
		 * @param vy V flow [m/s].
		 * @param vz W flow [m/s].
		 */
		public TransportMechanism(double vx, double vy, double vz) {
			super();
			this.vx = vx;
			this.vy = vy;
			this.vz = vz;
		}

		/** Move a particle at time t. */
		public void move(Particle p, double t, double dt) {

			// System.err.printf("Flow: %f, %f, %f \n", vx,vy,vz);

			// where are we moving from...
			// Cell lastc = p.getCurrentCell();

			double xlast = p.getX();
			double ylast = p.getY();
			double zlast = p.getZ();
			Cell lastc = mesh.findCell(xlast, ylast, null);

			// track the particle
			double x = p.getX();
			double y = p.getY();
			double z = p.getZ();

			x += vx * dt;
			y += vy * dt;
			z += vz * dt;

			// what cell are we in now moved to?
			Cell c = mesh.findCell(x, y, null);

			// System.err.printf("Transp::move: T: %6.2f XYZ: (%5.2f %5.2f %5.2f) Cell: %s
			// \n", t, x, y, z, c);

			// in a real cell?
			if (c != null) {
				p.updatePosition(x, y, z);

				// did we land
				if (c.zc > z) {

					// particle landed.
					FinalPoint point = new FinalPoint(x, y, c, CrossingType.LAND);
					System.err.printf("Particle has hit seabed at depth %f in cell depth: %f\n", z, c.zc);

					p.setLandingPosition(x, y, z);
					p.setLandingTime((long) t);
					p.setState(ParticlesState.ONBED);
					return; // particle has been updated
				}
			}

			if (c == null || c.zc > 0.0) { // ood or inside a land cell
				System.err.printf(
						"Particle left domain at: t=%f, position: (%f, %f, %f) LKC: %d : investigating edge crossings...\n",
						t, x, y, z, lastc.ref);

				CrossingType isect = lastc.intersectEdges(xlast, ylast, x, y);
				// System.err.printf("Cells own intersect alg gives: %s \n", isect.name());

				if (isect == CrossingType.SEA) {
					FinalPoint point = trackToEnd(xlast, ylast, dt / 2.0);
					lastc = point.cell;
					xlast = point.x;
					ylast = point.y;

					CrossingType crossing = point.crossing;
					if (crossing == CrossingType.LAND) {
						p.updatePosition(xlast, ylast, p.getZ()); // Z???
						p.setLandingPosition(xlast, ylast, p.getZ()); // Z ??
						p.setLandingTime((long) t);
						p.setState(ParticlesState.ONBED);
					} else {
						p.updatePosition(xlast, ylast, p.getZ()); // Z???
						p.setState(ParticlesState.LOST);
						p.setLostTime((long) t);
					}

					return;
				} else {
					// System.err.printf("Completed tracking, last cell crossed is: %d \n",
					// lastc.ref);
					FinalPoint point = new FinalPoint(xlast, ylast, lastc, isect);

					if (isect == CrossingType.LAND) {
						p.updatePosition(xlast, ylast, p.getZ()); // Z???
						p.setLandingPosition(xlast, ylast, p.getZ()); // Z ??
						p.setLandingTime((long) t);
						p.setState(ParticlesState.ONBED);
					} else {
						p.updatePosition(xlast, ylast, p.getZ()); // Z???
						p.setLostTime((long)t);
						p.setState(ParticlesState.LOST);
					}
					return;
				}

			}

		} // move()

		private FinalPoint trackToEnd(double startx, double starty, double step) {

			// System.err.printf("Start tracking to final cell...\n");

			double x = startx;
			double y = starty;

			double dt = step;
			Cell c = mesh.findCell(x, y, null);

			Cell lastc = c;
			double lastx = x;
			double lasty = y;

			boolean notdone = true;
			// repeat tracking till we have found a suitable edge crossing
			while (notdone) {
				// try {Thread.sleep(1000L);} catch (Exception ix) {}
				// System.err.printf(" Track to final cell with: dt=%f", dt);

				// use the latest known sea position to start from
				x = lastx;
				y = lasty;
				c = mesh.findCell(x, y, null);
				// System.err.printf(" starting position: (%f, %f) in cell: %d \n", x, y,
				// c.ref);

				int nm = 0;
				// track the particle from its last known position to ood or land
				while (c != null) {
					nm++;
					x += vx * dt;
					y += vy * dt;
					c = mesh.findCell(x, y, null);
					// System.err.printf(" at (%f, %f) cell: %d\n", x, y, (c != null ? c.ref : 0));
					// record the last sea cell we hit
					if (c != null) {
						lastc = c;
						lastx = x;
						lasty = y;
					}
				}
				// System.err.printf(
				// " Exited sea region after %d steps, last sea cell was: %d last position: (%f,
				// %f) \n", nm,
				// lastc.ref, lastx, lasty);
				// we either hit an OOD or LAND cell, work out which
				// draw a line from where we started to where we are now
				CrossingType isect = lastc.intersectEdges(startx, starty, x, y);

				// we crossed into land or ood so done
				// TODO If the final leap is still quite large we might try to half the timestep
				//      and attempt to get closer to the edge
				// NOTE we could still leap a block of land cells if fast flow.
				
				if (isect == CrossingType.LAND || isect == CrossingType.OPEN) {
					// System.err.printf(" Exiting track to final in sea cell: %d status: %s\n",
					// lastc.ref,
					// isect.name());
					return new FinalPoint(lastx, lasty, lastc, isect);
				}
				// if we find a SEA edge then we have jumped several internal cells
				// half the time step and repeat
				dt /= 2.0;

			}

			return null;

		}

	} // [TransportMech]

}
