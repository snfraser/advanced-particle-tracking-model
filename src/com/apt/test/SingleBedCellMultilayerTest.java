/**
 * 
 */
package com.apt.test;

import java.awt.BorderLayout;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JPanel;

import com.apt.data.BedLayerType;
import com.apt.data.ChemicalComponent;
import com.apt.data.ChemicalComponentType;
import com.apt.data.CompoundMass;
import com.apt.data.Particle;
import com.apt.data.ParticleClass;
import com.apt.data.ParticlesState;
import com.apt.data.Seabed;
import com.apt.data.SeabedType;
import com.apt.gui.BedLevelsCountPlot;
import com.apt.gui.CombinedTimeSeries1;
import com.apt.gui.CombinedTimeSeries2;
import com.apt.gui.MassTimeSeriesPanel;
import com.apt.models.IBedLayer;
import com.apt.models.IBedTransportModel;
import com.apt.models.IChemicalDegradeModel;
import com.apt.models.IConsolidationModel;
import com.apt.models.IErosionModel;
import com.apt.models.IFlowModel;
import com.apt.models.bed.BedCellModel;
import com.apt.models.bed.BedLayer;
import com.apt.models.bed.BedLayerModel;
import com.apt.models.bed.ExcessStressLinearErosionModel;
import com.apt.models.chemical.NoChemicalDegradeModel;
import com.apt.models.consolidation.DefaultConsolidationModel;
import com.apt.models.flow.DepthVaryingFlowModel;
import com.apt.models.flow.NOCFlowModel;
import com.apt.models.transport.NoBedTransportModel;

/**
 * @author SA05SF
 *
 */
public class SingleBedCellMultilayerTest {

	static final int NLAYERS = 6;
	static final int NDAYS = 80;

	// sim runs set 1:: 0.14 m/s 0.045 kg per deposition
	// sim runs set 2:: 0.1 m/s kg per deposition

	/** Mass of the single waste particle each timestep. */
	//static final double WASTE_MASS = 0.046;
	static final double WASTE_MASS = 0.07;

	
	static final double MEANFLOW = 0.14;
	static final double TIDAL = 6 * 3600 * 1000.0;
	static final double SPRINGS = 14 * 24 * 3600 * 1000.0;

	List<IBedLayer> layers;
	BedLayerModel blm;
	BedCellModel cell;
	IBedTransportModel notrans;
	IChemicalDegradeModel nochem;

	IFlowModel flow;

	// MassTimeSeriesPanel panel;
	CombinedTimeSeries1 cts;
	CombinedTimeSeries2 ctsSurface;
	CombinedTimeSeries2 ctsL1;
	CombinedTimeSeries2 ctsBasement;
	
	BedLevelsCountPlot blp;

	int ip = 0;

	/**
	 * 
	 */
	public SingleBedCellMultilayerTest() {

		// create surface
		CompoundMass surfaceMass = new CompoundMass();
		surfaceMass.addComponent(new ChemicalComponent(ChemicalComponentType.SOLIDS, 0.0));
		BedLayer surface = new BedLayer(surfaceMass);
		surface.setDensity(1700.0);

		// create basement
		CompoundMass basementMass = new CompoundMass();
		basementMass.addComponent(new ChemicalComponent(ChemicalComponentType.SOLIDS, 0.0));
		BedLayer basement = new BedLayer(basementMass);
		basement.setDensity(1700.0);
		Seabed seabed = new Seabed(SeabedType.MUD, 0.02, 0.0002, 0.0002);
		
		blm = new BedLayerModel(1, 2, 5, surface, basement);
		IConsolidationModel consolidation = new DefaultConsolidationModel(900, 600, 1200);
		
		IErosionModel erosion = new ExcessStressLinearErosionModel(0.031);
		cell = new BedCellModel(1, 1000, 1000, 100.0, 20.0, seabed, erosion, consolidation, blm); // area: 100m2, depth 20m

		notrans = new NoBedTransportModel();
		nochem = new NoChemicalDegradeModel();
				
		// double depthrate = (0.5 - 0.1)/13.0; // drops from 0.5 to 0.1 in 25m
		// flow = new DepthVaryingFlowModel(30.0, 0.5, 0.02, depthrate, 12*3600.0);
		// flow = new ExponentialDepthVaryingFlowModel(30.0, 0.45, 12 * 3600.0);
		flow = new NOCFlowModel(20.0, 0.0012, 18.0, MEANFLOW, 0.0);
	
		// output panel
		cts = new CombinedTimeSeries1();
		JPanel panel = cts.createChartPanel("Deposition / erosion", "Single cell");

		JFrame f = new JFrame("Deposition mass time-series");
		f.getContentPane().setLayout(new BorderLayout());
		f.getContentPane().add(panel, BorderLayout.CENTER);
		f.pack();
		f.setSize(1200, 800);
		f.setLocation(100, 100);
		f.setVisible(true);
		
		
		blp = new BedLevelsCountPlot();
		JPanel panel2 = blp.createChartPanel("Bed layers count");
		
		JFrame fb = new JFrame("Bed layers");
		fb.getContentPane().setLayout(new BorderLayout());
		fb.getContentPane().add(panel2, BorderLayout.CENTER);
		fb.pack();
		fb.setSize(500, 500);
		fb.setLocation(1400, 100);
		fb.setVisible(true);
		
		ctsSurface = new CombinedTimeSeries2();
		cts2(ctsSurface, "Surface layer", 1400, 600);
		
		ctsL1 = new CombinedTimeSeries2();
		cts2(ctsL1, "Layer 1", 1000, 600);
		
		ctsBasement = new CombinedTimeSeries2();
		cts2(ctsBasement, "basement layer", 1400, 500);
		
	}
	
	private void cts2(CombinedTimeSeries2 cts2, String subtitle, int x, int y) {
		JPanel panel = cts2.createChartPanel("Critical stress", subtitle);
		
		JFrame f = new JFrame("Critical Stress");
		f.getContentPane().setLayout(new BorderLayout());
		f.getContentPane().add(panel, BorderLayout.CENTER);
		f.pack();
		f.setSize(500, 500);
		f.setLocation(x, y);
		f.setVisible(true);
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		SingleBedCellMultilayerTest test = new SingleBedCellMultilayerTest();
		test.run();

	}

	private void run() {
		
		double thresholdMass = cell.getThresholdMass();

		double resusMass = 0.0; // mass eroded recently and now being re-deposited

		long timestep = 60000L;

		double depomass = 0.0;

		// a series of times...
		for (int it = 0; it < 1440 * NDAYS; it++) {

			long time = it * timestep;

			double meanFlow = MEANFLOW * Math.sin(2.0 * Math.PI * (double) time / TIDAL)
					* Math.sin(2.0 * Math.PI * (double) time / SPRINGS);
			((NOCFlowModel) flow).updateMean(meanFlow, 0.0);

			System.err.println(
					"---------------------------------------------------------------------------------------------------");
			System.err.printf("Start cycle: %d: time: %d ms [%.2f h]\n", it, time, (double) time / 3600000.0);

			if (resusMass > 0.0) {
				Particle rp = createParticle(resusMass, time);
				cell.deposit(rp);
			}

			Particle p = createParticle(WASTE_MASS, time);
			System.err.printf("Depositing particle: %s with mass: %f to bed load\n", p.getId2(),
					p.getMasses().getComponentMass(ChemicalComponentType.SOLIDS));
			cell.deposit(p);
			depomass += p.getMasses().getComponentMass(ChemicalComponentType.SOLIDS);

			double bedloaddmass = cell.getBedLoad().getComponentMass(ChemicalComponentType.SOLIDS);

			// what is the flow at the landing position ie this cell
			double flowS = flow.getFlow(p.getX(), p.getY(),   0.0, time).getU(); // surface
			double flowu = flow.getFlow(p.getX(), p.getY(), -19.8, time).getU(); // boundary

			cell.processBedLoad(time, timestep, flow, notrans, nochem); // ideally include the flow profile at the cell location

			double totalMass = cell.getTotalMass().getComponentMass(ChemicalComponentType.SOLIDS);
			System.err.printf("Total mass in cell: %f from %d layers \n", totalMass,
					cell.getLayers().getNumberLayers());
			System.err.println();

			double surfaceMass = cell.getLayers().getSurfaceLayer().getMass()
					.getComponentMass(ChemicalComponentType.SOLIDS);
			// panel.updateMass(time, totalMass);
			double[] data = cell.getMyData();

			System.err.printf("Bedload: %f SV: %f, BV: %f. BFV: %f TauB: %f E: %f\n", bedloaddmass, flowS, flowu, data[0],
					data[1], data[2]);
			System.err.printf("Bed deposit so far: %f \n", depomass);
			cts.addSurfaceLayerMassData(time, totalMass, thresholdMass);
			// cts.addSurfaceLayerMassData(time, surfaceMass);
			cts.addBedLoad(time, bedloaddmass);
			cts.addSurfaceFlowData(time, flowS); // flows
			cts.addBoundaryFlowData(time, flowu); // flowu
			cts.addBFVData(time, data[0]); // bfv
			cts.addTauBData(time, data[1]); // taub
			cts.addErosionFluxData(time, data[2]); // erosion flux

			// try {Thread.sleep(50L);} catch (Exception e) {}

			resusMass = 0.2 * (0.1 + Math.random() * 0.2) * data[2]; // add some of eroded mass onto the deposition next
																		// cycle
			// TODO later we will add into water-column particles with appropriate release
			// height and characteristics

			//try {Thread.sleep(10L);} catch (InterruptedException ix) {}
			
			displayLayersCount(time);
			
		} // next timestep

		System.err.printf("Total waste material deposited: %f \n", depomass);

	}

	private Particle createParticle(double mass, long time) {
		Particle p = new Particle();
		p.setX(1000);
		p.setY(1000);
		p.setZ(-20.0);
		p.setStartPosition(1000, 1000, -20.0);
		p.setLandingTime(time);
		p.setLandingPosition(p.getX(), p.getY(), -20.0);
		p.setState(ParticlesState.ONBED);
		p.setCreationTime(time); // created 1 minute ago
		//p.setFeedMass(0.0);
		p.setType(ParticleClass.FEED);
		p.setId2(String.format("P%5d", ++ip));
		p.addChemical(new ChemicalComponent(ChemicalComponentType.SOLIDS, mass)); // 5 kg/cell/minute
		return p;
	}

	private void displayLayersCount(long time) {
		int nl = blm.getNumberLayers();
		nl += 2;
		blp.updateLayerCount(time, nl);
		
		double taus = blm.getSurfaceLayer().getCurrentTauCrit();
		double taues = blm.getSurfaceLayer().getEquilibriumTauCrit();
		ctsSurface.addTauCData(time, taus, taues);
		
		if (blm.getNumberLayers() > 0) {
			double tau1 = blm.getLayer(0).getCurrentTauCrit();
			double taue1 = blm.getLayer(0).getEquilibriumTauCrit();
			ctsL1.addTauCData(time, tau1, taue1);
		}
		
		double taub = blm.getBasement().getCurrentTauCrit();
		double taueb = blm.getBasement().getEquilibriumTauCrit();
		ctsBasement.addTauCData(time, taub, taueb);
	}

}
