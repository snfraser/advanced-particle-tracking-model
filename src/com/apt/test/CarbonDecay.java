/**
 * 
 */
package com.apt.test;

import com.apt.data.ChemicalComponent;
import com.apt.data.ChemicalComponentType;
import com.apt.data.CompoundMass;
import com.apt.models.chemical.DefaultChemicalDegradeModel;

/**
 * Test the default carbon decay model.
 */
public class CarbonDecay {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
	
		ChemicalComponent cref = new ChemicalComponent(ChemicalComponentType.CARBON_REFACTORY, 0.0);
		ChemicalComponent clab = new ChemicalComponent(ChemicalComponentType.CARBON_LABILE, 0.0);
		ChemicalComponent co2 =  new ChemicalComponent(ChemicalComponentType.CO2, 0.0);
		
		CompoundMass mass = new CompoundMass();
		mass.addComponent(clab);
		mass.addComponent(cref);
		mass.addComponent(co2);
		
		DefaultChemicalDegradeModel decay = new DefaultChemicalDegradeModel();
		
		long time = 0L;
		while (time < 365*86400*1000L) {
			
			// add some carbon to the pool
			cref.addMass(0.1);
			clab.addMass(0.1);
			
			decay.degrade(mass, 3600000L);
			
			double cmass = cref.getMass() + clab.getMass();
			System.err.printf("%8.1f h C: %8.5f R: %8.5f L: %8.5f CO2: %8.5f \n", (double)time/3600.0, cmass, cref.getMass(), clab.getMass(), co2.getMass());
			
			time += 3600000L; // each hour
		}
		
		
		
		

	}

}
