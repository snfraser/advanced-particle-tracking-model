/**
 * 
 */
package com.apt.test;

import java.util.ArrayList;
import java.util.List;

import com.apt.data.PositionVector;
import com.apt.models.Cage;
import com.apt.models.cage.MultiGroupCagePositionManager;
import com.apt.models.cage.RegularGridCagePositionManager;

/**
 * @author SA05SF
 *
 */
public class CreateCageLayoutsCheckPositions {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		List<Cage> allcages = new ArrayList<Cage>();
		
		
		RegularGridCagePositionManager g1 = new RegularGridCagePositionManager(1000.0, 1000.0, 70.0, 70.0, 25.0);
		for (int i=0; i < 2; i++) {
			for (int j=0; j < 6; j++) {
				Cage cage = new Cage(String.format("C1:%d:%d",i,j), 0, 0, 30, 30, 10, 0, 0, null, null); 
				g1.add(cage, i, j);
				allcages.add(cage);
			}
		}
		
		RegularGridCagePositionManager g2 = new RegularGridCagePositionManager(1200.0, 1600.0, 70.0, 70.0, 35.0);
		for (int i=0; i < 2; i++) {
			for (int j=0; j < 5; j++) {
				Cage cage = new Cage(String.format("C2:%d:%d",i,j), 0, 0, 30, 30, 10, 0, 0, null, null); 
				g2.add(cage, i, j);
				allcages.add(cage);
			}
		}
		
		MultiGroupCagePositionManager mg  = new MultiGroupCagePositionManager();
		mg.addGroup(g1);
		mg.addGroup(g2);
		
		// now lets find each of these cages
		for (Cage c : allcages) {
			PositionVector pos = mg.getPosition(c.getName());
			System.err.printf("cage: %s at: (%.2f, %.2f) \n", c.getName(), pos.getX(), pos.getY());
		}
		
		
		

	}

}
