/**
 * 
 */
package com.apt.test;

import com.apt.models.flow.NOCFlowModel;

/**
 * @author SA05SF
 *
 */
public class DepthProfileNOCFlow {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		double DEPTH = 37.0;
		double Z0 = 0.05;
		double Z1 = 0.005;
		double Z2 = 0.0005;
		double DELTA = 37.0;
		double MEAN = 0.04;
		double ANGLE = 0.0;
		
		NOCFlowModel flow0 = new NOCFlowModel(DEPTH, Z0, DELTA, MEAN, ANGLE);
		NOCFlowModel flow1 = new NOCFlowModel(DEPTH, Z1, DELTA, MEAN, ANGLE);
		NOCFlowModel flow2 = new NOCFlowModel(DEPTH, Z2, DELTA, MEAN, ANGLE);
		
		double h = 0;
		while (h < DEPTH) {
			double zz = -(DEPTH - h); // zz is depth from surface always -ve
			double uflow0 = flow0.getFlow(0, 0, h-DEPTH, 0).getU();
			double uflow1 = flow1.getFlow(0, 0, h-DEPTH, 0).getU();
			double uflow2 = flow2.getFlow(0, 0, h-DEPTH, 0).getU();
			System.err.printf("Z: %f H: %f flow: %f %f %f\n", zz, h, uflow0, uflow1, uflow2);
			h += 0.1;
		}
		

	}

}
