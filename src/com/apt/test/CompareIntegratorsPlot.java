/**
 * 
 */
package com.apt.test;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JPanel;

import com.apt.data.Cell;
import com.apt.data.FlowVector;
import com.apt.data.Particle;
import com.apt.data.PositionVector;
import com.apt.models.IFlowModel;
import com.apt.models.transport.EulerIntegrator;
import com.apt.models.transport.Integrator;
import com.apt.models.transport.RungeKutta2Integrator;
import com.apt.models.transport.RungeKutta4Integrator;

/**
 * @author SA05SF
 *
 */
public class CompareIntegratorsPlot {

	Integrator euler;
	Integrator rk4;
	Integrator rk2;

	// save the track positions as particles
	List<PositionVector> ep;
	List<PositionVector> rp2;
	List<PositionVector> rp4;

	PlotPanel pp;

	public CompareIntegratorsPlot() {

		IFlowModel flowModel = new InternalFlowModel();

		euler = new EulerIntegrator(flowModel);
		rk2 = new RungeKutta2Integrator(flowModel);
		rk4 = new RungeKutta4Integrator(flowModel);

		ep = new ArrayList<PositionVector>();
		rp2 = new ArrayList<PositionVector>();
		rp4 = new ArrayList<PositionVector>();

		pp = new PlotPanel();
		JFrame f = new JFrame("Euler, RK2, RK4 integrator test");
		f.getContentPane().setLayout(new BorderLayout());
		f.getContentPane().add(pp, BorderLayout.CENTER);
		f.setBounds(100, 100, 600, 600);
		f.setVisible(true);

	}

	private void run() {

		double startx = 1000.0;
		double starty = 1000.0;

		Particle p1 = new Particle();
		p1.setStartPosition(startx, starty, 0.0);

		Particle p2 = new Particle();
		p2.setStartPosition(startx, starty, 0.0);

		Particle p3 = new Particle();
		p3.setStartPosition(startx, starty, 0.0);

		long time = 0L;
		long step = 10 * 1000L; // 2s step size
		int istep = 0;
		double ex = 0.0;
		double ey = 0.0;
		double r2x = 0.0;
		double r2y = 0.0;
		double r4x = 0.0;
		double r4y = 0.0;

		// time step the particle position for 1 hours
		while (time < 3600 * 4 * 1000L) {

			// euler calc
			PositionVector newPosition = euler.move(p1, time, step);
			ex = newPosition.getX();
			ey = newPosition.getY();
			p1.setX(ex);
			p1.setY(ey);
			PositionVector pv1 = new PositionVector(p1.getX(), p1.getY(), 0.0);
			ep.add(pv1);

			// rk2 calc
			PositionVector rk2Position = rk2.move(p2, time, step);
			r2x = rk2Position.getX();
			r2y = rk2Position.getY();
			p2.setX(r2x);
			p2.setY(r2y);
			PositionVector pv2 = new PositionVector(p2.getX(), p2.getY(), 0.0);
			rp2.add(pv2);

			// rk4 calc
			PositionVector rk4Position = rk4.move(p3, time, step);
			r4x = rk4Position.getX();
			r4y = rk4Position.getY();
			p3.setX(r4x);
			p3.setY(r4y);
			PositionVector pv3 = new PositionVector(p3.getX(), p3.getY(), 0.0);
			rp4.add(pv3);

			// log the differences
			// System.err.printf("Step: %d Time: %d Euler: (%f, %f) RK4: (%f, %f) \n",
			// istep, time/1000, ex, ey, rx, ry);

			System.err.printf("[%d] %ds E: %f %f RK2: %f %f RK4: %f %f\n", istep, time / 1000, ex, ey, r2x, r2y, r4x, r4y);

			istep++;
			time += step;

			pp.repaint();

			try {
				Thread.sleep(50L);
			} catch (Exception e) {
			}

		}

		double dxeuler = ex - startx;
		double dyeuler = ey - starty;
		double dxrk2 = r2x - startx;
		double dyrk2 = r2y - starty;
		double dxrk4 = r4x - startx;
		double dyrk4 = r4y - starty;
		double deuler = Math.sqrt(dxeuler * dxeuler + dyeuler * dyeuler);
		double drk2 = Math.sqrt(dxrk2 * dxrk2 + dyrk2 * dyrk2);
		double drk4 = Math.sqrt(dxrk4 * dxrk4 + dyrk4 * dyrk4);

		System.err.printf("Total distance moved: Euler: %fm  RK2: %fm  RK4: %fm \n", deuler, drk2, drk4);

	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		CompareIntegratorsPlot cp = new CompareIntegratorsPlot();
		cp.run();

	}

	private class PlotPanel extends JPanel {

		public void paint(Graphics g) {
			int W = getSize().width;
			int H = getSize().height;

			g.setColor(Color.green);
			// find the latest e and rk positions
			for (PositionVector p : ep) {

				int x = (int) (p.getX() * W / 4000.0);
				int y = (int) (p.getY() * H / 4000.0);

				g.fillOval(x, y, 2, 2);
			}
			
			g.setColor(Color.blue);
			// find the latest e and rk positions
			for (PositionVector p : rp2) {

				int x = (int) (p.getX() * W / 4000.0);
				int y = (int) (p.getY() * H / 4000.0);

				g.fillOval(x, y, 2, 2);
			}


			g.setColor(Color.red);
			// find the latest e and rk positions
			for (PositionVector p : rp4) {

				int x = (int) (p.getX() * W / 4000.0);
				int y = (int) (p.getY() * H / 4000.0);

				g.fillOval(x, y, 2, 2);
			}

		}
	}

	private class InternalFlowModel implements IFlowModel {

		double peak = 0.2; // m/s
		double TIDAL_PERIOD = 12.0 * 3600.0 * 1000.0; // ms
		double DIR = Math.toRadians(45.0); // ne-sw
		double ucomp = Math.sin(DIR);
		double vcomp = Math.cos(DIR);

		public FlowVector getFlow(double x, double y, double z, long time, Cell cell) {
			return getFlow(x, y, z, time);
		}

		@Override
		public FlowVector getFlow(double x, double y, double z, long time) {
			double flow = peak * Math.sin(2.0 * Math.PI * (double) time / TIDAL_PERIOD);
			// adda small increment to v as fn of x (x=0, dv = 0.0; x = 2000, dv = 0.1:
			double du = -0.1 * (y / 2000.0);
			double dv = 0.1 * (x / 2000.0);
			return new FlowVector(flow * ucomp + du, flow * vcomp + dv, 0.0);
		}

		@Override
		public double getBoundaryFlowHeight() {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public FlowVector getBoundaryFlow(double x, double y, long time) {
			// TODO Auto-generated method stub
			return null;
		}

	}
}
