/**
 * 
 */
package com.apt.test;

import com.apt.models.ISeabedTemperatureModel;
import com.apt.models.seabed.DefaultSeabedTemperatureModel;

/**
 * @author SA05SF
 *
 */
public class SeabedTemperatureYear {

	/** Mean value of temperature (C). */
	static double MEAN_TEMP = 15.0;

	/** Variation period (high-freq eg daily) (ms). */
	static double HI_FREQ_PERIOD = 24.0 * 86400.0 * 1000.0;

	/** Variation period (low-freq e.g. seasonal) (ms). */
	static double LO_FREQ_PERIOD = 12.0 * 30.0 * 86400.0 * 1000.0;

	/** Amplitude of temperature variation (high-freq/diurnal) (C). */
	static double HI_FREQ_AMP = 5.0;

	/** Amplitude of temperature variation (low-freq/seasonal) (C). */
	static double LO_FREQ_AMP = 10;

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		DefaultSeabedTemperatureModel sbt = new DefaultSeabedTemperatureModel(MEAN_TEMP, HI_FREQ_PERIOD, LO_FREQ_PERIOD,
				HI_FREQ_AMP, LO_FREQ_AMP);

		double time = 0.0; // start
		while (time < 365.0 * 86400.0 * 1000.0) {
			double temp = sbt.getTemperature(time);
			System.err.printf("%f %f\n", time / 86400000.0, temp);
			time += 6.0 * 3600.0 * 1000.0; // 6 hours
		}

	}

}
