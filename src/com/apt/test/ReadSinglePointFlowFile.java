/**
 * 
 */
package com.apt.test;

import java.io.File;

import javax.swing.JFileChooser;

import com.apt.data.FlowVector;
import com.apt.data.Mesh;
import com.apt.models.IFlowModel;
import com.apt.readers.RegularGridBathymetryReader;
import com.apt.readers.SinglePointFlowmetryReader;

/**
 * @author SA05SF
 *
 */
public class ReadSinglePointFlowFile {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		JFileChooser chooser = new JFileChooser();
		if (chooser.showOpenDialog(null) != JFileChooser.APPROVE_OPTION)
			return;

		File flowFile = chooser.getSelectedFile();

		SinglePointFlowmetryReader rdr = new SinglePointFlowmetryReader();

		IFlowModel flow = null;
		try {
			flow = rdr.readFile(flowFile);
			System.err.printf("Read in flow data: %s \n", flow);
		} catch (Exception e) {
			e.printStackTrace();
		}

		// now lets see if we can get the flow at some specific points....
		long time = 0L;
		long te = 360 * 3600000L; // 360 hours ahead
		while (time < te) {
			double sigma = -0.95;
			double tt = (double)time/1000.0;
			System.err.printf("Time: %fs ", tt);
			while (sigma < 0.1) {
				FlowVector fv = flow.getFlow(0.0, 0.0, sigma, time);
				double u = fv.getU();
				double v = fv.getV(); 
				double speed = Math.sqrt(u*u + v*v);
				System.err.printf(" %.2f : %f,  ", sigma, speed);
				sigma += 0.2;
			}
			System.err.println();
			time += 4*3600000L;
		}
		
		// lets try timing the retrieval..
		long tts = System.currentTimeMillis();
		int ns = 1000000;
		for (int i = 0; i < ns; i++) {
			long tSample = (long)(Math.random()*te);
			double sigmaSample = -1.0*Math.random();
			FlowVector fv = flow.getFlow(0.0, 0.0, sigmaSample, tSample);
			double u = fv.getU();
			double v = fv.getV(); 
			double speed = Math.sqrt(u*u + v*v);
			//System.err.printf("Sample %5d at: time: %f sigma: %f : (%f, %f) -> %f\n", i, (double)tSample, sigmaSample, u, v, speed);
		}
		long tused = System.currentTimeMillis() - tts;
		System.err.printf("Time for: %d samples was: %f ms (%f ms per sample) \n", ns, (double)tused, (double)tused/ns);
		
		// jump ahead from cycle to cycle
		time = 0L;
		te = 40*24*3600000L;
		while (time < te) {
			FlowVector fv = flow.getFlow(0.0, 0.0, -0.5, time);
			double u = fv.getU();
			double v = fv.getV(); 
			double speed = Math.sqrt(u*u + v*v);
			System.err.printf(" %fh : %f \n", (double)time/(3600*1000), speed);
			time += 6*3600000L; // 6h at a time
		}
		
		
	}

}
