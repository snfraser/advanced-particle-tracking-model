/**
 * 
 */
package com.apt.test;

import com.apt.data.ChemicalComponentType;
import com.apt.data.CompoundMass;
import com.apt.models.Cage;
import com.apt.models.feed.FeedModel;
import com.apt.models.growth.GrowthModel;
import com.apt.models.stock.StockModel;

/**
 * @author SA05SF
 *
 */
public class BiomassGrowthCalc {

	final static long HOURS = 3600*1000L;
	final static long DAYS = 24*HOURS;
	
	public BiomassGrowthCalc() {}
	
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		BiomassGrowthCalc calc = new BiomassGrowthCalc();
		calc.execute();

	}

	private void execute() {
		
		FeedModel fm = new FeedModel(24*HOURS, 10, 1/1000.0, 0.03, 0.3, 0.002, 0.007);
		GrowthModel gm = new GrowthModel();
		
		StockModel stock = new StockModel(1000, 0.5); // 1000 fish at 0.5kg
		
		Cage cage = new Cage("01", 0, 0, 0, 0, 0, 0, 0, stock, fm);
		
		double initbiomass = stock.getBiomass();
		double totalfeed = 0.0;
		long time = 0L;
		while (time < 365*DAYS) {
			
			CompoundMass release = fm.releaseFeed(cage, time);
			if (release != null)
				totalfeed += release.getComponentMass(ChemicalComponentType.SOLIDS);
			
			// once a day we feed them fishes
			if (time % DAYS == 0) {
				
				System.err.printf("hour: %d stock: %f kg \n", time/HOURS, stock.getBiomass());
				
				System.err.printf("   release feed: %f kg  \n", release.getComponentMass(ChemicalComponentType.SOLIDS));
				
				gm.applyFeed(stock, release, time);
				
			}
			
			
			time += 1*HOURS;
		}
		
		double finalbiomass = stock.getBiomass();
		double diffbiomass = finalbiomass - initbiomass;
		double fcr = totalfeed/diffbiomass;
		double sgr = Math.log(finalbiomass/initbiomass)*100.0/365.0;
		System.err.printf("init bio: %f, final bio: %f, increase: %f totalfeed: %f fcr: %f sgr: %f\n", 
				initbiomass, finalbiomass, diffbiomass, totalfeed, fcr, sgr);
	
		
	}
}
