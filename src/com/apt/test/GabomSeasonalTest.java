package com.apt.test;

public class GabomSeasonalTest {

	public static void main(String[] args) {
		double sfr = 0.007;

		double sfrLo = 0.35 * sfr;
		double sfrHi = sfr;
		double ww = 365.0;
		double t0 = 135.0;

		for (int i = 0; i < 500; i++) {

			double t = (double) i;
			double sinarg = 2 * Math.PI *(t - t0) / ww;
			double sin = Math.sin(sinarg);
			double var = (1.0 + sin);
			double mysfr = sfrLo + 0.5 * (sfrHi - sfrLo) * var;
			System.err.printf("Day: %d agr: %f rads sin: %f var: %f sfr: %.10f \n", i, sinarg, sin, var, mysfr);

		}

	}

}
