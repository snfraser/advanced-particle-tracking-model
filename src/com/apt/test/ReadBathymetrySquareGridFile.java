/**
 * 
 */
package com.apt.test;

import java.io.File;

import javax.swing.JFileChooser;

import com.apt.data.Cell;
import com.apt.data.Domain;
import com.apt.data.Mesh;
import com.apt.data.RegularSquareMesh;
import com.apt.data.Spacing;
import com.apt.models.IBathymetryModel;
import com.apt.models.bathymetry.MeshBathymetry;
import com.apt.readers.RegularGridBathymetryReader;

/**
 * @author SA05SF
 *
 */
public class ReadBathymetrySquareGridFile {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
	
		JFileChooser chooser = new JFileChooser();
		if (chooser.showOpenDialog(null) != JFileChooser.APPROVE_OPTION)
			return;
		
		File bathyFile = chooser.getSelectedFile();
		
		RegularGridBathymetryReader rdr = new RegularGridBathymetryReader();
		
		Mesh mesh = null;
		try {
		    mesh = rdr.readFile(bathyFile);
			System.err.printf("Read in square mesh: ", mesh);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		Domain d = mesh.getDomainBounds();
		double x0 = d.x0;
		double y0 = d.y0;
		double x1 = d.x1;
		double y1 = d.y1;
		
		Spacing spacing = ((RegularSquareMesh)mesh).getSpacing();
		double dx = spacing.dx;
		double dy = spacing.dy;
		
		// work out number of cells in rows and cols (note nodes)
		int nx = (int)Math.ceil((x1-x0)/dx);
		int ny = (int)Math.ceil((y1-y0)/dy);
		
		System.err.printf("Test: Expecting nx (cell cols): %d, ny (cell rows): %d \n", nx, ny);
		
		IBathymetryModel bathy = new MeshBathymetry(mesh);
		
		for (int k = 0; k < 10; k++) {
			double x = x0 + Math.random()*dx*(nx-1);
			double y = y0 + Math.random()*dy*(ny-1);
			
			System.err.println();
			System.err.printf("Locate cell containing: (%f, %f) ....\n", x, y);
			Cell cell = mesh.findCell(x, y, null);
		
			System.err.printf(" cell is: %s \n", cell); 
			
			double bdepth = bathy.getDepth(x, y);
			System.err.printf("Bathy cell depth: %f \n", bdepth);
		}
		
		
	}

}
