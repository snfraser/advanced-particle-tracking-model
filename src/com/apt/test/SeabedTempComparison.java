/**
 * 
 */
package com.apt.test;

import com.apt.data.CarbonFraction;
import com.apt.models.ISeabedTemperatureModel;
import com.apt.models.carbon.DefaultCarbonDecayModel;
import com.apt.models.carbon.FixedCarbonDecayModel;
import com.apt.models.seabed.DefaultSeabedTemperatureModel;

/**
 * @author SA05SF
 *
 */
public class SeabedTempComparison {

	static final double HOUR = 3600.0*1000;
	static final double DAY = 86400.0*1000;
	static final double YEAR = 365.0*86400.0*1000;
	
	/** Mean value of temperature (C).*/
	static double MEAN_TEMP = 15.0;
	
	/** Variation period (high-freq eg daily) (ms).*/
	static double HI_FREQ_PERIOD = 24.0*86400.0*1000.0;
	
	/** Variation period (low-freq e.g. seasonal) (ms).*/
	static double LO_FREQ_PERIOD = 12.0*30.0*86400.0*1000.0;
	
	/** Amplitude of temperature variation (high-freq/diurnal) (C).*/
	static double HI_FREQ_AMP = 0.5;
	
	/** Amplitude of temperature variation (low-freq/seasonal) (C).*/
	static double LO_FREQ_AMP = 5;
	
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {

		final double M0 = 1000.0; // 1000kg of carbon 97% labile, 3% refractory

		CarbonFraction cm0 = new CarbonFraction(0.97 * M0, 0.03 * M0);

		
		// seasonal and diurnal variation
		ISeabedTemperatureModel sbm = new DefaultSeabedTemperatureModel(MEAN_TEMP, HI_FREQ_PERIOD, LO_FREQ_PERIOD, HI_FREQ_AMP, LO_FREQ_AMP);

		FixedCarbonDecayModel fcm = new FixedCarbonDecayModel(cm0); // fixed exponential decay 1 component
		DefaultCarbonDecayModel vcm = new DefaultCarbonDecayModel(sbm, cm0); // variable crohn model 2 components

		double time = 0.0; // ms
		while (time < YEAR) {
			double timed = time / (DAY); // time days
			CarbonFraction cma = fcm.getCarbonFractions(time);
			CarbonFraction cmb = vcm.getCarbonFractions(time);

			double temp = sbm.getTemperature(time);
			
			System.err.printf("%f %f %f %f %f %f %f %f\n", 
					timed, 
					temp,
					cma.getLabileMass(), cma.getRefractoryMass(), cma.getTotalMass(),
					cmb.getLabileMass(), cmb.getRefractoryMass(), cmb.getTotalMass());

			time += DAY;
		}

	}

}
