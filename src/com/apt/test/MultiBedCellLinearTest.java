/**
 * 
 */
package com.apt.test;

import java.awt.BorderLayout;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JPanel;

import com.apt.data.BedLayerType;
import com.apt.data.ChemicalComponent;
import com.apt.data.ChemicalComponentType;
import com.apt.data.CompoundMass;
import com.apt.data.Particle;
import com.apt.data.ParticleClass;
import com.apt.data.ParticlesState;
import com.apt.data.Seabed;
import com.apt.data.SeabedType;
import com.apt.gui.CombinedTimeSeries1;
import com.apt.gui.LinearMassProfile1;
import com.apt.models.IBedLayer;
import com.apt.models.IBedTransportModel;
import com.apt.models.IChemicalDegradeModel;
import com.apt.models.IConsolidationModel;
import com.apt.models.IErosionModel;
import com.apt.models.IFlowModel;
import com.apt.models.bed.BedCellModel;
import com.apt.models.bed.BedLayer;
import com.apt.models.bed.BedLayerModel;
import com.apt.models.bed.ExcessStressLinearErosionModel;
import com.apt.models.chemical.NoChemicalDegradeModel;
import com.apt.models.consolidation.DefaultConsolidationModel;
import com.apt.models.flow.DepthVaryingFlowModel;
import com.apt.models.flow.NOCFlowModel;
import com.apt.models.transport.NoBedTransportModel;

/**
 * A line of bedcells with deposition from the surface from a point source.
 * 
 * @author SA05SF
 *
 */
public class MultiBedCellLinearTest {

	/** Number of bed layers. */
	static final int NLAYERS = 6;

	/** Number of cells in a row. */
	static final int NCELLS = 80;

	static final double X0 = 50.0;
	static final double Y = 500.0;
	static final double CELLDX = 25.00;
	static final double CELLDY = 25.00;
	static final double CELLAREA = 625.0;
	static final double DEPTH = 20.0;
	static final long TIMESTEP = 60000L;
	static final int NTIMES = 1440*20;
	static final int NPCLES = 1;
	static final double PMASS = 5.0; // mass per timestep
	static final int NRESUSPCLS = 1;
	static final double RESUSHEIGHT = 5.0; // resus particles start at .5m from seabed.
	static final double MEANFLOW = 0.35;
	static final double TIDAL = 6*3600*1000.0;
	static final double SPRINGS = 14*24*3600*1000.0;
	
	int icl = 0; // layer ID counter
	int ipc = 0; // suspension particle count
	int irc = 0; // resus particle count

	double releasedMass = 0.0;
	double depositedMass = 0.0;
	int lostParticleCount = 0;
	double lostMass = 0.0;
	int locationFailCount = 0;
	
	List<Particle> waterColumnParticles;
	List<BedCellModel> cells;
	IFlowModel flow;
	IBedTransportModel notrans;
	IChemicalDegradeModel nochem;

	double[] maxDepositionPerCell;
	
	LinearMassProfile1 cp;
	
	/**
	 * 
	 */
	public MultiBedCellLinearTest() {

		waterColumnParticles = new ArrayList<Particle>();

		cells = new ArrayList<BedCellModel>();

		for (int icell = 0; icell < NCELLS; icell++) {

			BedCellModel bcm = createCell(icell);
			cells.add(bcm); // retrieve using cells.get(icell) ...

		}

		notrans = new NoBedTransportModel();
		nochem = new NoChemicalDegradeModel();

		//flow = new ExponentialDepthVaryingFlowModel(35.0, 0.35, 12 * 3600.0);
		
		// depth 30m, z0 0.0012, delta 20, meanflow 0.2
		flow = new NOCFlowModel(20.0, 0.0012, 20, MEANFLOW, 0.0);
		
		maxDepositionPerCell = new double[NCELLS];
		
		
		// display a graph
	    //panel = new MassTimeSeriesPanel();
	    cp = new LinearMassProfile1();
	    JPanel panel = cp.createChartPanel("Cell mass deposition");
	    
	    
	    JFrame f = new JFrame("Deposition mass distribution");
		f.getContentPane().setLayout(new BorderLayout());
		f.getContentPane().add(panel, BorderLayout.CENTER);
		f.pack();
		f.setSize(1200, 800);
		f.setLocation(100, 100);
		f.setVisible(true);
	    
		
		
		

	}

	private BedCellModel createCell(int icell) {

		// surface layer
		CompoundMass surfaceMass = new CompoundMass();
		surfaceMass.addComponent(new ChemicalComponent(ChemicalComponentType.SOLIDS, 0.0));
		BedLayer surface = new BedLayer(surfaceMass);
		surface.setDensity(1700.0);
		
		// add a basement layer at the bottom
		CompoundMass basementMass = new CompoundMass();
		basementMass.addComponent(new ChemicalComponent(ChemicalComponentType.SOLIDS, 0.0));
		BedLayer basement = new BedLayer(basementMass);
		basement.setDensity(1700.0);
		
		Seabed seabed = new Seabed(SeabedType.MUD, 0.02, 0.0002, 0.0002);
	
		BedLayerModel blm = new BedLayerModel(icell, 2,5, surface,basement);
		
		IConsolidationModel consolidation = new DefaultConsolidationModel(900, 600, 1200);
			
		IErosionModel erosion = new ExcessStressLinearErosionModel(0.031);
		
		BedCellModel cell = new BedCellModel(icell, X0 + icell * CELLDX, Y, CELLAREA, DEPTH, seabed, erosion, consolidation, blm); // area: 1000m2,
																									// depth 20m
		System.err.printf("Create cell: %d at: %f \n", cell.getElementRef(), cell.getX());

		return cell;

	}

	/**
	 * Time-step the model. Drop some particles from source and follow them until
	 * they hit the bed at DEPTH
	 */
	public void run() {

		long timestep = TIMESTEP;

		List<Particle> landedParticles = new ArrayList<Particle>();
		List<Particle> lostParticles = new ArrayList<Particle>();
		List<Particle> erodedParticles = new ArrayList<Particle>();

		// a series of times...
		for (int it = 0; it < NTIMES; it++) {

			long time = it * timestep;

			// reset the mean flow speed which varies with time..
			double meanFlow = MEANFLOW*Math.cos(2.0*Math.PI*(double)time/TIDAL)*Math.cos(2.0*Math.PI*(double)time/SPRINGS);
			((NOCFlowModel)flow).updateMean(meanFlow, 0.0);
			
			System.err.println(
					"---------------------------------------------------------------------------------------------------");
			System.err.printf("Start cycle: %d: time: %d ms [%.2f h] WC: %d particles flow: %f \n", it, time,
					(double) time / 3600000.0, waterColumnParticles.size(), meanFlow);

			lostParticles.clear();
			landedParticles.clear();

			// add any eroded particles last timestep to the WC
			waterColumnParticles.addAll(erodedParticles);

			// track any existing WC particles.
			for (Particle p : waterColumnParticles) {
				double px = p.getX();
				double pz = p.getZ();
				double settling = 0.035;// 3.5 cm/s
				double flowU = flow.getFlow(p.getX(), p.getY(), -p.getZ(), time).getU();
				double dx = flowU * timestep / 1000;
				double dz = settling * timestep / 1000;
				p.setX(px + dx);
				p.setZ(pz + dz);
				System.err.printf(" Transport: P: %s x: %f z: %f h: %f mass: %f\n", 
						p.getId2(), p.getX(), -p.getZ(), (DEPTH - p.getZ()), p.getMasses().getComponentMass(ChemicalComponentType.SOLIDS));
				// check if its escaped
				if (p.getX() > X0 + (NCELLS - 0.5) * CELLDX || p.getX() < X0 - 0.5 * CELLDX) {
					lostParticles.add(p);
				}

				// check if its landed
				if (p.getZ() >= DEPTH) {
					p.setState(ParticlesState.ONBED);
					landedParticles.add(p);
				}

			}

			lostParticleCount += lostParticles.size();
			for (Particle p : lostParticles) {
				lostMass += p.getMasses().getComponentMass(ChemicalComponentType.SOLIDS);
			}
			
			System.err.printf("Lost: %d, Landed: %d \n", lostParticles.size(), landedParticles.size());

			// process lost particles.
			waterColumnParticles.removeAll(lostParticles);
			waterColumnParticles.removeAll(landedParticles);

			// create some new waste particles at source (NCELLS/2, Y, 0), add to WC.
			double releaseX = X0 + NCELLS * CELLDX * 0.5;
			for (int ip = 0; ip < NPCLES; ip++) {
				Particle p = createWasteParticle(time, releaseX, Y, 0.0, PMASS / NPCLES);
				waterColumnParticles.add(p);
				releasedMass += p.getMasses().getComponentMass(ChemicalComponentType.SOLIDS);
			}

			// process the landed particles here
			
			for (Particle p : landedParticles) {
				// which cell are we in...
				double x = p.getX();
				int icell = (int) Math.floor((x - X0 + 0.5*CELLDX) / CELLDX);
				System.err.printf("Processing landed particle: %s at: x: %f incell: %d \n", p.getId2(), p.getX(), icell);
				if (icell >= 0 && icell < NCELLS) {
					BedCellModel cell = cells.get(icell);
					cell.deposit(p);
				} else {
					locationFailCount++;
					System.err.printf("Computed cell number: %d for particle %s landed at: %f is not valid:  \n", icell, p.getId2(), x);
				}
			}
			
			for (BedCellModel c : cells) {
				CompoundMass cellErosion = c.processBedLoad(time, timestep, flow, notrans, nochem);
				// convert erosion to particle
				List<Particle> erosionParticles = convertToParticles(time, cellErosion, c);
				if (erosionParticles != null) {
					waterColumnParticles.addAll(erosionParticles);
					System.err.printf("Adding %d resus particles in cell: %d \n", erosionParticles.size(),
							c.getElementRef());
				}
			}
			
			// run over the cells and count the mass deposited..
			depositedMass = 0.0;
			for (BedCellModel c : cells) {
				double cellDeposition = c.getTotalMass().getComponentMass(ChemicalComponentType.SOLIDS);
				//System.err.printf("Cell: %d x: %f mass: %f \n", c.getElementRef(), c.getX(), cellDeposition);
				depositedMass += cellDeposition;
				// check the max deposition in this cell
				if (cellDeposition > maxDepositionPerCell[c.getElementRef()]) {
					maxDepositionPerCell[c.getElementRef()] = cellDeposition;
				}
				
				// only every hour
				if (it % 60 == 0)
					cp.addDataA(c.getX(), cellDeposition);
				
			}
			System.err.printf("Total released mass: %f bedmass: %f lost particles: %d Mass: %f kg\n", 
					releasedMass, depositedMass, lostParticleCount, lostMass);
			 //try {Thread.sleep(50L);}catch (Exception ex) {}
			
			cp.addInfo(time, flow.getFlow(X0+0.5*CELLDX*NCELLS, Y, 0.0, time).getU());
		} // next timestep

		// run over the cells and count the mass deposited..
		//for (BedCellModel c : cells) {
			//System.err.printf("Cell: %d x: %f mass: %f \n", c.getElementRef(), c.getX(),
				//	c.getTotalMass().getComponentMass(ChemicalComponentType.SOLIDS));
		//}
		
		System.err.printf("Total lost particles: %d Mass: %f kg\n, failed location: %d \n", lostParticleCount, lostMass, locationFailCount);
		//System.err.println("Maximum deposition per cell...");
		for (int icell = 0; icell < NCELLS; icell++) {
			BedCellModel cell = cells.get(icell);
			//System.err.printf("Cell: %d x: %f mass: %f \n", cell.getElementRef(), cell.getX(), maxDepositionPerCell[icell]);
		}

	}

	private Particle createWasteParticle(long time, double x, double y, double z, double mass) {

		Particle p = new Particle();
		p.setId2(String.format("P%d", ipc++));
		p.setX(x);
		p.setY(y);
		p.setZ(z);
		p.setStartPosition(x, y, z);
		p.setState(ParticlesState.SUSPEND);
		p.setCreationTime(time);
		//p.setFeedMass(1.0);
		p.setType(ParticleClass.FEED);
		p.addChemical(new ChemicalComponent(ChemicalComponentType.SOLIDS, mass));
		return p;

	}

	private List<Particle> convertToParticles(long time, CompoundMass erosionMass, BedCellModel cell) {
		if (erosionMass == null)
			return null;
		List<Particle> resusParticles = new ArrayList<Particle>();
		double pmass = erosionMass.getComponentMass(ChemicalComponentType.SOLIDS) / NRESUSPCLS;
		for (int ip = 0; ip < NRESUSPCLS; ip++) {
			double x = cell.getX() + (Math.random() - 0.5)*CELLDX;
			Particle p = createWasteParticle(time, x, cell.getY(), DEPTH - RESUSHEIGHT, pmass);
			p.setId2(String.format("R%d", irc++));
			resusParticles.add(p);
		}
		return resusParticles;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		MultiBedCellLinearTest test = new MultiBedCellLinearTest();
		test.run();
	}

}
