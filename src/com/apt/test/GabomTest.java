package com.apt.test;

/**
 * The GaBoM growth and biomass model
 * 
 * @author SA05SF
 *
 */
public class GabomTest {

	public static void main(String[] args) {
		
		/** Initial number of fish.*/
		int nf0 = 100000; 
		
		/** Initial fish average mass [kg].*/
		double avMass0 = 0.5;
		
		/** Max number days for simulation.*/
		double maxDays = 500.0;
		
		/** Mortality rate per day [fraction of fish dead per day] assumes 10% stock loss per annum.*/
		double mortalityRatePerDay = 10.0/365.0/100.0;
		
		/** Specific feed rate (7kg/tonne biomass).*/
		double sfr = 7.0/1000.0;
		
		/** Target feed conversion ratio (FCR) [kg feed per kg growth].*/
		double fcr = 1.2;
		
		
		double avMass = avMass0;
		int nf = nf0;
		double biomass0 = avMass0*(double)nf0;
		
		double biomass = biomass0;
		
		double totalFeedApplied = 0.0;
		double totalDeathMass = 0.0;
		
		double timeDays = 0.0;
		while (timeDays < maxDays) {
			
			// feed applied = SFR*biomass (7kg per tonne of biomass)
			// FCR = 1.2 => 1 tonne change in biomass per tonne feed
			// growth = feed applied/FCR
			
			// kill a few fish off
			int ndeaths = (int)(mortalityRatePerDay*(double)nf);
			
			double deadMass = ndeaths*avMass; 
			totalDeathMass += deadMass;
			
			nf = nf - ndeaths;
			
			biomass = nf*avMass;
			
			double feedApplied = sfr*biomass;
			
			totalFeedApplied += feedApplied;
			
			double growth = feedApplied/fcr;
			
			biomass += growth; // new biomass
			
			avMass = biomass/(double)nf;
			
			System.err.printf(" %4.1f  %d (-%d) av: %f kg fd: %f kg grw: %f kg biom: %f kg mort: %f kg \n", timeDays, nf, ndeaths, avMass, feedApplied, growth, biomass, totalDeathMass);
			
			timeDays += 1.0;
		}

		// calculate final FCR
		double sfrFinal = (totalFeedApplied / (biomass - biomass0));
		
		System.err.printf("Final conversion ratio: %f \n", sfrFinal);
		
	}

}
