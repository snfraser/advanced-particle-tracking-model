/**
 * 
 */
package com.apt.test;

import com.apt.models.defaecation.DefacationModel;

/**
 * @author SA05SF
 *
 */
public class RunDefacModel1 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
	
		long FEED_PERIOD = 12*3600*1000L; // every 12 hours
		
		DefacationModel dm = new DefacationModel();
		double biomass = 60000.0; // 60 tonnes
		
		long time = 0l;
		double timeSinceFeed = 0.0;
		
		double totalFaeces = 0.0;
		
		double timeStepHours = 1.0/60.0;
				
		// 1 day run
		while (time < 86400*1000L) {
			
			if (time % FEED_PERIOD == 0) {
				timeSinceFeed = 0.0; // reset feed time
				System.err.printf("%d is Feeding time \n", time/1000);
			}
			// 210 kg feed for 60kton twice a day....
			double defacrate = dm.getFaecesRate(210.0, timeSinceFeed);
			// quantity of poo = rate * biomass * timeperiod (60s)
			double faecesMass = defacrate*timeStepHours;
			totalFaeces += faecesMass;
			
			System.err.printf("%d Defac rate: %f kg/hr, Mass: %f kg,  Cumulative: %f kg \n", time/1000, defacrate, faecesMass, totalFaeces);
			
			timeSinceFeed += 60000.0; // 1 minute
			time += 60000L; //1 minute
		}
		
		

	}

}
