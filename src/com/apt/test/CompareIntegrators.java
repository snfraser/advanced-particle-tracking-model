/**
 * 
 */
package com.apt.test;

import com.apt.data.Cell;
import com.apt.data.FlowVector;
import com.apt.data.Particle;
import com.apt.data.PositionVector;
import com.apt.models.IFlowModel;
import com.apt.models.transport.EulerIntegrator;
import com.apt.models.transport.Integrator;
import com.apt.models.transport.RungeKutta2Integrator;
import com.apt.models.transport.RungeKutta4Integrator;

/**
 * Compare different integrators for a particle track.
 * 
 * @author SA05SF
 *
 */
public class CompareIntegrators {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		IFlowModel flowModel = new InternalFlowModel();

		Integrator euler = new EulerIntegrator(flowModel);
		Integrator rk2 = new RungeKutta2Integrator(flowModel);
		Integrator rk4 = new RungeKutta4Integrator(flowModel);

		double startx = Math.random() * 2000.0;
		double starty = Math.random() * 2000.0;

		Particle p1 = new Particle();
		p1.setStartPosition(startx, starty, 0.0);
		
		Particle p2 = new Particle();
		p2.setStartPosition(startx, starty, 0.0);

		Particle p3 = new Particle();
		p3.setStartPosition(startx, starty, 0.0);

		long time = 0L;
		long step = 60 * 1000L; // 1 minute step size
		int istep = 0;
		double ex = 0.0;
		double ey = 0.0;
		double r2x = 0.0;
		double r2y = 0.0;
		double r4x = 0.0;
		double r4y = 0.0;
		
		// time step the particle position for 1 hours
		while (time < 3600 * 1 * 1000L) {

			// euler calc
			PositionVector newPosition = euler.move(p1, time, step);
			ex = newPosition.getX();
			ey = newPosition.getY();
			p1.setX(ex);
			p1.setY(ey);

			// rk2 calc
			PositionVector rk2Position = rk2.move(p2, time, step);
			r2x = rk2Position.getX();
			r2y = rk2Position.getY();
			p2.setX(r2x);
			p2.setY(r2y);

			// rk4 calc
			PositionVector rk4Position = rk4.move(p3, time, step);
			r4x = rk4Position.getX();
			r4y = rk4Position.getY();
			p3.setX(r4x);
			p3.setY(r4y);

			// log the differences
			// System.err.printf("Step: %d Time: %d Euler: (%f, %f) RK4: (%f, %f) \n",
			// istep, time/1000, ex, ey, rx, ry);

			System.err.printf("[%d] %ds E: %f %f RK2: %f %f RK4: %f %f\n", istep, time / 1000, ex, ey, r2x, r2y, r4x, r4y);

			istep++;
			time += step;
		}

		double dxeuler = ex - startx;
		double dyeuler = ey - starty;
		double dxrk2 = r2x - startx;
		double dyrk2 = r2y - starty;
		double dxrk4 = r4x - startx;
		double dyrk4 = r4y - starty;
		double deuler = Math.sqrt(dxeuler * dxeuler + dyeuler * dyeuler);
		double drk2 = Math.sqrt(dxrk2 * dxrk2 + dyrk2 * dyrk2);
		double drk4 = Math.sqrt(dxrk4 * dxrk4 + dyrk4 * dyrk4);

		System.err.printf("Total distance moved: Euler: %fm RK2: %fm RK4: %fm \n", deuler, drk2, drk4);

	}

	private static class InternalFlowModel implements IFlowModel {

		double peak = 0.2; // m/s
		double TIDAL_PERIOD = 12.0 * 3600.0 * 1000.0; // ms
		double DIR = Math.toRadians(45.0); // ne-sw
		double ucomp = Math.sin(DIR);
		double vcomp = Math.cos(DIR);

		public FlowVector getFlow(double x, double y, double z, long time, Cell cell) {
			return getFlow(x, y, z, time);
		}

		@Override
		public FlowVector getFlow(double x, double y, double z, long time) {
			double flow = peak * Math.sin(2.0 * Math.PI * (double) time / TIDAL_PERIOD);
			// adda small increment to v as fn of x (x=0, dv = 0.0; x = 2000, dv = 0.1:
			double dv = 0.1 * (x / 2000.0);
			return new FlowVector(flow * ucomp, flow * vcomp + dv, 0.0);
		}

		@Override
		public double getBoundaryFlowHeight() {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public FlowVector getBoundaryFlow(double x, double y, long time) {
			// TODO Auto-generated method stub
			return null;
		}

	}

}
