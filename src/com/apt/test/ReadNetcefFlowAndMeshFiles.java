/**
 * 
 */
package com.apt.test;

import java.awt.BorderLayout;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JFileChooser;
import javax.swing.JFrame;

import com.apt.data.Cell;
import com.apt.data.FlowVector;
import com.apt.data.Particle;
import com.apt.data.ParticlesState;
import com.apt.data.TriangularMesh;
import com.apt.gui.ParticleMeshTrackingPanel;
import com.apt.models.flow.NetcdfFlowModel;
import com.apt.readers.NetcdfFlowReader;

/**
 * 
 */
public class ReadNetcefFlowAndMeshFiles {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		File meshFile = null;

		/*
		 * JFileChooser chooser = new JFileChooser(); int returnVal =
		 * chooser.showOpenDialog(null); if (returnVal == JFileChooser.APPROVE_OPTION) {
		 * meshFile = chooser.getSelectedFile(); }
		 */

		File flowFile = null;

		/*
		 * JFileChooser chooser2 = new JFileChooser(); returnVal =
		 * chooser2.showOpenDialog(null); if (returnVal == JFileChooser.APPROVE_OPTION)
		 * { flowFile = chooser2.getSelectedFile(); }
		 */

		File base = new File(System.getProperty("user.home"));
		File expandwork = new File(base + "/Desktop/EXPAND2-WORK");
		// File expandwork = new File(base + "/Downloads");
		meshFile = new File(expandwork, "WestCOMS2_Mesh.nc");
		flowFile = new File(expandwork, "westcoms2_20220101_0004.nc");
		// meshFile = new File(expandwork, "CaolMor_v5_0001.nc");
		// flowFile = new File(expandwork, "CaolMor_v5_0001.nc");

		long start = System.currentTimeMillis();
		try {
			NetcdfFlowReader reader = new NetcdfFlowReader();
			NetcdfFlowModel model = reader.readFile(meshFile, flowFile);
			long end = System.currentTimeMillis();

			System.err.printf("Built bathymetry and flow models: in %fs \n", (double) (end - start) / 1000.0);

			TriangularMesh mesh = model.getMesh();

			// work out some random point
			double x0 = 648892.26 + Math.random() * 8000.0;
			double y0 = 6230931.36 + Math.random() * 8000.0;
			double z0 = 0.0;
			long t0 = 5116176000000L + 3 * 3600 * 1000L;

			ParticleMeshTrackingPanel ptp = new ParticleMeshTrackingPanel(x0 - 6000, y0 - 6000, 12000, 12000);
			JFrame f = new JFrame();
			f.getContentPane().setLayout(new BorderLayout());
			f.getContentPane().add(ptp, BorderLayout.CENTER);
			f.setBounds(50, 50, 800, 800);
			f.setVisible(true);

			double settling = -0.0015; // 1.5mm/s
			long dt = 10000L; // 10 seconds
			long t = t0;

			int it = 0;
			
			Map<Integer, Particle> cellmap = new HashMap<Integer, Particle>();
			
			// start each pcel and localize it
			for (int i = 0; i < 100; i++) {
				double x = x0 + (Math.random() - 0.5) * 4000.0;
				double y = y0 + (Math.random() - 0.5) * 4000.0;
				double z = z0;
				// start the pcle
				Cell cell = mesh.findCell(x, y, null);
				Particle p = new Particle();
				p.setX(x);
				p.setY(y);
				p.setZ(z);
				p.setCurrentCell(cell);
				p.setState(ParticlesState.SUSPEND);
				cellmap.put(i, p);
			}

			while (t < t0 + 25 * 3600 * 1000L) {
				it++;

				for (int i = 0; i < 100; i++) {

					
					Particle p = cellmap.get(i);
					double x = p.getX();
					double y = p.getY();
					double z = p.getZ();
					Cell cell = p.getCurrentCell();
					if (p.getState() == ParticlesState.ONBED || p.getState() == ParticlesState.LOST)
						continue;
					
					System.err.println();
					//System.err.printf("main: Get Flow at: (%f, %f, %f) at time: %f s\n", x, y, z,
					//		(double) t0 / 1000.0);
					FlowVector flow = model.getFlow(x, y, z, t, cell);
					System.err.printf("main: Flow is: (%f, %f) \n", flow.getU(), flow.getV());
					System.err.printf("main: Pcl: %d Cell is: %d \n", i, cell.ref);

					//System.err.println();
					//System.err.printf("Starting particle tracking for pcle #%3d\n", i);

				
					double kx = 0.1;
					double ky = 0.1;

					// System.err.printf("\nTracking particle on timestep: %d .. \n", it);
					// System.err.printf(" -> Current cell: %s \n", cell);
					//flow = model.getFlow(x, y, z, t, cell);
					x += flow.getU() * (double) dt / 1000.0;
					y += flow.getV() * (double) dt / 1000.0;
					z += settling * (double) dt / 1000.0;

					// add some randomness sqrt(2*kx*dt)
					double rdx = (Math.random() - 0.5) * Math.sqrt(6.0 * kx * dt / 1000.0);
					double rdy = (Math.random() - 0.5) * Math.sqrt(6.0 * ky * dt / 1000.0);

					x += rdx;
					y += rdy;

					// System.err.printf(" ->Time: [%4d] %d (%f) (%f, %f, %f) move: (%f, %f) \n",
					// it, t,
					// (double) t / 1000.0, x, y, z, x - x0, y - y0);

					// display on the panel...
					ptp.addTrackPoint(i, x, y);
					
					p.updatePosition(x, y, z);

					Cell newcell = mesh.findCell(x, y, cell); // start with previous cell?
					if (newcell == null) {
						System.err.printf("    -> Left domain boundary");
						p.setState(ParticlesState.LOST);
						// leftDomain = true;
						break;
					}
					// System.err.printf(" -> Current cell: %s \n", cell);
					if (z < cell.zc) {
						//System.err.printf("    -> Landed at z: %f in depth: %f \n", z, newcell.zc);
						System.err.printf("    -> Landed at z: %f \n", z);
						p.setState(ParticlesState.ONBED);
						// leftDomain = true;
						break;
					}
					if (cell != newcell)
						System.err.printf("    -> Next cell: %s \n", newcell);
					cell = newcell;
					p.setCurrentCell(cell);
					ptp.addCell(cell);

				} // next pcle
				t += dt;
			} // next t

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
