/**
 * 
 */
package com.apt.test;

import java.awt.BorderLayout;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;

import com.apt.data.Domain;
import com.apt.data.FlowVector;
import com.apt.data.RegularSquareMesh;
import com.apt.data.SeabedType;
import com.apt.data.Spacing;
import com.apt.gui.ParticleTrackPanel;
import com.apt.instrumentation.Instrumentation;
import com.apt.instrumentation.NullInstrumentation;
import com.apt.data.Node;
import com.apt.data.NodeType;
import com.apt.data.Particle;
import com.apt.data.ParticleClass;
import com.apt.data.ParticlesState;
import com.apt.models.IBathymetryModel;
import com.apt.models.IFlowModel;
import com.apt.models.ISeabedModel;
import com.apt.models.ISettlingModel;
import com.apt.models.ITransportModel;
import com.apt.models.ITurbulenceModel;
import com.apt.models.ITidalModel;
import com.apt.models.bathymetry.MeshBathymetry;
import com.apt.models.flow.NOCFlowModel;
import com.apt.models.seabed.SeabedModel;
import com.apt.models.settling.DefaultSettlingModel;
import com.apt.models.tidal.FixedTidalModel;
import com.apt.models.transport.EulerIntegrator;
import com.apt.models.transport.Integrator;
import com.apt.models.transport.TransportModel;
import com.apt.models.turbulence.DefaultTurbulenceModel;

/**
 * 
 */
public class CreateFjordAndTestEngine {

	static double TIDAL_PERIOD = 12.4*3600.0;
	
	static double CONSOLIDATION_TIME = 3*3600.0*1000.0;
	
	IBathymetryModel bathy;

	IFlowModel flow;

	ITransportModel transport;
	
	ParticleTrackPanel pp;
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		CreateFjordAndTestEngine test = new CreateFjordAndTestEngine();
		JFrame f = test.createFrame();
		f.setVisible(true);
		test.runModel();
	}

	/**
	 * 
	 */
	public CreateFjordAndTestEngine() {
		bathy = createBathymetry();
		flow = createFlowmetry();	
		transport = createTransportModel();
	}
	
	private JFrame createFrame() {
		pp = new ParticleTrackPanel(bathy, null);
		JFrame f = new JFrame("Sim001");
		f.getContentPane().setLayout(new BorderLayout());
		f.getContentPane().add(pp, BorderLayout.CENTER);
		f.setBounds(50,50, 1000, 800);
		return f;
		
	}
	
	private void runModel() {
		
		List<Particle> particles = new ArrayList<Particle>();
		List<Particle> alllanded = new ArrayList<Particle>();
		List<Particle> allcon = new ArrayList<Particle>();
		int totallost = 0;
		int totalp = 0;
		int totalcon = 0;
		double t = 0.0; // time secs
		int it = 0;
		while (t < 10.0*86400.0) {
			long time = (long)t*1000L;
			// release some particles at centre every minute
			if (it % 6 == 0) {
				Particle p = createParticle(t);
				particles.add(p);
				totalp++;
			}
			
			// compute flow vector
			FlowVector fv = createFlowVector(t);
			double speed = fv.getMagnitude();
			double angle = fv.getAngle();
			((NOCFlowModel)flow).updateMean(speed, angle);
			
			List<Particle> lost = new ArrayList<Particle>();
			List<Particle> landed = new ArrayList<Particle>();
			// move the particles
			for (Particle p : particles) {
				transport.move(p, (long)(t*1000.0), 10000L);
				double x = p.getX();
				double y = p.getY();
				double z = p.getZ();
				double depth = bathy.getDepth(x, y);
				double sigma = -z/depth;
				//System.err.printf("t: %d [%s] (%f, %f) z: %f  sigma: %f %s\n", it, p.getId(), x, y, z, sigma, p.getState());
				if (p.getState() == ParticlesState.ONBED) {
					//System.err.printf("t: %d [%s] (%f, %f) z: %f dp: %f sigma: %f state: %s ltime: %d\n", 
						//	it, p.getId(), x, y, z, depth, sigma, p.getState(), p.getLandingTime());
					landed.add(p);
					
				}
				if (p.getState() == ParticlesState.LOST) {
					System.err.printf("t: %d [%s] (%f, %f) z: %f state: %s\n", it, p.getId(), x, y, z, p.getState());
					lost.add(p);
				}
			}
//			System.err.printf("t: %d A: np: %d, land: %d allland: %d lost: %d all: %d alllost: %d\n", 
	//				it, particles.size(), landed.size(), alllanded.size(), lost.size(), totalp, totallost);
			
			pp.addParticles(time, particles);
			
			// remove any lost or landed particles
			totallost += lost.size();
			particles.removeAll(lost);
			lost.clear();
			alllanded.addAll(landed);
			particles.removeAll(landed);
			landed.clear();
			
			// check each of our already landed particles and see if they have gone over the consolidation time
			List<Particle> cons = new ArrayList<Particle>();
			for (Particle p : alllanded) {
				double tsl = (double)(time - p.getLandingTime());
				//System.err.printf("  cc: p: [%s], tnow: %d lt: %d tsl: %f \n", p.getId(), time, p.getLandingTime(), tsl);
				if (tsl > CONSOLIDATION_TIME) {
					totalcon++;
					cons.add(p);
				}
			}
			alllanded.removeAll(cons);
			cons.clear();
			
			
			//System.err.printf("t: %d B: np: %d, land: %d allland: %d lost: %d all: %d alllost: %d allcon: %d\n", 
				//	it, particles.size(), landed.size(), alllanded.size(), lost.size(), totalp, totallost, totalcon);
			
			t += 10.0;
			it++;
			
			try {Thread.sleep(5L);} catch (InterruptedException ix) {}
		}
		
		System.err.printf("t: %d B: np: %d, allland: %d all: %d alllost: %d allcon: %d\n", 
				it, particles.size(), alllanded.size(), totalp, totallost, totalcon);
		
		
	}

	private IBathymetryModel createBathymetry() {

		Domain domain = new Domain(0.0, 1000.0, 0.0, 1000.0);
		Spacing spacing = new Spacing(10.0, 10.0);

		List<Node> nodes = new ArrayList<Node>();

		NodeType type;
		int inode = 0;
		for (int j = 0; j < 101; j++) {
			double y = j * 10.0;
			for (int i = 0; i < 101; i++) {
				double x = i * 10.0;

				double z = 0.0;
				if (j == 0 || j == 100) {
					type = NodeType.LAND_BOUNDARY;
					z = 10.0;
				} else if (i == 0 || i == 100) {
					type = NodeType.OPEN_BOUNDARY;
					z = -Math.abs(y - 500.0)*0.1 - 50.0;
				} else {
					type = NodeType.SEA;
					z = -Math.abs(y - 500.0)*0.1 - 50.0;
				}
				Node n = new Node(inode, x, y, z, type);
				System.err.printf("Node: [%d, %d] at: (%.2f,%.2f) : %fm \n", j, i, x, y, z);
				nodes.add(n);
			}
		}

		RegularSquareMesh mesh = new RegularSquareMesh(domain, spacing, nodes);
		
		MeshBathymetry bathy = new MeshBathymetry(mesh);
		return bathy;

	}
	
	private IFlowModel createFlowmetry() {
		
		IFlowModel flow = new NOCFlowModel(50.0, 0.001, 0.5, 0.0, 0.0);
		return flow;
	}
	
	private FlowVector createFlowVector(double t) {
		
		//double meanu = 0.25*Math.cos(2.0*Math.PI*t/TIDAL_PERIOD);
		//double meanv = 0.05*Math.sin(2.0*Math.PI*t/TIDAL_PERIOD);
		
		double meanu = 0.05*Math.cos(2.0*Math.PI*t/TIDAL_PERIOD);
		double meanv = 0.25*Math.sin(2.0*Math.PI*t/TIDAL_PERIOD);
		
		return new FlowVector(meanu, meanv, 0.0);
		
	}
	
	private ITransportModel createTransportModel() {
		ITidalModel tidal = new FixedTidalModel(0.0);
		ITurbulenceModel turbulence = new DefaultTurbulenceModel(0.1, 0.1, 0.01);
		//ISettlingModel settling = new DefaultSettlingModel();
		ISettlingModel settling = new SettlingModel();
		ISeabedModel seabed = new SeabedModel(SeabedType.MUD, 0.02,	0.001, 0.01);
		Integrator integrator = new EulerIntegrator(flow);
		Instrumentation inst = new NullInstrumentation(new File(""));
		TransportModel tm = new TransportModel(bathy, flow, tidal, turbulence, settling, seabed, integrator, inst);
		
		return tm;
	}
	
	private Particle createParticle(double t) {
		
		Particle p = new Particle();
		p.setStartPosition(500.0, 500.0, 0.0);
		if (Math.random() < 0.9)
			p.setType(ParticleClass.FEED);
		else
			p.setType(ParticleClass.FAECES);
		
		p.setCreationTime((long)t);
		p.setState(ParticlesState.SUSPEND);
		
		return p;
		
	}
	
	private class SettlingModel implements ISettlingModel {

		@Override
		public double getSettlingSpeed(Particle p) {
			switch (p.getType()) {
			case FAECES:
				return -0.01;
			case FEED:
				return -0.15;
			case MUD:
				return -0.01;
			default:
				return -0.01;
			}
					
		}
		
	}

}
