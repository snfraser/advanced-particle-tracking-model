/**
 * 
 */
package com.apt.test;

import java.io.File;

import javax.swing.JFileChooser;

import com.apt.readers.BinaryFlowReader;

/**
 * 
 */
public class ReadBinaryFlowFile {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		JFileChooser chooser = new JFileChooser();
		if (chooser.showOpenDialog(null) != JFileChooser.APPROVE_OPTION)
			return;
		
		File flowFile = chooser.getSelectedFile();
		
		try {
		BinaryFlowReader reader = new BinaryFlowReader();
		reader.readFile(null, flowFile);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
