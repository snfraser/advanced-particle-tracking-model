/**
 * 
 */
package com.apt.test;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JPanel;

import com.apt.data.Cell;
import com.apt.data.Domain;
import com.apt.data.FlowProfile;
import com.apt.data.FlowTimeSeries;
import com.apt.data.FlowVector;
import com.apt.data.Mesh;
import com.apt.data.Node;
import com.apt.data.NodeType;
import com.apt.data.Particle;
import com.apt.data.ParticleClass;
import com.apt.data.ParticlesState;
import com.apt.data.RegularSquareMesh;
import com.apt.data.SeabedType;
import com.apt.data.Spacing;
import com.apt.instrumentation.Instrumentation;
import com.apt.instrumentation.TestInstrumentation;
import com.apt.models.IBathymetryModel;
import com.apt.models.IBedTransportModel;
import com.apt.models.IFlowModel;
import com.apt.models.ISeabedModel;
import com.apt.models.ISettlingModel;
import com.apt.models.ITidalModel;
import com.apt.models.ITransportModel;
import com.apt.models.ITurbulenceModel;
import com.apt.models.bathymetry.MeshBathymetry;
import com.apt.models.flow.SinglePointFlowModel;
import com.apt.models.seabed.SeabedModel;
import com.apt.models.settling.DefaultSettlingModel;
import com.apt.models.tidal.FixedTidalModel;
import com.apt.models.transport.BedTransportModel;
import com.apt.models.transport.EulerIntegrator;
import com.apt.models.transport.Integrator;
import com.apt.models.transport.TransportModel;
import com.apt.models.turbulence.DefaultTurbulenceModel;

/**
 * A model setup to search for the "missing mass".
 * 
 */
public class SearchForTheMissingMass {

	private IBathymetryModel bathymetry;
	private IFlowModel flowmetry;

	private ITransportModel transport;

	private IBedTransportModel bedtransport;

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		SearchForTheMissingMass test = new SearchForTheMissingMass();

		ParticleGridPanel panel = test.displayParticlePanel();

		// single particle tracked to landing or lost
		// test.runSingleParticleTrack(panel);

		// multitrack
		test.runMultipleParticleTracks(panel);
	}

	private SearchForTheMissingMass() {

		// BATHYMETRY
		bathymetry = createBathymetry();

		System.err.printf("new bathy: %s \n", bathymetry);
		Mesh mesh = bathymetry.getMesh();
		List<Cell> cells = mesh.listCells();
		for (Cell c : cells) {
			System.err.printf("Cell: %s \n", c);
			Node[] nodes = c.nodes;
			for (Node n : nodes) {
				System.err.printf("  Node: %s \n", n);
			}
		}

		// FLOWMETRY
		flowmetry = createFlowmetry();

		double d = -19.0;
		long t = 1800L; // 30 mins into run
		while (d < 0.0) {
			// System.err.println();
			System.err.printf("Flow depth: %f m, u: %f m/s \n", d, flowmetry.getFlow(0, 0, d, t).getMagnitude());
			d += 3.0;
		}
		System.err.printf(" Boundary flow: height: %f speed: %f m/s \n", flowmetry.getBoundaryFlowHeight(),
				flowmetry.getBoundaryFlow(0, 0, t).getMagnitude());

		// flow times
		double z0 = 1.8 / 1000.0; // mm
		t = 0L;
		long dt = 30 * 60 * 1000L; // half an hour interval
		double VK = 0.41;
		double rho = 1030.0; // seawater density

		while (t < 24 * 3600 * 1000L) {
			double vlow = flowmetry.getFlow(0, 0, -18.0, t).getU();
			double vmid = flowmetry.getFlow(0, 0, -7.0, t).getU();
			double vsurf = flowmetry.getFlow(0, 0, 0.0, t).getU();
			double vbound = flowmetry.getBoundaryFlow(0, 0, t).getMagnitude();

			double vf = VK * vbound / Math.log(flowmetry.getBoundaryFlowHeight() / z0);
			double taub = rho * vf * vf;
			System.err.printf(
					"Flow at: t: %7.1f  bound: % 4.2f low: % 4.2f  mid: % 4.2f  surf: % 4.2f  vf: % 6.4f m/s  taub: %f Pa\n",
					(double) t / 1000.0, vbound, vlow, vmid, vsurf, vf, taub);
			t += dt;
		}

		// SUSPENSION TRANSPORT MECHANISM
		transport = createTransport();

		// BED TRANSPORT MECHANISM
		bedtransport = createBedTransport();

	}

	private IBathymetryModel createBathymetry() {

		Domain domain = new Domain(0, 80, 0, 10); // 40m long by 10m wide single row
		Spacing spacing = new Spacing(10, 10);

		List<Node> nodes = new ArrayList<Node>();
		nodes.add(createNode(1, NodeType.LAND_BOUNDARY, 0, 0, 15));
		nodes.add(createNode(2, NodeType.LAND_BOUNDARY, 10, 0, 15));
		nodes.add(createNode(3, NodeType.LAND_BOUNDARY, 20, 0, 15));
		nodes.add(createNode(4, NodeType.LAND_BOUNDARY, 30, 0, 15));
		nodes.add(createNode(5, NodeType.LAND_BOUNDARY, 40, 0, 13));
		nodes.add(createNode(6, NodeType.LAND_BOUNDARY, 50, 0, 12));
		nodes.add(createNode(7, NodeType.LAND_BOUNDARY, 60, 0, 18));
		nodes.add(createNode(8, NodeType.LAND_BOUNDARY, 70, 0, 18));
		nodes.add(createNode(9, NodeType.LAND_BOUNDARY, 80, 0, 18));

		nodes.add(createNode(10, NodeType.LAND_BOUNDARY, 0, 10, 15));
		nodes.add(createNode(11, NodeType.OPEN_BOUNDARY, 10, 10, -30));
		nodes.add(createNode(12, NodeType.OPEN_BOUNDARY, 20, 10, -40));
		nodes.add(createNode(13, NodeType.OPEN_BOUNDARY, 30, 10, -50));
		nodes.add(createNode(14, NodeType.OPEN_BOUNDARY, 40, 10, -60));
		nodes.add(createNode(15, NodeType.OPEN_BOUNDARY, 50, 10, -60));
		nodes.add(createNode(16, NodeType.OPEN_BOUNDARY, 60, 10, -50));
		nodes.add(createNode(17, NodeType.OPEN_BOUNDARY, 70, 10, -40));
		nodes.add(createNode(18, NodeType.OPEN_BOUNDARY, 80, 10, -30));

		RegularSquareMesh mesh = new RegularSquareMesh(domain, spacing, nodes);
		MeshBathymetry bathymetry = new MeshBathymetry(mesh);
		return bathymetry;
	}

	private Node createNode(int ref, NodeType type, double x, double y, double z) {
		Node n = new Node(ref, x, y);
		n.depth = z;
		n.type = type;
		return n;
	}

	private IFlowModel createFlowmetry() {

		double[] sigmas = new double[] { -0.9, -0.7, -0.5, -0.3, -0.1, 0.0 };

		double[] scales = new double[] { 0.27, 0.38, 0.57, 0.72, 0.85, 1.0 };

		FlowProfile profile = new FlowProfile();

		double step = 900; // timestep 900 seconds
		double period = 12 * 3600; // 12 hour flow period seconds
		double speed = 0.1; // tidal peak speed m/s.

		for (int is = 0; is < 6; is++) {

			double sigma = sigmas[is];

			FlowTimeSeries ts = new FlowTimeSeries(0, 900 * 1000L);
			int ntimes = 20 * 24 * 4; // 4 time steps per hour for 20 days

			double scale = scales[is];

			for (int it = 0; it < ntimes; it++) {
				double t = step * it; // time seconds
				double u = scale * speed * Math.sin(2.0 * Math.PI * t / period);
				//double v = -0.03 * scale; // slow drift south
				double v = 0.25*scale * speed * Math.cos(2.0 * Math.PI * t / period);
				double w = 0.0;
				ts.append(new FlowVector(u, v, w));
			} // next time step

			profile.addFlow(sigma, ts);

		} // next sigma level up

		SinglePointFlowModel spf = new SinglePointFlowModel(profile, 0L, (long) step * 1000L, 6, -20.0);

		return spf;
	}

	private ITransportModel createTransport() {

		ITidalModel tidal = new FixedTidalModel(0); // NO TIDE
		ITurbulenceModel turbulence = new DefaultTurbulenceModel(0, 0, 0); // NO TURBULANCE
		Integrator integrator = new EulerIntegrator(flowmetry);
		ISettlingModel settling = new DefaultSettlingModel();
		ISeabedModel sea = new SeabedModel(SeabedType.MUD, 0.02, 0.0002, 0.0002);
		Instrumentation instruments = new TestInstrumentation(null); // needs a run folder if used

		TransportModel trans = new TransportModel(bathymetry, flowmetry, tidal, turbulence, settling, sea, integrator,
				instruments);

		return trans;

	}

	private IBedTransportModel createBedTransport() {

		ITurbulenceModel noturbulence = new DefaultTurbulenceModel(0, 0, 0); // NO TURBULANCE
		ISeabedModel seabed = new SeabedModel(SeabedType.MUD, 0.02, 0.0002, 0.0002);
		Integrator nointegrator = new EulerIntegrator(flowmetry); // NOT USED
		Instrumentation instruments = new TestInstrumentation(null); // needs a run folder if used

		IBedTransportModel bedtrans = new BedTransportModel(bathymetry, flowmetry, noturbulence, seabed, nointegrator,
				instruments);

		return bedtrans;

	}

	private ParticleGridPanel displayParticlePanel() {

		ParticleGridPanel panel = new ParticleGridPanel();

		// TODO remember to scale the plot for the aspect ratio

		JFrame f = new JFrame("Particle tracks");
		f.getContentPane().setLayout(new BorderLayout());
		f.getContentPane().add(panel, BorderLayout.CENTER);
		f.setBounds(50, 50, 1200, 600);
		f.setVisible(true);

		return panel;

	}

	private void runSingleParticleTrack(ParticleGridPanel panel) {

		// TRACK a single particle
		long delta = 1000L; // stepping
		Particle p = new Particle();
		p.setId2("P:100");
		p.setStartPosition(25.0, 5.0, 0.0);
		p.setType(ParticleClass.FAECES);
		p.setCreationTime(0L);
		p.setDensity(1700.0);
		p.setDiameter(6.0 / 1000.0);

		long t0 = 30600 * 1000L; // start at 2 hours where there is some flow
		// long t0 = 9000 * 1000L;
		long t = t0;
		while (t < t0 + 1 * 3600 * 1000L) {
			if (p.getState() == ParticlesState.SUSPEND) {
				transport.move(p, t, delta);
			} else if (p.getState() == ParticlesState.ONBED) {
				bedtransport.move(p, t, delta);
			}
			Cell cell = p.getCurrentCell();
			String cellref = cell != null ? "" + cell.ref : "NONE";
			System.err.printf("Particle: t: %6.0f %8.8s (% 4.2f % 4.2f % 4.2f) cell: %s\n", (double) t / 1000.0,
					p.getState().name(), p.getX(), p.getY(), p.getZ(), cellref);
			t += delta;
			if (p.getState() == ParticlesState.LOST)
				break;

			panel.particlePosition(p);

			try {
				Thread.sleep(300L);
			} catch (InterruptedException ix) {
			}
		}

	}

	private void runMultipleParticleTracks(ParticleGridPanel panel) {

		List<Particle> plist = new ArrayList<Particle>();

		int ncell = bathymetry.listCells().size();
		int[] count = new int[ncell];

		// count consolidation in non-cells??
		int nomatch = 0;

		long delta = 1000L; // stepping

		int np = 0;

		long t0 = 30600 * 1000L; // start at 2 hours where there is some flow
		int it = 0;
		// long t0 = 9000 * 1000L;
		long t = t0;
		long MINS = 1 * 60 * 1000L;
		while (t < t0 + 12 * 3600 * 1000L) {
			it++;
			// create a new particle every ...
			if (it % 10 == 0) {
				np++;
				System.err.printf("Create particle %d at time: %f \n", np, (double) t / 1000.0);
				Particle p = new Particle();
				p.setId2("P:" + np);
				p.setStartPosition(Math.random() * 2.5 + 38, Math.random() * 2.5 + 3.0, 0.0);
				p.setType(ParticleClass.FAECES);
				p.setCreationTime(0L);
				p.setDensity(1700.0);
				p.setDiameter(6.0 / 1000.0);
				plist.add(p);
			}

			List<Particle> conlist = new ArrayList<Particle>();

			for (Particle p : plist) {

				if (p.getState() == ParticlesState.SUSPEND) {
					transport.move(p, t, delta);
				} else if (p.getState() == ParticlesState.ONBED) {
					bedtransport.move(p, t, delta);
					if (p.getTimeSinceLanded(t) > 15 * 60 * 1000L) {
						p.setState(ParticlesState.CONSOLIDATED);
					}

				}

				Cell cell = p.getCurrentCell();

				if (p.getState() == ParticlesState.CONSOLIDATED) {
					// count this in the cell
					int cellref = cell != null ? cell.ref - 1 : -1;
					if (cellref >= 0) {
						count[cellref]++;
						conlist.add(p);
					} else
						nomatch++;
				}

				String cellref = cell != null ? "" + cell.ref : "NONE";
				// System.err.printf("Particle: t: %6.0f %8.8s (% 4.2f % 4.2f % 4.2f) cell:
				// %s\n", (double) t / 1000.0,
				// p.getState().name(), p.getX(), p.getY(), p.getZ(), cellref);

				if (p.getState() == ParticlesState.LOST)
					continue;

				panel.particlePosition(p);

			} // next particle in list

			//System.err.printf("Conlist contains: %d pcls plist has: %d\n", conlist.size(), plist.size());
			plist.removeAll(conlist);
			panel.timenow(t);

			//if (it % 120 == 0)
				panel.repaint();

			
			 try { Thread.sleep(1L); } catch (InterruptedException ix) { }
			 
			t += delta;

		} // next timestep

		int nc = 0;
		int nl = 0;
		int ns = 0;
		int nb = 0;
		for (Particle p : plist) {
			if (p.getState() == ParticlesState.CONSOLIDATED)
				nc++;
			else if (p.getState() == ParticlesState.LOST)
				nl++;
			else if (p.getState() == ParticlesState.SUSPEND)
				ns++;
			else if (p.getState() == ParticlesState.ONBED)
				nb++;
		}

		int received = nc+nl+nb+ns;
		System.err.printf("Total particles created: %d \n", np);
		System.err.printf("Final counts: Consolidated: %d, Lost: %d Susp: %d Bed: %d\n", nc, nl, ns, nb);

		for (int i = 0; i < ncell; i++) {
			System.err.printf(" cell: %d  count: %d \n", i, count[i]);
			received += count[i];
		}
		System.err.printf("Particles retained or lost: %d pcles, nomatch cell: %d\n", received, nomatch);
		System.err.println();
		System.err.flush();
	}

	private class ParticleGridPanel extends JPanel {

		final Font TFONT = new Font("Serif", Font.PLAIN, 18);

		long mytime = 0L;

		double x0;
		double y0;
		double wx;
		double wy;

		int nx;
		int ny;
		double[][] cells;

		double lowestdepth = 999;

		List<Particle> particles;

		/**
		 * 
		 */
		public ParticleGridPanel() {

			wx = bathymetry.getDomainWidth();
			wy = bathymetry.getDomainHeight();
			x0 = bathymetry.getDomainX0();
			y0 = bathymetry.getDomainY0();

			nx = (int) (wx/1.0);// 1m squares
			ny = (int) (wy/1.0);// 1m squares

			cells = new double[ny][nx];

			for (int iy = 0; iy < ny; iy++) {

				for (int ix = 0; ix < nx; ix++) {
					double depth = bathymetry.getDepth(ix, iy);
					cells[iy][ix] = depth;
					if (depth < lowestdepth)
						lowestdepth = depth; // -ve depth
				}

			}
			lowestdepth = -lowestdepth; // make +ve

			particles = new ArrayList<Particle>();

		}

		@Override
		public void paint(Graphics g) {
			super.paint(g);

			int dx = getScreenX(1) - getScreenX(0);
			int dy = getScreenY(1) - getScreenY(0);

			for (int iy = 0; iy < ny; iy++) {

				for (int ix = 0; ix < nx; ix++) {

					double depth = cells[iy][ix];

					Color c = Color.white;

					if (depth > 0.0) {
						// land
						c = Color.yellow.darker();
					} else {
						depth = -depth;
						float count = (float) Math.abs(depth);
						float r = 1.0f - (float) count / (float) lowestdepth;
						if (r < 0.0f)
							r = 0.0f;

						c = new Color(0.95f * r, 0.95f * r, 1.0f);
					}
					
					int sx = getScreenX(ix);
					int sy = getScreenY(iy);

					g.setColor(c);
					g.fillRect(sx, sy, dx, dy);

				}
			}

			try {
				for (Particle p : particles) {

					int sx = getScreenX(p.getX());
					int sy = getScreenY(p.getY());

					ParticlesState state = p.getState();

					// System.err.printf("PP:draw particle: %s (%d, %d) state: %s \n",p.getId2(),
					// sx, sy, state.name());

					Color pc = Color.black;

					switch (state) {
					case SUSPEND:
						pc = Color.yellow;
						break;
					case ONBED:
						pc = Color.magenta;
						break;
					case LOST:
						pc = Color.red;
						break;
					case CONSOLIDATED:
						pc = Color.green;
					}

					g.setColor(pc);
					g.fillOval(sx, sy, 6, 6);

				}
			} catch (Exception e) {
			}

			g.setColor(Color.white);
			g.setFont(TFONT);
			g.drawString(String.format("%tH%tM", mytime, mytime), 500, 40);

		}

		public void particlePosition(Particle ap) {

			if (!particles.contains(ap)) {
				particles.add(ap);
			}

			int index = particles.indexOf(ap);
			Particle p = particles.get(index);

			p.updatePosition(ap.getX(), ap.getY(), ap.getZ());

			// repaint();
		}

		public void timenow(long time) {
			mytime = time;
		}

		private int getScreenX(double x) {

			int ww = getSize().width;
			int sx = (int) ((x - x0) * ww / wx);
			return sx;
		}

		private int getScreenY(double y) {

			int hh = getSize().height;
			int sy = hh - (int) ((y - y0) * hh / wy);
			return sy;

		}

	}

}
