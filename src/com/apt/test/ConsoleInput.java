/**
 * 
 */
package com.apt.test;

import java.io.BufferedReader;
import java.io.Console;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;

import org.json.JSONObject;

/**
 * Test input from a console.
 * 
 * @author SA05SF
 *
 */
public class ConsoleInput {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		final String CONTENT_URLFORM = "application/x-www-form-urlencoded";
		final String CONTENT_JSON = "application/json";
		final String NDM_USER_AGENT = "NewDepomod-MigClient-0.1";
		final String LOGIN_URL = "https://www.depomod-analytics.net/aptm/login.php";

		try {

			File aptmFolder = new File(System.getProperty("user.home"), ".aptm");
			File licenseFile = new File(aptmFolder, "license");
			File sessionFile = new File(aptmFolder, "session");

			Console con = System.console();

			String user = con.readLine("Please enter username: ");
			char[] chpasswd = con.readPassword("Password: ");
			String passwd = new String(chpasswd);

			// con.format("You entered user: %s with passwd: %s \n", user, new
			// String(chpasswd));

			// pull the license id from the license file...$HOME/.depomod/LICENSE or
			// $HOME/.aptm/license
			String license = null;
			try (BufferedReader bin = new BufferedReader(new FileReader(licenseFile))) {
				license = bin.readLine();
				con.format("License: %s \n", license);
			} catch (Exception e) {
				// handle missing license eg prompt for it
				con.format("error reading license: \n", e.getMessage());
				e.printStackTrace();
				license = con.readLine("License: ");
			}

			// TODO: should have valid user,pass,license values here - check em!

			// HttpResponse<JsonNode> reply = Unirest.post(LOGIN_URL).field("username",
			// user).field("passwd", passwd)
			// .field("license", license).asJson();

			HttpClient httpClient = HttpClient.newBuilder().version(HttpClient.Version.HTTP_2)
					.connectTimeout(Duration.ofSeconds(60)).build();

			String payload = String.format("username=%s&passwd=%s", user, passwd);

			HttpResponse<String> res = null;
			HttpRequest request = HttpRequest.newBuilder().POST(HttpRequest.BodyPublishers.ofString(payload))
					.uri(URI.create(LOGIN_URL)).setHeader("User-Agent", NDM_USER_AGENT) // add request header
					.header("Content-Type", CONTENT_URLFORM).header("Accept", CONTENT_JSON)
					.header("User-Agent", NDM_USER_AGENT).build();

			res = httpClient.send(request, HttpResponse.BodyHandlers.ofString());

			int status = res.statusCode();

			con.format("Status: %d\n", status);

			String body = res.body();
			JSONObject ob = new JSONObject(body);

			con.format("Message: \n %s \n", ob.toString(4));

			JSONObject auth = ob.getJSONObject("authentication");

			String alias = auth.getString("userAlias");
			int level = auth.getInt("authLevel");
			String token = auth.getString("token");

			con.format("UserAlias: %s\n", alias);
			con.format("AuthLevel: %d\n", level);
			con.format("SessionID: %s\n", token);

			// write session data
			try (PrintWriter pout = new PrintWriter(new FileWriter(sessionFile))) {
				pout.printf("%s\n", token);
			} catch (Exception e) {
				e.printStackTrace();
			}

			con.format("Session tokan has been saved\n");

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
