/**
 * 
 */
package com.apt.test;

import java.util.ArrayList;
import java.util.List;

import com.apt.data.ChemicalComponent;
import com.apt.data.ChemicalComponentType;
import com.apt.data.Particle;
import com.apt.data.ParticleClass;
import com.apt.data.ParticlesState;
import com.apt.models.disruption.ExponentialDisruptionModel;

/**
 * Test the disruption (break-up) of a set of particles using a specified
 * break-up model.
 * 
 * @author SA05SF
 *
 */
public class DisruptSomeParticles {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		List<Particle> particles = new ArrayList<Particle>();

		double feedWasteCarbonFraction = 0.03;
		double feedWasteChemicalfraction = 0.2;

		ExponentialDisruptionModel x = new ExponentialDisruptionModel(0.9, 4.0);

		int np = 100; // particles to start
		double wasteMass = 10.0; // kg

		// create a load of feed particles
		for (int i = 0; i < np; i++) {
			Particle p = new Particle();
			p.setStartPosition(1000.0, 1000.0, 0.0);
			p.setState(ParticlesState.SUSPEND);
			p.setType(ParticleClass.FEED);
			p.setCreationTime(0);

			double wasteSolidsMass = wasteMass / 50;
			p.addChemical(new ChemicalComponent(ChemicalComponentType.SOLIDS, wasteSolidsMass));

			double feedWasteCarbonMass = wasteSolidsMass * feedWasteCarbonFraction;
			p.addChemical(new ChemicalComponent(ChemicalComponentType.CARBON_REFACTORY, feedWasteCarbonMass));

			double feedWasteChemicalMass = wasteSolidsMass * feedWasteChemicalfraction;
			p.addChemical(new ChemicalComponent(ChemicalComponentType.TREATMENT_EMBZ, feedWasteChemicalMass));

			particles.add(p);

		}

		// set them off.

		long t = 0L;
		long dt = 1000L;

		double dz = 0.1 * (double) dt / 1000.0; // distance [m] of descent per sec
		System.err.printf("Descent in dt: %f sec = %f m \n", (double) dt / 1000, dz);
		System.err.printf("Starting with: %d particles \n", np);

		while (np > 0) {

			System.err.printf("time: %f tracking: %d particles \n", (double) t / 1000.0, np);

			List<Particle> xp = new ArrayList<Particle>();
			List<Particle> delp = new ArrayList<Particle>();
			List<Particle> addp = new ArrayList<Particle>();

			// go through current particles
			for (int k = 0; k < np; k++) {

				// move and check for disruption of particle
				Particle p = particles.get(k);
				double z = p.getZ();
				p.setZ(z + dz); // drops 0.1m every 1 secs
				// System.err.printf("Pcl: %d at depth: %f age: %f", k, p.getZ(),
				// (double)(p.getTimeSinceCreation(t))/1000.0);
				if (p.getZ() >= 20.0) {
					p.setState(ParticlesState.ONBED);
					delp.add(p);
					// System.err.println(" onbed add to dlist");
				} else {
					List<Particle> newp = x.degrade(p, t);

					if (!newp.contains(p)) {
						// new particles will be added
						// System.err.printf(" degraded to: %d pcles\n", newp.size());
						addp.addAll(newp);
						delp.add(p);
					} else {
						// System.err.println(" remains intact");
					}
				}

			}

			// add particles.
			// System.err.printf("Adding: %d and deleting: %d \n", addp.size(),
			// delp.size());
			particles.removeAll(delp);
			particles.addAll(addp);

			t += dt; // dt seconds

			np = particles.size();

			/*
			 * try { Thread.sleep(100L); } catch (Exception e) { }
			 */
		}

		System.err.printf("Total particles tracked: %d \n", x.getMapSize());
		
	}

}
