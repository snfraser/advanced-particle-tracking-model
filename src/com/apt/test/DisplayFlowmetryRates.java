package com.apt.test;

import java.awt.BorderLayout;

import javax.swing.JFrame;

import com.apt.data.FlowVector;
import com.apt.gui.FlowRatePanel;
import com.apt.models.flow.FixedFlowModel;

public class DisplayFlowmetryRates {

	public static void main(String[] args) {
	
		FixedFlowModel fm = new FixedFlowModel();
		
		FlowRatePanel panel = new FlowRatePanel();
		
		JFrame f = new JFrame("Flow rate");
		f.getContentPane().setLayout(new BorderLayout());
		f.getContentPane().add(panel.createChartPanel(), BorderLayout.CENTER);
		f.pack();
		f.setSize(800, 400);
		f.setLocation(200, 600);
		f.setVisible(true);
		
		long time = 0L;
		long YEAR = 360*86400*1000L;
		long MONTH = 30*86400*1000L;
		long MINUTES15 = 15*60*1000L;
		while (time < MONTH) {
			
			double x = 100.0;
			double y = 100.0;
			double z = 10.0;
			FlowVector flow = fm.getFlow(x, y, z, time);
			double uu = flow.getU();
			double vv = flow.getV();
			double rate = Math.sqrt(uu*uu+vv*vv);
			panel.updateFlowRate(time, rate);
			
			time += MINUTES15;
		}
		
		
	}

}
