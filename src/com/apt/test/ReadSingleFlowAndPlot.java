/**
 * 
 */
package com.apt.test;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JFileChooser;
import javax.swing.JFrame;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.ValueMarker;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.ui.RectangleAnchor;
import org.jfree.chart.ui.RectangleInsets;
import org.jfree.data.time.Second;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;

import com.apt.data.FlowVector;
import com.apt.models.IFlowModel;
import com.apt.readers.SinglePointFlowmetryReader;

/**
 * @author SA05SF
 *
 */
public class ReadSingleFlowAndPlot {

	private IFlowModel flow;
	
	/**
	 * 
	 */
	public ReadSingleFlowAndPlot(IFlowModel flow) {
		this.flow = flow;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
	
		JFileChooser chooser = new JFileChooser();
		if (chooser.showOpenDialog(null) != JFileChooser.APPROVE_OPTION)
			return;

		File flowFile = chooser.getSelectedFile();

		SinglePointFlowmetryReader rdr = new SinglePointFlowmetryReader();

		IFlowModel flow = null;
		try {
			flow = rdr.readFile(flowFile);
			System.err.printf("Read in flow data: %s \n", flow);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		ReadSingleFlowAndPlot test = new ReadSingleFlowAndPlot(flow);
		
		test.plot(0L, 50*86400*1000L);// plot 50 days of data

	}
	
	private void plot(long start, long end) {
		
		XYDataset dataset;

		TimeSeries series1 = new TimeSeries("sigma -0.1");
		TimeSeries series2 = new TimeSeries("sigma -0.5");
		TimeSeries series3 = new TimeSeries("sigma -0.95");
		
		TimeSeriesCollection tsc = new TimeSeriesCollection();
		tsc.addSeries(series1);
		tsc.addSeries(series2);
		tsc.addSeries(series3);
		
		JFreeChart chart = ChartFactory.createTimeSeriesChart(
				"Flow rate", "time [days]", "flow [m/s]", tsc, true, false, false);
		
	
		//chart.getLegend().setVisible(false);
		XYPlot plot = (XYPlot) chart.getPlot();
		SimpleDateFormat sdf = new SimpleDateFormat("D:m");
		DateAxis domainAxis = (DateAxis) plot.getDomainAxis();
		domainAxis.setDateFormatOverride(sdf);

		domainAxis.setLabelFont(new Font("Tahoma", Font.PLAIN, 12));
		domainAxis.setTickLabelFont(new Font("Tahoma", Font.PLAIN, 10));

		NumberAxis range = (NumberAxis) plot.getRangeAxis();
		// range.setRange(0.0, 550.0);
		range.setLabelFont(new Font("Tahoma", Font.PLAIN, 12));
		range.setTickLabelFont(new Font("Tahoma", Font.PLAIN, 10));
		ChartPanel chartPanel = new ChartPanel(chart);
		chartPanel.setPreferredSize(new Dimension(500, 250));
		
		JFrame f = new JFrame("NDart Flow rate");
		f.getContentPane().setLayout(new BorderLayout());
		f.getContentPane().add(chartPanel, BorderLayout.CENTER);
		f.setBounds(50, 50, 1600, 1000);
		f.setVisible(true);
		
		// now add some data
		long t = start;
		while (t < end) {
			Date date = new Date(t);
			
			// calc the flow at the half depth
			FlowVector fv1 = flow.getFlow(0.0, 0.0, -0.1, t);
			double rate1 = fv1.getMagnitude();
			series1.addOrUpdate(new Second(date), rate1);
			
			FlowVector fv2 = flow.getFlow(0.0, 0.0, -0.5, t+5*60*1000L);
			double rate2 = fv2.getMagnitude();
			series2.addOrUpdate(new Second(date), rate2);
			
			FlowVector fv3 = flow.getFlow(0.0, 0.0, -0.95, t+10*60*1000L);
			double rate3 = fv3.getMagnitude();
			series3.addOrUpdate(new Second(date), rate3);
			
			t += 3*60*60*1000L; // 60 minutes
		}
		
		addMarker(plot, 0.0);
		addMarker(plot, 15.0*86400*1000.0);
		addMarker(plot, 30.0*86400*1000.0);
		addMarker(plot, 45.0*86400*1000.0);
	}
	
	private void addMarker(XYPlot plot, double value) {
		
		ValueMarker marker = new ValueMarker(value, Color.black, new BasicStroke(2.0f));  // position is the value on the axis
		double days = value/(86400.0*1000.0);
		marker.setLabel(String.format("%.1f days", days));
		
		//marker.setLabelOffset(new RectangleInsets(-1, -1, 1, 1));
		//marker.setLabel("here"); // see JavaDoc for labels, colors, strokes
		plot.addDomainMarker(marker);
	}
	
	

}
