/**
 * 
 */
package com.apt.models;

import java.util.List;

/**
 * @author SA05SF
 *
 */
public interface IBedLayerModel {

	public int getNumberLayers();
	
	public IBedLayer getLayer(int n);
	
	public List<IBedLayer> getLayers();
	
	public void addTopLayer(IBedLayer layer);
	
	public void addBaseLayer(IBedLayer layer);
	
	public IBedLayer getSurfaceLayer();
	
	public IBedLayer getBasement();
	
	public void compress();
	
	public boolean decompress();

}
