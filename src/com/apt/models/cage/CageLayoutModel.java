package com.apt.models.cage;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.apt.models.Cage;
import com.apt.models.ICageLayoutModel;

public class CageLayoutModel implements ICageLayoutModel {
	
	private String layoutName;
	//private List<Cage> cages;
	private ICagePositionManager positionManager;


	/**
	 * Create an empty CageLayoutModel 
	 */
	public CageLayoutModel(String layoutName) {
		this.layoutName = layoutName;
		//cages = new ArrayList<Cage>();
	}

	

	/**
	 * @return the positionManager
	 */
	public ICagePositionManager getPositionManager() {
		return positionManager;
	}



	/**
	 * @param positionManager the positionManager to set
	 */
	public void setPositionManager(ICagePositionManager positionManager) {
		this.positionManager = positionManager;
	}



	@Override
	public String getLayoutName() {
		// TODO Auto-generated method stub
		return layoutName;
	}


	/**
	 * @return An iterator over the list of cages.
	 * @see java.util.List#iterator()
	 */
	@Override
	public List<Cage> cages() {
		// TODO: make a copy probably....
		return positionManager.getCages();
	}

	/**
	 * Add a cage.
	 * @param cage The cage to add.
	 * @return True if it worked.
	 */
	//public boolean addCage(Cage cage) {
		//return cages.add(cage);
	//}
	
	/**
	 * Add a list of cages.
	 * @param cages The cages to add.
	 * @return True if it worked.
	 */
	//public boolean addCages(List<Cage> cages) {
		//return this.cages.addAll(cages);
	//}


	@Override
	public String toString() {
		return String.format("CageLayoutModel [layoutName=%s, cages=%d]", layoutName, positionManager.getCages().size());
	}

	

}
