/**
 * 
 */
package com.apt.models.cage;

import java.util.List;

import com.apt.data.PositionVector;
import com.apt.models.Cage;

/**
 * @author SA05SF
 *
 */
public interface ICagePositionManager {
	
	/**
	* Find the position of a cage identified by its unique ID.
	 * @param cageID A unique cage identifier.
	 * @return The position (x,y) of the specified cage.
	 */
	public PositionVector getPosition(String cageID);
	
	/**
	 * fetches a list of all the cages in this PositionSpec.
	 * @return A list of all the cages in this PositionSpec.
	 */
	public List<Cage> getCages();

}
