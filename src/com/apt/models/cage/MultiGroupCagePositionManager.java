/**
 * 
 */
package com.apt.models.cage;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.apt.data.PositionVector;
import com.apt.models.Cage;

/**
 * A PositionSpec which includes multiple groups of cages.
 * 
 * @author SA05SF
 *
 */
public class MultiGroupCagePositionManager implements ICagePositionManager {

	/** Maps a cage ID to the specific layout group containing it.*/
	private Map<String, ICagePositionManager> map;
	private List<ICagePositionManager> groups;
	
	/**
	 * 
	 */
	public MultiGroupCagePositionManager() {
		map = new HashMap<String, ICagePositionManager>();
		groups= new ArrayList<ICagePositionManager>();
	}

	public void addGroup(ICagePositionManager group) {
		//extract all mapping in the group and map them ourselves
		List<Cage> cages = group.getCages();
		for (Cage c : cages) {
			map.put(c.getName(), group);
		}
		groups.add(group);
	}
	
	@Override
	public List<Cage> getCages() {
		// return all cages for any included groups
		List<Cage> allcages = new ArrayList<>();
		for (ICagePositionManager m : groups) {
			for (Cage c : m.getCages()) {
				allcages.add(c);
			}
		}
		return allcages;
	}
	
	@Override
	public PositionVector getPosition(String cageID) {
		// find its group then get its position
		ICagePositionManager group = map.get(cageID);
		PositionVector position = group.getPosition(cageID);
		return position;
	}

}
