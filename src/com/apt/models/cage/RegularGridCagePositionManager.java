/**
 * 
 */
package com.apt.models.cage;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.apt.data.GridPosition;
import com.apt.data.PositionVector;
import com.apt.models.Cage;

/**
 * A PositionSpec using a regular grid aligned at an angle to North.
 * 
 * @author SA05SF
 *
 */
public class RegularGridCagePositionManager implements ICagePositionManager {

	/** Mapping from cage ID to grid-position (i,j).*/
	private Map<String, GridPosition> map;
	
	/** List of cages.*/
	private List<Cage> cages;
	
	/** X coordinate of the corner (i=0, j=0) cage centre [m].*/
	private double xCorner;
	
	/** Y coordinate of the corner (i=0, j=0) cage centre [m].*/
	private double yCorner;
	
	/** Spacing between cages along the I axis [m].*/
	private double iSpacing;
	
	/** Spacing between cages along the J axis [m].*/
	private double jSpacing;
	
	/** Bearing of the J axis clockwise from North.*/
	private double bearing;
	
	
	
	/**
	 * Create a RegularGridPositionSpec with supplied spatial arrangement.
	 * @param xCorner
	 * @param yCorner
	 * @param iSpacing
	 * @param jSpacing
	 * @param bearing
	 */
	public RegularGridCagePositionManager(double xCorner, double yCorner, double iSpacing, double jSpacing, double bearing) {
		super();
		this.xCorner = xCorner;
		this.yCorner = yCorner;
		this.iSpacing = iSpacing;
		this.jSpacing = jSpacing;
		this.bearing = bearing;
		
		map = new HashMap<String, GridPosition>();
		cages = new ArrayList<Cage>();
	}
	
	@Override
	public List<Cage> getCages() {
		// a copy of the list - NOTE: same references
		return new ArrayList<Cage>(cages);
	}

	/**
	 * Add a cage at the specified grid position.
	 * @param cage The cage to add.
	 * @param i I (column) offset in the grid.
	 * @param j J (row) offset in the grid.
	 */
	public void add(Cage cage, int i, int j) {
		map.put(cage.getName(), new GridPosition(i, j));
		cages.add(cage);
	}

	@Override
	public PositionVector getPosition(String cageID) {
		GridPosition grid = map.get(cageID);
		PositionVector position = convertToXY(grid.getI(), grid.getJ());
		return position;
	}
	
	public GridPosition getGridPosition(String cageID) {
		return map.get(cageID);
	}

	
	private PositionVector convertToXY(int i, int j) {
		double cosb = Math.cos(Math.toRadians(bearing));
		double sinb = Math.sin(Math.toRadians(bearing));
		double xp = i*iSpacing;
		double yp = j*jSpacing;
		double x = xCorner + xp*cosb + yp*sinb;
		double y = yCorner - xp*sinb + yp*cosb;
		return new PositionVector(x, y, 0);
	}

	/**
	 * @return the xCorner
	 */
	public double getxCorner() {
		return xCorner;
	}

	/**
	 * @return the yCorner
	 */
	public double getyCorner() {
		return yCorner;
	}

	/**
	 * @return the iSpacing
	 */
	public double getiSpacing() {
		return iSpacing;
	}

	/**
	 * @return the jSpacing
	 */
	public double getjSpacing() {
		return jSpacing;
	}

	/**
	 * @return the bearing
	 */
	public double getBearing() {
		return bearing;
	}

	
	
}
