/**
 * 
 */
package com.apt.models.cage;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.apt.data.GridPosition;
import com.apt.data.PositionVector;
import com.apt.models.Cage;

/**
 * @author SA05SF
 *
 */
public class FreeFormCagePositionManager implements ICagePositionManager {

	/** Mapping from cage ID to position (x,y).*/
	private Map<String, PositionVector> map;
	
	/** List of cages.*/
	private List<Cage> cages;
	
	
	/**
	 * 
	 */
	public FreeFormCagePositionManager() {
		map = new HashMap<String, PositionVector>();
		cages = new ArrayList<Cage>();
	}

	/** 
	 * Add a cage at a specified position.
	 * @param cage The cage to add.
	 * @param position Position (x,y,z) of the cage centre [m].
	 */
	public void addCage(Cage cage, PositionVector position) {
		map.put(cage.getName(), position);
		System.err.printf("FFCPM: add cage: %s at (%f,%f) \n", cage.getName(), position.getX(), position.getY());
		cages.add(cage);
	}
	
	@Override
	public PositionVector getPosition(String cageID) {
		return map.get(cageID);
	}

	@Override
	public List<Cage> getCages() {
		// a copy of the list - NOTE: same references
		//return new ArrayList<Cage>(cages);
		return cages;
	}

}
