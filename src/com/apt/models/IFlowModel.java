package com.apt.models;

import com.apt.data.Cell;
import com.apt.data.FlowProfile;
import com.apt.data.FlowVector;

public interface IFlowModel {

	
	/**
	 * Calculates the flow at the specified location and time.
	 * @param x
	 * @param y
	 * @param z
	 * @param time
	 * @return the flow at the specified location and time. A NullFlow if there is no actual flow there
	 */
	public FlowVector getFlow(double x, double y, double z, long time);
	
	public double getBoundaryFlowHeight();
	
	public FlowVector getBoundaryFlow(double x, double y, long time);

}
