/**
 * 
 */
package com.apt.models;

import com.apt.data.Particle;
import com.apt.data.Seabed;

/**
 * 
 */
public interface IConsolidationModel {
	
	/**
	 * Determines if a specified particle is consolidated at specified time after landing on the seabed.
	 * 
	 * @param particle The particle.
	 * @param seabed The seabed characteristics.
	 * @param time Current time.
	 * @return True if the particle is consolidated into the seabed.
	 */
	public boolean consolidated(Particle particle, Seabed seabed, long time);

}
