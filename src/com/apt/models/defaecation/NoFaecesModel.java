/**
 * 
 */
package com.apt.models.defaecation;

import java.util.ArrayList;
import java.util.List;

import com.apt.data.CompoundMass;
import com.apt.data.Particle;
import com.apt.models.Cage;
import com.apt.models.IDefacationModel;
import com.apt.models.cage.ICagePositionManager;

/**
 * 
 */
public class NoFaecesModel  implements IDefacationModel {

	private List<Particle> none = new ArrayList<Particle>();
	
	@Override
	public double getFaecesRate(double feedMass, double timeSinceLastFeed) {
		return 0;
	}

	@Override
	public CompoundMass createFaecesMass(CompoundMass release, double timeSinceFeed, double dt) {
		return null;
	}

	@Override
	public List<Particle> generateFaecesParticles(Cage cage, ICagePositionManager pm, CompoundMass faecesMass) {
		return none;
	}

}
