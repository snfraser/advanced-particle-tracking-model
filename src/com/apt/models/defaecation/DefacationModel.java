package com.apt.models.defaecation;

import java.util.ArrayList;
import java.util.List;

import com.apt.models.Cage;
import com.apt.data.ChemicalComponent;
import com.apt.data.ChemicalComponentType;
import com.apt.data.CompoundMass;
import com.apt.data.Particle;
import com.apt.data.ParticleClass;
import com.apt.data.PositionVector;
import com.apt.models.IDefacationModel;
import com.apt.models.cage.ICagePositionManager;

/**
 * Works out how much fish dung is generated.
 * 
 * @author SA05SF
 *
 */
public class DefacationModel implements IDefacationModel {

	public static double totalPooMass = 0.0;
	
	static double FAECES_DIAMETER = 3/1000.0; // m
	static double FAECES_DENSITY = 1080.0; // kg/m3
	
	
	/** Typical mass of a single faeces particle (kg).*/
	//static final double WEIGHT_OF_FAECES = 0.0000486;
	
	/** Peak faeces generation time after feeding (h). */
	double tmax = 0.5;

	/** Peak defacation rate per unit biomass per second. 4g per day for 1 kg fish*/
	//double fmax = 0.002/86400.0;

	/** Decay time constant, time after peak defacation (h). */
	double q = 2.0;
	
	/** Specific Feeding Rate (SFR) [kg feed / kg biomass / day].*/
	double sfr = 0.007; // kg feed per kg biomass
	 
	/** Excreted Feed Solids Mass (EFSM) per kg feed. */
	double efsm = 0.132; // 
	
	/** Number of faeces particles per release - the total faeces mass is split between these.*/
	private int numberParticlesPerRelease = 1;
	
	private volatile int countParticlesCreated = 0;

	/**
	 * Calculates the faces generation rate per unit biomass at a time since last feed.
	 * 
	 * To get the actual faeces released during a time period use:
	 * 
	 * this value * time period (s) * biomass (kg)
	 * 
	 * 0.000926835 kg poo / kg stock per day
	 * 
	 * @param timeSinceLastFeed Time since last feed (ms).
	 * @return The rate of faeces generation (kg/sec/per kg biomass).
	 */
	@Override
	public double getFaecesRate(double feedMass, double timeSinceLastFeed) {

		double hrSinceLastFeed = timeSinceLastFeed / 3600000.0;
		double minSinceLastFeed = timeSinceLastFeed / 60000.0;
		
		//double feedMass = feed.getTotalMass(); // this is beta*N
		//System.err.printf("Def: feed load: %f kg at %f mins ago \n", feedMass, minSinceLastFeed);
		double fmax = efsm*feedMass/(q + tmax /2.0); // maximum rate per hour
		
		
		// feed per day per kg = sfr * efsm
		
		/*double exFeedPerDayPerKg = sfr*efsm; // kg/kg biomass
		System.err.println("Def: exfd per day per kg = " + exFeedPerDayPerKg);
		double exFeedPerHourPerKg = exFeedPerDayPerKg /24.0;
		System.err.println("Def: exfd per hour per kg = " + exFeedPerHourPerKg);
		
		double fmax = exFeedPerHourPerKg /(q + tmax/2);*/
		
		//System.err.println("Def: rate: fmax (kg/hr) = " + fmax);
		
		if (hrSinceLastFeed < tmax)
			return fmax * (double) hrSinceLastFeed / tmax;

		return fmax * Math.exp(-(hrSinceLastFeed - tmax) / q);

	}
	
	/**
	 * Create a faces release a time after feeding.
	 * 
	 * @param release       The actual feed mass applied.
	 * @param timeSinceFeed Time since feed mass was applied.
	 * @param dt            The time-step over which to calculate the release.
	 * @return
	 */
	@Override
	public CompoundMass createFaecesMass(CompoundMass release, double timeSinceFeed, double dt) {
		
		// temporary constants
		double defacFraction = 0.25; // total fraction of feed converted to poo
		double defacFoldingTime = 6.0 * 3600.0 * 1000.0; // e-folding time for decay is 6 hours

		// faeces mass is a fraction of the total released feed,

		double totalReleaseMass = defacFraction * release.getComponentMass(ChemicalComponentType.SOLIDS);

		// this is expelled over a period of 6 hours exponentially decreasing.
		// during any period after feed release, release rate is:
		// defacFraction*exp(-t(ms)/defacFoldingTime)
		// actual release in a time slot dt is: dt(ms)*releaseRate(kg/ms)
		// peak release at t=0: area under curve is: total = peak*foldingTime

		double peakReleaseRate = totalReleaseMass / defacFoldingTime;
		double releaseRateTime = peakReleaseRate * Math.exp(-timeSinceFeed / defacFoldingTime);

		double faecesMass = releaseRateTime * dt;
		//System.err.printf("Faeces released this step: %f kg \n", faecesMass);
		
		CompoundMass faeces = new CompoundMass();
		faeces.addComponent(new ChemicalComponent(ChemicalComponentType.SOLIDS, faecesMass));
		return faeces;
		
		
	}
	
	/**
	 * Create a release of faeces for a specified cage using the supplied total faeces mass.
	 * @param cage The cage to release from.
	 * @param faecesMass The total faeces compund mass.
	 * @return A list of faeces particles representing the release.
	 */
	@Override
	public List<Particle> generateFaecesParticles(Cage cage, ICagePositionManager pm, CompoundMass faecesMass) {
		List<Particle> particles = new ArrayList<Particle>();

		for (int i = 0; i < numberParticlesPerRelease; i++) {
			countParticlesCreated++;
			Particle p = new Particle();
			p.setId2(String.format("F%s:%d", cage.getName(), countParticlesCreated));
			PositionVector pv = pm.getPosition(cage.getName());
			double x = pv.getX();
			double y = pv.getY();
			p.setStartPosition(x + (Math.random() - 0.5) * cage.getWidth(),
					y + (Math.random() - 0.5) * cage.getLength(),
					-cage.getDepth() + (Math.random() - 0.5) * 0.5 * cage.getHeight());
			//System.err.printf("DM::new faec particle [%s] at: (%f, %f, %f) \n", p.getId2(), p.getX(), p.getY(), p.getZ());
			p.setType(ParticleClass.FAECES);
			// add some chemical components to the particles compounds, split by 3 for 3 pcles
			double faecesMassSolids = faecesMass.getComponentMass(ChemicalComponentType.SOLIDS)/numberParticlesPerRelease;
			p.addChemical(new ChemicalComponent(ChemicalComponentType.SOLIDS, faecesMassSolids));
			p.addChemical(new ChemicalComponent(ChemicalComponentType.CARBON_REFACTORY, 0.0005 + Math.random() * 0.0002));
			p.addChemical(new ChemicalComponent(ChemicalComponentType.CARBON_LABILE, 0.0005 + Math.random() * 0.0002));
			p.addChemical(new ChemicalComponent(ChemicalComponentType.TREATMENT_EMBZ, 0.002 + Math.random() * 0.0001));
			//p.setFeedMass(0.5/1000.0); // 0.5 grams
			p.setTransportMass(faecesMassSolids); // !! prob not needed
			
			p.setDensity(FAECES_DENSITY * (1.0+(Math.random()-0.5)/10.0));
			p.setDiameter(FAECES_DIAMETER * (1.0+(Math.random()-0.5)/10.0));
			particles.add(p);
			
			totalPooMass += faecesMassSolids;
		}

		// System.err.printf("PM:: at %d s, releasing %d \n", time/1000, 10);
		return particles;
		
	}

}
