/**
 * 
 */
package com.apt.models;

/**
 * @author SA05SF
 *
 */
public interface ISeabedTemperatureModel {

	/** Temperature of seabed at specfied time.*/
	public double getTemperature(double time);
	
}
