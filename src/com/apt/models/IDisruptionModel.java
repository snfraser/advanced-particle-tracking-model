/**
 * 
 */
package com.apt.models;

import java.util.List;

import com.apt.data.Particle;

/**
 * Models the break-up of particles.
 * 
 * @author SA05SF
 *
 */
public interface IDisruptionModel {

	/**
	 * Degrade (potentially break-up) a specific particle at the specified time.
	 * @param p The particle to potentially break-up.
	 * @param time The time.
	 * @return A list of particles derived from the original particle (may also be just the original particle).
	 */
	public List<Particle> degrade(Particle p, long time);
	
	/**
	 * Degrade (potentially break-up) all/any of the particles at the specified time.
	 * @param particles The list of particles to potentially break-up.
	 * @param time The time.
	 * @return A list of particles derived from the original particles (may also be just (some of) the original particles).
	 */
	public List<Particle> degrade(List<Particle> particles, long time);
	
}
