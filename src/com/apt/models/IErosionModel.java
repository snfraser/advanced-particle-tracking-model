/**
 * 
 */
package com.apt.models;

/**
 * @author SA05SF
 *
 */
public interface IErosionModel {
	
	/**
	 * Calauclate the erosion rate for the specified bed stress and critical bed-stress values.
	 * @param taub Actual bed stress at the instant. [Pa]
	 * @param tauc Critical bed stress at the instant [Pa].
	 * @return The erosion mass rate [kg/m2/s]
	 */
	public double getErosionRate(double taub, double tauc);

}
