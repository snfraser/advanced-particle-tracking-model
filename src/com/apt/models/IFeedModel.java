package com.apt.models;

import java.util.List;

import com.apt.models.Cage;
import com.apt.models.cage.ICagePositionManager;
import com.apt.data.CompoundMass;
import com.apt.data.Particle;

public interface IFeedModel {

	CompoundMass releaseFeed(Cage cage, long time);

	List<Particle> generateWasteFeedParticles(Cage cage, ICagePositionManager pm, CompoundMass release);

}