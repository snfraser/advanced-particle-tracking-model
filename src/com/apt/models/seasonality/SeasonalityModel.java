package com.apt.models.seasonality;

import com.apt.data.Season;
import com.apt.models.ISeasonalityModel;

/** Determines the season: 0.0 = mid-winter, 0.5 = mid-summer, 1.0 = next mid-winter.*/
public class SeasonalityModel implements ISeasonalityModel {

	/** Milliseconds in a day.*/
	public static final double MS_IN_DAY = 86400.0*1000.0;

	/** Milliseconds in a year.*/
	public static final double MS_IN_YEAR = 365.25*86400.0*1000.0;
	
	/** Start date. The number of days since mid-winter. 
	 * If startDate=0 then the simulation starts at mid-winter.
	 * If startDate=180 the simulation begins roughly mid-summer.
	 */
	private double startDate;
	
	/** Start time. The start of the model as day fraction.
	 */
	private double startTime;
	
	
	/**
	 * Create a SeasonalityModel with the supplied start date (days from start).
	 * The day supplied is the number of days since mid-winter.
	 * e.g. startDate=10 then mid-winter is at simulation time -10*86400s.
	 * e.g. startDate=30 simulation begins 30 days after mid-winter (about 20th January).
	 * @param startDate The number of days since mid-winter. 
	 * @param startTime Start time of model (day fraction).
	 */
	public SeasonalityModel(double startDate, double startTime) {
		this.startDate = startDate;
		this.startTime = startTime;
	}

/**
 * Calculate the season fraction from the supplied time (elapsed time in simulation).
 * @param time Elapsed time (ms since start of model).
 * @return A number range [0.0:1.0] 0.0=mid-winter, 0.5=mid-summer, 1.0=next mid-winter.
 */
	@Override
	public double getSeasonFraction(double time) {
		double years = time/MS_IN_YEAR + startDate;
		return years - Math.floor(years); // frac part only
	}
	
	/**
	 * Calculate and return the Season for a given simulation time (since start).
	 * @param time The time since start of simulation (ms).
	 * @return The season corresponding to this time.
	 */
	@Override
	public Season getSeason(double time) {
		double season = getSeasonFraction(time);
	    if (0.0 < season && season <= 0.125)
			return Season.WINTER;
		else if (0.125 < season && season <= 0.375)
			return Season.SPRING;
		else if (0.375 < season && season <= 0.625)
			return Season.SUMMER;
		else if (0.625 < season && season <= 0.875)
			return Season.AUTUMN;
		else if (0.875 < season && season <= 1.0 )
			return Season.WINTER;
		else return Season.UNKNOWN;
	}

	@Override
	public double getDayFraction(double time) {
		double days = (time/MS_IN_DAY + startTime); // number of days 
		return days - Math.floor(days); // frac part only
	}
}
