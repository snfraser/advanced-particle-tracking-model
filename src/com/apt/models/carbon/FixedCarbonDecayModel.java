/**
 * 
 */
package com.apt.models.carbon;

import com.apt.data.CarbonFraction;
import com.apt.models.ICarbonDecayModel;

/**
 * @author SA05SF
 *
 */
public class FixedCarbonDecayModel implements ICarbonDecayModel {
	
	// refactory rate is slow 60 days [1/day]
	static final double K = 1.0/(20.0);
	
	CarbonFraction initialMass;
	
	/**
	 * @param initialMass
	 */
	public FixedCarbonDecayModel(CarbonFraction initialMass) {
		this.initialMass = initialMass;
	}

	@Override
	public CarbonFraction getCarbonFractions(double time) {
		// mass after time is just Mo.exp(-kt)
		double ml0 = initialMass.getLabileMass();
		double mr0 = initialMass.getRefractoryMass();
		double timed = time/(1000.0*86400.0);
		double massL = ml0*Math.exp(-K*timed);
		double massR = mr0*Math.exp(-K*timed);
		return new CarbonFraction(massL, massR);
	}

}
