/**
 * 
 */
package com.apt.models.carbon;

import com.apt.data.CarbonFraction;
import com.apt.models.ICarbonDecayModel;
import com.apt.models.ISeabedTemperatureModel;

/**
 * @author SA05SF
 *
 */
public class DefaultCarbonDecayModel implements ICarbonDecayModel {

	static final double HOUR = 3600.0*1000;
	static final double DAY = 86400.0*1000;
	static final double YEAR = 365.0*86400.0*1000;
	
	static final double TR = 20.0 + 273.0; // ref temp [K]
	
	// refactory rate is slow 60 days [1/day]
	static final double KR = 1.0/(100.0);
	
	// labile rate is fast 10 days [1/day]
	static final double KL = 1.0/(20.0);
	
	static final double Q10 = 2.0;

	
	CarbonFraction initialMass;
	
	ISeabedTemperatureModel sbt;
	
	/**
	 * @param initialMass
	 */
	public DefaultCarbonDecayModel(ISeabedTemperatureModel sbt, CarbonFraction initialMass) {
		this.sbt = sbt;
		this.initialMass = initialMass;
	}



	@Override
	public CarbonFraction getCarbonFractions(double time) {
		
		// calculate cfracs after time using supplied temperature model
		double ml = initialMass.getLabileMass();
		double mr = initialMass.getRefractoryMass();
		double m0 = ml + mr;
		double a = ml/m0;
		
		
		// integrate temperature model to get T0
		double t0sumd = 0.0;
		double timed = time/DAY; // time in days
		double td = 0.0; // days
		double dtd = 1.0/24.0; // 1 hour interval in days for temp model
		while (td < timed) {
			double t = td*DAY;
			double tempi = sbt.getTemperature(t) + 273.0; // temp in kelvin
			t0sumd += Math.pow(Q10, (1.0 - TR/10.0)*(1.0 - TR/tempi))*dtd; // days
			td += dtd; // ms
		}
		//System.err.printf("T: %f  t0: %f \n", td, t0sumd);
		double massL = a*m0*Math.exp(-KL*t0sumd);
		double massR = (1-a)*m0*Math.exp(-KR*t0sumd) ;
		return new CarbonFraction(massL, massR);
		
	}
	
	
	
}
