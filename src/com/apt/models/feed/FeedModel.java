/**
 * 
 */
package com.apt.models.feed;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.apt.models.Cage;
import com.apt.data.ChemicalComponent;
import com.apt.data.ChemicalComponentType;
import com.apt.data.CompoundMass;
import com.apt.data.Particle;
import com.apt.data.ParticleClass;
import com.apt.data.PositionVector;
import com.apt.models.IFeedModel;
import com.apt.models.cage.ICagePositionManager;
import com.apt.models.stock.StockModel;

/**
 * 
 * Calculate amount of food to release based on stock and various params.
 * Longer term may want to include seasonality and tod into this model.
 * @author SA05SF
 *
 */
public class FeedModel implements IFeedModel {

	public static double totalFeedMass = 0.0;
	
	static double FEED_DIAMETER = 9.3/1000.0; // m
	static double FEED_DENSITY = 1180.0; // kg/m3
	
	/** Time of last feed.*/
	private long timeLastFeed;
	
	/** Feeding interval (ms).*/
	private long feedInterval;
	
	/** Number of waste particles per release - the total waste feed mass is split between these.*/
	private int numberParticlesPerRelease = 1;
	
	/*
	 * Model: Fish use 10% of mass per day to maintain energy per day. Want to
	 * increase biomass by 100% in 180 days. per hour need: biomass*0.1 +
	 * biomass*1.0/180.0/24.0
	 */
	//private double alpha = 0.1;
	//private double days100Pc = 180.0;
	private double particleMass = 0.5 / 1000.0; // 0.5g per particle
	private double wasteFraction = 0.03; // waste mass fraction
	private double feedCarbonFraction = 0.0;
	double feedChemicalFraction = 0.0;
	private double feedWasteCarbonFraction = 0.0005;
	private double feedWasteChemicalfraction = 0.002;
	//private double absorbedFraction = 0.5; // food fraction absorbed for growth.

	private double sfr = 0.007; // 7kg/tonne per day = 0.007 kg/kg or 0.7%
	
	// feed = biomass*(alpha/24 + 1/(24*nd))
	
	// feed release is biomass*sfr per day so multiply by feedIntervalHours/24

	private volatile int countParticlesCreated = 0;
	
	/**
	 * Create a FeedModel.
	 * 
	 * @param feedInterval The feeding interval (ms).
	 * @param numberParticlesPerRelease
	 * @param particleMass True mass of an individual particle.
	 * @param wasteFraction Fraction of feed wasted.
	 * @param feedWasteCarbonFraction fraction of waste which is carbon.
	 * @param feedWasteChemicalfraction fraction of waste which is treatment chemical.
	 * @param sfr
	 */
	public FeedModel(long feedInterval, int numberParticlesPerRelease, double particleMass, double wasteFraction,
			double feedWasteCarbonFraction, double feedWasteChemicalfraction, double sfr) {
		this.feedInterval = feedInterval;
		this.numberParticlesPerRelease = numberParticlesPerRelease;
		this.particleMass = particleMass;
		this.wasteFraction = wasteFraction;
		this.feedWasteCarbonFraction = feedWasteCarbonFraction;
		this.feedWasteChemicalfraction = feedWasteChemicalfraction;
		this.sfr = sfr;
	}



	@Override
	public CompoundMass releaseFeed(Cage cage, long time) {
		StockModel stock = cage.getStock();
		// work out the mass and hence the release amount if valid feed slot
		//System.err.printf("check release for cage: %f, %f: at %d \n", cage.getX(), cage.getY(), time);
		if ((time - timeLastFeed) % feedInterval == 0) {
		
			double biomass = stock.getBiomass();
			//double feedMass = biomass * (alpha / 24.0 + 1.0 / (24.0 * days100Pc));
			double feedMass = sfr*biomass*feedInterval/86400000;
			//System.err.printf("FM::Feed release for cage: %s biomass: %f kg at %d feed: %f kg\n", cage.getName(), biomass, time/1000, feedMass);
			CompoundMass release = createRelease(feedMass);
			timeLastFeed = time;
			return release;
		}
		return null;// no release of feed this time.
	}
	
	@Override
	public List<Particle> generateWasteFeedParticles(Cage cage, ICagePositionManager pm, CompoundMass release) {

		// at certain intervals release some new feed particles

		// typical particle: 2.5g: 0.5g carbon, 2g mixed

		ChemicalComponent solids = release.getComponent(ChemicalComponentType.SOLIDS);
		if (solids == null)
			return null;
		
		double totalMass = solids.getMass();
		double wasteMass = totalMass * wasteFraction;
	
		// actual number of particles that would be released
		int npcl = (int) (wasteMass / particleMass);
		//System.err.printf("Waste mass: %f kg, npcl: %d \n", wasteMass, npcl);

		List<Particle> particles = new ArrayList<Particle>();

		for (int i = 0; i < numberParticlesPerRelease; i++) {
			countParticlesCreated++;
			Particle p = new Particle();
			p.setId2(String.format("W%s:%d", cage.getName(), countParticlesCreated));
			PositionVector pv = pm.getPosition(cage.getName());
			double x = pv.getX();
			double y = pv.getY();
			p.setStartPosition(x + (Math.random() - 0.5) * cage.getWidth(),
					y + (Math.random() - 0.5) * cage.getLength(),
					-cage.getDepth() + (Math.random() - 0.5) * 0.5 * cage.getHeight());
			//System.err.printf("FM::new feed particle [%s] at: (%f, %f, %f) \n", p.getId2(), p.getX(), p.getY(), p.getZ());
			p.setType(ParticleClass.FEED);
			double wasteSolidsMass = wasteMass/numberParticlesPerRelease;
			p.addChemical(new ChemicalComponent(ChemicalComponentType.SOLIDS, wasteSolidsMass));
		
			double feedWasteCarbonMass = wasteSolidsMass*feedWasteCarbonFraction;
			p.addChemical(new ChemicalComponent(ChemicalComponentType.CARBON_REFACTORY, feedWasteCarbonMass));
			
			double feedWasteChemicalMass = wasteSolidsMass*feedWasteChemicalfraction;
			p.addChemical(new ChemicalComponent(ChemicalComponentType.TREATMENT_EMBZ, feedWasteChemicalMass));
			//p.setFeedMass(particleMass); // this is the real mass of the particle
			p.setTransportMass(wasteSolidsMass); // this is the repreesneted mass transported per particle
			
			p.setDiameter(FEED_DIAMETER * (1.0+(Math.random()-0.5)/10.0));
			p.setDensity(FEED_DENSITY *(1.0+(Math.random()-0.5)/10.0));
			
			particles.add(p);
			
			totalFeedMass += wasteSolidsMass;
		}

		// System.err.printf("PM:: at %d s, releasing %d \n", time/1000, 10);
		return particles;
	}
	
	private CompoundMass createRelease(double feedMass) {
		CompoundMass mass = new CompoundMass();
		mass.addComponent(new ChemicalComponent(ChemicalComponentType.SOLIDS, feedMass));
		return mass;
	}

}
