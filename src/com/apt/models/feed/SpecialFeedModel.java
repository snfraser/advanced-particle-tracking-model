/**
 * 
 */
package com.apt.models.feed;

import java.util.ArrayList;
import java.util.List;

import com.apt.data.ChemicalComponent;
import com.apt.data.ChemicalComponentType;
import com.apt.data.CompoundMass;
import com.apt.data.Particle;
import com.apt.data.ParticleClass;
import com.apt.data.PositionVector;
import com.apt.models.Cage;
import com.apt.models.IFeedModel;
import com.apt.models.cage.ICagePositionManager;

/**
 * 
 */
public class SpecialFeedModel implements IFeedModel {

	static double FEED_DIAMETER = 9.3 / 1000.0; // m
	static double FEED_DENSITY = 1180.0; // kg/m3

	private long timeLastFeed;

	long interval;

	long endRelease;

	double releaseMass;

	/**
	 * @param interval
	 * @param endRelease
	 */
	public SpecialFeedModel(long interval, long endRelease, double releaseMass) {
		this.interval = interval;
		this.endRelease = endRelease;
		this.releaseMass = releaseMass;
	}

	@Override
	public CompoundMass releaseFeed(Cage cage, long time) {
		if (time > endRelease)
			return null;
		
		if ((time - timeLastFeed) % interval == 0) {
			CompoundMass mass = new CompoundMass();
			mass.addComponent(new ChemicalComponent(ChemicalComponentType.SOLIDS, releaseMass));
			timeLastFeed = time;
			System.err.printf("SpFdModel::release:: releasing: %f kg at: %f \n", releaseMass, (double)time/1000.0);
			return mass;
		}
		return null;
	}

	@Override
	public List<Particle> generateWasteFeedParticles(Cage cage, ICagePositionManager pm, CompoundMass release) {

		List<Particle> particles = new ArrayList<Particle>();

		for (int i = 0; i < 10; i++) {
			Particle p = new Particle();
			PositionVector pv = pm.getPosition(cage.getName());
			double x = pv.getX();
			double y = pv.getY();
			p.setStartPosition(x, y, -cage.getDepth());
			// System.err.printf("FM::new feed particle at: (%f, %f, %f) \n", p.getX(),
			// p.getY(), p.getZ());
			p.setType(ParticleClass.FEED);
			double wasteSolidsMass = releaseMass / 10.0;
			p.addChemical(new ChemicalComponent(ChemicalComponentType.SOLIDS, wasteSolidsMass));
			p.setDiameter(FEED_DIAMETER * (1.0 + (Math.random() - 0.5) / 10.0));
			p.setDensity(FEED_DENSITY * (1.0 + (Math.random() - 0.5) / 10.0));

			particles.add(p);
		}

		return particles;
	}

}
