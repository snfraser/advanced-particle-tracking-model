package com.apt.models;

import java.util.List;

import com.apt.data.Cell;
import com.apt.data.Mesh;
import com.apt.data.Particle;


public interface IBathymetryModel {
	
	public static final double LAND_DEPTH = 10.0;
	
	public List<Cell> listCells();
	
	public String getBCN();
	
	public Mesh getMesh();
	
	/**
	 * Determines the depth at a specified point.
	 * @param a
	 * @param y
	 * @param cell
	 * @return
	 */
	public double getDepth(double x, double y, Cell cell);
	
	/**
	 * Determines depth at a specified point.
	 * @param x
	 * @param y
	 * @return
	 */
	public double getDepth(double x, double y);
	
	/**
	 * Determines the cell containing the point at (x,y).
	 * @param x X coordinates of point.
	 * @param y Y coordinate of point.
	 * @return
	 */
	public Cell getCell(double x, double y);
	
	/**
	 * Checks whether a point is within the domain bounds.
	 * @param x
	 * @param y
	 * @return
	 */
	public boolean isOutsideDomain(double x, double y);

	/**
	 * Checks whether a point is on land.
	 * @param x
	 * @param y
	 * @return
	 */
	public boolean isLand(double x, double y);
	
	public double getDomainWidth();
	
	public double getDomainHeight();
	
	public double getDomainX0();
	
	public double getDomainY0();
}