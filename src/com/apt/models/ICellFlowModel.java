/**
 * 
 */
package com.apt.models;

import com.apt.data.Cell;
import com.apt.data.FlowVector;

/**
 * 
 */
public interface ICellFlowModel {

	/**
	 * 
	 * @param cell
	 * @param z
	 * @param time
	 * @return
	 */
	public FlowVector getFlow(Cell cell, double z, long time);
	
}
