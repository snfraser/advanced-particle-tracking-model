/**
 * 
 */
package com.apt.models.growth;

import com.apt.cli.util.ConfigurationProperties;
import com.apt.data.ChemicalComponentType;
import com.apt.data.CompoundMass;
import com.apt.models.IGrowthModel;
import com.apt.models.stock.StockModel;

/**
 * @author SA05SF
 *
 */
public class FixedBiomassGrowthModel implements IGrowthModel {

	private ConfigurationProperties config;
	
	/**
	 * @param config
	 */
	public FixedBiomassGrowthModel(ConfigurationProperties config) {
		this.config = config;
	}

	@Override
	public void applyFeed(StockModel stock, CompoundMass feed, long time) {
		// No growth so leave all as is.
		
		double feedMass = feed.getComponentMass(ChemicalComponentType.SOLIDS);
		double stockMass = stock.getAverageFishMass();
		int nstock = stock.getStockNumber();
		double feedPerStock = feedMass / stock.getStockNumber();
		//System.err.printf("Growth:(Fix) apply: feedmass: %f kg, to: ns: %d avg mass: %f kg  rate/fish: %f kg \n", feedMass, nstock, stockMass, feedPerStock);
		// mass available for growth per fish
        double gm = 0.87*feedPerStock; 
		// if negative the fish loose weight !
	
		double newstockmass = stock.getAverageFishMass();
		//System.err.printf("Growth:(Fix) after feeding: apply growthmass: %f,  Stock av: %f -> %f \n", gm, stockMass, newstockmass);
		
		
	}

}
