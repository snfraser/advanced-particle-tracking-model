/**
 * 
 */
package com.apt.models.growth;

import com.apt.cli.util.ConfigurationProperties;
import com.apt.models.IGrowthModel;

/**
 * 
 */
public class GrowthModelFactory {

	public static IGrowthModel createGrowthModel(ConfigurationProperties config) {
		return new FixedBiomassGrowthModel(config);
	}
	
}
