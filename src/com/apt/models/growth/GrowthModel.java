package com.apt.models.growth;

import com.apt.data.ChemicalComponentType;
import com.apt.data.CompoundMass;
import com.apt.models.IGrowthModel;
import com.apt.models.stock.StockModel;

public class GrowthModel implements IGrowthModel {

	//StockModel stock;
	
	//beta is the decay time in the amount of absorbed feed which goes for growth. [80 days]
	double beta = 80.0*86400.0*1000.0;;
	
	
	
	/**
	 * @param beta
	 */
	public GrowthModel() {
	
	}



	/**
	 * Apply feed to the stock.
	 * @param feed The total feed available for all stock
	 */
	@Override
	public void applyFeed(StockModel stock, CompoundMass feed, long time) {
		double feedMass = feed.getComponentMass(ChemicalComponentType.SOLIDS);
		double stockMass = stock.getAverageFishMass();
		int nstock = stock.getStockNumber();
		double feedPerStock = feedMass / stock.getStockNumber();
		//System.err.printf("Growth: apply: feedmass: %f kg, to: ns: %d avg mass: %f kg  rate/fish: %f kg \n", feedMass, nstock, stockMass, feedPerStock);
		// mass available for growth per fish
        double gm = 0.87*feedPerStock; 
		// if negative the fish loose weight !
        
        //double deltaMass = gm;
        // account for age of fish and tail off in quantity used for growth
        double deltaMass = gm*Math.exp(-time/beta);
		
		stock.setAverageFishMass(stockMass + deltaMass); 
		double newstockmass = stock.getAverageFishMass();
		//System.err.printf("growth: after feeding: apply growthmass: %f,  Stock av: %f -> %f \n", gm, stockMass, newstockmass);
	}
	
}
