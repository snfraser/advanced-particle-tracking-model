/**
 * 
 */
package com.apt.models;

import com.apt.data.Particle;

/**
 * @author SA05SF
 *
 */
public interface IBedTransportModel {
	
	public void move(Particle p, long time, long dt);

}
