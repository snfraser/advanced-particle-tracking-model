/**
 * 
 */
package com.apt.models;

import com.apt.data.Particle;

/**
 * 
 */
public interface ISettlingModel {
	
	/**
	 * Get the settling speed for a particle of specified class.
	 * TODO may add extra parameters like temp, water density etc
	 * @param p A particle.
	 * @return Settling speed for this particle.
	 */
	public double getSettlingSpeed(Particle p);

}
