package com.apt.models;

import com.apt.data.CageEnvironment;
import com.apt.data.CageType;
import com.apt.models.defaecation.DefacationModel;
import com.apt.models.feed.FeedModel;
import com.apt.models.stock.StockModel;

public class Cage {

	private String name;
	
	private double x;
	
	private double y;
	
	private double width;
	
	private double length;
	
	private double height;
	
	private double depth;
	
	private double angle;
	
	private CageType type;
	
	private StockModel stock;
	
	private IFeedModel feed;

	private CageEnvironment env;
	
	/**
	 * @param name
	 * @param x
	 * @param y
	 * @param width
	 * @param length
	 * @param height
	 * @param depth
	 * @param angle
	 * @param stock
	 */
	public Cage(String name, double x, double y, double width, double length, double height, double depth, double angle,
			StockModel stock, IFeedModel feed) {
		this.name = name;
		this.x = x;
		this.y = y;
		this.width = width;
		this.length = length;
		this.height = height;
		this.depth = depth;
		this.angle = angle;
		this.stock = stock;
		this.feed = feed;
		env = new CageEnvironment();
	}

	
	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}



	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}



	/**
	 * @return the x
	 */
	public double getX() {
		return x;
	}

	/**
	 * @param x the x to set
	 */
	public void setX(double x) {
		this.x = x;
	}

	/**
	 * @return the y
	 */
	public double getY() {
		return y;
	}

	/**
	 * @param y the y to set
	 */
	public void setY(double y) {
		this.y = y;
	}

	/**
	 * @return the width
	 */
	public double getWidth() {
		return width;
	}

	/**
	 * @param width the width to set
	 */
	public void setWidth(double width) {
		this.width = width;
	}

	/**
	 * @return the length
	 */
	public double getLength() {
		return length;
	}

	/**
	 * @param length the length to set
	 */
	public void setLength(double length) {
		this.length = length;
	}

	/**
	 * @return the height
	 */
	public double getHeight() {
		return height;
	}

	/**
	 * @param height the height to set
	 */
	public void setHeight(double height) {
		this.height = height;
	}

	/**
	 * @return the depth
	 */
	public double getDepth() {
		return depth;
	}

	/**
	 * @param depth the depth to set
	 */
	public void setDepth(double depth) {
		this.depth = depth;
	}

	/**
	 * @return the angle
	 */
	public double getAngle() {
		return angle;
	}

	/**
	 * @param angle the angle to set
	 */
	public void setAngle(double angle) {
		this.angle = angle;
	}
	
	

	/**
	 * @return the type
	 */
	public CageType getType() {
		return type;
	}



	/**
	 * @param type the type to set
	 */
	public void setType(CageType type) {
		this.type = type;
	}



	/**
	 * @return the stock
	 */
	public StockModel getStock() {
		return stock;
	}

	/**
	 * @param stock the stock to set
	 */
	public void setStock(StockModel stock) {
		this.stock = stock;
	}

	/**
	 * @return the feed
	 */
	public IFeedModel getFeed() {
		return feed;
	}

	/**
	 * @param feed the feed to set
	 */
	public void setFeed(FeedModel feed) {
		this.feed = feed;
	}

	/**
	 * @return the env
	 */
	public CageEnvironment getEnv() {
		return env;
	}
	
	
}
