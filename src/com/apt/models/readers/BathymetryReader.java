/**
 * 
 */
package com.apt.models.readers;

import java.io.File;

import com.apt.data.Domain;
import com.apt.models.IBathymetryModel;
import com.apt.models.bathymetry.FixedBathymetryModel;


/**
 * @author SA05SF
 *
 */
public class BathymetryReader {

	/** 
	 * Load the bathymetry model from a mesh file.
	 * 
	 * @param meshFile
	 * @throws Exception
	 */
	public IBathymetryModel load(File meshFile) throws Exception {
		Domain domain = new Domain(0, 2000, 0, 2000);
	
		// for now just create a bathymetry
		FixedBathymetryModel bm = new FixedBathymetryModel(domain);
		return bm;
	}
	
}
