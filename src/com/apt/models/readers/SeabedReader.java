package com.apt.models.readers;

import java.io.File;

import com.apt.data.SeabedType;
import com.apt.models.ISeabedModel;
import com.apt.models.seabed.SeabedModel;


public class SeabedReader {
	/**
	 * Load the flowmetry modelfrom a mesh file.
	 * 
	 * @param meshFile
	 * @return
	 * @throws Exception
	 */
	public ISeabedModel load(File meshFile) throws Exception {
		ISeabedModel seabed  = new SeabedModel(SeabedType.MUD, 0.02, 5.0/1000.0, 0.0002);
		return seabed;
	}
}
