/**
 * 
 */
package com.apt.models.readers;

import java.io.File;

import com.apt.data.Domain;
import com.apt.models.IBathymetryModel;
import com.apt.models.IFlowModel;
import com.apt.models.bathymetry.FixedBathymetryModel;
import com.apt.models.flow.FixedFlowModel;

/**
 * @author SA05SF
 *
 */
public class FlowmetryReader {

	/**
	 * Load the flowmetry modelfrom a mesh file.
	 * 
	 * @param meshFile
	 * @return
	 * @throws Exception
	 */
	public IFlowModel load(File meshFile) throws Exception {
		IFlowModel flowmetry  = new FixedFlowModel();
		return flowmetry;
	}
	
}
