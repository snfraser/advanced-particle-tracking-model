/**
 * 
 */
package com.apt.models.readers;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.locationtech.jts.geom.Coordinate;

import com.apt.data.Cell;
import com.apt.data.Domain;
import com.apt.data.Seabed;
import com.apt.data.SeabedType;
import com.apt.data.VoronoiMesh;
import com.apt.models.ISeabedModel;
import com.apt.models.seabed.MultipointSeabedModel;


/**
 * 
 */
public class MultipointSeabedModelReader {

	/**
	 * Read and parse a mesh consisting a number of locations
	 * 
	 * @param meshFile
	 * @throws Exception
	 */
	public ISeabedModel readFile(File meshFile) throws Exception {

		VoronoiMesh mesh;
		
		List<Coordinate> coords = new ArrayList<Coordinate>();
		//List<Seabed> data = new ArrayList<Seabed>();

		Map<Coordinate, Seabed> samplemap = new HashMap<Coordinate, Seabed>();
		
		Map<Integer, Seabed> cellmap = new HashMap<Integer, Seabed>();
		
		try (FileReader fr = new FileReader(meshFile); BufferedReader br = new BufferedReader(fr)) {

			double xmin = 9999.99;
			double xmax = -9999.99;
			double ymin = 9999.99;
			double ymax = -9999.99;
			
			int dataLineCount = 0;
			String line = null;
			while ((line = br.readLine()) != null) {

				// skip comments
				if (line.startsWith("#"))
					continue;

				// skip empty lines
				if (line.isEmpty())
					continue;

				// extract metadata:
				if (line.startsWith("xmin:")) {
					String part = line.substring(5).trim();
					xmin = Double.parseDouble(part);
					System.err.printf("Parsing regular grid: xmin: %f\n", xmin);
				} else if (line.startsWith("xmax:")) {
					String part = line.substring(5).trim();
					xmax = Double.parseDouble(part);
				} else if (line.startsWith("ymin:")) {
					String part = line.substring(5).trim();
					ymin = Double.parseDouble(part);
					System.err.printf("Parsing regular grid: ymin: %f\n", xmin);
				} else if (line.startsWith("ymax:")) {
					String part = line.substring(5).trim();
					ymax = Double.parseDouble(part);
				} else {
					// a line of data... x,y,z
					dataLineCount++; // line count starting 1
					String[] parts = line.split(","); // check there are 3 items
					if (parts.length < 3)
						throw new Exception(String.format("Line: %d : contains wrong number of items: %d :: %s",
								dataLineCount, parts.length, line));

					double x = Double.parseDouble(parts[0].trim());
					double y = Double.parseDouble(parts[1].trim());
					double surfaceRougness = Double.parseDouble(parts[2].trim());
					double grainSize = Double.parseDouble(parts[3].trim());
					double tausub = Double.parseDouble(parts[4].trim());
					SeabedType type = SeabedType.valueOf(parts[5].trim().toUpperCase());
					
					Coordinate c = new Coordinate(x, y);
					coords.add(c);
					
					Seabed bed = new Seabed(type, tausub, surfaceRougness, grainSize);
					samplemap.put(c, bed);
					
					System.err.printf("Sample point: at: (%f, %f) :: %s \n", x, y, bed);
				}
			}
			
			Domain domain = new Domain(xmin, xmax, ymin, ymax);
			mesh = new VoronoiMesh(domain, coords); // note we cant see the original sample points after here
			
			// go through the original sample points and seabed values and locate the correct mesh cell red
			for (Coordinate c : coords) {
				Seabed seabed = samplemap.get(c);
				Cell cell = mesh.findCell(c.x, c.y, null);
				cellmap.put(cell.ref, seabed);
			}
		
			// we have a v-mesh but dont have cell refs yet, so we need to add a mapping from coord to seabed character
			MultipointSeabedModel msb = new MultipointSeabedModel(mesh, cellmap);
			
			return msb;
		}
		
	}

	
	
}
