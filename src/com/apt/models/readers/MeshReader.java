/**
 * 
 */
package com.apt.models.readers;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;

import com.apt.data.Domain;
import com.apt.data.Mesh;
import com.apt.data.Node;
import com.apt.data.RegularSquareMesh;
import com.apt.readers.RegularGridBathymetryReader;

/**
 * @author SA05SF
 *
 */
public class MeshReader {

	public Mesh readFile(File projectFolder, File meshDescriptionFile) throws Exception {

		StringBuffer buff = new StringBuffer();
		try (BufferedReader bin = new BufferedReader(new FileReader(meshDescriptionFile))) {
			String line = null;
			while ((line = bin.readLine()) != null) {
				buff.append(line);
			}
		} // automatically close file etc

		String json = buff.toString();

		// {mesh: {type: rectangular, file: "name-of-file"}}
		JSONObject containerObj = new JSONObject(json);

		JSONObject meshObj = containerObj.getJSONObject("mesh");

		String type = meshObj.getString("type");

		if (type.equalsIgnoreCase("rectangular")) {
			// x0,dx,nx y0, dy, ny
			/*
			 * JSONObject gridObj = meshObj.getJSONObject("grid"); double x0 =
			 * gridObj.getDouble("xmin"); double dx = gridObj.getDouble("dx"); int nx =
			 * gridObj.getInt("nx"); double y0 = gridObj.getDouble("ymin"); double dy =
			 * gridObj.getDouble("dy"); int ny = gridObj.getInt("ny");
			 * 
			 * Domain domain = new Domain(x0, x0+(nx-1)*dx, dx, y0, y0+(ny-1)*dy, dy);
			 * 
			 * // create the list of nodes... eg 81x81 nodes gives 80x80 cells List<Node>
			 * nodes = new ArrayList<Node>(); int index = 0; for (int j = 0; j < ny; j++) {
			 * double y = y0 + j*dy; for (int i=0; i < nx; i++) { double x = x0 + i*dx; Node
			 * n = new Node(++index, x, y); nodes.add(n); } } RegularSquareMesh mesh = new
			 * RegularSquareMesh(domain, nodes); return mesh;
			 */

			String bathymetryFileName = meshObj.getString("file");
			File meshGridFile = new File(projectFolder, "bathymetry/" + bathymetryFileName);
			RegularGridBathymetryReader rdr = new RegularGridBathymetryReader();
			Mesh mesh = rdr.readFile(meshGridFile);
			return mesh;

		} else if (type.equalsIgnoreCase("triangular")) {
			return null;
		}
		return null;

	}

}
