package com.apt.models;

public interface IStockModel {

	/**
	 * @return the stockNumber
	 */
	int getStockNumber();

	/**
	 * @return the averageFishMass
	 */
	double getAverageFishMass();

	/**
	 * @return The total biomass.
	 */
	double getBiomass();

}