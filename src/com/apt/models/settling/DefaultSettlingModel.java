/**
 * 
 */
package com.apt.models.settling;

import com.apt.data.Particle;
import com.apt.models.ISettlingModel;

/**
 * Calculates the basic settling speed of a particle dependant on its type. Other types will be added later including:
 * BIOFOULING, TREATMENT_CHEMICAL*, CLEANING_CHEMICAL*, others?
 * 
 */
public class DefaultSettlingModel implements ISettlingModel {

	@Override
	/**
	 * Calculate the settling speed for a specifed particle. This value can depend on type and other characteristics such as:
	 * size, density, porosity, chemical content.
	 */
	public double getSettlingSpeed(Particle p) {
		switch (p.getType()) {
		case FAECES: 
			return -0.03;
		case FEED:
			return -0.095;
		case MUD:
			return -0.02;
		case SAND:
			return -0.25;
		}
		return -999.9; // doesnt make sense!!
	}

}
