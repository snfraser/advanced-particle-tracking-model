package com.apt.models;

import com.apt.data.CompoundMass;
import com.apt.models.stock.StockModel;

public interface IGrowthModel {

	/**
	 * Apply feed to the stock in a cage.
	 * @param stock the StockModel for this cage.
	 * @param feed the applied feed mass.
	 * @param time When this is applied.
	 */
	void applyFeed(StockModel stock, CompoundMass feed, long time);

}