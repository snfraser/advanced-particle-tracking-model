/**
 * 
 */
package com.apt.models.flow;

import com.apt.data.Cell;
import com.apt.data.FlowVector;
import com.apt.models.IBathymetryModel;
import com.apt.models.IFlowModel;


/**
 * @author SA05SF
 *
 */
public class DepthVaryingMeanFlowModel implements IFlowModel {

	private double alpha; // 7.0 or 8.0
	
	private double beta; // 0.32 ish
	
	private double meanFlow;
	
	private double boundaryFlowHeight;
	
	private IBathymetryModel bathymetry;
	
	// private double flowBearing;
	
	// add some params for sinusoidal variation of speed.
	
	/**
	 * @param bathymetry
	 * @param meanFlow
	 * @param alpha
	 * @param beta
	 */
	public DepthVaryingMeanFlowModel(IBathymetryModel bathymetry, double boundaryFlowHeight, double meanFlow, double alpha, double beta) {
		super();
		this.bathymetry = bathymetry;
		this.boundaryFlowHeight = boundaryFlowHeight;
		this.meanFlow = meanFlow;
		this.alpha = alpha;
		this.beta = beta;
	}


	@Override
	public FlowVector getFlow(double x, double y, double z, long time) {
		// TODO Auto-generated method stub
		double h = -bathymetry.getDepth(x, y); // +ve height to surface
		double zz = z + h; // height above seabed
		double speed = 0.0;
		if (zz <= 0.5*h)
			speed =  Math.pow(zz/(beta*h), 1.0/alpha)*meanFlow;
		else
			speed = 1.07*meanFlow;
		return new FlowVector(speed, 0, 0);
			
	}


	@Override
	public double getBoundaryFlowHeight() {
		return boundaryFlowHeight;
	}


	@Override
	public FlowVector getBoundaryFlow(double x, double y, long time) {
		double zb = bathymetry.getDepth(x, y) + boundaryFlowHeight;
		return getFlow(x, y, zb, time);
	}

}
