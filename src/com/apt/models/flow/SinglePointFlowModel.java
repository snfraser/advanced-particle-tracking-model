/**
 * 
 */
package com.apt.models.flow;

import java.util.List;

import com.apt.data.Cell;
import com.apt.data.FlowProfile;
import com.apt.data.FlowVector;
import com.apt.data.Mesh;
import com.apt.models.IFlowModel;

/**
 * A series of flow profiles (by depth) for a period of time defined by a start time and regular step.
 * If flow is required outside this period the period is repeated cyclicly.
 * Profiles are at sigma levels.  -(2n-1)/2n by 1/n to -1/2n level_i (i=0,n-1) = -(2n-1)/2n + i/2n. 
 * @author SA05SF
 *
 */
public class SinglePointFlowModel implements IFlowModel {
	
	/**List of FlowProfiles by time.*/
	FlowProfile profile; 
	
	double siteDepth;
	
	/** Start time [ms].*/
	long time0;
	
	/** Time step [ms].*/
	long step;
	
	/** Number of steps.*/
	int nstep;
	
	/** Number of sigma depths in profiles.*/
	//int nsigma;
	
	//double sigma0;
	
	//double dsigma;
	
	/**
	 * Create a single-point-flowmetry using supplied params.
	 * @param profile A flow profile (collection of flow time-series at various sigma levels).
	 * @param time0 Start time [ms].
	 * @param step Step size [ms].
	 * @param nsigma Number of sigma levels.
	 * @param siteDepth Depth of site (ie depth at sigma = -1).
	 */
	public SinglePointFlowModel(FlowProfile profile, long time0, long step, int nsigma, double siteDepth) {
	
		this.profile = profile;
		this.siteDepth = siteDepth;
		this.time0 = time0;
		this.step = step;
		
		System.err.printf("SinglePointFlowModel::create: using: site: %f timestep: %d ms\n",
				siteDepth, step);
	}



	@Override
	public FlowVector getFlow(double x, double y, double z, long time) {
		// find profile for this timestamp...
		double sigma = -z/siteDepth;
		FlowVector flow = profile.getFlow(sigma, time);
		
		return flow;
		
	}
	
	@Override
	public double getBoundaryFlowHeight() {
		return -siteDepth * (1.0 + profile.getSigma(0)); // ???
	}
	
	@Override
	public FlowVector getBoundaryFlow(double x, double y, long time) {
		double lowestMeterDepth = -siteDepth*profile.getSigma(0);
		//System.err.printf("SinglePointFlowModel:: getboundaryflow: depth: %fm \n", lowestMeterDepth);
		return getFlow(x, y, lowestMeterDepth, time);
	}

	@Override
	public String toString() {
		return String.format("SinglePointFlowModel [time0=%s, step=%s, nstep=%s]", time0, step,
				nstep);
	}

}
