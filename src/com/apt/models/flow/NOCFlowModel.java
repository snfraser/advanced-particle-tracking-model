/**
 * 
 */
package com.apt.models.flow;

import com.apt.data.Cell;
import com.apt.data.FlowVector;
import com.apt.models.IFlowModel;

/**
 * A flow model using the NOC flow profile calculations.
 * 
 * Users must update the mean flow speed and angle as required using calls to
 * updateMean(speed, angle) before calling getFlow(...).
 * 
 * Alternatively a separate time-varying mean flow and angle model can be supplied.
 * 
 * @author SA05SF
 *
 */
public class NOCFlowModel implements IFlowModel {

	/** The depth of the cell [+m].*/
	private double depth;
	
	/** Surface roughness [m].*/
	private double z0;
	
	/** Boundary layer thickness [m]. In coastal water <20m this should be same as depth.*/
	private double delta;
	
	/** Mean (depth averaged) flow speed [m/s].*/
	private double meanSpeed;
	
	/** Angle of mean flow vector [rad acw east].*/
	private double angle;
	
	/**
	 * @param depth
	 * @param z0
	 * @param delta
	 */
	public NOCFlowModel(double depth, double z0, double delta, double meanSpeed, double angle) {
		super();
		this.depth = depth;
		this.z0 = z0;
		this.delta = delta;
		this.meanSpeed = meanSpeed;
		this.angle = angle;
	}

	/** Call this to reset the value of meanSpeed. This is needed where the mean speed varies with time.*/
	public void updateMean(double speed, double angle) {
		this.meanSpeed = speed;
		this.angle = angle;
	}
	
	

	@Override
	/**
	 * z is depth as -ve from surface.
	 */
	public FlowVector getFlow(double x, double y, double z, long time) {
		double h = depth + z;
		
		if (h < z0)
			// inside boundary
			return new FlowVector(0.0, 0.0, 0.0);
		if (h >= z0 && h < 0.5*depth) {
			// boundary to half depth
			double top = meanSpeed*Math.log(h/z0);
			double base = Math.log(delta/(2.0*z0)) - 0.5*delta/depth;
			double speed = top/base;
			return new FlowVector(speed*Math.cos(angle), speed*Math.sin(angle), 0.0);
		}
		// above half depth
		double top = Math.log(0.5*delta/z0);
		double base = Math.log(0.5*delta/z0) - 0.5*delta/depth;
		double speed = meanSpeed*top/base;
		return new FlowVector(speed*Math.cos(angle), speed*Math.sin(angle), 0.0);
	
	}

	@Override
	public double getBoundaryFlowHeight() {
		return depth/2.0;
	}

	@Override
	public FlowVector getBoundaryFlow(double x, double y, long time) {
		return getFlow(x, y, -depth/2.0, time);
	}

}
