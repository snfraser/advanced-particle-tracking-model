/**
 * 
 */
package com.apt.models.flow;

import com.apt.data.Cell;
import com.apt.data.FlowVector;
import com.apt.models.IFlowModel;

/**
 * @author SA05SF
 *
 */
public class DepthVaryingFlowModel implements IFlowModel {

	/** Maximum depth, after this the flow remains constant.*/
	double maxDepth;
	
	/** Peak flow rate at surface [m/s].*/
	double peakFlowSurface;
	
	/** Min flow rate at surface [m/s].*/
	double minFlowSurface;
	
	/** Rate that flow decreases with depth [m/s per m].*/
	double depthReductionRate; 
	
	/** Flow period (sine wave) [s].*/
	double flowPeriod;
	
	/** Height above seabed where boundary layer is [m].*/
	double boundaryFlowHeight;
	
	/**
	 * @param maxDepth
	 * @param peakFlowSurface
	 * @param minFlowSurface
	 * @param depthReductionRate
	 * @param flowPeriod
	 */
	public DepthVaryingFlowModel(double maxDepth, double boundaryFlowHeight, double peakFlowSurface, double minFlowSurface,
			double depthReductionRate, double flowPeriod) {
		super();
		this.maxDepth = maxDepth;
		this.boundaryFlowHeight = boundaryFlowHeight;
		this.peakFlowSurface = peakFlowSurface;
		this.minFlowSurface = minFlowSurface;
		this.depthReductionRate = depthReductionRate;
		this.flowPeriod = flowPeriod;
	}


	@Override
	public FlowVector getFlow(double x, double y, double z, long time) {
		// ignore xy.
		double depthFactor = z >= maxDepth ? 0.1 : 1.0 - z*depthReductionRate;
		double flowSurface = (peakFlowSurface - minFlowSurface)*Math.sin(2.0*Math.PI*time/flowPeriod/1000.0);// + 0.5*(peakFlowSurface + minFlowSurface);
		double flowAtDepth = depthFactor * flowSurface;
		return new FlowVector(flowAtDepth, 0.0, 0.0); // only a U component
	}


	@Override
	public double getBoundaryFlowHeight() {
		return boundaryFlowHeight;
	}


	@Override
	public FlowVector getBoundaryFlow(double x, double y, long time) {
		return getFlow(x, y, maxDepth + boundaryFlowHeight, time);
	}

}
