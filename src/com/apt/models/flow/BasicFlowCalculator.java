/**
 * 
 */
package com.apt.models.flow;

import com.apt.data.FlowVector;
import com.apt.models.IFlowCalculator;
import com.apt.models.IFlowModel;
import com.apt.models.ISeabedModel;

/**
 * 
 */
public class BasicFlowCalculator implements IFlowCalculator {

	static final double VON_KARMAN = 0.41;
	
	private IFlowModel flowmetry;
	
	private ISeabedModel seabedModel;
	
	/**
	 * @param flowmetry
	 */
	public BasicFlowCalculator(IFlowModel flowmetry, ISeabedModel seabedModel) {
		super();
		this.flowmetry = flowmetry;
		this.seabedModel = seabedModel;
	}




	@Override
	public double getFrictionVelocity(double x, double y, long time) {
		
		double h0 = flowmetry.getBoundaryFlowHeight();
		double roughness = seabedModel.getSeabed(x, y).getSurfaceRoughness();
		FlowVector boundaryFlow = flowmetry.getBoundaryFlow(x, y, time);
		
		return VON_KARMAN * boundaryFlow.getMagnitude() / Math.log(h0/roughness); // =ln()
		
	}

}
