package com.apt.models.flow;

import com.apt.data.FlowVector;

public class NoFlow extends FlowVector {

	public NoFlow() {
		super(0.0, 0.0, 0.0);
	}

	public static NoFlow newInstance() {return new NoFlow();}
}
