package com.apt.models.flow;

import java.io.File;
import java.util.Map;

import javax.xml.crypto.NodeSetData;

import com.apt.data.Cell;
import com.apt.data.FlowProfile;
import com.apt.data.FlowVector;
import com.apt.data.IndexingSearchStrategy;
import com.apt.data.Mesh;
import com.apt.data.TriangularMesh;
import com.apt.models.ICellFlowModel;
import com.apt.models.IFlowModel;
import com.apt.readers.HydroModelReader;

import ucar.ma2.ArrayFloat;

public class NetcdfFlowModel implements IFlowModel{

	private TriangularMesh mesh;
	private Map<Integer, FlowProfile> profiles;

	/** Create a FlowModel by importing data from a NetCDF file onto a pre-existing mesh. */
	public NetcdfFlowModel(TriangularMesh mesh, Map<Integer, FlowProfile> profiles) throws Exception {
		this.mesh = mesh;
		this.profiles = profiles;
	
		System.err.printf("Netcdf::():: mesh = %s\n", mesh);
		//mesh.setSearchStrategy(new IndexingSearchStrategy());

	}
	

	/**
	 * @param x    The x position (m).
	 * @param y    The y position (m).
	 * @param z    The z position ie depth (m).
	 * @param time The time (milliseconds).
	 * @param cell The last known cell.
	 */
	public FlowVector getFlow(double x, double y, double z, long time, Cell cell) {
		//System.err.printf("NetFlowModel::getFlow() at: (%f, %f %f) at %d c: %d \n", x, y, z, time, cell != null ? cell.ref: -1);
		if (cell != null) {
			FlowProfile profile = profiles.get(cell.ref);
			double sigma = -z/cell.zc;
			return profile.getFlow(sigma, time);
		} else {
			return null;
		}
	}

	@Override
	/**
	 * @param x    The x position (m).
	 * @param y    The y position (m).
	 * @param z    The z position ie depth (m).
	 * @param time The time (milliseconds).
	 */
	public FlowVector getFlow(double x, double y, double z, long time) {
	
		// locate the cell containing the x,y at t - this is the expensive operation
		Cell c = mesh.findCell(x, y, null);
		return getFlow(x, y, z, time, c); 
	}

	public TriangularMesh getMesh() { return mesh;}


	@Override
	public double getBoundaryFlowHeight() {
		return profiles.get(0).getSigma(0);
	}


	@Override
	public FlowVector getBoundaryFlow(double x, double y, long time) {
		Cell c = mesh.findCell(x, y, null);
		if (c != null) {
			FlowProfile profile = profiles.get(c.ref);
			double sigma = profile.getSigma(0);
			return profile.getFlow(sigma, time);
		} 
		return null; // weirdy weirdness!!
	}



}
