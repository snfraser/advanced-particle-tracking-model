package com.apt.models.flow.test;

import java.io.File;

import com.apt.models.flow.NetcdfFlowModel;

public class TestReadNectcdfFlowFile {

	static final String HOME = System.getProperty("user.home");
	static final File WC1 = new File(HOME, "mesh.nc");
	
	
	public static void main(String[] args) {
		double X = 200.0;
		double Y = 200.0;
		double Z = 5.0;
		long T = 3600*3*1000L + 2568L; // 5 hours into run (ms)
		//long T = -50L; // before all times
		//long T = 400*86400*1000L; // after all times
		try {
			NetcdfFlowModel flow = new NetcdfFlowModel(null, null);
			
			// try a variety of locations...
			for (double x = 100; x < 900.0; x += 201.0) {
				for (double y = 100.0; y < 900; y += 201.0) {
					flow.getFlow(x, y, Z, T);
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
