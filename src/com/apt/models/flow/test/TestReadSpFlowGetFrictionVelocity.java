/**
 * 
 */
package com.apt.models.flow.test;

import java.io.File;

import javax.swing.JFileChooser;

import com.apt.data.FlowVector;
import com.apt.models.IFlowModel;
import com.apt.models.ISeabedModel;
import com.apt.models.flow.BasicFlowCalculator;
import com.apt.models.readers.SeabedReader;
import com.apt.readers.SinglePointFlowmetryReader;

/**
 * 
 */
public class TestReadSpFlowGetFrictionVelocity {

	static double RHO = 1027.0;
	
	/**
	 * Read a single point flow file and cal friction velocity.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {

		try {
			JFileChooser chooser = new JFileChooser();
			chooser.showOpenDialog(chooser);
			File f = chooser.getSelectedFile();

			SinglePointFlowmetryReader reader = new SinglePointFlowmetryReader();
			IFlowModel flowmetry = reader.readFile(f);
			
			SeabedReader sbr = new SeabedReader();
			ISeabedModel seabed = sbr.load(f);
			
			double h0 = flowmetry.getBoundaryFlowHeight();
			
			// -------------------------
			// Surface roughness values.
			// -------------------------
			// 0.0001 = SILT
			// 0.001  = SAND
			// 0.01   = GRIT 
			// 0.1    = GRAVEL
			
			double rough = seabed.getSeabed(0, 0).getSurfaceRoughness();
			double scale = 0.41/Math.log(h0/rough);
			
			System.err.printf("Boundary flow height: %f m \n", h0);
			
			System.err.printf("Surface roughness length: %f mm Ho/zo: %f scale factor: %f\n", 
					rough*1000.0, h0/rough, scale);
			
			BasicFlowCalculator fc = new BasicFlowCalculator(flowmetry, seabed);
			
			long t = 0L;
			t = 12*3600*1000L; 
			double x = 100.0;
			double y = 100.0;
			
			// precalc for 1 location 
			
			
			//while (t < 10*86400*1000L) {
				
			while (t < 260*3600*1000L) {
				System.err.println();
				FlowVector surflow = flowmetry.getFlow(x, y, 0.0, t);
				FlowVector midflow = flowmetry.getFlow(x, y, -20.0, t); // we dont know the actual midflow height
				FlowVector bflow = flowmetry.getBoundaryFlow(x, y, t);
				
				double bfv = fc.getFrictionVelocity(x, y, t);
				
				double taub = RHO*bfv*bfv;
				
				System.err.printf("T: %8.1f Surf: %8.3f, Mid: %8.3f, Boundary: %8.3f BFV: %8.5f TAUB: %f Pa\n",
						(double)t/1000.0, surflow.getMagnitude(), midflow.getMagnitude(), bflow.getMagnitude(), bfv, taub);
				
				t += 24*3600*1000L; // +30 minutes
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
