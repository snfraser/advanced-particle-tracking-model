package com.apt.models.flow.test;

import java.io.File;

import com.apt.data.FlowVector;
import com.apt.models.flow.NetcdfFlowModel;

public class TestPlotFlowWithTime {

	static final String HOME = System.getProperty("user.home");
	static final File WC1 = new File(HOME, "mesh.nc");
	
	public static void main(String[] args) {
	
		try {
			double X = 150.0;
			double Y = 550.0;
			double Z = 5.0;
			
			NetcdfFlowModel flow = new NetcdfFlowModel(null, null);
			
			long ts = 0;
			long te = 3*24*3600*1000L; //6 hours
			
			for (long t = ts; t < te; t += 900*1000L) {
				FlowVector uvt = flow.getFlow(X, Y, Z, t);
				double u = uvt.getU();
				double v = uvt.getV();
				double speed = Math.sqrt(u*u + v*v);
				System.err.printf("T: %f u: %f v: %f speed: %f\n", (double)t/1000.0, u, v, speed);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

}
