/**
 * 
 */
package com.apt.models.flow;

import java.util.Map;

import com.apt.data.Cell;
import com.apt.data.FlowVector;
import com.apt.data.IFlowProfile;
import com.apt.data.Mesh;
import com.apt.models.ICellFlowModel;
import com.apt.models.IFlowModel;

/**
 * A flow model where each cell has a set of flow timeseries per layer for a number of layers.
 * 
 */
public class MeshBasedFlowModel implements IFlowModel, ICellFlowModel {
	
	private Mesh mesh;
	
	private Map<Integer, IFlowProfile> flows;

	/**
	 * 
	 * @param mesh
	 * @param flows
	 */
	public MeshBasedFlowModel(Mesh mesh, Map<Integer, IFlowProfile> flows) {
		this.mesh = mesh;
		this.flows = flows;
	}
	
	@Override
	/**
	 * Fetch the flow at position (x,y,z) at time.
	 * @param x Position x-coord [m].
	 * @param y Position y-coord [m].
	 * @param z Position z-coord [m] +ve above surface.
	 * @param time Time [ms].
	 */
	public FlowVector getFlow(double x, double y, double z, long time) {
		Cell c = mesh.findCell(x, y, null); 
		return getFlow(c, z, time);
	}

	@Override
	/**
	 * Fetch the flow in a cell at time.
	 * @param cell The cell.
	 * @param z Position z-coord [m] +ve above surface.
	 * @param time Time [ms].
	 */
	public FlowVector getFlow(Cell cell, double z, long time) {
		double sigma = -z/cell.zc; 
		return flows.get(cell.ref).getFlow(sigma, time);
	}

	@Override
	public double getBoundaryFlowHeight() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public FlowVector getBoundaryFlow(double x, double y, long time) {
		// TODO Auto-generated method stub
		return null;
	}
	
}
