/**
 * 
 */
package com.apt.models.flow;

import com.apt.data.FlowVector;
import com.apt.data.Seabed;
import com.apt.models.IFlowCalculator;
import com.apt.models.IFlowModel;

/**
 * Flow calculator for a specific (cell) location.
 */
public class CellFlowCalculator implements IFlowCalculator {
	
	static final double VON_KARMAN = 0.41;
	
	private IFlowModel flowModel;
	
	private Seabed seabed;

	/**
	 * @param flowModel
	 * @param seabed
	 */
	public CellFlowCalculator(IFlowModel flowModel, Seabed seabed) {
		super();
		this.flowModel = flowModel;
		this.seabed = seabed;
	}

	@Override
	public double getFrictionVelocity(double x, double y, long time) {
		
		double h0 = flowModel.getBoundaryFlowHeight();
		double roughness = seabed.getSurfaceRoughness();
		FlowVector boundaryFlow = flowModel.getBoundaryFlow(x, y, time);
		
		//System.err.printf("CFC:: h0: %f, roughness: %f bf: () \n", h0, roughness, boundaryFlow.getU(), boundaryFlow.getV());
		
		return VON_KARMAN * boundaryFlow.getMagnitude() / Math.log(h0/roughness); // =ln()
	}
	
	

}
