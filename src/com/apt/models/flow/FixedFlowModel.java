/**
 * 
 */
package com.apt.models.flow;

import com.apt.data.Cell;
import com.apt.data.FlowVector;
import com.apt.models.IFlowModel;

/**
 * @author SA05SF
 *
 */
public class FixedFlowModel implements IFlowModel {

	// flow direction [rads]
	static final double THETA0 = Math.PI + 0.2;
	
	static final double HOURS = 3600*1000.0;
	
	static final double DAYS = 24.0*HOURS;
	
	// tidal period 12 [hours]
	static final double TIDAL_PERIOD = 12.0*HOURS;
	
	// springs period 14 [days] 
	static final double SPRINGS_PERIOD = 14.0*DAYS;
	
	static final double FLAT_START_PERIOD = 0.0*DAYS;
	
	// peak mean flow rate [m/s]
	//static final double VMEAN = 0.085;
	static final double VMEAN = 0.11;
	
	
	public FixedFlowModel() {}
	

	
	
	public FlowVector getFlow(double x, double y, double z, long time) {
		/*double phase = (double)time/TIDAL_PERIOD;
		// random 90deg +- 50
		double direction = 0.5*Math.PI + 0.25*Math.random()*Math.PI;
		 //double flow = rate* Math.sin(2 * Math.PI * phase); 
		double rate = Math.random() * 0.5 - 0.25;
		return new FlowVector(rate*Math.cos(direction), rate*Math.sin(direction), Math.random()*0.025 - 0.0125);
		*/
		
		// no flow for first period
		if (time < FLAT_START_PERIOD)
			return new FlowVector(0.0, 0.0, 0.0);
		
		double phase = 2.0*Math.PI*(double)time/TIDAL_PERIOD;
		double sprphase = 2.0*Math.PI*(double)time/SPRINGS_PERIOD;
		
		//double angle = ((Math.toRadians(40.0) + (Math.random()-0.5)*Math.toRadians(10.0)));
		//double angle = Math.toRadians(50.0 + Math.random()*60.0);
		double angle = Math.toRadians(40.0);
		double rate = VMEAN*Math.sin(phase)*Math.sin(sprphase);
		return new FlowVector(rate*Math.cos(angle), rate*Math.sin(angle), 0.0);
		
	}
	
	public String toString() { return String.format("FixedFlowmetry: (TP:%f, SP:%f, V0:%f)", TIDAL_PERIOD, SPRINGS_PERIOD, VMEAN);}




	@Override
	public double getBoundaryFlowHeight() {
		// TODO Auto-generated method stub
		return 0;
	}




	@Override
	public FlowVector getBoundaryFlow(double x, double y, long time) {
		// TODO Auto-generated method stub
		return null;
	}

	
}
