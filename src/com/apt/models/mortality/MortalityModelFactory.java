/**
 * 
 */
package com.apt.models.mortality;

import com.apt.cli.util.ConfigurationProperties;
import com.apt.models.IMortalityModel;

/**
 * 
 */
public class MortalityModelFactory {

	public static IMortalityModel createMortalityModel(ConfigurationProperties config) {
		return new MortalityModel(0.0); // use cfg params
	}
	
}
