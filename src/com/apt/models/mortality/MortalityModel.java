package com.apt.models.mortality;

import com.apt.models.Cage;
import com.apt.models.IMortalityModel;

public class MortalityModel implements IMortalityModel {

	/** Mortality rate fraction of biomass per annum.*/ 
	private double mortalityRate;
	
	/** Not currently used (preset to 5 days): How often we allow biomass to be killed off [ms].*/
	private double period;
	
	
	/**
	 * Create a basic Mortality model using supplied rate.
	 * @param mortalityRate Rate of mortality (fraction of biomass dying per annum).
	 */
	public MortalityModel(double mortalityRate) {
		super();
		this.mortalityRate = mortalityRate;
	}

	/**
	 * The amount of biomass killed off ina cage at a time. This is timed to occur
	 * only at intervals defined by @see period, otherwise is zero.
	 * @param cage The cage where biomass is culled.
	 * @param time When the killing off occurs [ms].
	 * @return The mortality in the specified cage [kg].
	 */
	@Override
	public double deathMass(Cage cage, long time) {
		
		// mortality rate: average of: 10% to 14.5% biomass each year!
		// mortalityRate = 0.145;
		
		double cageBiomass = cage.getStock().getBiomass();
		double deathMass = cageBiomass * mortalityRate * (5.0/365.0);
		
		// every 5 days loose 10% year * 2/365 = 0.05479 % biomass 
		long timeSec = time/1000;
		int day = (int)((double)time/86400000.0);
		int FIVE_DAYS_SEC = 5*86400;
		if (timeSec % FIVE_DAYS_SEC == 0) {
			System.err.printf("Mortality model: time: %d : biomass: %f kg, morts: %f kg \n", timeSec, cageBiomass, deathMass);
			return deathMass;
		}
		return 0.0;
	}
	
}
