/**
 * 
 */
package com.apt.models.bed;

import com.apt.cli.util.ConfigurationProperties;
import com.apt.models.IBathymetryModel;
import com.apt.models.IBedLayoutModel;
import com.apt.models.IFlowModel;
import com.apt.models.ISeabedModel;

/**
 * 
 */
public class BedModelFactory {

	public static IBedLayoutModel createBedLayoutModel(IBathymetryModel bathymetry, IFlowModel flowmetry, ISeabedModel seabed, ConfigurationProperties config) throws Exception {
		// extract parameters to set in model
		//int nlevels = config.getIntValue("BedModel.layers.count");
		int nlevels = 3;
		// also tauc, texpand, tcontract, tconsolidate, h0z0, rho_sea, viscosity, h_rel, nresuspclarea etc...
		// return new RegularGridBedLayoutModel(bathymetry, flowmetry, nlevels);
		RegularGridBedLayoutModel rgbm = new RegularGridBedLayoutModel(bathymetry, flowmetry, seabed, config);
		
		rgbm.createBedCells();
		return rgbm;
	}
	
}
