package com.apt.models.bed;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.w3c.dom.html.HTMLLIElement;

import com.apt.data.BedLayerType;
import com.apt.data.Cell;
import com.apt.data.ChemicalComponent;
import com.apt.data.ChemicalComponentType;
import com.apt.data.CompoundMass;
import com.apt.data.Particle;
import com.apt.data.ParticlesState;
import com.apt.data.Seabed;
import com.apt.models.IBedCellModel;
import com.apt.models.IBedLayer;
import com.apt.models.IBedLayerModel;
import com.apt.models.IBedTransportModel;
import com.apt.models.IChemicalDegradeModel;
import com.apt.models.IConsolidationModel;
import com.apt.models.IErosionModel;
import com.apt.models.IFlowCalculator;
import com.apt.models.IFlowModel;
import com.apt.models.ISeabedModel;
import com.apt.models.chemical.DefaultChemicalDegradeModel;
import com.apt.models.consolidation.DefaultConsolidationModel;
import com.apt.models.flow.BasicFlowCalculator;
import com.apt.models.flow.CellFlowCalculator;

/**
 * Represents the processes and deposition occurring on the sea-bed within a
 * cell area.
 * 
 * @author SA05SF
 *
 */
public class BedCellModel implements IBedCellModel {

	public static double CONSOLIDATION_TIME_SEC; // = 30 * 60; // 30 minutes

	// static double LAYER_EXPANSION_TIME_SEC = 4*60*60.0; // 4 hours
	public static double LAYER_EXPANSION_TIME_SEC; // = 9*60*60.0; // 9 hours

	public static double LAYER_CONTRACTION_TIME_SEC;// = 60.0 * 30.0; // 30 minutes

	// static double THRESHOLD_MASS_PER_AREA = 5.4; // kg/m2 NDM standard

	// static double THRESHOLD_MASS_PER_AREA = 1.9; // kg/m2
	// static double THRESHOLD_MASS_PER_AREA = 0.9; // kg/m2
	public static double THRESHOLD_MASS_PER_AREA; // = 0.25; // kg/m2

	static double DENSITY_OF_SEAWATER = 1017.0;

	static double GRAVITY_CONSTANT = 9.81;

	public static double FRICTION_COEFF = 0.42447;

	public static double MIN_TAU_CRIT; // = 0.2;

	// static double Z0 = 0.0012; // bottom roughness length - 1mm

	public static double H0_Z0; // = 5000.0; // bottom flow height above seabed / z0 ratio.

	/** X position [m] (cell centroid). */
	double x;

	/** Y position [m] (cell centroid). */
	double y;

	/** Layer counter. */
	int iref = 10;

	/** Element reference in grid. */
	private int elementRef;

	/** Area of element [m2]. */
	double area;

	/** The depth of this cell [m] (from the associated bathymetry cell). */
	double depth;

	/** Seabed characteristics for this cell. */
	Seabed seabed;

	double tauECritMin = MIN_TAU_CRIT; // TEMP hard coded, could vary if seabed composition changes

	double thresholdMass;

	/** Model of bed layers. */
	BedLayerModel layers;

	IErosionModel erosionModel;

	IConsolidationModel consolidationModel;

	IFlowCalculator flowCalculator;

	double[] mydata;

	double depositedMass = 0.0; // anything added to bedload
	double transferedMass = 0.0; // anything transferred out of bedload
	double erodedMass = 0.0; // anything eroded
	double consolidatedMass = 0.0; // anything consolidated
	double lostMass = 0.0; // anything lost

	/**
	 * Set to indicate that this cell is being observed and should produce extra
	 * logging.
	 */
	private boolean observed;

	/**
	 * Bed load particles. Particles which have landed but not yet been
	 * consolidated.
	 */
	List<Particle> bedLoad;

	/** Particles lost in bedload during transport. */
	List<Particle> lostOnBedParticles;

	/**
	 * @param x
	 * @param y
	 */
	public BedCellModel(int iel, double x, double y, double area, double depth, Seabed seabed, IErosionModel erosionModel, IConsolidationModel consolidationModel, BedLayerModel layers) {
		this.elementRef = iel;
		this.x = x;
		this.y = y;
		this.area = area;
		this.depth = depth;
		// surface = new CompoundMass();
		this.seabed = seabed;
		this.layers = layers;
		
		bedLoad = new ArrayList<Particle>();
		lostOnBedParticles = new ArrayList<Particle>();
		
		this.erosionModel = erosionModel;
		this.consolidationModel = consolidationModel;
		
		thresholdMass = THRESHOLD_MASS_PER_AREA * area;

		initialiseTauCProfile(tauECritMin);

		mydata = new double[6];

		// System.err.printf("Creating bedcell [%5d] at (%f, %f) with seabed: %s \n",
		// iel, x, y, seabed);

	}

	/** Chemical content of the surface layer. */
	// private CompoundMass surface;

	/**
	 * Deposit particle into cell's bed-load.
	 * 
	 * @param p The particle to add to the bed-load.
	 */
	@Override
	public void deposit(Particle p) {
		bedLoad.add(p);
		depositedMass += p.getMasses().getComponentMass(ChemicalComponentType.SOLIDS);
	}

	/**
	 * Remove a particle from this cell's bedload (probably to transfer to another
	 * cell's bedload). If the particle is not in the bedload then returns false.
	 * 
	 * @param p The particle.
	 * @return True if it was removed.
	 */
	public boolean removeFromBedLoad(Particle p) {
		if (bedLoad.contains(p)) {
			transferedMass += p.getMasses().getComponentMass(ChemicalComponentType.SOLIDS);
			return bedLoad.remove(p);
		}
		return false;
	}

	/**
	 * Transfer any bed-load particles which have consolidated into the surface
	 * deposition layer. Carry out any bed-transport on remaining particles:
	 * rolling, saltation, lifting/jump.
	 * 
	 * @param time   Time [ms].
	 * @param step   Step size [ms].
	 * @param btrans Bed transport model.
	 */
	public CompoundMass processBedLoad(long time, long step, IFlowModel flowModel, IBedTransportModel bedTransport,
			IChemicalDegradeModel chemicalModel) {

		// [1]. DEPOSITION Add any bed load particles which have passed their
		// consolidation threshold

		List<Particle> consolidated = new ArrayList<Particle>();

		for (Particle p : bedLoad) {
			//long timeOnBed = time - p.getLandingTime();
			//long timeOnBed = p.getTimeSinceLanded(time);

			if (consolidationModel.consolidated(p, seabed, time)) {
				consolidated.add(p);
				p.setState(ParticlesState.CONSOLIDATED);
				// add ALL the masses in this particle to the top layer
				layers.getSurfaceLayer().deposit(p.getMasses());
				consolidatedMass += p.getMasses().getComponentMass(ChemicalComponentType.SOLIDS);
			}
		}

		// lose any newly consolidated particles from bedload
		bedLoad.removeAll(consolidated);

		// [2]. EROSION

		// NOTE flow.getflow uses: x,y,Z,t
		double surfaceFlow = flowModel.getFlow(x, y, 0.1 * depth, time).getPlaneMagnitude();

		double boundaryFlow = flowModel.getBoundaryFlow(x, y, time).getPlaneMagnitude();

		// System.err.printf("BCM:: flow at: depth: %f is %f \n", depth, boundaryFlow);
		mydata[0] = surfaceFlow; // for instr
		mydata[1] = boundaryFlow; // for instr

		double erosionMass = calculateErosionFlux(step, flowModel, x, y, time); // based just on surface layer for
																				// now !!!
		double surfaceMass = layers.getSurfaceLayer().getMass().getComponentMass(ChemicalComponentType.SOLIDS);
		// we should consider ALL mass not just NRS (or do we have a ALL mass type)
		// TODO:: 
		//surfaceMass = layers.getSurfaceLayer().getMass().getCombinedMass()

		double erodeFraction = erosionMass / surfaceMass; // 0 to 1, if >1 make 1 (min(ef, 1.0)
		CompoundMass erodeMass = null;

		if (erosionMass > 0.0) {

			if (surfaceMass > erosionMass) {
				// if (erosionFraction < 1.0

				// System.err.printf("Erosion flux %f lower than surface mass: %f \n",
				// erosionMass, surfaceMass);
				// remove this mass from the (top layer only) for now!!
				erodeMass = new CompoundMass();
				// this should be ALL mass components in appropriate fractions
				erodeMass.addComponent(new ChemicalComponent(ChemicalComponentType.SOLIDS, erosionMass));
				layers.getSurfaceLayer().erode(erodeMass);
				// surfaceLayer.erodeFraction(erosionMass/combinedSurfaceMass)
				erodedMass += erosionMass;
				// displayLayers();
			} else {
				// TODO: erosion mass exceeds available surface mass 
				// - can we erode the next layer??
				// for now - we erode just the top layer completely.

				if (surfaceMass < 0.01) {
					// Surface layer was emptied, nothing to erode at the moment but...
				} else {
					// Erosion flux is higher than surface mass: erode all surface

					erodeMass = new CompoundMass();
					erodeMass.addComponent(new ChemicalComponent(ChemicalComponentType.SOLIDS, surfaceMass));
					layers.getSurfaceLayer().erode(erodeMass);
					erodedMass += surfaceMass;
				}
			}
		} else {
			// no erosion here

		}

		// [3]. MANAGE LAYERS

		double thresholdMass = THRESHOLD_MASS_PER_AREA * area;
		surfaceMass = layers.getSurfaceLayer().getMass().getComponentMass(ChemicalComponentType.SOLIDS);

		// (a) transfer any excess mass down the layers if we are gaining mass...
		while (surfaceMass > 1.5 * thresholdMass) {

			// transfer one block of THRESH value into new top layer
			CompoundMass transferMass = new CompoundMass();
			transferMass.addComponent(new ChemicalComponent(ChemicalComponentType.SOLIDS, thresholdMass));
			layers.getSurfaceLayer().erode(transferMass);

			// create a new top layer on the active stack.
			BedLayer newtop = new BedLayer(transferMass);

			// depth of this layer is based on the mass overlying it, which is the remaining
			// surfacemass
			double overlyingMass = layers.getSurfaceLayer().getMass().getComponentMass(ChemicalComponentType.SOLIDS);

			// this is the depth of the surface layer which overlies the new top layer
			double ldepth = overlyingMass / (layers.getSurfaceLayer().getDensity() * area);
			double tauceq = calculateTauC(ldepth, tauECritMin, layers.getSurfaceLayer().getDensity());

			// this new layer came from the surface layer which had tauecritmin so not yet
			// compressed
			// Because new L1 was part of surface layer it retains the taucritmin value but should perhaps be higher
			// as it was near the bottom of that layer and may have had time to compress a bit?
			newtop.setDensity(layers.getSurfaceLayer().getDensity());
			newtop.setCurrentTauCrit(tauECritMin);
			newtop.setEquilibriumTauCrit(tauceq); // should be higher than tauc because there is some mass in the reduced surface layer

			layers.addTopLayer(newtop);

			double totalmass = getTotalMass().getComponentMass(ChemicalComponentType.SOLIDS);

			System.err.printf("BCM: [%5d] (%f,%f) adding new [L1] nlayer:[%2d], tauce: %f total mass all layers: %f\n",
					elementRef, x, y, layers.getNumberLayers(), tauceq, totalmass);

			surfaceMass = layers.getSurfaceLayer().getMass().getComponentMass(ChemicalComponentType.SOLIDS);
		}

		// lose any excess lower layers into the basement if needed
		layers.compress();

		// surface = layers.getSurfaceLayer();
		surfaceMass = layers.getSurfaceLayer().getMass().getComponentMass(ChemicalComponentType.SOLIDS);

		// (b) - Check for decompression if we are losing mass...
		// check if we need to decompress if surface is empty
		if (surfaceMass < 0.01) {
			double massBefore = getTotalMass().getComponentMass(ChemicalComponentType.SOLIDS);
			
			// always try to promote if the surface is empty.
			if (layers.promote()) {
				// now its on the surface we expand towards the surface value, leave its actual tau as before
				layers.getSurfaceLayer().setEquilibriumTauCrit(tauECritMin);
			}
			
			// now we can decompress the basement into a new lowest active layer.
			if (layers.decompress()) {
			
				CompoundMass transferMass = new CompoundMass();
				transferMass.addComponent(new ChemicalComponent(ChemicalComponentType.SOLIDS, thresholdMass));
				// a new layer at the base of the active stack
				BedLayer newbase = new BedLayer(transferMass);

				// tauc and taue are taken from the basement
				// so these need to be calculated each tstep
				IBedLayer basement = layers.getBasement();
				newbase.setDensity(basement.getDensity());
				newbase.setCurrentTauCrit(basement.getCurrentTauCrit());
				newbase.setEquilibriumTauCrit(basement.getEquilibriumTauCrit());
				layers.addBaseLayer(newbase);

				layers.getBasement().erode(transferMass);
				double massAfter = getTotalMass().getComponentMass(ChemicalComponentType.SOLIDS);

				if (Math.abs(massAfter - massBefore) > 0.001) {
					System.err.printf("BCM::WARNING: Mass changed during decompression!!! mb: %f kg, ma: %f kg \n",
							massBefore, massAfter);
				}
				// do we need to reset tauc on the basement yet??
			}
		}

		// [4] Bioturbation and mixing
		// bioturbateLayers();

		// [5]. Calculate equilibrium Tau_c value
		calculateEquilibriumTauC();

		// [6]. Relax Tau_c values toward equilibrium
		relaxTauC(step);

		// [7]. Degrade and chemical processes
		degrade(chemicalModel, step);

		// displayLayers();

		return erodeMass; // may be null
	}

	// TODO new call outside the bed processing operations
	/**
	 * Transport any bed load particles which have yet to consolidate into bed.
	 * 
	 * @param bedTransport Transport mechanism [rolling, sliding, saltation].
	 * @param time         Current model time [ms].
	 * @param step         Model time-step [ms].
	 * @return
	 */
	public List<Particle> transportBedLoad(IBedTransportModel bedTransport, long time, long step) {

		// prepare a list to hold any particles which move out of this cell.
		List<Particle> movedParticles = new ArrayList<Particle>();

		// list of particles which have gone out-of-bounds.
		lostOnBedParticles.clear();

		for (Particle p : bedLoad) {
			Cell oldCell = p.getCurrentCell();
			bedTransport.move(p, time, step); // move the particle on the bed if valid
			// We record lost and cell transfers so can be arranged at a higher level

			if (p.getState() == ParticlesState.LOST) {
				lostOnBedParticles.add(p);
				lostMass += p.getMasses().getComponentMass(ChemicalComponentType.SOLIDS);
			} else {
				// inside domain, but has it changed cell
				Cell newCell = p.getCurrentCell();
				if (oldCell != newCell) {
					movedParticles.add(p);
					transferedMass += p.getMasses().getComponentMass(ChemicalComponentType.SOLIDS);
				}
			}
		}

		if (lostOnBedParticles.size() > 0) {
			System.err.printf("BCM: [%d] lost %d particles during bed transport, remove from BedLoad..\n", elementRef,
					lostOnBedParticles.size());
			bedLoad.removeAll(lostOnBedParticles);
		}

		// return list of moved particles and let the bed layout model handle inter-cell
		// transfers
		return movedParticles;
	}

	public List<Particle> getLostBedLoadParticles() {
		return lostOnBedParticles;
	}

	/** Carry out bioturbation and mixing in the bed layers. */
	private void bioturbateLayers() {
		// move material between layers - mixing, components drift bwteen layers.
	}

	/** Carry out degradation and chemical processes in all bed layers. */
	private void degrade(IChemicalDegradeModel chemicalModel, long step) {

		/*
		 * IBedLayer surface = layers.surface; chemicalModel.degrade(surface.getMass(),
		 * step);
		 * 
		 * for (int ilayer = 0; ilayer < layers.getNumberLayers(); ilayer++) { IBedLayer
		 * thisLayer = layers.getLayer(ilayer);
		 * chemicalModel.degrade(thisLayer.getMass(), step); } IBedLayer basement =
		 * 
		 * layers.getBasement(); chemicalModel.degrade(basement.getMass(), step);
		 */

	}

	/**
	 * Initialise the tau_c profile for the layers.
	 * 
	 * @param tauECritMin
	 */
	private void initialiseTauCProfile(double tauECritMin) {

		IBedLayer surface = layers.getSurfaceLayer();
		surface.setCurrentTauCrit(tauECritMin);

		IBedLayer layerAbove = surface;
		double cumulativeDepth = 0;

		for (int ilayer = 0; ilayer < layers.getNumberLayers(); ilayer++) {
			IBedLayer thisLayer = layers.getLayer(ilayer);
			double overlyingMass = layerAbove.getMass().getComponentMass(ChemicalComponentType.SOLIDS);
			cumulativeDepth += overlyingMass / layerAbove.getDensity() / area;
			double tauc = calculateTauC(cumulativeDepth, tauECritMin, layerAbove.getDensity());
			thisLayer.setCurrentTauCrit(tauc);
			layerAbove = thisLayer;
		}

	}

	/**
	 * Calculate the equilibrium tau values in each layer.
	 */
	private void calculateEquilibriumTauC() {
		// for each layer...[index]

		IBedLayer surface = layers.getSurfaceLayer();
		IBedLayer basement = layers.getBasement();

		surface.setEquilibriumTauCrit(tauECritMin);
		surface.setCurrentTauCrit(tauECritMin); // ALWAYS taucritmin in SURFACE layer

		IBedLayer layerAbove = surface;

		double cumulativeDepth = 0.0;

		// taucE depends on the thickness and density of overlying material
		// ie layers STRICTLY above it and NOT its own thickness

		for (int ilayer = 0; ilayer < layers.getNumberLayers(); ilayer++) {

			IBedLayer thisLayer = layers.getLayer(ilayer);

			double olmass = layerAbove.getMass().getComponentMass(ChemicalComponentType.SOLIDS);
			cumulativeDepth += olmass / layerAbove.getDensity() / area;
			double taucrit = calculateTauC(cumulativeDepth, tauECritMin, layerAbove.getDensity());
			thisLayer.setEquilibriumTauCrit(taucrit);

			if (observed && ilayer == 1) {
				// System.err.printf("BM:: [%d] Calculate eq tauc for layer1: OL Mass: %f
				// Cdepth: %f TCE: %f\n",
				// elementRef, olmass, cumulativeDepth, taucrit);
			}

			layerAbove = thisLayer;

		}
		double olmass = layerAbove.getMass().getComponentMass(ChemicalComponentType.SOLIDS);
		cumulativeDepth += olmass / layerAbove.getDensity() / area;

		basement.setEquilibriumTauCrit(calculateTauC(cumulativeDepth, tauECritMin, layerAbove.getDensity()));

	}

	private double calculateTauC(double cumulativeDepth, double tauECritMin, double sedimentDensity) {
		return tauECritMin
				+ ((sedimentDensity - DENSITY_OF_SEAWATER) * GRAVITY_CONSTANT * cumulativeDepth * FRICTION_COEFF);
	}

	private void relaxTauC(long step) {
		IBedLayer surface = layers.getSurfaceLayer();
		// relaxLayer(surface, step); // tauc should always be taucritmin in surface
		// layer!!!
		for (int ilayer = 0; ilayer < layers.getNumberLayers(); ilayer++) {
			IBedLayer layer = layers.getLayer(ilayer);
			relaxLayer(layer, step);
		}
		IBedLayer basement = layers.getBasement();
		relaxLayer(basement, step);

	}

	private void relaxLayer(IBedLayer layer, long step) {

		double tauCriticalAbsoluteAccuracy = 0.0001;

		double dts = (double) step / 1000.0; // seconds

		double expansionFactor = 1.0 - Math.pow(0.5, dts / LAYER_EXPANSION_TIME_SEC);
		double contractionFactor = 1.0 - Math.pow(0.5, dts / LAYER_CONTRACTION_TIME_SEC);

		double tauCriticalCurrent = layer.getCurrentTauCrit();
		// double tauCriticalTranslation = layer.getCurrentTauCrit();
		double tauCriticalDifference = layer.getEquilibriumTauCrit() - tauCriticalCurrent;
		double factor;

		if (Math.abs(tauCriticalDifference) > tauCriticalAbsoluteAccuracy) {

			if (tauCriticalDifference < 0.0) {
				if (expansionFactor <= 0.0) {
					// instantaneous expansion
					factor = 1.0;
				} else {
					factor = expansionFactor;
				}
				// System.err.printf("Layer: Tau crit diff calc: expansion: expf: %.8f f: %.8f
				// \n", expansionFactor, factor);
			} else {
				if (contractionFactor <= 0.0) {
					// instantaneous contraction
					factor = 1.0;
				} else {
					factor = contractionFactor;
				}
				// System.err.printf("Layer: Tau crit diff calc: contraction: conf: %f f: %f
				// \n", contractionFactor, factor);
			}
			double tauCriticalOld = tauCriticalCurrent;
			tauCriticalCurrent = tauCriticalCurrent + factor * tauCriticalDifference;
			layer.setCurrentTauCrit(tauCriticalCurrent);
		}

	}

	/**
	 * Calculate the erosion flux from the surface layer (only).
	 * 
	 * @param step Step size (ms or s?).
	 */
	private double calculateErosionFlux(double step, IFlowModel flowModel, double x, double y, long time) {

		// double boundaryFlow = flowModel.getBoundaryFlow(x, y,
		// time).getPlaneMagnitude();

		if (flowCalculator == null)
			flowCalculator = new CellFlowCalculator(flowModel, seabed);

		double bottomFrictionVelocity = flowCalculator.getFrictionVelocity(x, y, time);

		// double bottomFrictionVelocity = VON_KARMAN * boundaryFlow / Math.log(H0_Z0);

		mydata[2] = bottomFrictionVelocity; // TODO:: TEMP needs instrumentation callback here

		// bfv = flowCalculator.getFrictionVelocity(x,y,time);

	
		double tauB = DENSITY_OF_SEAWATER * Math.pow(bottomFrictionVelocity, 2.0);
		mydata[3] = tauB; // TODO:: TEMP needs instrumentation callback here

		//System.err.printf("cell: [%5d] bfv: %f, taub: %f Pa \n", iref, bottomFrictionVelocity, tauB);
		
		// TODO depending on whether this layer is in contact with the seabed and the
		// value of surface roughness
		// we may use the substrate based tauc value not the waste tauc value.
		double tauC = layers.getSurfaceLayer().getCurrentTauCrit();
		double Em = Math.max(erosionModel.getErosionRate(tauB, tauC), 0.0); // Em is [kg/m2/s]
		// erosion model should use a sigmoid NOT the linear arrangement
		double flux = Em * step * area / 1000.0; // step is [ms]
		mydata[4] = flux; // TODO:: TEMP needs instrumentation callback here

		return flux;

	}

	private void displayLayers() {
		int ii = 0;

		System.err.printf(" [C%d] :: ", elementRef);
		IBedLayer surface = layers.getSurfaceLayer();
		System.err.printf(" (S) %f (%f/%f)", surface.getMass().getComponentMass(ChemicalComponentType.SOLIDS),
				surface.getCurrentTauCrit(), surface.getEquilibriumTauCrit());
		for (IBedLayer l : layers.getLayers()) {
			System.err.printf(" (%d) %f (%f/%f)", ii++, l.getMass().getComponentMass(ChemicalComponentType.SOLIDS),
					l.getCurrentTauCrit(), l.getEquilibriumTauCrit());
		}
		IBedLayer basement = layers.getBasement();
		System.err.printf(" || (B) %f (%f/%f)", basement.getMass().getComponentMass(ChemicalComponentType.SOLIDS),
				basement.getCurrentTauCrit(), basement.getEquilibriumTauCrit());
		System.err.printf("\n");
	}

	/**
	 * @return the elementRef
	 */
	@Override
	public int getElementRef() {
		return elementRef;
	}

	/**
	 * @param elementRef the elementRef to set
	 */
	@Override
	public void setElementRef(int elementRef) {
		this.elementRef = elementRef;
	}

	/**
	 * @return the x
	 */
	@Override
	public double getX() {
		return x;
	}

	/**
	 * @return the y
	 */
	@Override
	public double getY() {
		return y;
	}

	@Override
	public double getArea() {
		return area;
	}

	// @Override
	// public CompoundMass getSurface() {
	// return surface;
	// }

	@Override
	public String toString() {
		return "BedCellModel [x=" + x + ", y=" + y + ", elementRef=" + elementRef + "]";
	}

	@Override
	public void erode(CompoundMass erosionMass) {
		// TODO Auto-generated method stub

	}

	@Override
	public IBedLayerModel getLayers() {
		return layers;
	}

	@Override
	public CompoundMass getTotalMass() {
		double solids = layers.getSurfaceLayer().getMass().getComponentMass(ChemicalComponentType.SOLIDS);
		for (IBedLayer layer : layers.getLayers()) {
			CompoundMass layerMass = layer.getMass();
			// amass.mergeCompound(layerMass);
			solids += layerMass.getComponentMass(ChemicalComponentType.SOLIDS);
		}
		solids += layers.getBasement().getMass().getComponentMass(ChemicalComponentType.SOLIDS);
		CompoundMass amass = new CompoundMass();
		amass.addComponent(new ChemicalComponent(ChemicalComponentType.SOLIDS, solids));
		return amass;
	}

	// UNUSED ???
	private int nextLayerRef() {
		return iref++;
	}

	// TEMP:: return some data that should be fed back via a callback or telemetry
	public double[] getMyData() {
		return mydata;
	}

	// TEMP: mark this cell as observed for extra logging.
	public void setObserved(boolean observed) {
		this.observed = observed;
	}

	@Override
	public CompoundMass getBedLoad() {
		// NOTE AVOID USE MERGE !!!!!!
		CompoundMass load = new CompoundMass();
		for (Particle p : bedLoad) {
			// load.mergeCompound(p.getMasses());
			CompoundMass pm = p.getMasses();
			Iterator<ChemicalComponent> ic = pm.components();
			while (ic.hasNext()) {
				ChemicalComponent c = ic.next();
				if (load.getComponent(c.getType()) != null) {
					load.getComponent(c.getType()).addMass(c.getMass());
				} else {
					ChemicalComponent ncc = new ChemicalComponent(c.getType(), c.getMass());
					load.addComponent(ncc);
				}
			}
		}
		return load;
	}

	public double getThresholdMass() {
		return thresholdMass;
	}

	/**
	 * @return the depositedMass
	 */
	public double getDepositedMass() {
		return depositedMass;
	}

	/**
	 * @return the transferedMass
	 */
	public double getTransferedMass() {
		return transferedMass;
	}

	/**
	 * @return the erodedMass
	 */
	public double getErodedMass() {
		return erodedMass;
	}

	/**
	 * @return the consolidatedMass
	 */
	public double getConsolidatedMass() {
		return consolidatedMass;
	}

	public double[] getMassFractions() {
		double[] f = new double[5];
		f[0] = depositedMass;
		f[1] = consolidatedMass;
		f[2] = transferedMass;
		f[3] = erodedMass;
		f[4] = lostMass;

		return f;

	}

}
