/**
 * 
 */
package com.apt.models.bed;

import com.apt.data.BedLayerType;
import com.apt.data.ChemicalComponent;
import com.apt.data.ChemicalComponentType;
import com.apt.data.CompoundMass;
import com.apt.models.IBedLayer;

/**
 * @author SA05SF
 *
 */
public class BedLayer implements IBedLayer {

	private CompoundMass mass;

	//private BedLayerType layerType;

	private double currentTauCrit;

	private double equilibriumTauCrit;

	private double density;

	//private int ref;

	/**
	 * @param mass
	 * @param layerType
	 */
	public BedLayer(CompoundMass mass) {
		this.mass = mass;
	}

	/**
	 * @param mass the mass to set
	 */
	public void setMass(CompoundMass mass) {
		this.mass = mass;
	}

	/**
	 * @param layerType the layerType to set
	 */
	/*public void setLayerType(BedLayerType layerType) {
		this.layerType = layerType;
	}*/

	//@Override
	public CompoundMass getMass() {
		return mass;
	}

	//@Override
	/*public BedLayerType getLayerType() {
		return layerType;
	}*/

	/**
	 * @return the ref
	 */
	/*public int getRef() {
		return ref;
	}*/

	/**
	 * @param ref the ref to set
	 */
	/*public void setRef(int ref) {
		this.ref = ref;
	}*/

	@Override
	public void deposit(CompoundMass depositionMass) {
		mass.mergeCompound(depositionMass);
		// System.err.printf("Layer:: deposited mass: %f total now: %f\n",
		// depositionMass.getComponentMass(ChemicalComponentType.SOLIDS),
		// mass.getComponentMass(ChemicalComponentType.SOLIDS));
	}

	@Override
	public CompoundMass copyMass() {
		CompoundMass copy = mass.copyComponentMasses();
		return copy;
	}
	
	@Override
	public void erode(CompoundMass eroded) {
		mass.extractCompound(eroded);
		// System.err.printf("Layer:: eroded mass: %f total now: %f \n",
		// eroded.getComponentMass(ChemicalComponentType.SOLIDS),
		// mass.getComponentMass(ChemicalComponentType.SOLIDS));
	}

	@Override
	public CompoundMass erodeAll() {
		CompoundMass copy = mass.copyComponentMasses();
		mass.extractAll();
		return copy;
	}

	/**
	 * @param currentTauCrit the currentTauCrit to set
	 */
	public void setCurrentTauCrit(double currentTauCrit) {
		this.currentTauCrit = currentTauCrit;
	}

	/**
	 * @param equilibriumTauCrit the equilibriumTauCrit to set
	 */
	public void setEquilibriumTauCrit(double equilibriumTauCrit) {
		this.equilibriumTauCrit = equilibriumTauCrit;
	}

	@Override
	public double getCurrentTauCrit() {
		return currentTauCrit;
	}

	@Override
	public double getEquilibriumTauCrit() {
		return equilibriumTauCrit;
	}

	/**
	 * @return the density
	 */
	public double getDensity() {
		return density;
	}

	/**
	 * @param density the density to set
	 */
	public void setDensity(double density) {
		this.density = density;
	}

}
