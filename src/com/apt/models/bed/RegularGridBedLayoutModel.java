package com.apt.models.bed;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.apt.cli.util.ConfigurationProperties;
import com.apt.data.BedLayerType;
import com.apt.data.Cell;
import com.apt.data.ChemicalComponent;
import com.apt.data.ChemicalComponentType;
import com.apt.data.CompoundMass;
import com.apt.data.Domain;
import com.apt.data.Particle;
import com.apt.data.ParticleClass;
import com.apt.data.ParticlesState;
import com.apt.data.Seabed;
import com.apt.models.IBathymetryModel;
import com.apt.models.IBedCellModel;
import com.apt.models.IBedLayer;
import com.apt.models.IBedLayoutModel;
import com.apt.models.IBedTransportModel;
import com.apt.models.IChemicalDegradeModel;
import com.apt.models.IConsolidationModel;
import com.apt.models.IErosionModel;
import com.apt.models.IFlowModel;
import com.apt.models.ISeabedModel;
import com.apt.models.consolidation.DefaultConsolidationModel;
import com.apt.models.transport.BedTransportModel;

/**
 * Represents the layout of cells across the domain. Each cell is represented by a BedCellModel.
 * 
 * @author SA05SF
 *
 */
public class RegularGridBedLayoutModel implements IBedLayoutModel {


	static double MUD_DIAMETER = 1.1/1000.0; // m
	static double MUD_DENSITY = 1400.0; // kg/m3
	
	public static int nResusParticlesPerCell = 1;
	
	/** Max height of the mixing layer above the seabed where resus particles are released. [m]*/
	public static double resusMaxHeight = 0.0;
	
	
	//private Domain domain;
	private ConfigurationProperties config;
	
	private IBathymetryModel bathymetry;
	
	private IFlowModel flowmetry;
	
	private ISeabedModel seabedModel;

	//private BedCellModel[][] bedcells;
	
	private Map<Integer, BedCellModel> mbedcells;
	
	private volatile long countResusParticles = 0;
	

	private List<Particle> resusParticles = new ArrayList<Particle>();
	
	
	/**
	 * @param domain
	 */
	public RegularGridBedLayoutModel(IBathymetryModel bathymetry, IFlowModel flowmetry, ISeabedModel seabedModel, ConfigurationProperties config) {
		//System.err.printf("BLM:: create bed model using domain: X: %f -> %f Y: %f -> %f  DX: %f  DY: %f \n", domain.x0, domain.x1, domain.y0, domain.y1, domain.dx, domain.dy);
		//this.domain = domain;
		this.bathymetry = bathymetry;
		this.flowmetry = flowmetry;
		this.seabedModel = seabedModel;
		this.config = config;
		
		
	}
	
	/**
	 * Create a set of bed cells for each cell location. 
	 */
	public void createBedCells() throws Exception {
		
		int minLevels = config.getIntValue("BedModel.Layers.minCount");
		int maxLevels = config.getIntValue("BedModel.Layers.maxCount");
		
		nResusParticlesPerCell = config.getIntValue("BedModel.Cell.resusParticlesPerCell");
		resusMaxHeight = config.getDoubleValue("BedModel.Cell.resusReleaseHeight");
		
		//int nj = (int)Math.floor((domain.getY1() - domain.getY0())/domain.getDy());
		//int ni = (int)Math.floor((domain.getX1() - domain.getX0())/domain.getDx());
		
		//this.bedcells = new BedCellModel[nj][ni];
		
		mbedcells = new HashMap<Integer, BedCellModel>();
		
		
		// create a bedcell for each bathy cell
		for (Cell cell: bathymetry.listCells()) {
		
			//List<IBedLayer> layers = new ArrayList<IBedLayer>();
			//for (int il = 0; il < nlayers; il++) {
				CompoundMass mass = new CompoundMass();
				mass.addComponent(new ChemicalComponent(ChemicalComponentType.SOLIDS, 0.0));
				BedLayer surface = new BedLayer(mass);
				surface.setDensity(1700.0);
				//layers.add(layer);
			//}
			// add a basement layer at the bottom
			CompoundMass basementMass = new CompoundMass();
			basementMass.addComponent(new ChemicalComponent(ChemicalComponentType.SOLIDS, 0.0));
			BedLayer basement = new BedLayer(basementMass);
			basement.setDensity(1700.0);
			//layers.add(basement);
			BedLayerModel lm = new BedLayerModel(cell.ref, minLevels, maxLevels, surface, basement);
			//double depth = bathymetry.getDepth(cell.xc, cell.yc);
			double depth = cell.zc;
			//bedcells[j][i] = 
			// NOTE: bedcell uses same cell ref as bathycell.
			// NOTE: no bedcell over land areas...
			//if (depth < 0.0) {
			if (depth < 0.5) { // allow marginal coastal cells to have BM cfg::  BedModel.CoastalHeightLimit
				
				// TODO add in the seabed characteristics of this cell
				Seabed seabed = seabedModel.getSeabed(cell.xc, cell.yc);
				
				// convert congif values to ms from s.
				IConsolidationModel consolidationModel = new DefaultConsolidationModel(
						1000*config.getDoubleValue("BedModel.consolidationTime.Feed"),
						1000*config.getDoubleValue("BedModel.consolidationTime.Faeces"),
						1000*config.getDoubleValue("BedModel.consolidationTime.Mud"));
				
				IErosionModel erosionModel = new ExcessStressLinearErosionModel(config.getDoubleValue("BedModel.erosionCoefficient"));
				
				BedCellModel bm = new BedCellModel(cell.ref, cell.xc, cell.yc, cell.area, depth, seabed, erosionModel, consolidationModel, lm);
				bm.CONSOLIDATION_TIME_SEC = config.getDoubleValue("BedModel.Layers.consolidationTime");
				bm.H0_Z0 = config.getDoubleValue("BedModel.Surface.h0z0");
				bm.THRESHOLD_MASS_PER_AREA = config.getDoubleValue("BedModel.Layers.thresholdMass");
				bm.LAYER_CONTRACTION_TIME_SEC = config.getDoubleValue("BedModel.Layers.contractionTime");
				bm.LAYER_EXPANSION_TIME_SEC = config.getDoubleValue("BedModel.Layers.expansionTime");
				bm.MIN_TAU_CRIT = config.getDoubleValue("BedModel.CriticalStress.value");
				mbedcells.put(cell.ref, bm);
			}
			
		}
		
		
		
		/*for (int j = 0; j < nj; j++) {
			for (int i = 0; i < ni; i++) {
				iel++;
				double x = domain.x0 + domain.dx*(double)i; // left corner
				double y = domain.y0 + domain.dy*(double)j; // lower corner
				//BedCellModel acell = new BedCellModel(iel, x, y);
				//System.err.printf("Create bed cell: %d [%d,%d] at (%f, %f) \n", iel, j, i, x, y);
				//cell.setElementRef(iel);
				
				// create some layers layer#0 is the top layer
				List<IBedLayer> layers = new ArrayList<IBedLayer>();
				for (int il = 0; il < nlayers; il++) {
					CompoundMass mass = new CompoundMass();
					mass.addComponent(new ChemicalComponent(ChemicalComponentType.SOLIDS, 0.0));
					BedLayer layer = new BedLayer(il, mass, BedLayerType.NONERODABLE);
					layer.setDensity(1700.0);
					layers.add(layer);
				}
				// add a basement layer at the bottom
				CompoundMass basementMass = new CompoundMass();
				basementMass.addComponent(new ChemicalComponent(ChemicalComponentType.SOLIDS, 0.0));
				BasementLayer basement = new BasementLayer(basementMass);
				basement.setDensity(1700.0);
				layers.add(basement);
				BedLayerModel lm = new BedLayerModel(layers);
				double depth = bathymetry.getDepth(x, y);
				bedcells[j][i] = new BedCellModel(iel, x, y, domain.dx*domain.dy, depth, lm);
			}
		}*/
		
	}
	
	/** Locate the bed cell at (x,y).
	 * @param x
	 * @param y
	 * @return
	 */
	public IBedCellModel getBedModel(double x, double y) throws Exception {
		//System.err.printf("BLM:: Check for cell at: %f %f \n", x,y);
		//if (x < domain.x0 || x > domain.x1 || y < domain.y0 || y > domain.y1)
		if (bathymetry.isOutsideDomain(x, y))
			throw new Exception(String.format("Location: (%f,%f) is outside domain", x, y));
		
		//int jj = (int)Math.floor((y - domain.getY0())/domain.getDy());
		//int ii = (int)Math.floor((x - domain.getX0())/domain.getDx());
		//System.err.printf("BLM:: cell is: I: %d J: %d Cells is: %s \n", ii, jj, cells);
		//System.err.printf("BLM:: Cells size: %d x %d \n", cells.length, cells[0].length);
		//System.err.printf("BLM:: Cells[%d][%d] = %s \n", jj, ii, cells[jj][ii]);
		
		//return bedcells[jj][ii];
		// find the bathycell and use its iref to find the bedcell model
		Cell c = bathymetry.getCell(x, y);
		BedCellModel bcm = mbedcells.get(c.ref); // null if not found
		if (bcm == null)
			throw new Exception(String.format("Location: (%f,%f) is not a cell", x, y));
		return bcm;
	}
	
	public List<IBedCellModel> getCells() {
		List<IBedCellModel> acells = new ArrayList<IBedCellModel>();
		// scan the cells
		for (BedCellModel bcm : mbedcells.values()) {
			acells.add(bcm);
		}
		return acells;
	}

	@Override
	public void deposit(List<Particle> landedParticles) {
		//System.err.printf("BLM:: deposit: received: %d particles \n", landedParticles.size());
		for (Particle p : landedParticles) {
			try {
				IBedCellModel cell = getBedModel(p.getX(), p.getY());
				cell.deposit(p);
			} catch (Exception e) {
				// do nothing here particle is not in a cell????
				//boolean land = bathymetry.isLand(p.getX(), p.getY());
				//boolean ood = bathymetry.isOutsideDomain(p.getX(), p.getY());
				//double dx = p.getX() - bathymetry.getDomainX0();
				//double dy = p.getY() - bathymetry.getDomainY0();
				//System.err.printf("RGLM::deposit():particle %s is not in a cell at: (%f, %f) z: %f) land: %b, ood: %b\n", 
					//	p.getId(), dx, dy, p.getZ(), land, ood);
			}
			
		}
	}
	
	@Override
	public void processBed(long time, long step, IBedTransportModel btrans, IChemicalDegradeModel chemicalModel) {
		
		// clear out the resus list before we start bed model processing, 
		// this will be re-populated by the time we are done
		resusParticles.clear();
		
		// for each: Bed Cell		
		for (BedCellModel bcm : mbedcells.values()) {
				
				//IBedCellModel cell = bedcells[j][i];
				CompoundMass cellErosionMass = bcm.processBedLoad(time, step, flowmetry, btrans, chemicalModel);
				
				List<Particle> erosionParticles = convertToParticles(time, cellErosionMass, bcm);
				if (erosionParticles != null) {
					resusParticles.addAll(erosionParticles);
					//System.err.printf("Adding %d resus particles in cell: %d \n", erosionParticles.size(),
							//cell.getElementRef());
				}
	
				// all these particles need to transfer from bedload on this cell to a new cell
				List<Particle> moved = bcm.transportBedLoad(btrans, time, step);
				 
				for (Particle p: moved) {
					try {
						BedCellModel newbcm = (BedCellModel)getBedModel(p.getX(), p.getY());
						bcm.removeFromBedLoad(p); // check it was actually removed to avoid any double counting issues!!!!
						newbcm.deposit(p);
						double dx = p.getX()-p.getBedx();
						double dy = p.getY()-p.getBedy();
						double roll = Math.sqrt(dx*dx + dy*dy);
						double rolltime = (double)(time - p.getLandingTime());
						//System.err.printf("RGLM::procbed.cellxfer(): t: %8d particle: %s moved from cell: %d to cell: %d rolled: %f m in: %f s\n", 
								//time, p.getId2(), bcm.getElementRef(), newbcm.getElementRef(), roll, rolltime/1000.0);
						
					} catch (Exception e) {
						Cell bcell = bathymetry.getCell(p.getX(), p.getY());
						
						System.err.printf("RGLM::procbed.cellxfer(): t: %8d particle %s is not in a cell at: (%f, %f, %f) in bathycell: %d depth: %f\n", 
								time, p.getId(), p.getX(), p.getY(), p.getZ(), bcell.ref, bcell.zc);
						// probably beached so push it back to where it came from??  ideally the bed intercept location
					
						p.updatePosition(p.getLx(), p.getLy(),p.getLz());
					}
				}
				
		} // next bed cell
		
	}
	
	@Override
	public List<Particle> getResuspensionParticles() {
		return resusParticles;
	}
	
	@Override
	public List<Particle> getLostBedLoadParticles() {
		List<Particle> lostBedParticles = new ArrayList<Particle>();
		for (BedCellModel bcm : mbedcells.values()) {
			lostBedParticles.addAll(bcm.getLostBedLoadParticles());
		}
		return lostBedParticles;
	}
	
	private List<Particle> convertToParticles(long time, CompoundMass erosionMass, BedCellModel bedcell) {
		if (erosionMass == null)
			return null;

		double depth = bedcell.depth;
		List<Particle> cellResusParticles = new ArrayList<Particle>();
		double pmass = erosionMass.getComponentMass(ChemicalComponentType.SOLIDS) / nResusParticlesPerCell;
		for (int ip = 0; ip < nResusParticlesPerCell; ip++) {
			double x = bedcell.getX(); // add the cell area sqrt to get a random offset within the cell???
			double y = bedcell.getY(); // or mesh::cell.getRandomInternalPoint()
			double releaseHeightAboveSeabed = Math.random()*resusMaxHeight; // somewhere inside the mixing layer
			double releaseDepth = depth + releaseHeightAboveSeabed;
			Particle p = createResusParticle(time, x, y, releaseDepth, pmass);
			//System.err.printf("In cell: %d (%f, %f) depth: %f create resus at depth: %f \n", cell.getElementRef(), x, y, depth, rdepth);
		
			cellResusParticles.add(p);
		}
		return cellResusParticles;
	}
	
	private Particle createResusParticle(long time, double x, double y, double z, double mass) {
		countResusParticles++;

		Particle p = new Particle();
		p.setId2(String.format("M%d", 	countResusParticles));
		p.setX(x);
		p.setY(y);
		p.setZ(z);
		p.setStartPosition(x, y, z);
		p.setState(ParticlesState.SUSPEND);
		p.setCreationTime(time);
		//p.setFeedMass(1.0);
		p.setType(ParticleClass.MUD);
		p.addChemical(new ChemicalComponent(ChemicalComponentType.SOLIDS, mass));
		p.setTransportMass(mass); // needed??
		p.setDensity(MUD_DENSITY * (1.0+(Math.random()-0.5)/10.0));
		p.setDiameter(MUD_DIAMETER * (1.0+(Math.random()-0.5)/10.0));
		return p;

	}

	
}
