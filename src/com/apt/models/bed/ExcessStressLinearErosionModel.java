/**
 * 
 */
package com.apt.models.bed;

import com.apt.models.IErosionModel;

/**
 * @author SA05SF
 *
 */
public class ExcessStressLinearErosionModel implements IErosionModel {

	  private double mudErosionCoefficient;;

	
	/**
	 * @param mudErosionCoefficient
	 */
	public ExcessStressLinearErosionModel(double mudErosionCoefficient) {
		super();
		this.mudErosionCoefficient = mudErosionCoefficient;
	}


	@Override
	public double getErosionRate(double taub, double tauc) {
		 return Math.max(mudErosionCoefficient * (taub - tauc), 0);
	}

}
