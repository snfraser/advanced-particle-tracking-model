/**
 * 
 */
package com.apt.models.bed;

import java.util.ArrayList;
import java.util.Deque;
import java.util.List;
import java.util.Stack;

import com.apt.cli.test.TestRunModel;
import com.apt.data.ChemicalComponent;
import com.apt.data.ChemicalComponentType;
import com.apt.data.CompoundMass;
import com.apt.models.IBedLayer;
import com.apt.models.IBedLayerModel;

/**
 * @author SA05SF
 *
 */
public class BedLayerModel implements IBedLayerModel {

	private int iref;
	
	private int minLayers;
	private int maxLayers;

	/** The set of active layers.*/
	private List<IBedLayer> layers;

	/** The surface (active) layer.*/
	private IBedLayer surface;


	/** 
	 * Basement layer. Can contain many times the normal layer mass. 
	 * Acts as a storage buffer to allow the active layer count to remain in limits. 
	 */
	private IBedLayer basement;

	/**
	 * @param minLayers Min number of layers before we start to empty the basement
	 *                  layer.
	 * @param maxLayers Max number of layers before we start to dump mass into
	 *                  basement layer.
	 */
	public BedLayerModel(int iref, int minLayers, int maxLayers, IBedLayer surface, IBedLayer basement) {
		this.iref = iref;
		this.minLayers = minLayers;
		this.maxLayers = maxLayers;
		layers = new ArrayList<IBedLayer>();
		this.surface = surface;
		this.basement = basement;
	}

	@Override
	public IBedLayer getLayer(int n) {
		return layers.get(n);
	}

	@Override
	public int getNumberLayers() {
		return layers.size();
	}

	@Override
	public List<IBedLayer> getLayers() {
		return layers;
	}

	@Override
	/**
	 * Add a new top layer to the active layers (this is not the surface layer).
	 */
	public void addTopLayer(IBedLayer layer) {
		layers.add(0, layer);
	}
	
	@Override
	public void addBaseLayer(IBedLayer layer) {
		int nlayers = layers.size();
		layers.add(layer); 
	}

	@Override
	public IBedLayer getSurfaceLayer() {
		return surface;
	}

	@Override
	public IBedLayer getBasement() {
		return basement;
	}
	

	@Override
	/**
	 * Compress lower layers after mass has been transferred out of a heavy surface layer.
	 * If the number of layers exceeds the maxlayers then the lowest layers are dumped into the basement/buffer layer.
	 */
	public void compress() {
				
		// too many layers - transfer lowest into basement
		int nlayers = layers.size();
		while (nlayers > maxLayers) {
		
			IBedLayer lowest = layers.get(nlayers - 1);

			CompoundMass mass = lowest.copyMass();
			basement.deposit(mass);

			lowest.erodeAll(); // can use m=lowest.erodeAll() makes a copy before trashing the layer.
		
			layers.remove(lowest);
			
			nlayers = layers.size();
		}

	}

	/**
	 * Promote the top active layer to the new surface layer if the surface is empty.
	 * If there is an active layer below the surface this is promoted to become the
	 * new surface layer. Otehrwise the empty surface layer remains.
	 * @return True if the top active layer was promoted.
	 */
	public boolean promote() {
		//System.err.printf("BLM:: promoting...\n");
		
		// check if we have some active layers below the surface
		int nlayers = layers.size();
		if (nlayers >= 1) {
			surface = layers.remove(0);
			return true;
		}
		return false;
		
	}
	
	/**
	 * When surface layer becomes empty then promote the first of the lower layers.
	 * @return True if the basement can and should be raided.
	 */
	public boolean decompress() {
		//System.err.printf("BLM:: decompressing...\n");
	
		// check if we can and need to pull material out of the basement
		if (layers.size() < minLayers) {
			if (basement.getMass().getComponentMass(ChemicalComponentType.SOLIDS) >= 0.01) {
				// create new lowest layer from basement
				return true;
			}
		}
		return false;

	}


}
