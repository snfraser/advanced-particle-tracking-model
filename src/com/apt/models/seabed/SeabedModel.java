package com.apt.models.seabed;

import com.apt.data.Domain;
import com.apt.data.Seabed;
import com.apt.data.SeabedType;
import com.apt.models.ISeabedModel;


public class SeabedModel implements ISeabedModel {
	
	private Seabed seabed;
	
	/**
	 * @param seabedType
	 * @param tauSubstrate
	 * @param surfaceRoughness
	 * @param averageGrainSize
	 */
	public SeabedModel(SeabedType seabedType, double tauSubstrate, double surfaceRoughness, double averageGrainSize) {
		this.seabed = new Seabed(seabedType, tauSubstrate, surfaceRoughness, averageGrainSize);
	}


	@Override
	public Domain getDomain() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public Seabed getSeabed(double x, double y) {
		return seabed;
	}

	
}
