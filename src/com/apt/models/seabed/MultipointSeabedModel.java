/**
 * 
 */
package com.apt.models.seabed;

import java.util.Map;

import com.apt.data.Cell;
import com.apt.data.Domain;
import com.apt.data.Seabed;
import com.apt.data.VoronoiMesh;
import com.apt.models.ISeabedModel;

/**
 * @author SA05SF
 *
 */
public class MultipointSeabedModel implements ISeabedModel {
	
	private VoronoiMesh mesh;
	
	private Domain domain;
	
	private Map<Integer, Seabed> bedmap;
	
	/** 
	 * Create a seabed model from a small set of samples. The mesh is a voronoi mesh created from the samples.
	 * Before calling any methods check the location is within the mesh domain.
	 * @param mesh
	 */
	public MultipointSeabedModel(VoronoiMesh mesh, Map<Integer, Seabed> bedmap) {
		this.mesh = mesh;
		this.domain = mesh.getDomainBounds();
		this.bedmap = bedmap;
	}

	@Override
	public Seabed getSeabed(double x, double y) {
		//System.err.printf("MPSBM::getseabed at: (%f,%f) \n", x, y);
		Cell cell = mesh.findCell(x, y, null);
		//System.err.printf("MPSBM::getseabed cell: %d (%f, %f)\n", cell==null? "NOCELL": cell.ref, cell.xc, cell.yc);
		Seabed sea = bedmap.get(cell.ref);
		//System.err.printf("MPSBM::getseabed sea: %s \n", sea != null? sea:"NONE");
		return sea;
	}

	@Override
	public com.apt.data.Domain getDomain() {
		return domain;
	}

}
