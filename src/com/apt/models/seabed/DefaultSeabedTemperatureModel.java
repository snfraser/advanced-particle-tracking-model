/**
 * 
 */
package com.apt.models.seabed;

import com.apt.models.ISeabedTemperatureModel;

/**
 * @author SA05SF
 *
 */
public class DefaultSeabedTemperatureModel implements ISeabedTemperatureModel {

	/** Phase offset. Offset of sine wave for low frequency component.*/
	double loFreqPhaseOffset;
	
	/** Phase offset. Offset of sine wave for high frequency component.*/
	double hiFreqPhaseOffset;
	
	/** Mean value of temperature (C).*/
	double meanTemp;
	
	/** Variation period (high-freq eg daily) (ms).*/
	double periodHighFreq;
	
	/** Variation period (low-freq e.g. seasonal) (ms).*/
	double periodLowFreq;
	
	/** Amplitude of temperature variation (high-freq) (C).*/
	double amplitudeHighFreq;
	
	/** Amplitude of temperature variation (low-freq) (C).*/
	double amplitudeLowFreq;
	
	
	// TODO add the low and high frequency phase offsets.
	/**
	 * @param meanTemp
	 * @param periodHighFreq Diurnal or short period.
	 * @param periodLowFreq Seasonal preiod.
	 * @param amplitudeHighFreq Diurnal or short period amplitude.
	 * @param amplitudeLowFreq Seasonal amplitude.
	 */
	public DefaultSeabedTemperatureModel(double meanTemp, double periodHighFreq, double periodLowFreq,
			double amplitudeHighFreq, double amplitudeLowFreq) {
		super();
		this.meanTemp = meanTemp;
		this.periodHighFreq = periodHighFreq;
		this.periodLowFreq = periodLowFreq;
		this.amplitudeHighFreq = amplitudeHighFreq;
		this.amplitudeLowFreq = amplitudeLowFreq;
	}



	@Override
	public double getTemperature(double time) {
		double temp = meanTemp + amplitudeHighFreq*Math.sin(2.0*Math.PI*time/periodHighFreq) + amplitudeLowFreq*Math.sin(2.0*Math.PI*time/periodLowFreq) ;
		return temp;
	}

}
