package com.apt.models;

import java.util.List;

import com.apt.data.CompoundMass;
import com.apt.data.Particle;
import com.apt.models.transport.BedTransportModel;


public interface IBedLayoutModel {

	public IBedCellModel getBedModel(double x, double y) throws Exception;
	
	public List<IBedCellModel> getCells();
	
	public void deposit(List<Particle> landedParticles);
	
	/**
	 * Carry out any bed model processing.
	 * 
	 * @param time Current time [ms].
	 * @param step Time-step size [ms].
	 * @param btrans Bed transport model (handles transport of non-consolidated bed-load particles).
	 */
	public void processBed(long time, long step, IBedTransportModel btrans, IChemicalDegradeModel chemicalModel);
		
	public List<Particle> getResuspensionParticles();
	
	public List<Particle> getLostBedLoadParticles();
	
}