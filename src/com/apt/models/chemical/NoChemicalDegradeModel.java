/**
 * 
 */
package com.apt.models.chemical;

import com.apt.data.CompoundMass;
import com.apt.models.IChemicalDegradeModel;

/**
 * 
 */
public class NoChemicalDegradeModel implements IChemicalDegradeModel {

	@Override
	public void degrade(CompoundMass compound, long delta) {
		// DO NOTHING
	}

}
