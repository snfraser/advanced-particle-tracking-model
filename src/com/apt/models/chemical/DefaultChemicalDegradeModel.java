/**
 * 
 */
package com.apt.models.chemical;

import com.apt.data.ChemicalComponent;
import com.apt.data.ChemicalComponentType;
import com.apt.data.CompoundMass;
import com.apt.models.IChemicalDegradeModel;

/**
 * 
 */
public class DefaultChemicalDegradeModel implements IChemicalDegradeModel {

	public static final double DAYS = 86400*1000.0;
	
	private double degradeTimeLabileCarbon = 22.0 * DAYS;

	private double degradeTimeRefractoryCarbon = 112.0 * DAYS;

	private double degradeTimeEmbz = 36 * DAYS;

	private double degradeTimeTfbz = 126 * DAYS;

	@Override
	public void degrade(CompoundMass mass, long delta) {
		
		for (ChemicalComponent chemical : mass.listComponents()) {

			switch (chemical.getType()) {
			case CARBON_LABILE:
				// decrease the labile carbon and increase the product CARBON_DEGRADED
				double originalMass = chemical.getMass();
				double degradedMass = originalMass * delta / degradeTimeLabileCarbon;
				chemical.removeMass(degradedMass);
				ChemicalComponent dc = new ChemicalComponent(ChemicalComponentType.CO2, degradedMass);
				mass.mergeComponent(dc);
				break;
			case CARBON_REFACTORY:
				// decrease the refractory carbon and increase the product CARBON_DEGRADED
				double originalMass2 = chemical.getMass();
				double degradedMass2 = originalMass2 * delta / degradeTimeRefractoryCarbon;
				chemical.removeMass(degradedMass2);
				ChemicalComponent dc2 = new ChemicalComponent(ChemicalComponentType.CO2, degradedMass2);
				mass.mergeComponent(dc2);
				break;
			case TREATMENT_EMBZ:

				break;
			case TREATMENT_TFBZ:

				break;
			}
		}
	}

	/**
	 * @param degradeTimeLabileCarbon the degradeTimeLabileCarbon to set
	 */
	public void setDegradeTimeLabileCarbon(double degradeTimeLabileCarbon) {
		this.degradeTimeLabileCarbon = degradeTimeLabileCarbon;
	}

	/**
	 * @param degradeTimeRefractoryCarbon the degradeTimeRefractoryCarbon to set
	 */
	public void setDegradeTimeRefractoryCarbon(double degradeTimeRefractoryCarbon) {
		this.degradeTimeRefractoryCarbon = degradeTimeRefractoryCarbon;
	}

	/**
	 * @param degradeTimeEmbz the degradeTimeEmbz to set
	 */
	public void setDegradeTimeEmbz(double degradeTimeEmbz) {
		this.degradeTimeEmbz = degradeTimeEmbz;
	}

	/**
	 * @param degradeTimeTfbz the degradeTimeTfbz to set
	 */
	public void setDegradeTimeTfbz(double degradeTimeTfbz) {
		this.degradeTimeTfbz = degradeTimeTfbz;
	}
	
	

}
