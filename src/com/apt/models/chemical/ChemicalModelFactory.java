/**
 * 
 */
package com.apt.models.chemical;

import com.apt.cli.util.ConfigurationProperties;
import com.apt.models.IChemicalDegradeModel;

/**
 * 
 */
public class ChemicalModelFactory {

	public static IChemicalDegradeModel createChemicalModel(ConfigurationProperties config) throws Exception {
		String clazz = config.getProperty("Chemical.class");
		if (clazz.equalsIgnoreCase("NONE")) {
			return new NoChemicalDegradeModel();
		} else if (clazz.equalsIgnoreCase("DEFAULT")) {
			double dtLabileCarbon = config.getDoubleValue("Chemical.degradeTimeLabileCarbon"); 
			double dtRefractoryCarbon = config.getDoubleValue("Chemical.degradeTimeRefractoryCarbon");
			double dtEmbz = config.getDoubleValue("Chemical.degradeTimeEmbz");
			double dtTfbz = config.getDoubleValue("Chemical.degradeTimeTfbz");
			DefaultChemicalDegradeModel chem = new DefaultChemicalDegradeModel();
			chem.setDegradeTimeLabileCarbon(dtLabileCarbon);
			chem.setDegradeTimeRefractoryCarbon(dtRefractoryCarbon);
			chem.setDegradeTimeEmbz(dtEmbz);
			chem.setDegradeTimeTfbz(dtTfbz);
			return chem;
		}
		throw new Exception(String.format("Unknown chemical degrade model class: %s", clazz));

	}
}
