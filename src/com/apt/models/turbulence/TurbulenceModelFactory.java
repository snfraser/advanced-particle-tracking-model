/**
 * 
 */
package com.apt.models.turbulence;

import com.apt.cli.util.ConfigurationProperties;
import com.apt.models.ITurbulenceModel;

/**
 * 
 */
public class TurbulenceModelFactory {

	public static ITurbulenceModel createTurbulenceModel(ConfigurationProperties config) {
		// Turbulence.class=DEFAULT + Turbulence.kx, Turbulence.ky, Turbulence.kz, 
		return new DefaultTurbulenceModel(0.1, 0.1, 0.01);
	}
}
