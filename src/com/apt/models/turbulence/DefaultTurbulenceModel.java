/**
 * 
 */
package com.apt.models.turbulence;

import com.apt.data.Turbulence;
import com.apt.models.ITurbulenceModel;

/**
 * @author SA05SF
 *
 */
public class DefaultTurbulenceModel implements ITurbulenceModel {

	private double xParam;
	
	private double yParam;
	
	private double zParam;
	
	private Turbulence standardTurbulence;
	
	/**
	 * @param xParam
	 * @param yParam
	 * @param zParam
	 */
	public DefaultTurbulenceModel(double xParam, double yParam, double zParam) {
		super();
		this.xParam = xParam;
		this.yParam = yParam;
		this.zParam = zParam;
		standardTurbulence = new Turbulence(xParam, yParam, zParam);
	}



	@Override
	public Turbulence getTurbulence(double x, double y, double z, long time) {
		return standardTurbulence;
	}

}
