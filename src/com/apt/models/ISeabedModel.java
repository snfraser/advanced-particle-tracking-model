/**
 * 
 */
package com.apt.models;

import com.apt.data.Domain;
import com.apt.data.Seabed;
import com.apt.data.SeabedType;

/**
 * Characterisation of the seabed over a mesh. eg roughness, grainsize, stickyness  etc
 * @author SA05SF
 *
 */
public interface ISeabedModel {

	public Seabed getSeabed(double x, double y); 
	public Domain getDomain();
	
}
