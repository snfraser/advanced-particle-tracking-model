/**
 * 
 */
package com.apt.models.time;

import com.apt.models.ITimeModel;

/**
 * Provides timing information to the engine.
 * 
 * @author SA05SF
 *
 */
public class TimeModel implements ITimeModel {
	
	/** Start time of run (ms).*/
	private long start;
	
	/** End time of run (ms).*/
	private long end;
	
	/** Length of time-step (ms).*/
	private long step;
	
	/** The time into the run (ms).*/
	private long time;
	
	/** Counts the number of steps into run.*/
	private int nstep;
	
	
	/**
	 * @param start
	 * @param end
	 * @param step
	 */
	public TimeModel(long start, long end, long step) {
		this.start = start;
		this.end = end;
		this.step = step;
	}

	/** Set the time-model to its start point.*/
	@Override
	public void reset() {
		time = start;
		nstep = 0;
	}
	
	/** Advance the time model one step.
	 * @return the current (new) time.
	 */
	@Override
	public long step() {
		 time += step;
		 nstep++;
		 return time;
	}

	@Override
	public boolean end() {
		return time >= end;
	}
	
	@Override
	public long time() {
		return time;
	}
	
	@Override
	public long getStep() {
		return step;
	}
	
	@Override
	public int getNStep() {
		return nstep;
	}
	
	@Override
	public long getRunDuration() {
		return end-start;
	}
	
	@Override
	public double getProgress() {
		return (double)time/(double)end;
	}
}
