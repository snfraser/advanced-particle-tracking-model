package com.apt.models;

import com.apt.models.Cage;

public interface IMortalityModel {

	/**
	 * The mortality rate [kg/hr].
	 * @param cage
	 * @param time
	 * @return The mortality rate in the specified cage.
	 */
	double deathMass(Cage cage, long time);

}