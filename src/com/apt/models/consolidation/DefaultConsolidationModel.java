/**
 * 
 */
package com.apt.models.consolidation;

import com.apt.data.Particle;
import com.apt.data.Seabed;
import com.apt.models.IConsolidationModel;

/**
 * 
 */
public class DefaultConsolidationModel implements IConsolidationModel {

	/** Consolidation time (since landing on bed) for feed particles [ms].*/
	private double consolidationTimeFeed;
	
	/** Consolidation time (since landing on bed) for faeces particles [ms].*/
	private double consolidationTimeFaeces;
	
	/** Consolidation time (since landing on bed) for mud particles [ms].*/
	private double consolidationTimeMud;
	
	
	/**
	 * @param consolidationTimeFeed [ms]
	 * @param consolidationTimeFaeces [ms]
	 * @param consolidationTimeMud [ms]
	 */
	public DefaultConsolidationModel(double consolidationTimeFeed, double consolidationTimeFaeces,
			double consolidationTimeMud) {
		this.consolidationTimeFeed = consolidationTimeFeed;
		this.consolidationTimeFaeces = consolidationTimeFaeces;
		this.consolidationTimeMud = consolidationTimeMud;
	}

	@Override
	/**
	 * @param particle The particle to check for consolidation.
	 * @param seabed The seabed conditions at particle position.
	 * @param time Current time.
	 */
	public boolean consolidated(Particle particle, Seabed seabed, long time) {
		long timeOnBed = time - particle.getLandingTime();
		
		// later we might add in effects of particle size etc
		// later we might add effects of seabed type and parameters
		if (timeOnBed > consolidationTimeFaeces)
			return true;
		return false;
	}

}
