package com.apt.models;

import com.apt.data.Season;

public interface ISeasonalityModel {

	/**
	 * Calculate the season fraction from the supplied time (elapsed time in simulation).
	 * @param time Elapsed time (ms since start of model).
	 * @return A number in range [0.0:1.0] 0.0=mid-winter, 0.5=mid-summer, 1.0=next mid-winter.
	 */
	double getSeasonFraction(double time);

	/**
	 * Calculate and return the Season for a given simulation time (since start).
	 * @param time The time since start of simulation (ms).
	 * @return The season corresponding to this time.
	 */
	Season getSeason(double time);
	
	/**
	 * Calculate the day fraction from the supplied time (elapsed time in simulation).
	 * @param time Elapsed time (ms since start of model).
	 * @return A number in range [0:1.0] 0.0=midnight, 0.5=midday, 1.0=next midnight. 
	 */
	public double getDayFraction(double time);

}