package com.apt.models;

public interface ITimeModel {

	/** Set the time-model to its start point.*/
	void reset();

	/** Advance the time model one step.
	 * @return the current (new) time.
	 */
	long step();

	boolean end();

	long time();

	long getStep();

	int getNStep();

	long getRunDuration();

	double getProgress();

}