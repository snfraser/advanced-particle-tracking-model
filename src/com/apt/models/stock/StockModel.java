package com.apt.models.stock;

import com.apt.models.IStockModel;

public class StockModel implements IStockModel {

	/** Number of stock (fish).*/
	private int stockNumber;

	/** The mass of an average fish.*/
	private double averageFishMass;

	
	
	/**
	 * @param stockNumber
	 * @param averageFishMass
	 */
	public StockModel(int stockNumber, double averageFishMass) {
		this.stockNumber = stockNumber;
		this.averageFishMass = averageFishMass;
	}
	
	

	/**
	 * Copy constructor.
	 */
	public StockModel(StockModel template) {
		this.stockNumber = template.getStockNumber();
		this.averageFishMass = template.getAverageFishMass();
	}



	/**
	 * @return the stockNumber
	 */
	@Override
	public int getStockNumber() {
		return stockNumber;
	}

	/**
	 * @param stockNumber the stockNumber to set
	 */
	
	public void setStockNumber(int stockNumber) {
		this.stockNumber = stockNumber;
	}

	/**
	 * @return the averageFishMass
	 */
	@Override
	public double getAverageFishMass() {
		return averageFishMass;
	}

	/**
	 * @param averageFishMass the averageFishMass to set
	 */
	
	public void setAverageFishMass(double averageFishMass) {
		this.averageFishMass = averageFishMass;
	}
	
	/**
	 * @return The total biomass.
	 */
	@Override
	public double getBiomass() {return stockNumber*averageFishMass;}
	
	
}
