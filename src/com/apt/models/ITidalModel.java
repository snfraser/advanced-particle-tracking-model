/**
 * 
 */
package com.apt.models;

/**
 * 
 */
public interface ITidalModel {

	public double getTidalDepth(long time);
	
}
