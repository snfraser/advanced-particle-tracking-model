/**
 * 
 */
package com.apt.models.bathymetry;

import java.util.List;

import com.apt.data.Cell;
import com.apt.data.Domain;
import com.apt.data.Mesh;
import com.apt.models.IBathymetryModel;

/**
 * @author SA05SF
 *
 */
public class FixedBathymetryModel implements IBathymetryModel {

	private static final double D1 = -20.0;
	private static final double DD = 5.0;
	
	private double dx;
	private double dy;
	
	private Domain domain;
	
	double[][] depth;

	/**
	 * TEMP Using fixed xy grid domain, this may become a more general network later
	 */
	public FixedBathymetryModel(Domain domain) {
		this.domain = domain;
		this.dx = domain.x1 - domain.x0;
		this.dy = domain.y1 - domain.y0;
		depth = new double[80][80];
		for (int j = 0; j < 80; j++) {
			for (int i = 0; i < 80; i++) {
				depth[j][i] = D1 + Math.random() * DD;
				//depth[j][i] = D1; //FLAT
			}
		}
	}
	
	

	@Override
	public boolean isOutsideDomain(double x, double y) {
		int row = getCellRow(y);
		int col = getCellCol(x);
		if (row < 0 || row > 79 || col < 0 || col > 79)
			return true;
		return false;
	}

	@Override
	public boolean isLand(double x, double y) {
		return (getDepth(x, y)) > 0.0;
	}

	@Override
	public double getDomainWidth() {
		return dx;
	}

	@Override
	public double getDomainHeight() {
		return dy;
	}

	@Override
	public double getDepth(double x, double y, Cell cell) {
		return getDepth(x, y);
	}
	
	@Override
	public double getDepth(double x, double y) {
		// System.err.println("bm;getdepth: " + x +" " + y);
		int row = getCellRow(y);
		int col = getCellCol(x);
		if (row < 0 || row > 79 || col < 0 || col > 79)
			return 0.0;
		return depth[col][row];
	}

	private int getCellCol(double x) {
		int i = (int) Math.floor(x / dx * 80.0);
		return i;
	}

	private int getCellRow(double y) {
		int j = (int) Math.floor(y / dy * 80.0);
		return j;
	}
	
	public String toString() {return String.format("FixedBathymetry: (D1:%f, DD:%f)", D1, DD);}


	@Override
	public Cell getCell(double x, double y) {
		return null;
	}
	
	
	public double getDomainX0() {
		return domain.x0;
	}

	public double getDomainY0() {
		return domain.y0;
	}



	@Override
	public List<Cell> listCells() {
		// TODO Auto-generated method stub
		return null;
	}



	@Override
	public Mesh getMesh() {
		// TODO Auto-generated method stub
		return null;
	}



	@Override
	public String getBCN() {
		return "FIXED";
	}

	
}
