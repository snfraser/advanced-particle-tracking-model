/**
 * 
 */
package com.apt.models.bathymetry;

import java.util.List;

import com.apt.data.Cell;
import com.apt.data.Domain;
import com.apt.data.Mesh;
import com.apt.data.Node;
import com.apt.models.IBathymetryModel;

/**
 * @author SA05SF
 *
 */
public class MeshBathymetry implements IBathymetryModel {

	private Mesh mesh;

	private Domain domain;

	// use a RegularGridMesh from depomod.netcdf project

	/**
	 * @param mesh
	 */
	public MeshBathymetry(Mesh mesh) {
		this.mesh = mesh;
		domain = mesh.getDomainBounds();
	}

	@Override
	/**
	 * Get the depth at a position (within a cell).
	 * This method calculates the depth at an internal point using a distance weighting to
	 * the known depths at the cells bounding nodes. A simpler solution is just to use the cell centre depth.
	 */
	public double getDepth(double x, double y, Cell cell) {
		double sumDepth = 0.0;
		double ww = 0.0;
		Node[] nodes = cell.nodes;
		//System.err.printf("MB:: getdepth: %f,%f cell: %s :: ", x,y, cell);
		for (int i = 0; i < nodes.length; i++) {
			double ddx = nodes[i].x - x;
			double ddy = nodes[i].y - y;
			double distanceToNode = Math.sqrt(ddx * ddx + ddy * ddy) + 0.01;
			ww += 1.0 / distanceToNode;
			sumDepth += nodes[i].depth / distanceToNode;
			//System.err.printf(" [%d,%f,%f]", nodes[i].ref, distanceToNode, nodes[i].depth);
		}
		//System.err.println();
		return sumDepth / ww; // depths at nodes weighted by inverse distance to node.
		
		// ALT:: return cell.zc;
		
	}

	@Override
	public double getDepth(double x, double y) {
		Cell cell = mesh.findCell(x, y, null);
		
		if (cell != null) {
			// find the nodes surrounding this cell
			return getDepth(x, y, cell);
		} else {
			// no cell contains xy so must be land or outside domain
			return LAND_DEPTH; // land why land depth?????
		}
	}

	@Override
	public Cell getCell(double x, double y) {
		return mesh.findCell(x, y, null);
	}

	@Override
	public boolean isOutsideDomain(double x, double y) {
		return (!domain.contains(x, y));
	}

	@Override
	public boolean isLand(double x, double y) {
		return getDepth(x, y) > 0.0;
	}

	@Override
	public double getDomainWidth() {
		return domain.x1 - domain.x0;
	}

	@Override
	public double getDomainHeight() {
		return domain.y1 - domain.y0;
	}

	public double getDomainX0() {
		return domain.x0;
	}

	public double getDomainY0() {
		return domain.y0;
	}

	@Override
	public List<Cell> listCells() {
		// TODO Auto-generated method stub
		return mesh.listCells();
	}

	@Override
	public Mesh getMesh() {
		return mesh;
	}

	@Override
	public String getBCN() {
		return "MESH " + mesh;
	}

}
