package com.apt.models.bathymetry;

import java.io.File;
import java.util.List;

import com.apt.data.Cell;
import com.apt.data.Domain;
import com.apt.data.Mesh;
import com.apt.data.Node;
import com.apt.data.TriangularMesh;
import com.apt.models.IBathymetryModel;
import com.apt.readers.HydroModelReader;


public class NetcdfBathymetryModel implements IBathymetryModel{

	private TriangularMesh mesh;
	
	private Domain domain;

	/** Create a FlowModel by importing data from a NetCDF file. */
	public NetcdfBathymetryModel(File flowFile) throws Exception {

		HydroModelReader reader = new HydroModelReader(flowFile);
		mesh = reader.getMesh();
		domain = mesh.getDomainBounds();
	}



	@Override
	public boolean isOutsideDomain(double x, double y) {
		 return (!domain.contains(x,y));
	}
	
	@Override
	public boolean isLand(double x, double y) {
		return getDepth(x,y) > 0.0;
	}
	
	@Override
	public double getDepth(double x, double y) {
		
		//mesh.setSearchStrategy(new RingSearchStrategy());  - 1 or 2 rings of neighbours
		//mesh.setSearchStrategy(new IndexingSearchStrategy()); - overlay regular grid
		//mesh.setSearchStrategy(new EnhancedGridSearchStrategy()); - overlay regular grid with secondary sub-grids
		Cell cell = mesh.findCell(x, y, null);
		
		if (cell != null) {
			// find the nodes surrounding this cell
			return getDepth(x, y, cell);
		} else {
			// no cell contains xy so must be land or outside domain
			return LAND_DEPTH; // land
		}
	}

	@Override
	public double getDepth(double x, double y, Cell cell) {
		Node[] nodes = cell.nodes;
		// interpolate - weighted by reverse distance or similar...
		double wsum = 0.0;
		double ww = 0.0;
		for (Node n: nodes) {
			double dx = x - n.x;
			double dy = y - n.y;
			double d = Math.sqrt(dx*dx + dy*dy);
			ww += 1/d;
			wsum += n.depth/d;
		}
		return wsum/ww; // sum over weights
	}



	@Override
	public Cell getCell(double x, double y) {
		return mesh.findCell(x, y, null);
	}



	@Override
	public double getDomainWidth() {
		return domain.x1 - domain.x0;
	}



	@Override
	public double getDomainHeight() {
		return domain.y1 - domain.y0;
	}


	public double getDomainX0() {
		return domain.x0;
	}

	public double getDomainY0() {
		return domain.y0;
	}



	@Override
	public List<Cell> listCells() {
		// TODO Auto-generated method stub
		return mesh.getCellList();
	}



	@Override
	public Mesh getMesh() {
		// TODO Auto-generated method stub
		return mesh;
	}



	@Override
	public String getBCN() {
	return "NETCDF";
	}

}
