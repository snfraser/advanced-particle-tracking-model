package com.apt.models;

import java.util.List;

import com.apt.models.Cage;
import com.apt.models.cage.ICagePositionManager;
import com.apt.data.CompoundMass;
import com.apt.data.Particle;

public interface IDefacationModel {

	/**
	 * Calculates the faces generation rate per unit biomass at a time since last feed.
	 * 
	 * To get the actual faeces released during a time period use:
	 * 
	 * this value * time period (s) * biomass (kg)
	 * 
	 * 0.000926835 kg poo / kg stock per day
	 * 
	 * @param timeSinceLastFeed Time since last feed (ms).
	 * @return The rate of faeces generation (kg/sec/per kg biomass).
	 */
	double getFaecesRate(double feedMass, double timeSinceLastFeed);

	/**
	 * Create a faces release a time after feeding.
	 * 
	 * @param release       The actual feed mass applied.
	 * @param timeSinceFeed Time since feed mass was applied.
	 * @param dt            The time-step over which to calculate the release.
	 * @return
	 */
	CompoundMass createFaecesMass(CompoundMass release, double timeSinceFeed, double dt);

	/**
	 * Create a release of faeces for a specified cage using the supplied total faeces mass.
	 * @param cage The cage to release from.
	 * @param faecesMass The total faeces compund mass.
	 * @return A list of faeces particles representing the release.
	 */
	List<Particle> generateFaecesParticles(Cage cage, ICagePositionManager pm, CompoundMass faecesMass);

}