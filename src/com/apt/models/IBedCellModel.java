package com.apt.models;

import com.apt.data.CompoundMass;
import com.apt.data.Particle;

public interface IBedCellModel {

	/**
	 * Deposit mass compound onto the surface layer.
	 * @param deposition The deposition mass compound.
	 */
	void deposit(Particle p);
	
	/**
	 * Process any bed - consolidate nay old bed-load particles and process the consolidated layers.
	 * @param time Simulation time [ms].
	 * @param step Time-step size [ms].
	 * @param flow Flow model.
	 * @param btrans Bed transport model implementation.
	 * @return The eroded mass (may be zero).
	 */
	CompoundMass processBedLoad(long time, long step, IFlowModel flow, IBedTransportModel bedTransport, IChemicalDegradeModel chemicalModel);

	void erode(CompoundMass erosionMass);
	
	/**
	 * @return the elementRef
	 */
	int getElementRef();

	/**
	 * @param elementRef the elementRef to set
	 */
	void setElementRef(int elementRef);

	/**
	 * @return the x [m]
	 */
	double getX();

	/**
	 * @return the y [m]
	 */
	double getY();
	
	/**
	 * 
	 * @return the cell area [m2]
	 */
	double getArea();

	/**
	 * @return the surface
	 */
	//CompoundMass getSurface();

	/**
	 * @return the set of layers
	 */
	IBedLayerModel getLayers();
	
	/**
	 * Get the bed load as a merged CompoundMass.
	 * 
	 * @return
	 */
	public CompoundMass getBedLoad();
	
	public CompoundMass getTotalMass();
	
	public double getThresholdMass();
	
}