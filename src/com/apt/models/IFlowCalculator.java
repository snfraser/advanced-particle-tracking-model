/**
 * 
 */
package com.apt.models;

/**
 * 
 */
public interface IFlowCalculator {
	
	/**
	 * Calculate the friction velocity at a position and time given the supplied surface roughness.
	 * @param x X coordinate of location [m].
	 * @param y Y coordinate of location [m].
	 * @param time Time at which value is required [ms].
	 * @param roughness Surface roughness [m].
	 * @return
	 */
	public double getFrictionVelocity(double x, double y, long time);

}
