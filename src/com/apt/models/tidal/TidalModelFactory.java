/**
 * 
 */
package com.apt.models.tidal;

import com.apt.cli.util.ConfigurationProperties;
import com.apt.models.ITidalModel;

/**
 * 
 */
public class TidalModelFactory {

	public static ITidalModel createTidalModel(ConfigurationProperties config) throws Exception {
		if (config.getProperty("Tidal.class").equalsIgnoreCase("FIXED_HEIGHT")) {
			double fixedTide = config.getDoubleValue("Tidal.fixedHeight");
			return new FixedTidalModel(fixedTide); 
		}
		return null;
		// Tidal.class=SPRING_NEAP, +Tidal.springHigh, Tidal.neapHigh, Tidal.springLow, Tidal.neapLow, Tidal.springOffsetDays
		//return new RegularSpringNeapTidalModel(sphi, splo, nphi, nplo, spoffset);
	}
}
