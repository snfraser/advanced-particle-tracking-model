/**
 * 
 */
package com.apt.models.tidal;

import com.apt.models.ITidalModel;

/**
 * 
 */
public class FixedTidalModel implements ITidalModel {

	private double constantDepth;
	
	/**
	 * @param constantDepth
	 */
	public FixedTidalModel(double constantDepth) {
		this.constantDepth = constantDepth;
	}

	@Override
	public double getTidalDepth(long time) {
		// TODO Auto-generated method stub
		return constantDepth;
	}

}
