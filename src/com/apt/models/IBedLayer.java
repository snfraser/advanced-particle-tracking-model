/**
 * 
 */
package com.apt.models;

import com.apt.data.BedLayerType;
import com.apt.data.CompoundMass;

/**
 * @author SA05SF
 *
 */
public interface IBedLayer {
	
	public CompoundMass getMass();

	//public void setLayerType(BedLayerType type);
	
	//public BedLayerType getLayerType();
	
	//public int getRef();
	
	public void deposit(CompoundMass mass);
	
	public void erode(CompoundMass eroded);
	
	public CompoundMass copyMass();
	
	public CompoundMass erodeAll();
	
	public double getCurrentTauCrit();
	
	public double getEquilibriumTauCrit();
	
	public void setCurrentTauCrit(double currentTauCrit); 
	
	public void setEquilibriumTauCrit(double equilibriumTauCrit); 

	public double getDensity();
}
