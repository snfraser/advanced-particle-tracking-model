/**
 * 
 */
package com.apt.models;

import com.apt.data.Turbulence;

/**
 * @author SA05SF
 *
 */
public interface ITurbulenceModel {

	/**
	 * Find the turbulence parameters at a position and time.
	 * @param x X position.
	 * @param y Y position.
	 * @param z Z position.
	 * @param time Time (s).
	 * @return
	 */
	public Turbulence getTurbulence(double x, double y, double z, long time);
	
	
}
