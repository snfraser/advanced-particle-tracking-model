/**
 * 
 */
package com.apt.models;

import com.apt.data.CarbonFraction;

/**
 * Models the decay of carbon.
 * 
 * @author SA05SF
 *
 */
public interface ICarbonDecayModel {
	
	CarbonFraction getCarbonFractions(double time);
	

}
