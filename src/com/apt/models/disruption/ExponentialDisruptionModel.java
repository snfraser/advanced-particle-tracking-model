/**
 * 
 */
package com.apt.models.disruption;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.apt.data.ChemicalComponent;
import com.apt.data.ChemicalComponentType;
import com.apt.data.CompoundMass;
import com.apt.data.Particle;
import com.apt.data.ParticlesState;
import com.apt.models.IDisruptionModel;

/**
 * A simple implementation of a disruption model where the probability of
 * break-up increases with the age of the particle . No account is taken of
 * turbulence, flow, or particle composition.
 * 
 * @author SA05SF
 *
 */
public class ExponentialDisruptionModel implements IDisruptionModel {

	//double B = 0.9; // asymptotic limit on probability (max).

	//double T1 = 4.0; // x-axis time offset ([mins]

	Map<String, Double> map = new HashMap<String, Double>();
	
	double b;
	double t1;
	
	/**
	 * @param b
	 * @param t1
	 */
	public ExponentialDisruptionModel(double b, double t1) {
		this.b = b;
		this.t1 = t1;
	}


	@Override
	public List<Particle> degrade(Particle p, long time) {

		List newParticles = new ArrayList<Particle>();

		String PID = p.getId();
		
		// reject all non-suspended particles...
		if (p.getState() != ParticlesState.SUSPEND) {
			map.remove(PID); // we dont want to track this particle from now onward
			newParticles.add(p);
			return newParticles;
		}
		
		// a particle breaks up with a probability dependent on its age.

		long age = time - p.getCreationTime();

		double pbreakup = calculateP(age);

		// this is the only value to be used for this point....
		double point = 0.0;
		
		if (map.containsKey(PID)) {
			point = map.get(PID);
		} else {
			point = Math.random();// pick a value
			map.put(PID, point);
		}
		
		
		// handle non-suspension particles and those which dont meet 
		// the random breakup point criterion
		if (point > pbreakup) {
			newParticles.add(p);
			return newParticles;
		}

		int np = 2 + (int) (pbreakup * 3.0); // 2 to 5 pcles
		for (int i = 0; i < np; i++) {
			Particle newp = new Particle();
			newp.setStartPosition(p.getX(), p.getY(), p.getZ());
			newp.setState(p.getState());
			newp.setType(p.getType());
			newp.setCreationTime(time);

			CompoundMass m = p.getMasses();
			Iterator<ChemicalComponent> ic = m.components();
			while (ic.hasNext()) {
				ChemicalComponent chem = ic.next();
				newp.addChemical(chem);
			}
			newParticles.add(newp);
		}

		return newParticles;
	}

	
	@Override
	public List<Particle> degrade(List<Particle> particles, long time) {
	
		List allNewParticles = new ArrayList<Particle>();
		
		for (Particle p : particles) {

			List<Particle> replacements = degrade(p, time);
			allNewParticles.addAll(replacements);
			
		}
		
		return allNewParticles;
		
	}
	
	
	/**
	 * Calculate probability of break-up given age.
	 * 
	 * @param t Time [ms].
	 * @return
	 */
	private double calculateP(long t) {
		// param C is in minutes		
		double tm = (double) t / 60000.0;
		return b / (1.0 + Math.exp(-(tm - t1)));
	}
	
	public int getMapSize() {return map.size();}

	public void docalc() {

		long t = 0;
		while (t < 10 * 60 * 1000L) {

			double p = calculateP(t);
			System.err.printf(" Time: %d s  : p= %f \n", t / 1000, p);

			t += 10000L;
		}

	}

	

}
