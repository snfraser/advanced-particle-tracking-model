/**
 * 
 */
package com.apt.models.disruption;

import com.apt.cli.util.ConfigurationProperties;
import com.apt.models.IDisruptionModel;

/**
 * 
 */
public class DisruptionModelFactory {

	public static IDisruptionModel createDisruptionModel(ConfigurationProperties config) throws Exception {
		//double B = 0.9; // asymptotic limit on probability (max).
		//double T1 = 4.0; // x-axis time offset ([mins]
		// Disruption.class=EXPONENTIAL + Disruption.asymptoticLimit, Disruption.timeOffsetMins
		if (config.getProperty("Disruption.class").equalsIgnoreCase("EXPONENTIAL")) {
			double b = config.getDoubleValue("Disruption.asymptoticLimit");
			double t1 = config.getDoubleValue("Disruption.timeOffsetMins");
			return new ExponentialDisruptionModel(b, t1);
		}
		return new DefaultDisruptionModel(); // dont use this....
	}
}
