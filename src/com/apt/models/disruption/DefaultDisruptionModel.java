/**
 * 
 */
package com.apt.models.disruption;

import java.util.ArrayList;
import java.util.List;

import com.apt.data.Particle;
import com.apt.models.IDisruptionModel;

/**
 * @author SA05SF
 *
 */
public class DefaultDisruptionModel implements IDisruptionModel {

	@Override
	public List<Particle> degrade(Particle p, long time) {
	
		List<Particle> newParticles = new ArrayList<Particle>();
		newParticles.add(p);
		return newParticles;
		
	}

	@Override
	public List<Particle> degrade(List<Particle> particles, long time) {
	
		return particles;
		
	}

}
