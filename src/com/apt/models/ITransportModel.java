package com.apt.models;

import java.util.List;

import com.apt.data.Particle;

public interface ITransportModel {

	public void move(Particle p, long time, long delta);
	
	
	
}
