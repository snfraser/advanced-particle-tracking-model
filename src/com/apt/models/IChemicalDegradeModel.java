/**
 * 
 */
package com.apt.models;

import com.apt.data.ChemicalComponent;
import com.apt.data.CompoundMass;

/**
 * A model which allows constituent chemical masses in a sediment to degrade into other chemical mass types.
 * 
 */
public interface IChemicalDegradeModel {

	/**
	 * Degrade the chemicals in the supplied compound mass.
	 * @param chemical The compound mass.
	 * @param delta The time during which the decay occurs (step-length) [ms].
	 */
	public void degrade(CompoundMass compound, long delta);
	
}
