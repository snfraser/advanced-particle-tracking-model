package com.apt.models;

import java.util.Iterator;
import java.util.List;

import com.apt.models.Cage;
import com.apt.models.cage.ICagePositionManager;

public interface ICageLayoutModel {

	
	public String getLayoutName();
	
	public ICagePositionManager getPositionManager();
	
	/**
	 * @return An iterator over the list of cages.
	 * @see java.util.List#iterator()
	 */
	List<Cage> cages();

}