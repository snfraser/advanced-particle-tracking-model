/**
 * 
 */
package com.apt.models.transport;

import com.apt.data.Particle;
import com.apt.data.PositionVector;

/**
 * Integrates the equation of motion for a particle.
 * 
 * @author SA05SF
 *
 */
public interface Integrator {
	
	/** Move the specified particle by one time-step.
	 * 
	 * @param p The particle.
	 * @param dt The times-tep (s).
	 * @return the new position.
	 * */
	public PositionVector move(Particle p, double time, double dt);

	public String getName();
	
}
