package com.apt.models.transport;

import com.apt.models.IFlowModel;

public class IntegratorFactory {

	public static Integrator getIntegrator(String name, IFlowModel flowModel) {
		if (name.equalsIgnoreCase("EULER")) {
			return new EulerIntegrator(flowModel);
		} else if (name.equalsIgnoreCase("RK4")) {
			return new RungeKutta4Integrator(flowModel);
		} else if (name.equalsIgnoreCase("RK2")) {
			return new RungeKutta2Integrator(flowModel);
		}
		return null;
	}

}
