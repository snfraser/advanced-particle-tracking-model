package com.apt.models.transport;

import com.apt.instrumentation.Instrumentation;
import com.apt.instrumentation.ParticleCellTracker;
import com.apt.models.IBathymetryModel;
import com.apt.models.IFlowModel;
import com.apt.models.ISeabedModel;
import com.apt.models.ISettlingModel;
import com.apt.models.ITidalModel;
import com.apt.models.ITimeModel;
import com.apt.models.ITurbulenceModel;
import com.apt.models.bathymetry.FixedBathymetryModel;
import com.apt.models.time.TimeModel;

public class TransportFactory {

	private ITimeModel timeModel;
	private IBathymetryModel bathymetryModel;
	private IFlowModel flowModel;
	private ITidalModel tidalModel;
	private ITurbulenceModel turbulenceModel;
	private ISettlingModel settlingModel;
	private ISeabedModel seabed;
	private Integrator integrator;

	
	/**
	 * @param timeModel
	 */
	public TransportFactory(ITimeModel timeModel, IBathymetryModel bathymetryModel, IFlowModel flowModel, ITidalModel tidalModel, ITurbulenceModel turbulenceModel, ISettlingModel settlingModel, ISeabedModel seabed, Integrator integrator) {
		this.timeModel = timeModel;
		this.bathymetryModel = bathymetryModel;
		this.flowModel = flowModel;
		this.tidalModel = tidalModel;
		this.turbulenceModel = turbulenceModel;
		this.settlingModel = settlingModel;
		this.seabed = seabed;
		this.integrator = integrator;
	}

	public TransportModel getTransportModel(Instrumentation instrumentation) {
		//Instrumentation instruments = instrumentController.getInstruments();
		return new TransportModel(bathymetryModel, flowModel, tidalModel, turbulenceModel, settlingModel, seabed, integrator, instrumentation);
	}
	
}
