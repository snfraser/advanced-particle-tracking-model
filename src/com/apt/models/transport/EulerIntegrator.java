/**
 * 
 */
package com.apt.models.transport;

import com.apt.data.Cell;
import com.apt.data.FlowVector;
import com.apt.data.Particle;
import com.apt.data.ParticleTrackPoint;
import com.apt.data.PositionVector;
import com.apt.models.IFlowModel;


/**
 * @author SA05SF
 *
 */
public class EulerIntegrator implements Integrator {

	private IFlowModel flowModel;
	
	/**
	 * @param flowModel
	 */
	public EulerIntegrator(IFlowModel flowModel) {
		super();
		this.flowModel = flowModel;
	}

	@Override
	public PositionVector move(Particle p, double time, double dt) {
		
		double step = dt/1000.0;
		
		PositionVector current = p.getCurrentPosition();
		
		Cell flowCell = p.getCurrentCell(); // may be null 
		FlowVector uvw = flowModel.getFlow(p.getX(), p.getY(), p.getZ(), (long)time);
		
		double dx = uvw.getU()*step;
		double dy = uvw.getV()*step;
		double dz = uvw.getW()*step;
		PositionVector offset = new PositionVector(dx, dy, dz);
		PositionVector newPosition = current.add(offset);
		return newPosition;
	}

	@Override
	public String getName() {
		return "EULER";
	}

}
