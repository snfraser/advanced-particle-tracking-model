/**
 * 
 */
package com.apt.models.transport;

import com.apt.models.IBathymetryModel;
import com.apt.models.IBedTransportModel;
import com.apt.models.IFlowModel;
import com.apt.models.ISeabedModel;
import com.apt.models.ITimeModel;
import com.apt.models.ITurbulenceModel;
import com.apt.data.Cell;
import com.apt.data.FlowVector;
import com.apt.data.Node;
import com.apt.data.Particle;
import com.apt.data.ParticlesState;
import com.apt.instrumentation.Instrumentation;

/**
 * @author SA05SF
 *
 */
public class BedTransportModel implements IBedTransportModel {

	static double SEAWATER_DENSITY = 1027.0;
	static double GRAVITY_CONST = 9.81;
	static double KINEMATIC_VISCOSITY = 0.000001212;
	static double VON_KARMAN = 0.41;
	static double H0_Z0 = 250.0;

	static double FF = 0.075; // fudge factor to handle saltation jumps

	private ITimeModel timeModel;
	private IBathymetryModel bathyModel;
	private IFlowModel flowModel;
	private ITurbulenceModel turbulenceModel;
	private ISeabedModel seabed;
	// private ParticleCellTracker ptrack;
	private Integrator integrator;
	private Instrumentation instruments;
	
	/**
	 * @param flowModel
	 */
	public BedTransportModel(IBathymetryModel bathyModel, IFlowModel flowModel, ITurbulenceModel turbulenceModel,
			ISeabedModel seabed, Integrator integrator, Instrumentation instruments) {
		this.bathyModel = bathyModel;
		this.flowModel = flowModel;
		this.turbulenceModel = turbulenceModel;
		// this.ptrack = ptrack;
		this.integrator = integrator;
		this.instruments = instruments;
	}

	@Override
	public void move(Particle p, long time, long dt) {

		double oldx = p.getX();
		double oldy = p.getY();
		double oldz = p.getZ();

		Cell oldcell = p.getCurrentCell();
		// System.err.printf("BTM:: move: Particle at (%f,%f) cell: %d \n", oldx, oldy,
		// oldcell != null ? oldcell.ref: "-1");

		// flow in/around the boundary layer : fm(x, y, sigma, t)
		// AAAAAAARGH!!!!
		FlowVector boundaryFlow = flowModel.getBoundaryFlow(p.getX(), p.getY(), time);
		
		// now work out how we can move this chappy
		
		// 1. can it roll or lift?
		double ds = p.getDiameter()
				* Math.pow((p.getDensity() / SEAWATER_DENSITY - 1.0) * GRAVITY_CONST / KINEMATIC_VISCOSITY, 1.0 / 3.0);
		double thetaMotion = 0.3 / (1.0 + 1.2 * ds) + 0.055 * (1.0 - Math.exp(-0.02 * ds)); // soulsby
		double thetaLift = 0.3 / (1.0 + ds) + 0.1 * (1.0 - Math.exp(-0.05 * ds)); // soulsby

		// TODO use Z0 from seabedmodel here and flow.getbflowheight, flowcalc.getfrictionvelocity
		double bottomFrictionVelocity = VON_KARMAN * boundaryFlow.getMagnitude() / Math.log(H0_Z0);
		double tauB = SEAWATER_DENSITY * Math.pow(bottomFrictionVelocity, 2.0);
		double theta = tauB / ((p.getDensity() - SEAWATER_DENSITY) * GRAVITY_CONST * ds);
		boolean canroll = (theta > thetaMotion);
		boolean canlift = (theta > thetaLift);
		//if (canlift || canroll)
			//System.err.printf(
				//	"BTM: P: %s Flow: %f Bfv: %f tauB: %f Pa :: thetaL: %f thetaM: %f theta: %f canroll: %b canlift: %b\n ",
				//	p.getType().name(), boundaryFlow.getMagnitude(), bottomFrictionVelocity, tauB, thetaLift, thetaMotion, theta, canroll,
				//	canlift);
		
		// TEMP swicth off rolling rule during the search for missing mass !!!!!
	
		//if (!canroll) //bail out if no rolling for now... 
			//return;
		
		// 2. can it slide ?

		
		// for now just let everything move at a fixed rate relative to flow speed // 0.035 as fudge factor
		double jumpDistanceX = (FF + Math.random() * 0.035) * boundaryFlow.getU() * dt / 1000; // m
		double jumpDistanceY = (FF + Math.random() * 0.035) * boundaryFlow.getV() * dt / 1000; // m
		// System.err.printf("BTC:: move particle: %f,%f \n", jumpDistanceX,
		// jumpDistanceY);
		double x = oldx + jumpDistanceX;
		double y = oldy + jumpDistanceY;
		double z = oldz;

		// did we move cells - TODO we need to handle beaching and OOD properly here !!!
		Cell newcell = bathyModel.getMesh().findCell(x, y, oldcell); // using oldcell to start the search - irrelevant
		
		if (bathyModel.isOutsideDomain(x, y)) {
			System.err.printf("BTM:: particle assumed lost, new cell is not defined for pcle at: (%f,%f,%f) \n", x, y, z);
			// push back to previous location or is it lost?? reg grid - its lost
			p.setState(ParticlesState.LOST); // we have not moved the pcle position from its old one - is this right??
			p.setLostTime(time);
			return;
		}
		
		// if regular grid
		if (newcell == null) {
		//
		}
		
		// ok its not lost so record new location, may still be beached....
		p.updatePosition(x, y, z); // records last position and new position
		p.setCurrentCell(newcell);

		// check if the pcle has beached, leave it at its old position till it consolidates or rolls off in another direction
		if (bathyModel.getDepth(x, y) > 0.0) {
			double depth = bathyModel.getDepth(x, y);
			//System.err.printf("BTC:: move particle to: (%f,%f) z: %f, particle has beached in depth %f\n", x,y,z, depth);
			p.updatePosition(oldx, oldy, depth);
			p.setCurrentCell(oldcell);
		}
	

	}

}
