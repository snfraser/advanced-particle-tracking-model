/**
 * 
 */
package com.apt.models.transport;

import com.apt.instrumentation.Instrumentation;
import com.apt.models.IBathymetryModel;
import com.apt.models.IBedTransportModel;
import com.apt.models.IFlowModel;
import com.apt.models.ISeabedModel;
import com.apt.models.ITimeModel;
import com.apt.models.ITurbulenceModel;

/**
 * @author SA05SF
 *
 */
public class BedTransportFactory {

	private ITimeModel timeModel;
	private IBathymetryModel bathymetryModel;
	private IFlowModel flowModel;
	private ITurbulenceModel turbulenceModel;
	private ISeabedModel seabed;
	//private ParticleCellTracker tracker;
	private Integrator integrator;

	
	/**
	 * @param timeModel
	 */
	public BedTransportFactory(ITimeModel timeModel, IBathymetryModel bathymetryModel, IFlowModel flowModel, ITurbulenceModel turbulenceModel, ISeabedModel seabed, Integrator integrator) {
		this.timeModel = timeModel;
		this.bathymetryModel = bathymetryModel;
		this.flowModel = flowModel;
		this.turbulenceModel = turbulenceModel;
		this.seabed = seabed;
		this.integrator = integrator;
	}

	public IBedTransportModel getBedTransportModel(Instrumentation instruments) {
		return new BedTransportModel(bathymetryModel, flowModel, turbulenceModel, seabed, integrator, instruments);
		//return new NoBedTransportModel();
	}
	
	
}
