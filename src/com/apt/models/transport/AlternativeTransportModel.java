/**
 * 
 */
package com.apt.models.transport;

import java.util.List;

import com.apt.data.Cell;
import com.apt.data.CrossingType;
import com.apt.data.FlowVector;
import com.apt.data.Mesh;
import com.apt.data.Particle;
import com.apt.data.ParticlesState;
import com.apt.data.Turbulence;
import com.apt.models.IBathymetryModel;
import com.apt.models.IFlowModel;
import com.apt.models.ITidalModel;
import com.apt.models.ITimeModel;
import com.apt.models.ITransportModel;
import com.apt.models.ITurbulenceModel;

/**
 * 
 */
public class AlternativeTransportModel implements ITransportModel {

	private ITimeModel timeModel;
	private IBathymetryModel bathyModel;
	private IFlowModel flowModel;
	private ITidalModel tidalModel;
	private ITurbulenceModel turbulenceModel;
	//private ParticleCellTracker ptrack;
	private Integrator integrator;
	private Mesh mesh;
	
	
	/**
	 * @param timeModel
	 * @param bathyModel
	 * @param flowModel
	 * @param tidalModel
	 * @param turbulenceModel
	 * @param integrator
	 */
	public AlternativeTransportModel(ITimeModel timeModel, IBathymetryModel bathyModel, IFlowModel flowModel,
			ITidalModel tidalModel, ITurbulenceModel turbulenceModel, Integrator integrator) {
		super();
		this.timeModel = timeModel;
		this.bathyModel = bathyModel;
		this.flowModel = flowModel;
		this.tidalModel = tidalModel;
		this.turbulenceModel = turbulenceModel;
		this.integrator = integrator;
		mesh = bathyModel.getMesh();
	}

	@Override
	public void move(Particle p, long time, long step) {
		
		double xlast = p.getX();
		double ylast = p.getY();
		double zlast = p.getZ();
	
		Cell lastCell = p.getCurrentCell(); // will initially be null
		
		if (lastCell == null) {
			lastCell = bathyModel.getCell(p.getX(), p.getY());
			p.setCurrentCell(lastCell);
		}
	
		FlowVector flow = flowModel.getFlow(p.getX(), p.getY(), p.getZ(), time);
		if (flow == null)
			throw new IllegalArgumentException(String.format("no flow available using: %s at: %.4f, %.4f at t=%d",
					flow.getClass().getName(),p.getX(), p.getY(), time));
		
		double settling = getSettling(p);
		
		Turbulence turb = turbulenceModel.getTurbulence(p.getX(), p.getY(), p.getZ(), time);
		
		// alt turb calc
		double turbx = (Math.random() - 0.5) * Math.sqrt(2.0*turb.getxParam()*step/1000.0);
		double turby = (Math.random() - 0.5) * Math.sqrt(2.0*turb.getyParam()*step/1000.0);
		double turbz = (Math.random() - 0.5) * Math.sqrt(2.0*turb.getzParam()*step/1000.0);
		
		// track the particle
		double x = p.getX();
		double y = p.getY();
		double z = p.getZ();

		double newx = p.getX() + flow.getU()*step/1000 + turbx ;
		double newy = p.getY() + flow.getV()*step/1000 + turby;
		double newz = p.getZ() + (flow.getW() + settling)*step/1000 + turbz;
		

		// what cell are we in now moved to?
		Cell c = mesh.findCell(x, y, lastCell);

		// in a real cell?
		if (c != null) {
			p.updatePosition(x, y, z);

			// did we land
			if (c.zc > z) {

				// particle landed.
				FinalPoint point = new FinalPoint(x, y, c, CrossingType.LAND);
				System.err.printf("Particle has hit seabed at depth %f in cell depth: %f\n", z, c.zc);

				p.setLandingPosition(x, y, z);
				p.setLandingTime(time);
				p.setState(ParticlesState.ONBED);
				return; // particle has been updated
			}
		}

		if (c == null || c.zc > 0.0) { // ood or inside a land cell
			System.err.printf(
					"Particle left domain at: t=%f, position: (%f, %f, %f) LKC: %d : investigating edge crossings...\n",
					time, x, y, z, lastCell.ref);

			CrossingType isect = lastCell.intersectEdges(xlast, ylast, x, y);
			// System.err.printf("Cells own intersect alg gives: %s \n", isect.name());

			if (isect == CrossingType.SEA) {
				FinalPoint point = trackToEnd(xlast, ylast, zlast, lastCell, time, step / 2.0);
				lastCell = point.cell;
				xlast = point.x;
				ylast = point.y;
				//zlast = point.z;
				CrossingType crossing = point.crossing;
				if (crossing == CrossingType.LAND) {
					p.updatePosition(xlast, ylast, p.getZ()); // Z???
					p.setLandingPosition(xlast, ylast, p.getZ()); // Z ??
					p.setLandingTime(time);
					p.setState(ParticlesState.ONBED);
				} else {
					p.updatePosition(xlast, ylast, p.getZ()); // Z???
					p.setState(ParticlesState.LOST);
					p.setLostTime(time);
				}

				return;
			} else {
				// System.err.printf("Completed tracking, last cell crossed is: %d \n",
				// lastc.ref);
				FinalPoint point = new FinalPoint(xlast, ylast, lastCell, isect);

				if (isect == CrossingType.LAND) {
					p.updatePosition(xlast, ylast, p.getZ()); // Z???
					p.setLandingPosition(xlast, ylast, p.getZ()); // Z ??
					p.setLandingTime(time);
					p.setState(ParticlesState.ONBED);
				} else {
					p.updatePosition(xlast, ylast, p.getZ()); // Z???
					p.setLostTime(time);
					p.setState(ParticlesState.LOST);
				}
				return;
			}

		}


	}
	
	/**
	 * Track a particle to the end...
	 * @param startx X start.
	 * @param starty Y start.
	 * @param step Step length [ms].
	 * @return Final track point within domain.
	 */
	private FinalPoint trackToEnd(double startx, double starty, double startz, Cell startCell, long startTime, double step) {

		double x = startx;
		double y = starty;
		double z = startz;
		long time = startTime;

		double dt = step;
		Cell c = mesh.findCell(x, y, startCell); 

		Cell lastCell = c;
		double lastx = x;
		double lasty = y;
		double lastz = z;
		long lastTime = time;
		
		boolean notdone = true;
		// repeat tracking till we have found a suitable edge crossing
		while (notdone) {
			
			x = lastx;
			y = lasty;
			z = lastz;
			time = lastTime;
			
			c = mesh.findCell(x, y, lastCell);
		
			int nm = 0;
			// track the particle from its last known position to ood or land
			while (c != null) {
				nm++;
				
				FlowVector flow = flowModel.getFlow(x, y, z, time);
				
				x += flow.getU()*step/1000;
				y += flow.getV()*step/1000;
				z += flow.getW()*step/1000; // plus settling
				time += dt;
				c = bathyModel.getCell(x, y);
		
				if (c != null) {
					lastCell = c;
					lastx = x;
					lasty = y;
					lastz = z;
					lastTime = time;
				}
			}
		
			CrossingType isect = lastCell.intersectEdges(startx, starty, x, y);

			// we crossed into land or ood so done
			// TODO If the final leap is still quite large we might try to half the timestep
			//      and attempt to get closer to the edge
			// NOTE we could still leap a block of land cells if fast flow.
			
			if (isect == CrossingType.LAND || isect == CrossingType.OPEN) {
				return new FinalPoint(lastx, lasty, lastCell, isect);
			}
			// if we find a SEA edge then we have jumped several internal cells
			// half the time step and repeat
			dt /= 2.0;

		}

		return null;

	}

	private double getSettling(Particle p) {
		switch (p.getType()) {
		case FAECES: 
			return -0.03;
		case FEED:
			return -0.095;
		case MUD:
			return -0.02;
		case SAND:
			return -0.25;
		}
		return 0.0;
	}

	
	private class FinalPoint {
		double x;
		double y;
		Cell cell;
		CrossingType crossing;

		/**
		 * @param x
		 * @param y
		 * @param cell
		 * @param crossing
		 */
		public FinalPoint(double x, double y, Cell cell, CrossingType crossing) {
			this.x = x;
			this.y = y;
			this.cell = cell;
			this.crossing = crossing;
		}

	}

}
