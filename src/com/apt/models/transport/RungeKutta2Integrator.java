/**
 * 
 */
package com.apt.models.transport;

import com.apt.data.Cell;
import com.apt.data.FlowVector;
import com.apt.data.Particle;
import com.apt.data.PositionVector;
import com.apt.models.IFlowModel;

/**
 * 
 */
public class RungeKutta2Integrator implements Integrator {

	private IFlowModel flowModel;
	
	/**
	 * @param flowModel
	 */
	public RungeKutta2Integrator(IFlowModel flowModel) {
		super();
		this.flowModel = flowModel;
	}
	
	@Override
	/**
	 * @param p A particle.
	 * @param time Time (ms).
	 * @param dt Timestep (ms).
	 */
	public PositionVector move(Particle p, double time, double dt) {
		double step = dt/1000.0; // step size in seconds
		//System.err.printf("RK2: (%f, %f) t: %f dt: %f \n", p.getX(), p.getY(), time, dt);
		
		PositionVector current = p.getCurrentPosition();
		
		Cell flowCell = p.getCurrentCell(); // may be null 
		// flow.getFlow(x,y,z,t) // xyz [m], t [ms]
		FlowVector k1 = flowModel.getFlow(p.getX(),                    p.getY(),                    p.getZ(), (long)time);
		FlowVector k2 = flowModel.getFlow(p.getX() + step*k1.getU(),   p.getY() + step*k1.getV(),   p.getZ(), (long)(time+dt));
		
		//System.err.printf("RK2: k1: %f, k2: %f \n", k1.getU(), k2.getU());
		double nx = current.getX() + step*(k1.getU() + k2.getU())/2.0;
		double ny = current.getY() + step*(k1.getV() + k2.getV())/2.0;
		PositionVector newPosition = new PositionVector(nx, ny, p.getZ());
		
		return newPosition;
	}
	
	
	@Override
	public String getName() {
		return "RK2";
	}
}
