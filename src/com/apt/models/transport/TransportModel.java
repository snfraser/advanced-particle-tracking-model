/**
 * 
 */
package com.apt.models.transport;

import java.util.List;

import com.apt.data.Cell;
import com.apt.data.FlowVector;
import com.apt.data.Particle;
import com.apt.instrumentation.Instrumentation;
import com.apt.instrumentation.ParticleCellTracker;
import com.apt.data.ParticleClass;
import com.apt.data.ParticlesState;
import com.apt.data.PositionVector;
import com.apt.data.Turbulence;
import com.apt.models.IBathymetryModel;
import com.apt.models.IFlowModel;
import com.apt.models.ISeabedModel;
import com.apt.models.ISettlingModel;
import com.apt.models.ITidalModel;
import com.apt.models.ITimeModel;
import com.apt.models.ITransportModel;
import com.apt.models.ITurbulenceModel;
import com.apt.models.bathymetry.FixedBathymetryModel;


/**
 * Provides the particle transport.
 * 
 * @author SA05SF
 *
 */
public class TransportModel implements ITransportModel {

	private ITimeModel timeModel;
	private IBathymetryModel bathyModel;
	private IFlowModel flowModel;
	private ITidalModel tidalModel;
	private ITurbulenceModel turbulenceModel;
	//private ParticleCellTracker ptrack;
	private ISettlingModel settlingModel;
	private ISeabedModel seabed;
	private Integrator integrator;
	
	private Instrumentation instruments;
	
	/**
	 * @param flowModel
	 */
	public TransportModel(IBathymetryModel bathyModel, IFlowModel flowModel, ITidalModel tidalModel, ITurbulenceModel turbulenceModel, ISettlingModel settlingModel, ISeabedModel seabed, Integrator integrator, Instrumentation instruments) {
		this.bathyModel = bathyModel;
		this.flowModel = flowModel;
		this.tidalModel = tidalModel;
		this.turbulenceModel = turbulenceModel;	
		this.settlingModel = settlingModel;
		this.seabed = seabed;
		this.integrator = integrator;
		this.instruments = instruments;
	}

	/**
	 * Move a particle at a specified time and with specified time-step
	 * @param p The particle to move.
	 * @param time Time (ms since start of simulation run).
	 * @param step Step size (ms).
	 */
	public void move(Particle p, long time, long step) {

		// check pcle is still in water column
		if (p.getState() != ParticlesState.SUSPEND)
			return;

		//long time = timeModel.time();
		//long steps = timeModel.getStep() / 1000; // step in seconds

		// settling model for particles.. just some distribution or maybe particle mass/shape dependant
		double settling = settlingModel.getSettlingSpeed(p);
		
		// TODO: Find the last cell the particle was in (when new this is null)
		//Cell lastCell = ptrack.getCell(p);
	
		Cell lastCell = p.getCurrentCell(); // will initially be null
		
		if (lastCell == null) {
			lastCell = bathyModel.getCell(p.getX(), p.getY()); // may still be null
			//System.err.printf("Trans:: timestep: %d use temp cell %d with depth: %f \n", time, lastCell.ref, lastCell.centreDepth);
			p.setCurrentCell(lastCell);
		}
		//System.err.printf("trans::move: lastflow cell was: %i \n", lastCell != null ? lastCell.ref: "none") ;
		FlowVector flow = flowModel.getFlow(p.getX(), p.getY(), p.getZ(), time);
		if (flow == null)
			throw new IllegalArgumentException(String.format("no flow available using: %s at: %.4f, %.4f at t=%d",
					flow.getClass().getName(),p.getX(), p.getY(), time));
		
		//instruments.updateFlow(time, flow.getPlaneMagnitude());
		
		Turbulence turb = turbulenceModel.getTurbulence(p.getX(), p.getY(), p.getZ(), time);
	
		// Dispersion due to turbulence - the turb parameters are: kx, ky, kz
		double turbx = (Math.random() - 0.5) * Math.sqrt(2.0*turb.getxParam()*step/1000.0);
		double turby = (Math.random() - 0.5) * Math.sqrt(2.0*turb.getyParam()*step/1000.0);
		double turbz = (Math.random() - 0.5) * Math.sqrt(2.0*turb.getzParam()*step/1000.0);

	
		// alt trub calc
		double newx = p.getX() + flow.getU()*step/1000 + turbx ;
		double newy = p.getY() + flow.getV()*step/1000 + turby;
		double newz = p.getZ() + (flow.getW() + settling)*step/1000 + turbz;
		
		//System.err.printf("TMM: %d Flow: U: %f V: %f Turb: [%f %f] \n", time/1000, flow.getU(), flow.getV(), turbx, turby);
		
		// TODO !!!!! dont use this position yet !!!!!
		//PositionVector newPosition = integrator.move(p, time, step); 
		// NOTE: 1 - time and step are both ms
		// NOTE: 2 - this is the new position, the particle itself has not been moved yet
		
		// 1. check if weve exiting the domain
		// 2. check if weve hit the land
		// 3. check if weve hit the seabed
		// 4. check if weve intercepted some fixed structure (enc-cage-side/catcher-cone/enc-cage-bottom)
		
		// find the cell the particle is moving into using mesh search and last known position
		Cell newCell = bathyModel.getCell(newx, newy);
		
		
		// 1. Exit domain or hit land, carefull! if tri mesh possible to jump a whole cell into exterior 
		/*if (newFlowCell == null) {
			CrossingType crossing = lastFlowCell.intersectEdges(p.getX(), p.getY(), newx, newy);
			if (crossing == CrossingType.LAND) {
				
			} else if (crossing == CrossingType.OPEN) {
				
			}
		}*/
		
		// 2. Seabed intercept
		
		
		// 3. fixed structure intercept - typical flow moves 6m (upto 24m for vfast) in a timestep
		// leaving cage side, exiting base, hitting catcher cone, entering cage side
		
		p.updatePosition(newx, newy, newz);
		p.setCurrentCell(newCell); // THIS COULD BE NULL!!!
	
		
		// if we went out of domain then 
		// 1. check the cell dimension re the jump size - if too big (xn) chop the timestep by 2n
		// and re calc at shorter steps, at ech short step check for ood and a then a valid crossing
		
		// check for lost to domain, if it crosses the boundary whatever the height from seabed
		//if (bathyModel.isOutsideDomain(newx, newy) || p.getCurrentCell() == null) { // watch the null this could be on land!!
		if (bathyModel.isOutsideDomain(newx, newy)) {
			//System.err.printf("Particle lost at: %d  (%f, %f) \n", time, newx, newy);
			p.setState(ParticlesState.LOST);
			p.setLostTime(time);
			// instr.particleLost(p, time);
		} else {

			// check for intercept use seabed-intercept model
			// TODO: depth = bathy.getDepth(p, thiscell) - saves having to search for cell
			double seabedDepth = bathyModel.getDepth(p.getX(), p.getY()) + tidalModel.getTidalDepth(time);
			if (newz < seabedDepth) {
				newz = seabedDepth;
				p.setState(ParticlesState.ONBED);
				p.setLandingTime(time);
				p.updatePosition(newx, newy, newz); 
				p.setLandingPosition(p.getX(), p.getY(), newz);
			}
		}
		
	}


}
