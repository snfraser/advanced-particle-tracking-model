/**
 * 
 */
package com.apt.models.transport;

import com.apt.data.Particle;
import com.apt.models.IBedTransportModel;

/**
 * @author SA05SF
 *
 */
public class NoBedTransportModel implements IBedTransportModel {

	@Override
	public void move(Particle p, long time, long dt) {
		// DO NOTHING
	}

}
