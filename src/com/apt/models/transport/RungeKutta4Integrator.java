/**
 * 
 */
package com.apt.models.transport;

import com.apt.data.Cell;
import com.apt.data.FlowVector;
import com.apt.data.Particle;
import com.apt.data.PositionVector;
import com.apt.models.IFlowModel;


/**
 * Implements the 4th order Runge-Kutta integration (RK4).
 * 
 * @author SA05SF
 *
 */
public class RungeKutta4Integrator implements Integrator {

	private IFlowModel flowModel;
	
	
	/**
	 * @param flowModel
	 */
	public RungeKutta4Integrator(IFlowModel flowModel) {
		super();
		this.flowModel = flowModel;
	}

	@Override
	/**
	 * @param p A particle.
	 * @param time Time (ms).
	 * @param dt Timestep (ms).
	 */
	public PositionVector move(Particle p, double time, double dt) {
		double step = dt/1000.0; // step size in seconds
		//System.err.printf("RK4: (%f, %f) t: %f dt: %f \n", p.getX(), p.getY(), time, dt);
		
		PositionVector current = p.getCurrentPosition();
		
		Cell flowCell = p.getCurrentCell(); // may be null 
		// flow.getFlow(x,y,z,t) // xyz [m], t [ms]
		FlowVector k1 = flowModel.getFlow(p.getX(),                    p.getY(),                    p.getZ(), (long)time);
		FlowVector k2 = flowModel.getFlow(p.getX() + step*k1.getU()/2, p.getY() + step*k1.getV()/2, p.getZ(), (long)(time+dt/2));
		FlowVector k3 = flowModel.getFlow(p.getX() + step*k2.getU()/2, p.getY() + step*k2.getV()/2, p.getZ(), (long)(time+dt/2));
		FlowVector k4 = flowModel.getFlow(p.getX() + step*k3.getU(),   p.getY() + step*k3.getV(),   p.getZ(), (long)(time+dt));
		
	
		
		//System.err.printf("RK4: k1: %f, K2: %f, K3: %f, K4: %f \n", k1.getU(), k2.getU(), k3.getU(), k4.getU());
		double nx = current.getX() + step*(k1.getU() + 2*k2.getU() + 2*k3.getU() + k4.getU())/6.0;
		double ny = current.getY() + step*(k1.getV() + 2*k2.getV() + 2*k3.getV() + k4.getV())/6.0;
		PositionVector newPosition = new PositionVector(nx, ny, p.getZ());
		
		return newPosition;
	}

	@Override
	public String getName() {
		return "RK4";
	}

	
}
