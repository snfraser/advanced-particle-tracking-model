package com.apt.gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.io.File;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import com.apt.models.Cage;
import com.apt.data.ChemicalComponent;
import com.apt.data.ChemicalComponentType;
import com.apt.data.Particle;
import com.apt.data.ParticlesState;
import com.apt.maths.contour.ConrecContourer;
import com.apt.maths.contour.ContourSegment;
import com.apt.models.IBathymetryModel;
import com.apt.models.IBedCellModel;
import com.apt.models.IBedLayoutModel;
import com.apt.models.ICageLayoutModel;
import com.apt.models.bathymetry.FixedBathymetryModel;
import com.apt.models.bed.BedCellModel;
import com.apt.models.bed.RegularGridBedLayoutModel;
import com.apt.models.cage.CageLayoutModel;
import com.apt.models.cage.ICagePositionManager;

public class MassDepositionPanel extends JPanel {

	// Cell width and length !!!!
	static final double DX = 20.0;
	static final double DY = 20.0;
	
	private final double bb = 1000.0;
	private final Color[] colors = new Color[] { Color.white, Color.blue.brighter(), Color.blue, Color.green,
			Color.yellow, Color.orange.darker(), Color.orange, Color.red };

	final Color color12 = new Color(97, 16, 0);
	final Color color11 = new Color(122, 13, 0);
	final Color color10 = new Color(145, 9, 0);
	final Color color09 = new Color(165, 4, 0);
	final Color color08 = new Color(183, 0, 0);
	final Color color07 = new Color(194, 26, 24);
	final Color color06 = new Color(204, 52, 48);
	final Color color05 = new Color(214, 78, 72);
	final Color color04 = new Color(222, 103, 96);
	final Color color03 = new Color(230, 129, 120);
	final Color color02 = new Color(236, 153, 143);
	final Color color01 = new Color(242, 177, 167);
	final Color color00 = new Color(247, 200, 191);
	// final Color colorzz = new Color(255, 255, 255);
	final Color colorzz = new Color(252, 223, 215);

	private final Color[] colorMap = new Color[] { colorzz, color00, color01, color02, color03, color04, color05,
			color06, color07, color08, color09, color10, color11, color12, Color.BLUE, Color.BLUE.darker() };

	private final Color BG_COLOR = Color.WHITE;
	private final Color LAND_COLOR = Color.GRAY.brighter();
	private final Color CONTOUR_COLOR = Color.MAGENTA;

	private final Font TFONT = new Font("Serif", Font.PLAIN, 18);
	private final Font KFONT = new Font("Serif", Font.PLAIN, 8);

	IBathymetryModel bathymetry;

	private ICageLayoutModel layout;

	// int[][] surface;
	int nx;
	int ny;

	private double dx;
	private double dy;
	private double x0;
	private double y0;
	
	private double obsx = 0.0;
	private double obsy = 0.0;

	private IBedLayoutModel bed;
	private ICagePositionManager pm;

	private long time;

	/** Water-column particles. */
	private List<Particle> wcParticles;

	/** Water column mass. */
	private double[][] wcMass;
	private double maxMass = 0.0;
	private double susMass = 0.0;
	private int nSusCells = 0;

	double[][] bathygrid;
	double[] xa;
	double[] ya;

	ConrecContourer contourer = new ConrecContourer();

	Map<Double, LinkedList<ContourSegment>> contours;

	/**
	 * @param bathymetry
	 * @param layout
	 */
	public MassDepositionPanel(IBathymetryModel bathymetry, ICageLayoutModel layout) {
		this.bathymetry = bathymetry;
		this.layout = layout;
		this.pm = layout.getPositionManager();
		nx = (int) (bathymetry.getDomainWidth() / DX)+1;
		ny = (int) (bathymetry.getDomainHeight() / DY)+1;
		dx = bathymetry.getDomainWidth();
		dy = bathymetry.getDomainHeight();
		x0 = bathymetry.getDomainX0();
		y0 = bathymetry.getDomainY0();
		
		System.err.printf("Massdp: Domain: start: (%f, %f) width: (%f x %f) \n", x0, y0, dx, dy);
		System.err.printf("Massdp size: %d x %d \n", nx, ny);
		
		wcMass = new double[ny][nx];

		// ImageIcon icon = ImageIO.read(new
		// File(ClassLoader.getSystemResourceAsStream("image/button1.png")));

		createBathyGrid();
		// fill out the grid

		contours = new HashMap<Double, LinkedList<ContourSegment>>();

	}

	private void createBathyGrid() {
		System.err.printf("MassDP:: create bathy grid size::[%dx%d] \n", nx, ny);
		bathygrid = new double[nx][ny];
		xa = new double[nx];
		ya = new double[ny];
		for (int j = 0; j < ny; j++) {
			double y = y0 + DY * j;
			ya[j] = y;
			for (int i = 0; i < nx; i++) {
				double x = x0 + DX * i;
				xa[i] = x;
				
				bathygrid[i][j] = bathymetry.getDepth(x, y);
			}
		}
		
	}

	@Override
	public void paint(Graphics g) {
		super.paint(g);

		double maxmass = 0.0;

		// Plot any land areas and depth
		showBathymetry(g);

		// water column suspended mass ONLY if not displaying BED
		if (bed == null) {
			showSuspendedMatter(g);
		}

		// find cell with highest mass content

		// final or intermediate bedmass
		if (bed != null) {
			maxmass = findMaxMass();
			showFinalDeposition(g, maxmass);
		}

		// paint boundary
		showCrosshairs(g);

		// paint cages
		showCages(g);

		// paint colorbar (SE)
		showColorBar(g, maxmass);

		// contours....not in final deposition plot
		if (bed == null) {
			showContours(g, -20.0);
			showContours(g, -30.0);
			showContours(g, -50.0);
			showContours(g, -70.0);
			//showContours(g, -90.0);
			// contours(g, -100.0);
		}

		// paint timestamp
		showData(g);

	}

	private int getScreenX(double x) {

		double ww = (double) getSize().width;
		int gx = (int) (ww * (x - x0) / dx);
		return gx;

	}

	private int getScreenY(double y) {

		double hh = (double) getSize().height;
		int gy = (int) (hh * (1.0 - (y - y0) / dy));
		return gy;
	}

	private double findMaxMass() {
		double maxmass = 0.0;
		double maxx = 0.0;
		double maxy = 0.0;
		int nc0 = 0;
		int nc1 = 0;
		int nc10 = 0;
		int nc100 = 0;
		// find maximum mass in a cell...
		List<IBedCellModel> cells = bed.getCells();
		for (IBedCellModel cell : cells) {

			double cellmass = cell.getTotalMass().getComponentMass(ChemicalComponentType.SOLIDS);

			// ChemicalComponent cc =
			// cell.getSurface().getComponent(ChemicalComponentType.SOLIDS);
			// ChemicalComponent cc =
			// cell.getLayers().getSurfaceLayer().getMass().getComponent(ChemicalComponentType.SOLIDS);
			// if (cc != null) {
			// double cellmass = cc.getMass();
			// cell.getSurface().getComponentMass(ChemicalComponentType.SOLIDS);
			if (cellmass > maxmass) {
				maxmass = cellmass;
				maxx = cell.getX();
				maxy = cell.getY();
			}

			if (cellmass > 0.0)
				nc0++;
			if (cellmass > 1.0)
				nc1++;
			if (cellmass > 10.0)
				nc10++;
			if (cellmass > 100.0)
				nc100++;
			// }
		}
		//System.err.printf("MDP:: max mass in a cell is: %f kg at: (%f, %f) %d cells > 0.1kg, %d cells > 1.0 kg, %d cells > 10.0 kg, %d cells > 100.0 kg\n", 
			//	maxmass, maxx,
			//	maxy, nc0, nc1, nc10, nc100);

		return maxmass;
	}

	private void showData(Graphics g) {
		g.setColor(Color.black);
		g.setFont(TFONT);
		// g.drawString(String.format("T: %f", (double)time/86400000.0), 150, 100);
		g.drawString(String.format("Time: %tj:%tH", time, time), 50, 30);
		g.drawString(String.format("S/kg: %f", susMass), 50, 50);
		g.drawString(String.format("Wc/n: %d", wcParticles != null ? wcParticles.size() : 0), 50, 70);
		// g.drawString(String.format("Nrcl: %d", nSusCells), 50, 90);

	}

	private void showBathymetry(Graphics g) {
		for (int j = 0; j < ny; j++) {
			for (int i = 0; i < nx; i++) {
				double xg = x0 + DX * i;
				double yg = y0 + DY * j;
				int x = getScreenX(xg);
				int y = getScreenY(yg);
				int d = Math.abs(getScreenX(DX) - getScreenX(0.0)) + 1;
				if (bathymetry.getDepth(xg, yg) > 0.0) {
					g.setColor(LAND_COLOR);
					g.fillRect(x, y - d, d, d);
				}
			}
		}
	}

	private void showSuspendedMatter(Graphics g) {
		for (int j = 0; j < ny; j++) {
			for (int i = 0; i < nx; i++) {
				// color from number in cell
				// int nc = (int)Math.rint((double)surface[j][i]/500.0);
				// System.err.printf("Nc for: %d %d : %d -> %d \n",j ,i, surface[j][i], nc);
				// if (nc > 7) nc = 7;
				// Color c = colors[nc];
				double mass = wcMass[j][i];
				float gg = 1.0f - (float) mass / (float) maxMass;
				if (gg < 0.0f)
					gg = 0.0f;
				// Color c = new Color(r, 1.0f, 1.0f);
				Color c = new Color(0.95f * gg, 1.0f, 0.95f * gg);

				// if (count == 0)
				// c = Color.gray.brighter();

				double xg = x0 + DX * i;
				double yg = y0 + DY * j;
				int x = getScreenX(xg);
				int y = getScreenY(yg);
				int d = Math.abs(getScreenX(DX) - getScreenX(0.0)) + 1;
				
				// dont overwrite the land sectors
				if (bathymetry.getDepth(xg, yg) >= 0.0) {
					g.setColor(LAND_COLOR);
				} else {
					if (mass < 0.1)
						g.setColor(BG_COLOR);
					else
						g.setColor(c);
				}

				g.fillRect(x, y - d, 20, 20);

				// if (nc > 1)
				// System.err.printf("Surface plot: (%d, %d) : %d,%d %d \n",i,j,x,y,d);
			}
		}

	}
	
	
	private void showFinalDeposition(Graphics g, double maxmass) {
		double x = x0;
		while (x < x0 + dx) {
			double y = y0;
			while (y < y0 + dy) {

				// at position xy, get cell
				try {
					IBedCellModel cell = bed.getBedModel(x, y);
					// System.err.printf("MDP:: cell at: (%f, %f) deposition: %s \n", x, y,
					// cell.getTotalMass().getComponentMass(ChemicalComponentType.SOLIDS));

					// double mass = 0.0;
					// ChemicalComponent cc =
					// cell.getLayers().getSurfaceLayer().getMass().getComponent(ChemicalComponentType.SOLIDS);
					// if (cc != null) {
					// mass = cc.getMass();
					// }

					double mass = cell.getTotalMass().getComponentMass(ChemicalComponentType.SOLIDS);

					// System.err.printf("MDP:: cell at: (%f, %f) Solids: %f kg \n", x, y, mass);
					// calculate coloring heat map
					
					// one way fo working out color
					//float b = 1.0f - (float) mass / (float) maxmass; // scale to 100kg
					//if (b < 0.0f)
						//b = 0.0f;
					//Color c = new Color(1.0f, 0.9f * b, 0.9f * b);

					// g.setColor(c);

					// cal color using colormap: 0-13
					int index = (int) Math.floor(13 * mass / maxmass);
					
					//int index = 4*(int)Math.log10(mass/maxmass)+13;
					// if (index > 13) index = 13;
					if (index > 15)
						index = 15;
					
					// take care of values where log is less than -15!ish
					//if (index < 0)
						//index = 0;

					
					if (bathymetry.getDepth(x, y) >= 0.0) {
						g.setColor(LAND_COLOR);
					} else {
						
						if (index == 0) {
							if (mass < 0.1) {
								g.setColor(BG_COLOR);
								//g.setColor(Color.YELLOW);
							} else {
								//g.setColor(Color.YELLOW);
								g.setColor(colorMap[index]);
							}
						} else {
							g.setColor(colorMap[index]);
						}
						
					}

					int sx = getScreenX(x);
					int sy = getScreenY(y);
					int ddx = Math.abs(getScreenX(DX) - getScreenX(0.0) + 2);
					int ddy = Math.abs(getScreenY(DY) - getScreenY(0.0) + 2);
					g.fillRect(sx, sy, ddx, ddy);
					if (mass > 1.0) {
						int ii = (int) ((x - x0) / DX);
						int jj = (int) ((y - y0) / DY);
						// System.err.printf("MDP:: final mass in cell: [%d,%d] at: (%f,%f) is: %f \n",
						// ii, jj, x, y, mass);
					}
				} catch (Exception e) {
				}
				y += 20.0;
			}
			x += 20.0;
		}

	}

	private void showColorBar(Graphics g, double maxmass) {
		int bx = (int) (0.15 * getSize().width);
		int by = (int) (0.85 * getSize().height);
		g.setFont(KFONT);
		for (int index = 0; index < 13; index++) {
			g.setColor(colorMap[index]);
			g.fillRect(bx, by, 10, 10);
			g.drawString(String.format("%.2f", index * maxmass / 13.0), bx + 15, by + 10);
			//double value = Math.pow(maxmass, (index-13)/4);
			//g.drawString(String.format("%.4f", value), bx + 15, by + 10);
			by -= 10;
		}
	}

	private void showCrosshairs(Graphics g) {
		g.setColor(Color.red);
		int ww = getSize().width;
		int hh = getSize().height;
		g.drawLine(ww / 2, 0, ww / 2, hh);
		g.drawLine(0, hh / 2, ww, hh / 2);
	}

	private void showCages(Graphics g) {
		for (Cage c : layout.cages()) {
			double cx = pm.getPosition(c.getName()).getX();
			double cy = pm.getPosition(c.getName()).getY();
			int sx = getScreenX(cx - 0.5 * c.getWidth());
			int sy = getScreenY(cy - 0.5 * c.getLength());
			int w = Math.abs(getScreenX(c.getWidth()) - getScreenX(0.0));
			int h = Math.abs(getScreenY(c.getLength()) - getScreenY(0.0));
			// System.err.printf("cage %s at:: (%f,%f) ij: [%d, %d] w: %d h: %d \n",
			// c.getName(), cx, cy, sx, sy, w, h);
			//System.err.printf("MDP:: cage %s at:: (%f,%f) ij: [%d, %d] w: %d h: %d \n", c.getName(), cx, cy, sx, sy, w, h);
			g.setColor(Color.black);
			g.drawOval(sx, sy, w, h);
		}
	}

	private void showContours(Graphics g, double depth) {
		return;
	/*	LinkedList<ContourSegment> contour;

		// fetch the already built contours if available.
		if (contours.containsKey(depth))
			contour = contours.get(depth);
		else {
			contour = contourer.contour(bathygrid, 0, nx - 1, 0, ny - 1, xa, ya, depth);
			contours.put(depth, contour);
		}
		g.setColor(CONTOUR_COLOR);
		for (ContourSegment s : contour) {
			int sx = getScreenX(s.getStartX());
			int sy = getScreenY(s.getStartY());
			int ex = getScreenX(s.getEndX());
			int ey = getScreenY(s.getEndY());
			g.drawLine(sx, sy, ex, ey);
		}*/
	}

	public void updateBed(RegularGridBedLayoutModel bed, long time) {
		System.err.println("MDP:: updatebed...");

		this.bed = bed;
		this.time = time;

		repaint();

		/*
		 * final JComponent fthis = this; SwingUtilities.invokeLater(new Runnable() {
		 * 
		 * @Override public void run() { fthis.repaint(); } });
		 */
	}

	public void updateSuspensionParticles(long time, List<Particle> particles) {
		this.time = time;
		wcParticles = particles;
		// suspension particles in the water-column
		maxMass = 0.0;
		susMass = 0.0;
		nSusCells = 0; // cant count cells with suspension atm as need to record which cells has
						// particles above...

		for (int jj = 0; jj < ny; jj++) {
			for (int ii = 0; ii < nx; ii++) {
				wcMass[jj][ii] = 0.0;
			}
		}

		int np = particles.size();

		int nsus = 0;
		for (Particle p : wcParticles) {
			if (p.getState() != ParticlesState.SUSPEND)
				continue;
			nsus++;
		}

		// System.err.printf("MDP:: got particles: %d (%d)\n", np, nsus);

		for (Particle p : wcParticles) {
			if (p.getState() != ParticlesState.SUSPEND)
				continue;

			// which cell is this
			double xa = p.getX();
			double xx = xa - x0;
			double ya = p.getY();
			double yy = ya - y0;
			int ii = (int) (xx / DX);
			int jj = (int) (yy / DY);
			// System.err.printf("MDP:: sus pcle at: (%f, %f) -> (%f, %f) : [%d, %d]
			// (%f)\n", xa, ya, xx, yy, ii, jj, wcMass[jj][ii]);
			if (ii < 0 || ii >= nx || jj < 0 || jj >= ny)
				continue; // OOD
			double mass = p.getMasses().getComponentMass(ChemicalComponentType.SOLIDS);
			susMass += mass;
			wcMass[jj][ii] += mass;
			if (wcMass[jj][ii] > maxMass)
				maxMass = mass;
		}
		// System.err.printf("Suspension mass total: %f kg \n", susMass);
		repaint();
	}

}
