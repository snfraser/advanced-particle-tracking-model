/**
 * 
 */
package com.apt.gui;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;

import com.apt.models.IBathymetryModel;

/**
 * @author SA05SF
 *
 */
public class BathymetryPanel extends JPanel {

	Color LAND_COLOR = Color.YELLOW.darker();
	
	IBathymetryModel bathymetry;

	double[][] surface;
	
	int nx;
	int ny;
	private double x0;
	private double y0;
	private double wx;
	private double wy;

	private double maxdepth = -9999.99;
	
	/**
	 * @param bathymetry
	 */
	public BathymetryPanel(IBathymetryModel bathymetry) {
		super();
		this.bathymetry = bathymetry;
		x0 = bathymetry.getDomainX0();
		y0 = bathymetry.getDomainY0();
		nx = (int) (bathymetry.getDomainWidth() / 25.0);
		ny = (int) (bathymetry.getDomainHeight() / 25.0);
		wx = bathymetry.getDomainWidth();
		wy = bathymetry.getDomainHeight();
	
		//System.err.printf("Bathy Surface size: %d x %d start: %f, %f  width: %fx%f\n", nx, ny, x0, y0, wx, wy);
		surface = new double[ny][nx];
		for (int j = 0; j < ny; j++) {
			double y = y0 + j*25;
			for (int i = 0; i < nx; i++) {
				double x = x0 + i*25;
				double depth = bathymetry.getDepth(x, y);
				surface[j][i] = depth;
				//System.err.printf("BP::Depth at: [%d, %d] (%f, %f) : %f \n", i, j, x, y, depth);
				if (Math.abs(depth) > maxdepth)
					maxdepth = Math.abs(depth);
				
			}
		}
		
	}
	
	
	@Override
	public void paint(Graphics g) {
		super.paint(g);

		for (int j = 0; j < ny; j++) {
			for (int i = 0; i < nx; i++) {
				double depth = surface[j][i];
				float count = (float)Math.abs(depth);
				float r = 1.0f - (float) count / (float) maxdepth;
				if (r < 0.0f)
					r = 0.0f;
			
				Color c = new Color(0.95f * r, 0.95f * r, 1.0f);
			
				if (depth > 0.0)
					c = LAND_COLOR;
				g.setColor(c);
				int x = getScreenX(x0 + 25 * i);
				int y = getScreenY(y0 + 25 * j);
				int d = Math.abs(getScreenX(25.0) - getScreenX(0.0)) + 1;
				g.fillRect(x, y - d, d+1, d+1);
				
			}
		}
	}
	
	private int getScreenX(double x) {
		double ww = (double)getSize().width;
		int gx = (int)(ww*(x - x0)/wx);
		return gx;
		
	}

	private int getScreenY(double y) {
		double hh = (double)getSize().height;
		int gy = (int)(hh*(1.0 - (y - y0)/wy));
		return gy;
	}
	
	
	
}
