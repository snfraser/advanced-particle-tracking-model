/**
 * 
 */
package com.apt.gui.test;

import java.awt.BorderLayout;
import java.io.File;

import javax.swing.JFrame;

import com.apt.data.VoronoiMesh;
import com.apt.gui.SeabedStateDisplay;
import com.apt.models.ISeabedModel;
import com.apt.models.readers.MultipointSeabedModelReader;
import com.apt.models.seabed.MultipointSeabedModel;

/**
 * @author SA05SF
 *
 */
public class TestDisplayMultipointSeabed {

	public static final String MESHFILENAME = "seabed.mesh"; // or use name.mesh?
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		try {
			File home = new File(System.getProperty("user.home"));
			File meshFile = new File(home, MESHFILENAME);
		
			MultipointSeabedModelReader reader = new MultipointSeabedModelReader();
			ISeabedModel seabed = reader.readFile(meshFile);
		
			
			//SeabedStateDisplay display = new SeabedStateDisplay(seabed);
			
			JFrame f = new JFrame("Seabed");
			//f.getContentPane().setLayout(new BorderLayout());
			//f.getContentPane().add(display, BorderLayout.CENTER);
			f.setLocation(100, 100);
			f.setSize(800, 800);
			f.pack();
			f.setVisible(true);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
