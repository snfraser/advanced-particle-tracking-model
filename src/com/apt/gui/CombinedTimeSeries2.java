/**
 * 
 */
package com.apt.gui;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.CombinedDomainXYPlot;
import org.jfree.chart.plot.Marker;
import org.jfree.chart.plot.ValueMarker;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.chart.title.TextTitle;
import org.jfree.chart.title.Title;
import org.jfree.data.time.Second;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;

/**
 * @author SA05SF
 *
 */
public class CombinedTimeSeries2 {

	static Font TITLE_FONT = new Font("Tahoma", Font.PLAIN, 16);
	static Font SUB_FONT = new Font("Tahoma", Font.ITALIC, 14);
	
	TimeSeries series1;
	TimeSeries series2;
	TimeSeries series3;
	
	TimeSeriesCollection tsc1;
	TimeSeriesCollection tsc2;
	TimeSeriesCollection tsc3;
	
	public ChartPanel createChartPanel(String title, String subtitleText) {

		SimpleDateFormat sdf = new SimpleDateFormat("DD:HH");
		DateAxis dx = new DateAxis("Date/Time");
		dx.setDateFormatOverride(sdf);

		CombinedDomainXYPlot all = new CombinedDomainXYPlot(dx);
		all.setGap(10);

		series1 = createDataset("L1-Tauc");
		XYPlot plot1 = createPlot(series1, dx, "Crit. Stress [Pa]");
		
		Marker marker = new ValueMarker(0.02);
		// marker.setOutlinePaint(Color.red);
		marker.setPaint(Color.black);
		marker.setStroke(new BasicStroke(0.3f));
		plot1.addRangeMarker(marker);

		series2 = createDataset("L1-EQ-Tauc");
		XYPlot plot2 = createPlot(series2, dx, "Eq. Crit. Stress [Pa]");
		
		series3 = createDataset("L1-Diff");
		XYPlot plot3 = createPlot(series3, dx, "Diff. [Pa]");
		
		all.add(plot1, 2);
		all.add(plot2, 2);
		all.add(plot3, 1);
		
		JFreeChart chart = new JFreeChart(title, all);
		chart.getTitle().setFont(TITLE_FONT);
		Title subtitle = new TextTitle(subtitleText, SUB_FONT);
		chart.addSubtitle(subtitle);
		
		chart.setBackgroundPaint(Color.gray.brighter());
		//chart.getPlot().setBackgroundPaint(Color.blue.darker());
		//chart.getPlot().setBackgroundAlpha(0.4f);
		ChartPanel p = new ChartPanel(chart);
		return p;
		
	}
	
	private TimeSeries createDataset(String label) {
		TimeSeries series = new TimeSeries(label);
		return series;
	}
	
	private XYPlot createPlot(TimeSeries ts, DateAxis xax, String ylabel) {
		TimeSeriesCollection tsc  = new TimeSeriesCollection(ts);
		XYPlot plot = new XYPlot(tsc, xax, new NumberAxis(ylabel), new XYLineAndShapeRenderer(true, false));
		plot.setBackgroundPaint(Color.blue);
		plot.setBackgroundAlpha(0.1f);
		return plot;
	}
	
	public void addTauCData(long time, double tauc, double eqtauc) { 
		Date date = new Date(time);
		series1.addOrUpdate(new Second(date), tauc);
		series2.addOrUpdate(new Second(date), eqtauc);
		series3.addOrUpdate(new Second(date), tauc-eqtauc);
	}
	
}
