/**
 * 
 */
package com.apt.gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import com.apt.data.Particle;
import com.apt.data.ParticleClass;
import com.apt.data.ParticleTrackPoint;
import com.apt.data.ParticlesState;
import com.apt.models.Cage;
import com.apt.models.IBathymetryModel;
import com.apt.models.ICageLayoutModel;
import com.apt.models.bathymetry.FixedBathymetryModel;
import com.apt.models.cage.ICagePositionManager;

/**
 * @author SA05SF
 *
 */
// implements ParticleDataHandler etc
public class ParticleTrackPanel extends JPanel {

	private static final Color LAND_COLOR = Color.YELLOW.darker();
	private static final Font TFONT = new Font("Serif", Font.PLAIN, 20);
	private static int NNP = 1;

	private IBathymetryModel bathymetry;
	private ICageLayoutModel layout;
	private ICagePositionManager pm;

	int nx;
	int ny;
	private double x0;
	private double y0;
	private double wx;
	private double wy;

	private double obsx = 0.0;
	private double obsy = 0.0;
	
	long time = 0L;

	/** Bathy surface. */
	double[][] surface;

	/** all particles posted - ie ones which have landed. */
	private List<Particle> particles;
	private List<Particle> xparticles;

	private double maxdepth = -9999.99;

	int landcount = 0;
	int lostcount = 0;
	int preceived = 0;

	/**
	 * @param bathymetry
	 */
	public ParticleTrackPanel(IBathymetryModel bathymetry, ICageLayoutModel layout) {
		this.bathymetry = bathymetry;
		this.layout = layout;
		this.pm = layout != null ? layout.getPositionManager() : null;
		x0 = bathymetry.getDomainX0();
		y0 = bathymetry.getDomainY0();
		nx = (int) (bathymetry.getDomainWidth() / 20.0) + 1;
		ny = (int) (bathymetry.getDomainHeight() / 20.0) + 1;
		wx = bathymetry.getDomainWidth();
		wy = bathymetry.getDomainHeight();
		particles = new ArrayList<Particle>();
		xparticles = new ArrayList<Particle>();

		surface = new double[ny][nx];
		for (int j = 0; j < ny; j++) {
			double y = y0 + j * 20;
			for (int i = 0; i < nx; i++) {
				double x = x0 + i * 20;
				double depth = bathymetry.getDepth(x, y);
				surface[j][i] = depth;
				// System.err.printf("BP::Depth at: [%d, %d] (%f, %f) : %f \n", i, j, x, y,
				// depth);
				if (Math.abs(depth) > maxdepth)
					maxdepth = Math.abs(depth);

			}
		}
	}

	public void observedCell(double x, double y) {
		this.obsx = x;
		this.obsy = y;
	}

	
	@Override
	public void paint(Graphics g) {
		super.paint(g);

		// add any new particles
		particles.addAll(xparticles);
		xparticles.clear();

		// bathymetry
		for (int j = 0; j < ny; j++) {
			for (int i = 0; i < nx; i++) {
				double depth = surface[j][i];
				float count = (float) Math.abs(depth);
				float r = 1.0f - (float) count / (float) maxdepth;
				if (r < 0.0f)
					r = 0.0f;

				Color c = new Color(0.95f * r, 0.95f * r, 1.0f);

				if (depth > 0.0)
					c = LAND_COLOR;
				g.setColor(c);
				int x = getScreenX(x0 + 20 * i);
				int y = getScreenY(y0 + 20 * j);
				int d = Math.abs(getScreenX(20.0) - getScreenX(0.0)) + 1;
				g.fillRect(x, y - d, d + 1, d + 1);

			}
		}

		// split particles into 3 sublists..
		List<Particle> fdparticles = new ArrayList<Particle>();
		List<Particle> faeparticles = new ArrayList<Particle>();
		List<Particle> mudparticles = new ArrayList<Particle>();
		List<Particle> bedloadparticles = new ArrayList<Particle>();
		List<Particle> lostparticles = new ArrayList<Particle>();
		List<Particle> conparticles = new ArrayList<Particle>();
		
		Iterator<Particle> pp = particles.iterator();
		while (pp.hasNext()) {
			Particle p = pp.next();
			if (p == null)
				return;
			
			if (p.getState() == ParticlesState.LOST)
				lostparticles.add(p);
			
			// take the final locations of these
			if (p.getState() == ParticlesState.CONSOLIDATED) {
				conparticles.add(p);
				if (p.getType() == ParticleClass.FAECES)
					faeparticles.add(p);
				else if (p.getType() == ParticleClass.FEED)
					fdparticles.add(p);
				else if (p.getType() == ParticleClass.MUD)
					mudparticles.add(p);
			}
			// all others are still in play on the bed
			bedloadparticles.add(p);
		}
		//System.err.printf("PTUI:: All particles: %d BedLoad: %d Fd: %d Fae: %d Mud: %d Lost: %d\n", 
			//	particles.size(), bedloadparticles.size(), fdparticles.size(), faeparticles.size(), mudparticles.size(), lostparticles.size());
		
		particles.removeAll(lostparticles);
	
		// render moving bedload
		g.setColor(Color.gray.brighter());
		for (Particle p : bedloadparticles) {
			int x = getScreenX(p.getX());
			int y = getScreenY(p.getY());

			g.fillOval(x, y, 2, 2);

		}

		// render faeces first
		g.setColor(Color.orange);
		for (Particle p : faeparticles) {
			if (p == null)
				return;
			int x = getScreenX(p.getX());
			int y = getScreenY(p.getY());

			g.fillOval(x, y, 2, 2);

		}

		//  resus
		g.setColor(Color.magenta);
		for (Particle p : mudparticles) {
			int x = getScreenX(p.getX());
			int y = getScreenY(p.getY());

			g.fillOval(x, y, 2, 2);
		}

		// render feed after others
		g.setColor(Color.green);
		for (Particle p : fdparticles) {
			int x = getScreenX(p.getX());
			int y = getScreenY(p.getY());

			g.fillOval(x, y, 3, 3);

		}

		// GRID
		showCrosshairs(g);

		// CAGES
		showCages(g);

		// TIMESTAMP
		showData(g);

		// OBS CELL MARKER
		showObservedCell(g);
		
		/*
		 * boolean start = true;
		 * 
		 * for (ParticleTrackPoint pt : p.getTrack()) {
		 * 
		 * if (start) { start = false; } else { int newx = (int)((pt.x -
		 * (dx/2-bb/2))*getSize().width/bb); int newy = (int)((pt.y -
		 * (dy/2-bb/2))*getSize().height/bb); g.drawLine(x, y, newx, newy); }
		 * 
		 * 
		 * }
		 */

	}

	/*
	 * private int getScreenX(double x) { int gx = (int) (((x - (dx - bb) / 2.0) /
	 * bb) * getSize().width); return gx; }
	 * 
	 * private int getScreenY(double y) { int gy = (int) ((1.0 - ((y - (dy - bb) /
	 * 2.0) / bb)) * getSize().height); // System.err.printf("gy: %f -> %d \n", y,
	 * gy); return gy; }
	 */

	private int getScreenX(double x) {
		// int gx = (int) (((x - (dx - bb) / 2.0) / bb) * getSize().width);

		double ww = (double) getSize().width;
		int gx = (int) (ww * (x - x0) / wx);
		return gx;

	}

	private int getScreenY(double y) {
		// int gy = (int) ((1.0 - ((y - (dy - bb) / 2.0) / bb)) * getSize().height);
		// System.err.printf("gy: %f -> %d \n", y, gy);
		double hh = (double) getSize().height;
		int gy = (int) (hh * (1.0 - (y - y0) / wy));
		return gy;
	}

	private void showData(Graphics g) {
		g.setColor(Color.yellow);
		g.setFont(TFONT);
		// g.drawString(String.format("T: %f", (double)time/86400000.0), 150, 100);
		g.drawString(String.format("Time: %tj:%tH", time, time), 50, 50);
		g.drawString(String.format("Lost: %d", lostcount), 50, 75);
		g.drawString(String.format("Bed: %d", landcount), 50, 100);
		g.drawString(String.format("Rcvd: %d", preceived), 50, 125);
	}

	private void showObservedCell(Graphics g) {
		g.setColor(Color.BLUE);
		int iobsx = getScreenX(obsx);
		int iobsy = getScreenY(obsy);
		g.drawLine(iobsx - 5,  iobsy, iobsx + 5, iobsy);
		g.drawLine(iobsx, iobsy -5, iobsx, iobsy +5);
	}
	
	private void showCrosshairs(Graphics g) {
		g.setColor(Color.red);
		int ww = getSize().width;
		int hh = getSize().height;
		g.drawLine(ww / 2, 0, ww / 2, hh);
		g.drawLine(0, hh / 2, ww, hh / 2);
	}

	private void showCages(Graphics g) {
		if (layout == null)
			return;
		for (Cage c : layout.cages()) {
			double cx = pm.getPosition(c.getName()).getX();
			double cy = pm.getPosition(c.getName()).getY();
			int sx = getScreenX(cx - 0.5 * c.getWidth());
			int sy = getScreenY(cy - 0.5 * c.getLength());
			int w = Math.abs(getScreenX(c.getWidth()) - getScreenX(0.0));
			int h = Math.abs(getScreenY(c.getLength()) - getScreenY(0.0));
			// System.err.printf("SDP:: cage %s at:: (%f,%f) ij: [%d, %d] w: %d h: %d \n",
			// c.getName(), cx, cy, sx, sy, w, h);
			g.setColor(Color.black);
			g.drawOval(sx, sy, w, h);
		}
	}

	public void addParticles(long time, List<Particle> addparticles) {
		// System.err.printf("PTrackPanel::Adding %d particles\n", addparticles.size());

		this.time = time;
		preceived += addparticles.size();

		// if (time % 3600000 == 0)
		// System.err.printf("TPUI:: got %d particles to follow\n", particles.size());

		if (particles.size() > 500000)
			return;

		int i = 0;
		// only add every 100th pcle
		for (Particle p : addparticles) {
			if (i % NNP == 0 && p.getState() == ParticlesState.ONBED) {
				xparticles.add(p);
				landcount++;
			}
			i++;

			if (p.getState() == ParticlesState.LOST) {
				lostcount++;
			}
		}

		repaint();
		// if (time % 600000 == 0) { // hourly
		// System.err.println("Surf::repaint....");
		/*
		 * final JComponent fthis = this; SwingUtilities.invokeLater(new Runnable() {
		 * 
		 * @Override public void run() { fthis.repaint(); } });
		 */

		// repaint();
		// }

	}

}
