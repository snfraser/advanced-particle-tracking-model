/**
 * 
 */
package com.apt.gui;

import java.awt.Dimension;
import java.awt.Font;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.annotations.XYTextAnnotation;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.title.Title;
import org.jfree.data.time.Second;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

/**
 * @author SA05SF
 *
 */
public class BedLevelsCountPlot {
	

	static Font TITLE_FONT = new Font("Tahoma", Font.PLAIN, 16);
	static Font SUB_FONT = new Font("Tahoma", Font.ITALIC, 14);
	
	
	TimeSeriesCollection tsc;
	TimeSeries series1;
	int index = 0;
	
	
	public ChartPanel createChartPanel(String title) {
	
		SimpleDateFormat sdf = new SimpleDateFormat("DD:HH");
		DateAxis dx = new DateAxis("Date/Time");
		dx.setDateFormatOverride(sdf);
		
		JFreeChart chart = ChartFactory.createTimeSeriesChart(title, "Time [days]", "Number layers",
			createDataset(), false, false, false);
	
	
	chart.getTitle().setFont(TITLE_FONT);

	//chart.getLegend().setVisible(false);
	XYPlot plot = (XYPlot) chart.getPlot();
	
	DateAxis domainAxis = (DateAxis) plot.getDomainAxis();
	domainAxis.setDateFormatOverride(sdf);

	domainAxis.setLabelFont(new Font("Tahoma", Font.PLAIN, 12));
	domainAxis.setTickLabelFont(new Font("Tahoma", Font.PLAIN, 10));

	NumberAxis range = (NumberAxis) plot.getRangeAxis();
	// range.setRange(0.0, 550.0);
	range.setLabelFont(new Font("Tahoma", Font.PLAIN, 12));
	range.setTickLabelFont(new Font("Tahoma", Font.PLAIN, 10));
	
	ChartPanel chartPanel = new ChartPanel(chart);
	chartPanel.setPreferredSize(new Dimension(500, 500));
	return chartPanel;
	
}
	
	private XYDataset createDataset() {
		series1 = new TimeSeries("Number of Bed Layers");
		
		tsc = new TimeSeriesCollection();
		tsc.addSeries(series1);
		
		return tsc;
	}
	
	public void updateLayerCount(long time, int count) {
		index++;
		//System.err.println("mtp:update mass:" +time+" "+mass);
		Date date = new Date(time);
		series1.addOrUpdate(new Second(date), count);
	
	}

}
