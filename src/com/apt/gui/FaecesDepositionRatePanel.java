package com.apt.gui;

import java.awt.Dimension;
import java.awt.Font;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JPanel;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.time.Second;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;

public class FaecesDepositionRatePanel  {

	XYDataset dataset;

	TimeSeriesCollection tsc;
	TimeSeries series1;
	TimeSeries series2;

	/**
	 * 
	 */
	public FaecesDepositionRatePanel() {
		
	}

	public ChartPanel createChartPanel() {

		JFreeChart chart = ChartFactory.createTimeSeriesChart("Cumulative deposition", "time [days]", "deposition [kg]",
				createDataset(), false, false, false);

		chart.getTitle().setFont(new Font("Tahoma", Font.PLAIN, 14));
		//chart.getLegend().setVisible(false);
		XYPlot plot = (XYPlot) chart.getPlot();
		SimpleDateFormat sdf = new SimpleDateFormat("D");
		DateAxis domainAxis = (DateAxis) plot.getDomainAxis();
		domainAxis.setDateFormatOverride(sdf);

		domainAxis.setLabelFont(new Font("Tahoma", Font.PLAIN, 12));
		domainAxis.setTickLabelFont(new Font("Tahoma", Font.PLAIN, 10));

		NumberAxis range = (NumberAxis) plot.getRangeAxis();
		// range.setRange(0.0, 550.0);
		range.setLabelFont(new Font("Tahoma", Font.PLAIN, 12));
		range.setTickLabelFont(new Font("Tahoma", Font.PLAIN, 10));
		ChartPanel chartPanel = new ChartPanel(chart);
		chartPanel.setPreferredSize(new Dimension(500, 250));
		return chartPanel;

	}

	private XYDataset createDataset() {
		series1 = new TimeSeries("Faeces");
		series2 = new TimeSeries("Waste feed");
		
		tsc = new TimeSeriesCollection();
		tsc.addSeries(series1);
		tsc.addSeries(series2);
		
		return tsc;
	}
	
	public void updateFaecesDeposition(long time, double rate) {
		Date date = new Date(time);
		series1.addOrUpdate(new Second(date), rate);
	}
	
	public void updateWasteFeedDeposition(long time, double rate) {
		Date date = new Date(time);
		series2.addOrUpdate(new Second(date), rate);
	}
	
}
