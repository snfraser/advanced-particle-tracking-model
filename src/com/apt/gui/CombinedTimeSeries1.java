/**
 * 
 */
package com.apt.gui;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.CombinedDomainXYPlot;
import org.jfree.chart.plot.Marker;
import org.jfree.chart.plot.ValueMarker;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.chart.title.TextTitle;
import org.jfree.chart.title.Title;
import org.jfree.chart.ui.Layer;
import org.jfree.data.time.Second;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;

/**
 * @author SA05SF
 *
 */
public class CombinedTimeSeries1 {

	static Font TITLE_FONT = new Font("Tahoma", Font.PLAIN, 16);
	static Font SUB_FONT = new Font("Tahoma", Font.ITALIC, 14);
	
static final double TAU = 6.0 * 3600 * 1000.0;
	
	static final double TAUC = 0.16;

	TimeSeries series1;
	TimeSeries series2;
	TimeSeries series2a;
	TimeSeries series3;
	TimeSeries series4;
	TimeSeries series5;
	TimeSeries series6;
	TimeSeries series7;

	TimeSeriesCollection tsc1;
	TimeSeriesCollection tsc2;
	TimeSeriesCollection tsc2a;
	TimeSeriesCollection tsc3;
	TimeSeriesCollection tsc4;
	TimeSeriesCollection tsc5;
	TimeSeriesCollection tsc6;
	TimeSeriesCollection tsc7;
	
	XYPlot plot1;
	
	Marker plot1Marker; 

	public ChartPanel createChartPanel(String title, String subtitleText) {

		SimpleDateFormat sdf = new SimpleDateFormat("DD:HH");
		DateAxis dx = new DateAxis("Date/Time");
		dx.setDateFormatOverride(sdf);

		CombinedDomainXYPlot all = new CombinedDomainXYPlot(dx);
		all.setGap(10);

		series1 = createDataset("Deposited mass");
		//tsc1 = new TimeSeriesCollection(series1);
		//XYPlot plot1 = new XYPlot(tsc1, dx, new NumberAxis("Surf. Mass [kg]"),
			//	new XYLineAndShapeRenderer(true, false));
		//plot1.setBackgroundPaint(Color.blue);
		//plot1.setBackgroundAlpha(0.2f);
		plot1 = createPlot(series1, dx, "Surf. Mass [kg]");
		
		series7 = createDataset("Resus");
		XYPlot plot7 = createPlot(series7, dx, "Resus. Mass [kg]");
		

		series2a = createDataset("Surface Flow speed");
		//tsc2a = new TimeSeriesCollection(series2a);
		//XYPlot plot2a = new XYPlot(tsc2a, dx, new NumberAxis("Surf. Flow [m/s]"),
		//		new XYLineAndShapeRenderer(true, false));
		//plot2a.setBackgroundPaint(Color.blue);
		//plot2a.setBackgroundAlpha(0.2f);
		XYPlot plot2a = createPlot(series2a, dx, "Surf. Flow [m/s]");
		
		series2 = createDataset("Boundary Flow speed");
		//tsc2 = new TimeSeriesCollection(series2);
		//XYPlot plot2 = new XYPlot(tsc2, dx, new NumberAxis("Bndy. Flow [m/s]"),
			//	new XYLineAndShapeRenderer(true, false));
		//plot2.setBackgroundPaint(Color.blue);
		//plot2.setBackgroundAlpha(0.2f);
		XYPlot plot2 = createPlot(series2, dx, "Bndy. Flow [m/s]");
		
		series3 = createDataset("BFV");
		//tsc3 = new TimeSeriesCollection(series3);
		//XYPlot plot3 = new XYPlot(tsc3, dx, new NumberAxis("BFV [m/s]"), new XYLineAndShapeRenderer(true, false));
		//plot3.setBackgroundPaint(Color.blue);
		//plot3.setBackgroundAlpha(0.2f);
		XYPlot plot3 = createPlot(series3, dx, "BFV [m/s]");
		
		series4 = createDataset("Tau_B");
		//tsc4 = new TimeSeriesCollection(series4);
		//XYPlot plot4 = new XYPlot(tsc4, dx, new NumberAxis("Tau_B [Pa]"), new XYLineAndShapeRenderer(true, false));
		//plot4.setBackgroundPaint(Color.blue);
		//plot4.setBackgroundAlpha(0.2f);
		XYPlot plot4 = createPlot(series4, dx, "Tau_B [Pa]");
				
		Marker marker = new ValueMarker(TAUC);
		// marker.setOutlinePaint(Color.red);
		marker.setPaint(Color.red);
		marker.setStroke(new BasicStroke(0.3f));
		plot4.addRangeMarker(marker);

		series5 = createDataset("Flux");
		//tsc5 = new TimeSeriesCollection(series5);
		//XYPlot plot5 = new XYPlot(tsc5, dx, new NumberAxis("E. Flux [kg]"), new XYLineAndShapeRenderer(true, false));
		//plot5.setBackgroundPaint(Color.blue);
		//plot5.setBackgroundAlpha(0.2f);
		XYPlot plot5 = createPlot(series5, dx, "E. Flux [kg]");
		
		
		series6 = createDataset("Bed load");
		//tsc6 = new TimeSeriesCollection(series6);
		//XYPlot plot6 = new XYPlot(tsc6, dx, new NumberAxis("Bed Load [kg]"), new XYLineAndShapeRenderer(true, false));
		//plot6.setBackgroundPaint(Color.blue);
		//plot6.setBackgroundAlpha(0.2f);
		XYPlot plot6 = createPlot(series6, dx, "Bed Load [kg]");
		
		all.add(plot1, 2);
		all.add(plot6, 1);
		all.add(plot7, 1);
		all.add(plot2a, 1);
		all.add(plot2, 1);
		all.add(plot3, 1);
		all.add(plot4, 1);
		all.add(plot5, 1);

		JFreeChart chart = new JFreeChart(title, all);
		chart.getTitle().setFont(TITLE_FONT);
		Title subtitle = new TextTitle(subtitleText, SUB_FONT);
		chart.addSubtitle(subtitle);
		
		chart.setBackgroundPaint(Color.gray.brighter());
		//chart.getPlot().setBackgroundPaint(Color.blue.darker());
		//chart.getPlot().setBackgroundAlpha(0.4f);
		ChartPanel p = new ChartPanel(chart);
		return p;
	}

	private TimeSeries createDataset(String label) {
		TimeSeries series = new TimeSeries(label);
		return series;
	}
	
	private XYPlot createPlot(TimeSeries ts, DateAxis xax, String ylabel) {
		TimeSeriesCollection tsc  = new TimeSeriesCollection(ts);
		XYPlot plot = new XYPlot(tsc, xax, new NumberAxis(ylabel), new XYLineAndShapeRenderer(true, false));
		plot.setBackgroundPaint(Color.blue);
		plot.setBackgroundAlpha(0.1f);
		return plot;
	}

	public void addSurfaceLayerMassData(long time, double value, double thresholdValue) { // surface layer mass
		//System.err.printf("CT1:: add mass: t: %d  m: %f \n", time, value);
		Date date = new Date(time);
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				series1.addOrUpdate(new Second(date), value);
				
				if (plot1Marker == null) {
					plot1Marker =  new ValueMarker(1.5*thresholdValue);
					plot1Marker.setPaint(Color.red);
					plot1Marker.setStroke(new BasicStroke(0.3f));
					plot1.addRangeMarker(plot1Marker);
				}
				
			}
		});
	}

	public void addSurfaceFlowData(long time, double value) { // surface flow
		Date date = new Date(time);
		series2a.addOrUpdate(new Second(date), value);
	}

	public void addBoundaryFlowData(long time, double value) { // boundary flow
		Date date = new Date(time);
		series2.addOrUpdate(new Second(date), value);
	}

	public void addBFVData(long time, double value) { // bfv
		Date date = new Date(time);
		series3.addOrUpdate(new Second(date), value);
	}

	public void addTauBData(long time, double value) { // taub
		//System.err.printf("CT1:: add taub: t: %d  taub: %f \n", time, value);
		Date date = new Date(time);
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				series4.addOrUpdate(new Second(date), value);
			}
		});
	}

	public void addErosionFluxData(long time, double value) { // erosion flux
		Date date = new Date(time);
		series5.addOrUpdate(new Second(date), value);
	}
	
	public void addBedLoad(long time, double bedload) {
		Date date = new Date(time);
		series6.addOrUpdate(new Second(date), bedload);
	}

	
	public void addResusMass(long time, double resusMass) {
		Date date = new Date(time);
		series7.addOrUpdate(new Second(date), resusMass);
	}
	
	public static void main(String args[]) {

		CombinedTimeSeries1 p = new CombinedTimeSeries1();

		JFrame f = new JFrame("Combined time-series");
		f.getContentPane().setLayout(new BorderLayout());
		f.getContentPane().add(p.createChartPanel("Combined plot test", ""), BorderLayout.CENTER);
		f.pack();
		f.setSize(1200, 800);
		f.setLocation(100, 100);
		f.setVisible(true);

		long time = System.currentTimeMillis();
		for (int i = 0; i < 5000; i++) {

			time += 5 * 60000L; // 5 minutes

			double a = 10.0 * Math.sin(2.0 * Math.PI * (double) time / TAU) + Math.random() * 5.0;
			p.addSurfaceLayerMassData(time, a, 10.0);
			double b = 10.0 * Math.sin(2.0 * Math.PI * (double) time / TAU) + Math.random() * 5.0;
			p.addBoundaryFlowData(time, b);
			double c = 10.0 * Math.sin(2.0 * Math.PI * (double) time / TAU) + Math.random() * 5.0;
			p.addBFVData(time, c);

		    // need to add these anyway?
			p.addTauBData(time, 0.0); // taub 
			p.addErosionFluxData(time, 0.0); // erosion flux
			
			
			try {
				Thread.sleep(500L);
			} catch (Exception e) {
			}
		}

	}

}
