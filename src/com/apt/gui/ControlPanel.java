/**
 * 
 */
package com.apt.gui;

import java.awt.FlowLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.imageio.ImageIO;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;

import com.apt.engine.IEngine;
import com.apt.engine.IEngineController;

/**
 * @author SA05SF
 *
 */
public class ControlPanel {
	
	private IEngineController controller;
	
	public JPanel createPanel(IEngineController controller) throws Exception {
		
		this.controller = controller;
		
		JPanel panel = new JPanel(true);
		panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
		
		// pause/resume/abort
	
		JButton pauseBtn = new JButton(getIcon("pause"));
	
		ActionListener pauseListener = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				controller.pauseEngine();
			}
		};
		pauseBtn.addActionListener(pauseListener);
		panel.add(pauseBtn);
		
		JButton resumeBtn = new JButton(getIcon("resume"));
		ActionListener resumeListener = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				controller.resumeEngine();
			}
		};
		resumeBtn.addActionListener(resumeListener);
		panel.add(resumeBtn);
		
		JButton abortBtn = new JButton(getIcon("abort"));
		ActionListener abortListener = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				controller.abortEngine();
			}
		};
		abortBtn.addActionListener(abortListener);
		panel.add(abortBtn);
	
		return panel;
	}

	private ImageIcon getIcon(String name) throws Exception {
		
		 ImageIcon icon = new ImageIcon(ImageIO.read(ClassLoader.getSystemResourceAsStream("com/apt/gui/resources/"+name+".png")));
		 return icon;
		 
	}

}
