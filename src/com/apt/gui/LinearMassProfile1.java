/**
 * 
 */
package com.apt.gui;

import java.awt.Font;
import java.text.SimpleDateFormat;

import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.annotations.XYAnnotation;
import org.jfree.chart.annotations.XYTextAnnotation;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.CombinedDomainXYPlot;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.DefaultXYDataset;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

/**
 * @author SA05SF
 *
 */
public class LinearMassProfile1 {


	static Font TITLE_FONT = new Font("Tahoma", Font.PLAIN, 16);
	static Font SUB_FONT = new Font("Tahoma", Font.ITALIC, 14);
	
	XYSeries series1;
	XYSeries series2;
	XYSeries series3;
	
	XYSeriesCollection xsc1;
	XYSeriesCollection xsc2;
	XYSeriesCollection xsc3;
	
	XYPlot plot1;
	XYTextAnnotation a1;
	XYTextAnnotation a2;
	
	public ChartPanel createChartPanel(String title) {

		//SimpleDateFormat sdf = new SimpleDateFormat("DD:HH");
		//DateAxis dx = new DateAxis("Date/Time");
		//dx.setDateFormatOverride(sdf);
		
		NumberAxis naxis = new NumberAxis("Cell location [m]");
		
		CombinedDomainXYPlot all = new CombinedDomainXYPlot(naxis);
		all.setGap(10);
		
		// Series #1 Surface mass v cell location (x)
		series1 = createDataset("Surface layer mass");
		xsc1 = new XYSeriesCollection(series1);
		NumberAxis yaxis = new NumberAxis("Surface mass [kg]");
		yaxis.setRange(0.0, 3500.0);
		plot1 = new XYPlot(xsc1, naxis, yaxis, new XYLineAndShapeRenderer(false, true));
		a1 = new XYTextAnnotation("Flow", 100.0, 1500.0);
		a1.setFont(new Font("serif", Font.BOLD, 18));
		plot1.addAnnotation(a1);
		a2 = new XYTextAnnotation("Time", 100.0, 1650.0);
		a2.setFont(new Font("serif", Font.BOLD, 18));
		plot1.addAnnotation(a2);
		all.add(plot1, 2);
	
		
		JFreeChart chart = new JFreeChart(title, all);
		chart.getTitle().setFont(TITLE_FONT);
		
		ChartPanel p = new ChartPanel(chart);
		return p;
	}
	
	
	private XYSeries createDataset(String label) {
		XYSeries series = new XYSeries("test");
		return series;
	}
	
	// x=cell location, y=surfacemass
	public void addDataA(double x, double y) {
		series1.addOrUpdate(x, y);
	}
	
	public void addInfo(long time, double uflow) {
		a1.setText(String.format("Flow: %.2f", uflow));
		a2.setText(String.format("Time: %d h", time/3600000));
	}
	
}
