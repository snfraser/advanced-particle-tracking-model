/**
 * 
 */
package com.apt.gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.util.Iterator;
import java.util.List;

import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import com.apt.models.Cage;
import com.apt.data.Particle;
import com.apt.data.ParticlesState;
import com.apt.models.IBathymetryModel;
import com.apt.models.ICageLayoutModel;
import com.apt.models.bathymetry.FixedBathymetryModel;
import com.apt.models.cage.CageLayoutModel;
import com.apt.models.cage.ICagePositionManager;

/**
 * @author SA05SF
 *
 */
public class SurfaceDepositionPanel extends JPanel {

	private final double bb = 1000.0;
	private final Color[] colors = new Color[] { Color.white, Color.blue.brighter(), Color.blue, Color.green,
			Color.yellow, Color.orange.darker(), Color.orange, Color.red };

	private final Color LAND_COLOR = Color.GRAY.brighter();

	private final Font TFONT = new Font("Serif", Font.PLAIN, 20);

	private IBathymetryModel bathymetry;
	private ICageLayoutModel layout;
	private ICagePositionManager pm;

	int[][] surface;
	int nx;
	int ny;

	int maxcount = 10000; // maximum count in any cell

	int lostcount;

	private double x0;
	private double y0;

	private double dx;
	private double dy;

	private long time;
	
	private double scale = 0.0;

	/**
	 * @param surface
	 */
	public SurfaceDepositionPanel(IBathymetryModel bathymetry, ICageLayoutModel layout) {
		this.bathymetry = bathymetry;
		this.layout = layout;
		this.pm = layout.getPositionManager();
	
		nx = (int) (bathymetry.getDomainWidth() / 25.0)+1;
		ny = (int) (bathymetry.getDomainHeight() / 25.0)+1;
		dx = bathymetry.getDomainWidth();
		dy = bathymetry.getDomainHeight();
		x0 = bathymetry.getDomainX0();
		y0 = bathymetry.getDomainY0();
		
		System.err.printf("Surface: Domain: start: (%f, %f) width: (%f x %f) \n", x0, y0, dx, dy);
		System.err.printf("Surface size: %d x %d \n", nx, ny);
		surface = new int[ny][nx];
		for (int j = 0; j < ny; j++) {
			for (int i = 0; i < nx; i++) {
				surface[j][i] = 0;
			}
		}
		
		
	}

	@Override
	public void paint(Graphics g) {
		super.paint(g);
		
		//double scalex = getSize().width/dx;
		//double scaley = getSize().height/dy;
		//scale = Math.min(scalex, scaley);
		

		// BED HEATMAP
		for (int j = 0; j < ny; j++) {
			for (int i = 0; i < nx; i++) {
				// color from number in cell
				// int nc = (int)Math.rint((double)surface[j][i]/500.0);
				// System.err.printf("Nc for: %d %d : %d -> %d \n",j ,i, surface[j][i], nc);
				// if (nc > 7) nc = 7;
				// Color c = colors[nc];
				int count = surface[j][i];
				float r = 1.0f - (float) count / (float) maxcount;
				if (r < 0.0f)
					r = 0.0f;
				// Color c = new Color(r, 1.0f, 1.0f);
				Color c = new Color(1.0f, 0.95f * r, 0.95f * r);

				// if (count == 0)
				// c = Color.gray.brighter();
				double xg = x0 + 25 * i;
				double yg = y0 + 25 * j;
				int x = getScreenX(xg);
				int y = getScreenY(yg);
				int d = Math.abs(getScreenX(25.0) - getScreenX(0.0)) + 1;

				if (bathymetry.getDepth(xg, yg) >= 0.0)
					g.setColor(LAND_COLOR);
				else
					g.setColor(c);

				g.fillRect(x, y - d, d, d);
				// if (nc > 1)
				// System.err.printf("Surface plot: (%d, %d) : %d,%d %d \n",i,j,x,y,d);
			}
		}

		// GRID
		showCrosshairs(g);
		
		// CAGES
		showCages(g);
		
		// TIMESTAMP
		showData(g);
	}

	
	
	
	private int getScreenX(double x) {

		double ww = (double) getSize().width;
		int gx = (int) (ww * (x - x0) / dx);
		return gx;

	}

	private int getScreenY(double y) {

		double hh = (double) getSize().height;
		int gy = (int) (hh * (1.0 - (y - y0) / dy));
		return gy;
	}

	
	
	
	
	private void showData(Graphics g) {
		g.setColor(Color.black);
		g.setFont(TFONT);
		// g.drawString(String.format("T: %f", (double)time/86400000.0), 150, 100);
		g.drawString(String.format("Time: %tj:%tH", time, time), 50, 50);
		g.drawString(String.format("Lost: %d", lostcount), 50, 100);
		
	}

	
	
	private void showCrosshairs(Graphics g) {
		g.setColor(Color.red);
		int ww = getSize().width;
		int hh = getSize().height;
		g.drawLine(ww / 2, 0, ww / 2, hh);
		g.drawLine(0, hh / 2, ww, hh / 2);
	}

	private void showCages(Graphics g) {
		for (Cage c : layout.cages()) {
			double cx = pm.getPosition(c.getName()).getX();
			double cy = pm.getPosition(c.getName()).getY();
			int sx = getScreenX(cx - 0.5 * c.getWidth());
			int sy = getScreenY(cy - 0.5 * c.getLength());
			int w = Math.abs(getScreenX(c.getWidth()) - getScreenX(0.0));
			int h = Math.abs(getScreenY(c.getLength()) - getScreenY(0.0));
			//System.err.printf("SDP:: cage %s at:: (%f,%f) ij: [%d, %d] w: %d h: %d \n", c.getName(), cx, cy, sx, sy, w, h);
			g.setColor(Color.black);
			g.drawOval(sx, sy, w, h);
		}
	}

	

	public void addParticles(long time, List<Particle> particles) {
		this.time = time;
		// System.err.println("Surf::Add particles..."+particles.size());
		for (Particle p : particles) {

			if (p.getState() == ParticlesState.LOST) {
				lostcount++;
				continue;
			}
			// work out which cell
			int ii = (int) ((p.getX() - x0) / 25.0);
			int jj = (int) ((p.getY() - y0) / 25.0);
			if (ii < 0 || ii >= nx || jj < 0 || jj >= ny)
				continue; // OOD
			// System.err.printf("Surf: particle at: i %d j %d \n", ii, jj);
			surface[jj][ii]++;
			// record the maximum count in any cell, this will be used to scale
			if (surface[jj][ii] > maxcount) {
				maxcount = surface[jj][ii];
			}
			// System.err.printf("surface: %d %d -> %d\n", jj,ii,surface[jj][ii]);
		}

		// this.time = time;
		if (time % 3600000 == 0) {
			// System.err.println("Surf::repaint....");
			final JComponent fthis = this;
			SwingUtilities.invokeLater(new Runnable() {

				@Override
				public void run() {
					fthis.repaint();
				}
			});

		}
	}

}
