package com.apt.gui;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.JPanel;

import com.apt.data.Domain;
import com.apt.data.Mesh;
import com.apt.data.SeabedType;
import com.apt.models.ISeabedModel;


/**
 * Display seabed data.
 * 
 * @author SA05SF
 *
 */
public class SeabedStateDisplay extends JPanel {

	ISeabedModel seabed;

	Mesh mesh;

	Domain domain;

	/**
	 * @param seabed
	 * @param mesh
	 */
	public SeabedStateDisplay(ISeabedModel seabed, Mesh mesh) {
		super();
		this.seabed = seabed;
		this.mesh = mesh;
		//this.domain = mesh.getDomainBounds(); // corrrect
		this.domain = seabed.getDomain();
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2 = (Graphics2D) g;
		g2.setColor(Color.red);

		// split the domain into a regular grid 80x80 for now
		double dx = (domain.x1 - domain.x0) / 80;
		double dy = (domain.y1 - domain.y0) / 80;

		for (int j = 0; j < 80; j++) {
			double x = domain.x0 + j * dx;
			for (int i = 0; i < 80; i++) {
				double y = domain.y0 + i * dy;
				// at (x,y) and upto x+dx, y+dy
				// fill the area with a number of points, number is reverse linear on av grain
				// size
				//System.err.printf("check... [%d, %d] at (%f, %f) \n", j, i, x, y);
				double grainmm = seabed.getSeabed(x + dx / 2, y + dy / 2).getAverageGrainSize() * 1000.0;
				SeabedType type = seabed.getSeabed(x + dx / 2, y + dy / 2).getType();
				int nn = getNumberPoints(type);
				Color color = getColor2(type);
				g2.setColor(color);
				// System.err.printf(" [%d, %d] at (%f, %f) :: GS: %fmm NN: nn\n", j, i, x, y,
				// grainmm, nn);

				for (int k = 0; k < nn; k++) {
					double xc = x + Math.random() * dx;
					double yc = y + Math.random() * dy;

					int ix = getScreenX(xc);
					int iy = getScreenY(yc);

					g2.fillOval(ix, iy, 4, 4);
				}

			}
		}

		// draw the cell boundaries...
		/*g2.setColor(Color.BLUE);
		for (int k = 0; k < 50; k++) {
			double x = domain.x0 + Math.random() * (domain.x1 - domain.x0);
			double y = domain.y0 + Math.random() * (domain.y1 - domain.y0);
			Cell c = mesh.findCell(x, y, null);
			if (c != null) {
				Node[] nodes = c.nodes;
				int x0 = getScreenX(nodes[0].x);
				int y0 = getScreenY(nodes[0].y);
				for (int in = 1; in < nodes.length; in++) {
					int x1 = getScreenX(nodes[in].x);
					int y1 = getScreenY(nodes[in].y);
					g2.setStroke(new BasicStroke(2.0f));
					g2.drawLine(x0, y0, x1, y1);
					x0 = x1;
					y0 = y1;
				}

			}
		}*/

	}

	private int getScreenX(double x) {
		int ww = getSize().width;
		int ix = (int) ((x - domain.x0) * ww / (domain.x1 - domain.x0));
		return ix;
	}

	private int getScreenY(double y) {
		int hh = getSize().height;
		int iy = hh - (int) ((y - domain.y0) * hh / (domain.y1 - domain.y0));
		return iy;
	}

	private Color getColor2(SeabedType type) {
		switch (type) {
		case CLAY:
			return new Color(224,224,224);
		case MUD:
			return new Color(192,192,192);
		case SAND:
			return new Color(160,160,160);
		case GRAVEL:
			return new Color(128,128,128);
		case PEBBLE:
			return new Color(96,96,96);
		case COBBLE:
			return new Color(64,64,64);
		default:
			return new Color(32,32,32);
		}
		
		
	}
	
	private Color getColor(SeabedType type) {
		switch (type) {
		case CLAY:
			return Color.gray;
		case MUD:
			return Color.orange.darker();
		case SAND:
			return Color.yellow;
		case GRAVEL:
			return Color.gray.brighter();
		case PEBBLE:
			return Color.cyan;
		case COBBLE:
			return Color.pink;
		default:
			return Color.white;
		}
	}
	
	private int getNumberPoints(SeabedType type) {
		switch (type) {
		case CLAY:
			return 50;
		case MUD:
			return 30;
		case SAND:
			return 15;
		case GRAVEL:
			return 8;
		case PEBBLE:
			return 5;
		case COBBLE:
			return 2;
		default:
			return 1;
		}
	}

}
