/**
 * 
 */
package com.apt.gui;

import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JPanel;

import com.apt.data.Cell;
import com.apt.data.Node;
import com.apt.data.PositionVector;
import com.apt.data.Track;

/**
 * 
 */
public class ParticleMeshTrackingPanel extends JPanel {
	
	static Color[] colors = new Color[] {Color.gray, Color.green, Color.magenta, Color.blue, Color.pink, Color.cyan };
	
	int it = 1;
	
	private double x0;
	private double y0;
	private double wx;
	private double wy;
	
	List<Track> tracks;
	
	List<Cell> cells;
	
	/**
	 * @param x0
	 * @param y0
	 * @param wx
	 * @param wy
	 */
	public ParticleMeshTrackingPanel(double x0, double y0, double wx, double wy) {
		super();
		this.x0 = x0;
		this.y0 = y0;
		this.wx = wx;
		this.wy = wy;
		tracks = new ArrayList<Track>();
		cells = new ArrayList<Cell>();
	}

	@Override
	public void paint(Graphics g) {
		super.paint(g);
		
		// draw centre cross-hairs
		g.setColor(Color.red);
		g.drawLine(getScreenX(x0+wx/2), getScreenY(y0),getScreenX(x0+wx/2), getScreenY(y0+wy));
		g.drawLine(getScreenX(x0), getScreenY(y0+wy/2),getScreenX(x0+wx), getScreenY(y0+wy/2));
		
		// draw cells
		g.setColor(Color.gray);
		for (Cell c : cells) {
			Node[] nodes = c.nodes;
			int sx = getScreenX(nodes[0].x);
			int sy = getScreenY(nodes[0].y);
			int nx = sx;
			int ny = sy;
			int ax = 0;
			int ay = 0;
			for (int in = 1; in < nodes.length; in++) {
				ax = getScreenX(nodes[in].x);
				ay = getScreenY(nodes[in].y);
				g.drawLine(nx, ny, ax, ay);
				nx = ax;
				ny = ay;
			}
			g.drawLine(nx, ny, sx, sy);
		}
		
		// loop tracks
		try {
		int itrack = 0;
		for (Track t : tracks) {
			itrack++;
			g.setColor(colors[itrack % 6]);
			List<PositionVector> points = t.getPoints();
			int ip = 0;
			for (PositionVector p : points) {
				int xp = getScreenX(p.getX());
				int yp = getScreenY(p.getY());
				
				ip++;
				if (ip == 1) {
					g.fillRect(xp, yp, 5, 5);
				} else if (ip == points.size()) {
					g.drawOval(xp, yp, 5, 5);
				} else {
					g.fillOval(xp, yp, 2, 2);
				}
				
				
			}
			
		}
		} catch (Exception e) {
			// ignore it....its likley a conc modex
		}
		
	}
	
	private int getScreenX(double x) {
		double ww = (double) getSize().width;
		int gx = (int) (ww * (x - x0) / wx);
		return gx;

	}

	private int getScreenY(double y) {
		double hh = (double) getSize().height;
		int gy = (int) (hh * (1.0 - (y - y0) / wy));
		return gy;
	}
	
	public void addTrackPoint(int itrack, double x, double y) {
		it++;
		if (tracks.size() <= itrack)
			tracks.add(new Track());
		tracks.get(itrack).addPoint(new PositionVector(x, y, 0));
		if (it % 100 == 0)
			repaint();
	}
	
	public void addCell(Cell c) {
		if (!cells.contains(c)) {
			cells.add(c);
			repaint();
		}
	}

}
