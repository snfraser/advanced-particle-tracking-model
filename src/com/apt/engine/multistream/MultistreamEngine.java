package com.apt.engine.multistream;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import com.apt.data.ChemicalComponent;
import com.apt.data.ChemicalComponentType;
import com.apt.data.Particle;
import com.apt.data.ParticleClass;
import com.apt.engine.IEngine;
import com.apt.instrumentation.Instrumentation;
import com.apt.instrumentation.NullInstrumentation;
import com.apt.models.IBedCellModel;
import com.apt.models.IBedLayoutModel;
import com.apt.models.ICageLayoutModel;
import com.apt.models.IDefacationModel;
import com.apt.models.IGrowthModel;
import com.apt.models.IMortalityModel;
import com.apt.models.ISeasonalityModel;
import com.apt.models.ITimeModel;
import com.apt.models.bed.BedCellModel;
import com.apt.models.bed.RegularGridBedLayoutModel;
import com.apt.models.cage.CageLayoutModel;
import com.apt.models.defaecation.DefacationModel;
import com.apt.models.growth.GrowthModel;
import com.apt.models.mortality.MortalityModel;
import com.apt.models.seasonality.SeasonalityModel;
import com.apt.models.time.TimeModel;
import com.apt.models.transport.TransportFactory;
import com.apt.models.transport.TransportModel;
import com.apt.project.RunConfiguration;

public class MultistreamEngine implements IEngine {

	ITimeModel tm;
	TransportFactory transf;
	Instrumentation instruments;
	ICageLayoutModel cm;
	IGrowthModel gm;
	IMortalityModel mm;
	IDefacationModel dm;
	IBedLayoutModel bed;
	ISeasonalityModel sem;
	
	ParticleCollator collator;
	ParticleRouter router;

	PrintWriter out; // TEST
	
	/**
	 * 
	 */
	public MultistreamEngine()  {
		
	}

	public MultistreamEngine(TimeModel tm, TransportFactory transf, CageLayoutModel cm, GrowthModel gm,
			DefacationModel dm, MortalityModel mm, SeasonalityModel sem, RegularGridBedLayoutModel bed,
			Instrumentation instruments) {
		this();
		this.tm = tm;
		this.transf = transf;
		this.cm = cm;
		this.gm = gm;
		this.dm = dm;
		this.mm = mm;
		this.sem = sem;
		this.bed = bed;
		this.instruments = instruments;

	   
	}
	
	@Override
	public void setBreakPoint() {}

	public void run(PrintWriter out) {
		
		collator = new ParticleCollator();
		router = new ParticleRouter(transf, collator);
		
		instruments.runLog("engine", "MULTISTREAMx"+router.getMaxNumExecutors());
		
		int numParticlesReleased = 0;
		long start = System.currentTimeMillis();
		
		tm.reset();
	
		while (!tm.end()) {

			collator.reset();
			
			long time = tm.time();
			
			System.err.println();
			System.err.printf("MSE:: Cycle: %d at: %d starting...\n", tm.getNStep(), time);
			
			
			List<Particle> feed = createFeedParticles();
			router.trackParticles(feed);
			numParticlesReleased += feed.size();
			
			List<Particle> faeces = createFaecesParticles();
			router.trackParticles(faeces);
			numParticlesReleased += faeces.size();
			
			List<Particle> resus = createResusParticles();
			router.trackParticles(resus);
			numParticlesReleased += resus.size();
			
			router.execute(time);
			
			List<Particle> landedParticles = collator.getLandedParticles();
			// handle particles that just landed on the seabed.
			// ALT:: replace below with....  bed.deposit(landedParticles);
			for (Particle p : landedParticles) {
				try {
					IBedCellModel cell = bed.getBedModel(p.getX(), p.getY());
					//System.err.printf("MSE:: Deposit on cell: %s Got Particle: %s \n", cell, p);
					// System.err.printf("TE1:: Got particle with masses: %s \n", p.getMasses());
					cell.deposit(p);
				} catch (Exception e) {
					System.err.printf("MSE::bed processing: No cell element at: %f %f : %s\n", p.getX(), p.getY(), e.getMessage());
				}
			}
			
			// TEMP
			instruments.addParticles(time, landedParticles);
			
			tm.step();
			
			//try {Thread.sleep(1000L);} catch (InterruptedException ix) {}
			
		} // next timestep

		router.shutdown();
		
		List<IBedCellModel> cells = bed.getCells();
		double maxMassPerCell = -999.99;
		for (IBedCellModel c : cells) {
			//ChemicalComponent solids = c.getSurface().getComponent(ChemicalComponentType.SOLIDS);
			ChemicalComponent solids = c.getLayers().getSurfaceLayer().getMass().getComponent(ChemicalComponentType.SOLIDS);
			if (solids != null) {
				if (solids.getMass() > maxMassPerCell) {
					maxMassPerCell = solids.getMass();
				}
			}
		}
		
		//instruments.updateBed(bed, time);
		
		long duration = System.currentTimeMillis() - start;
		double runDays = ((double)tm.getRunDuration()/86400000.0); 
		double maxDepositionConcentration = maxMassPerCell*(365.25/runDays)/625.0;
		System.err.printf("Maximum deposition of solids in a cell: %f kg flux: %f kg/m2/yr \n", maxMassPerCell, maxDepositionConcentration);
		System.err.printf("Total particles tracked: %d in %f sec \n", numParticlesReleased, duration/1000.0);
		
		instruments.runLog("duration", ""+runDays);
		instruments.runLog("max.concentration", ""+maxDepositionConcentration);
		instruments.runLog("total.particle", ""+numParticlesReleased);
		
		
	}

	private List<Particle> createFeedParticles() {
		
		int NP = 5 + (int)(Math.random()*4);
		
		List<Particle> particles = new ArrayList<Particle>();
		for (int i = 0; i < NP; i++) {
		Particle p = new Particle();
		p.setStartPosition(900.0+Math.random()*200.0, 900.0+Math.random()*200.0, 0.5);
		p.setType(ParticleClass.FEED);
		double stockMass = 2000.0;
		double wasteSolidsMass = 0.007*stockMass*0.03;
		p.addChemical(new ChemicalComponent(ChemicalComponentType.SOLIDS, wasteSolidsMass));
		p.addChemical(new ChemicalComponent(ChemicalComponentType.CARBON_REFACTORY, 0.0005 + Math.random() * 0.0002));
		p.addChemical(new ChemicalComponent(ChemicalComponentType.TREATMENT_EMBZ, 0.002 + Math.random() * 0.0001));
		particles.add(p);
		}
		return particles;
	}
	
	private List<Particle> createFaecesParticles() {
		int NP = 5 + (int)(Math.random()*5);
		
		List<Particle> particles = new ArrayList<Particle>();
		for (int i = 0; i < NP; i++) {
		Particle p = new Particle();
		p.setStartPosition(900.0+Math.random()*200.0, 900.0+Math.random()*200.0, 0.5);
		p.setType(ParticleClass.FAECES);
		double stockMass = 2000.0;
		double wasteSolidsMass = 0.007*stockMass*0.87;
		p.addChemical(new ChemicalComponent(ChemicalComponentType.SOLIDS, wasteSolidsMass));
		p.addChemical(new ChemicalComponent(ChemicalComponentType.CARBON_REFACTORY, 0.0005 + Math.random() * 0.0002));
		p.addChemical(new ChemicalComponent(ChemicalComponentType.TREATMENT_EMBZ, 0.002 + Math.random() * 0.0001));
		particles.add(p);
		}
		return particles;
	}
	
	private List<Particle> createResusParticles() {
		int NP = 5 + (int)(Math.random()*5);
		
		List<Particle> particles = new ArrayList<Particle>();
		for (int i = 0; i < NP; i++) {
		Particle p = new Particle();
		p.setStartPosition(900.0+Math.random()*200.0, 900.0+Math.random()*200.0, 0.5);
		p.setType(ParticleClass.MUD);
		double wasteSolidsMass = 10.0 + Math.random()*50.0;
		p.addChemical(new ChemicalComponent(ChemicalComponentType.SOLIDS, wasteSolidsMass));
		p.addChemical(new ChemicalComponent(ChemicalComponentType.CARBON_REFACTORY, 0.0005 + Math.random() * 0.0002));
		p.addChemical(new ChemicalComponent(ChemicalComponentType.TREATMENT_EMBZ, 0.002 + Math.random() * 0.0001));
		particles.add(p);
		}
		return particles;
	}

	@Override
	public void run(RunConfiguration runConfig) throws Exception {
		tm = runConfig.getTimeModel();
		transf = runConfig.getTransportFactory();
		
		gm = runConfig.getGrowthModel(); 
		dm = runConfig.getDefaecationModel();
		mm = runConfig.getMortalityModel();
		bed = runConfig.getBedLayoutModel();
		sem = runConfig.getSeasonalityModel();
		
		cm = runConfig.getCageLayoutModel();
		

	    collator = new ParticleCollator();
		router = new ParticleRouter(transf, collator);
		
		// controller opens a few files in the runfolder..
		File output = new File(runConfig.getRunFolder(), "run.log");
		FileWriter writer = new FileWriter(output);
		out = new PrintWriter(new BufferedWriter(writer));
		out.printf("Engine:: %s :: Starting model: %s run: %d \n", "MULTISTREAMENGINE", runConfig.getName(), runConfig.getRun());
		
		instruments = new NullInstrumentation(null);
		
		// rename this as the generic method of IEngine??
		run(out);
		
		instruments.outputRunLog();
		
		out.close();
	}
}
