package com.apt.engine.multistream;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import com.apt.data.Particle;
import com.apt.models.transport.TransportFactory;
import com.apt.models.transport.TransportModel;

public class ParticleRouter {

	/** Number of particles per executor, */
	private static final int NUM_PARTICLES_PER_EXECUTOR = 5;

	/** How many executors to use. */
	private static final int NUM_EXECUTORS = 8;

	private ExecutorService executor;

	// creates list of trackers..
	private TransportFactory transFactory;
	
	private ParticleCollator collator;
	
	List<Particle> allParticles;

	/**
	 * @param transFactory
	 */
	public ParticleRouter(TransportFactory transFactory, ParticleCollator collator) {
		super();
		this.transFactory = transFactory;
		this.collator = collator;
		allParticles = new ArrayList<Particle>();

		executor = Executors.newFixedThreadPool(NUM_EXECUTORS);

	}

	/**
	 * Request a set of particles to be tracked. This can be called several times in
	 * a timestep to handle different types of particles. e.g. feed, faeces and
	 * resuspension types. Once all the calls have been made, the executors
	 * (trackers) can be set running by a call to execute(). Only at this point are
	 * the particles are split up into groups and assigned to trackers.
	 * 
	 * @param particles A list of particles to track.
	 */
	public void trackParticles(List<Particle> particles) {
		// collect the particles
		allParticles.addAll(particles);
	}
	
	public void shutdown() {
		executor.shutdownNow();
	}
	
	/** Rests the particles list when a time-step has completed.*/
	public void reset() {
		allParticles.clear();
	}

	/**
	 * Assigns the collection of particles to their executors (trackers). For small
	 * numbers of particles only a few executors are used with
	 * NUM_PARTICLES_PER_EXECUTOR assigned to each executor. For larger numbers, all
	 * executors are used with the particles split between them. NOTE: This call
	 * blocks waiting for all tasks to complete...
	 */
	public void execute(long time) {
		
	
		// split the particles to executors
		// send MAX_PARTICLES to each executor
		int numberParticles = allParticles.size();
		int numParticlesPerExecutor = NUM_PARTICLES_PER_EXECUTOR;

		int numExecutorsToUse = 1 + (numberParticles / NUM_PARTICLES_PER_EXECUTOR);

		if (numExecutorsToUse > NUM_EXECUTORS) {
			// split the particles between ALL AVAILABLE executors
			numParticlesPerExecutor = numberParticles / NUM_EXECUTORS;
			numExecutorsToUse = NUM_EXECUTORS;
		}

		System.err.printf("Router: time: %d tracking %d particles, ppx: %d using: %d executors\n", 
				time, allParticles.size(), numParticlesPerExecutor, numExecutorsToUse);
		
		long runstart = System.currentTimeMillis();
		
		List[] lists = new ArrayList[numExecutorsToUse];
		for (int i = 0; i < numExecutorsToUse; i++) {
			lists[i] = new ArrayList<Particle>();
		}
		for (int i = 0; i < numberParticles; i++) {
			int r = i % numExecutorsToUse; // this is the executor to use for this particle
			lists[r].add(allParticles.get(i));
		}

		// all particles are assigned to a list.
		// launch the set of executors with the attached lists to track.
		List<Callable<String>> tasks = new ArrayList<Callable<String>>();
		for (int i = 0; i < numExecutorsToUse; i++) {
			TransportModel trans = transFactory.getTransportModel(null); // new model each time??
			Callable<String> tracker = new Tracker(i, time, trans, lists[i]);
			tasks.add(tracker);
		}

		// we have the executors, now submit them
		try {
			List<Future<String>> results = executor.invokeAll(tasks);

			for (Future<String> result : results) {
				try {
					//System.out.println(result.get());
					result.get();
				} catch (ExecutionException ee) {
					// handle this....somehow
				}
			}
		} catch (InterruptedException ie) {
			ie.printStackTrace();
		}
		long runend = System.currentTimeMillis();
		long timeused = runend-runstart;
		double timeperpcl = (double)timeused/(double)numberParticles;
		System.err.printf("Router: completed, time used: %d ms, perpcl: %.2f ns\n", timeused, timeperpcl*1000);
		
		// transfer the completed particles to the collator
		collator.addCompletedParticles(time, allParticles);
		
		reset();
	}
	
	public int getMaxNumExecutors() { return NUM_EXECUTORS;}

}
