package com.apt.engine.multistream;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import com.apt.data.Particle;
import com.apt.data.ParticlesState;
import com.apt.models.transport.TransportModel;

/**
 * Task to invoke the transport model on a list of particles. This executor
 * tracks each particle until it lands or goes out-of-domain.
 * 
 * @author SA05SF
 *
 */
public class Tracker implements Callable<String> {

	/** Maximum time (time-steps) to track a particle before giving up on it. */
	private static final long MAX_TRACKING_TIME = 50 * 60000L; // 10 minutes max

	/** Tracker reference. */
	int ref;

	/** Time to start tracking. */
	long startTime;

	/** The list of particles to track. */
	List<Particle> particles;

	/** The Transport-model to use for moving the particles. */
	TransportModel trans;

	/**
	 * Tracker follows a set of particles until they either leave the domain or hit
	 * the seabed.
	 * 
	 * @param ref      A unique reference for this Tracker.
	 * @param time     The time to start tracking particles.
	 * @param trans    a transport model to move the particles.
	 * @param particle The list of particles to track.
	 */
	public Tracker(int ref, long time, TransportModel trans, List<Particle> particles) {
		super();
		this.ref = ref;
		this.startTime = time;
		this.trans = trans;
		this.particles = particles;
	}

	/**
	 * Call used by the execution service to invoke this Tracker.
	 */
	@Override
	public String call() throws Exception {

		// temp just sleep a while...
		// try {Thread.sleep(3000L + (long)(Math.random()*300));} catch
		// (InterruptedException ix) {}

		// use transport model to track each particle to landing spot or exit domain
		for (Particle p : particles) {
			// track the particle till it lands
			for (long ptime = startTime; ptime < startTime + MAX_TRACKING_TIME; ptime += 60000) {
				trans.move(p, ptime, 60000);
				if (p.getState() != ParticlesState.SUSPEND) {
					// lost or landed, remove from list
					break; // done with this particle
				}
			}

		}

		return String.format("Completed tracker: %d with %d particles", ref, particles.size());
	}

}
