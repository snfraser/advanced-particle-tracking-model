package com.apt.engine.multistream.test;

import java.io.File;
import java.io.PrintWriter;

import com.apt.models.Cage;
import com.apt.cli.util.ConfigurationProperties;
import com.apt.data.Domain;
import com.apt.data.SeabedType;
import com.apt.instrumentation.ParticleCellTracker;
import com.apt.engine.multistream.MultistreamEngine;
import com.apt.engine.x.TestEngine;
import com.apt.instrumentation.NullInstrumentation;
import com.apt.models.IFlowModel;
import com.apt.models.ISeabedModel;
import com.apt.models.ISettlingModel;
import com.apt.models.ITidalModel;
import com.apt.models.ITurbulenceModel;
import com.apt.models.bathymetry.FixedBathymetryModel;
import com.apt.models.bed.RegularGridBedLayoutModel;
import com.apt.models.cage.CageLayoutModel;
import com.apt.models.defaecation.DefacationModel;
import com.apt.models.feed.FeedModel;
import com.apt.models.flow.FixedFlowModel;
import com.apt.models.flow.NetcdfFlowModel;
import com.apt.models.growth.GrowthModel;
import com.apt.models.mortality.MortalityModel;
import com.apt.models.seabed.SeabedModel;
import com.apt.models.seasonality.SeasonalityModel;
import com.apt.models.settling.DefaultSettlingModel;
import com.apt.models.stock.StockModel;
import com.apt.models.tidal.FixedTidalModel;
import com.apt.models.time.TimeModel;
import com.apt.models.transport.EulerIntegrator;
import com.apt.models.transport.Integrator;
import com.apt.models.transport.TransportFactory;
import com.apt.models.transport.TransportModel;
import com.apt.models.turbulence.DefaultTurbulenceModel;


public class TestRunMultistreamWithTestTracker {

	static final String HOME = System.getProperty("user.home");
	static final File FF1 = new File(HOME, "mesh.nc");
	
	public static void main(String[] args) {

		ConfigurationProperties config = new ConfigurationProperties();
		config.setProperty("BedModel.Layers.minCount", "2");
		config.setProperty("BedModel.Layers.minCount", "5");
		config.setProperty("BedModel.Layers.consolidationTime", "30");
		config.setProperty("BedModel.Surface.h0z0", "1500");
		config.setProperty("BedModel.Layers.thresholdMass", "2.0");
		config.setProperty("BedModel.Layers.contractionTime", "900");
		config.setProperty("BedModel.Layers.expansionTime", "14400");
		config.setProperty("BedModel.CriticalStress.value", "0.2");
	
		try {
		
			NullInstrumentation noinst = new NullInstrumentation(null);
			
			// time-model: start, end, dt
			TimeModel tm = new TimeModel(0L, 60 * 86400 * 1000L, 60000L);

			// domain: x0, x1, dx, y0, y1, dy
			Domain domain = new Domain(0, 2000, 0, 2000);
			FixedBathymetryModel bm = new FixedBathymetryModel(domain);
			IFlowModel fm = new FixedFlowModel();
			ISeabedModel sea = new SeabedModel(SeabedType.MUD, 0.02, 0.0002, 0.0002);
			RegularGridBedLayoutModel bed = new RegularGridBedLayoutModel(bm, fm, sea, config);
			ITidalModel tidm = new FixedTidalModel(0.0);
			//IFlowModel fm = new NetcdfFlowModel(FF1);
			ITurbulenceModel turb = new DefaultTurbulenceModel(0.02, 0.02, 0.01);
			ISettlingModel settling = new DefaultSettlingModel();
		
			Integrator euler = new EulerIntegrator(fm);
			TransportFactory transf = new TransportFactory(tm, bm, fm, tidm, turb, settling, sea, euler);
			CageLayoutModel cm = new CageLayoutModel("cages");
			DefacationModel dm = new DefacationModel();
			MortalityModel mm = new MortalityModel(0.1);
			SeasonalityModel sem = new SeasonalityModel(0.0, 0.0);

			
			GrowthModel gm = new GrowthModel();

			NullInstrumentation none = new NullInstrumentation(null);
		
			MultistreamEngine engine = new MultistreamEngine(tm, transf, cm, gm, dm, mm, sem, bed, none);
			
			engine.run(new PrintWriter(System.out));
			
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
