package com.apt.engine.multistream;

import java.util.ArrayList;
import java.util.List;

import com.apt.data.Particle;
import com.apt.data.ParticlesState;

/**
 * Collects time and position information about landed and lost particles.
 * Maintains a queue of particle collections per time-step interval.
 *  
 * @author SA05SF
 *
 */
public class ParticleCollator {

	List<Particle> particles;
	
	/**
	 * 
	 */
	public ParticleCollator() {
		particles = new ArrayList<Particle>();
	}

	/**
	 * Reset the queue containing completed particles.
	 */
	public void reset() {
		particles.clear();
	}
	
	/**
	 * Add the list of completed particles to the queue in the appropriate cell.
	 * This method is called whenever a bunch of particles, which might be:
	 * <ul>
	 *  <li> FEED </li>
	 *  <li> FAECES </li>
	 *  <li> SEDIMENT </li>
	 * </ul>
	 * are completed. This will happen each timestep so at the end of the timestep 
	 * when they are all done the Engine should collect all of these and reset the queue.
	 * @param completedParticles The list of completed particles including time and location.
	 */
	public void addCompletedParticles(long time, List<Particle> completedParticles) {
		// make a copy of the list as original will be wiped
		List<Particle> latestParticles = new ArrayList<Particle>(completedParticles);
		
		particles.addAll(latestParticles);
		System.err.printf("Collator received: %d particles: now size: %d \n",latestParticles.size(), particles.size());

		// do some stats or whatever here.
		for (Particle p : latestParticles) {
			double tafterTs =  (double)(p.getLandingTime() - time)/60000.0;
					
			//System.err.printf("Collator: P: completed after: %.2f time steps\n", tafterTs);
		}
	}
	
	public List<Particle> getLandedParticles() {
		List<Particle> landedParticles = new ArrayList<Particle>();
		for (Particle p : particles) {
			if (p.getState() == ParticlesState.ONBED)
				landedParticles.add(p);
			else 
				System.err.printf("Collator: particle terminated: %s \n", p.toString());
		}
		return landedParticles;
	}
	
	
}
