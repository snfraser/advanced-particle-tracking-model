/**
 * 
 */
package com.apt.engine;

import com.apt.project.RunConfiguration;

/**
 * @author SA05SF
 *
 */
public interface IEngine {
	
	public void run(RunConfiguration runConfig) throws Exception;
	
	public void setBreakPoint();

}
