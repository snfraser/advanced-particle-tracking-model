/**
 * 
 */
package com.apt.engine;

import com.apt.engine.multistream.MultistreamEngine;
import com.apt.engine.x.TestEngine;

/**
 * @author SA05SF
 *
 */
public class EngineFactory {

	public static IEngine createEngine(String engineName) throws IllegalArgumentException {
		
		if (engineName == null || engineName.equalsIgnoreCase(""))
			throw new IllegalArgumentException("Engine name was not supplied");
		
		if ("TEST".equalsIgnoreCase(engineName.toUpperCase().trim())) {
			return new TestEngine();
		} else if ("MULTISTREAM".equalsIgnoreCase(engineName.toUpperCase().trim())) {
			return new MultistreamEngine();
		}
		
		throw new IllegalArgumentException("Unknown engine type: " + engineName);
	}
	
}
