/**
 * 
 */
package com.apt.engine.x.test;

import java.awt.BorderLayout;
import java.io.PrintWriter;

import javax.swing.JFrame;

import com.apt.models.Cage;
import com.apt.models.IChemicalDegradeModel;
import com.apt.cli.util.ConfigurationProperties;
import com.apt.data.Domain;
import com.apt.data.SeabedType;
import com.apt.instrumentation.NullInstrumentation;
import com.apt.instrumentation.ParticleCellTracker;
import com.apt.engine.x.TestEngine;
import com.apt.gui.FaecesDepositionRatePanel;
import com.apt.gui.FlowRatePanel;
import com.apt.gui.MassDepositionPanel;
import com.apt.gui.ParticleCountPanel;
import com.apt.gui.ParticleTrackPanel;
import com.apt.gui.SurfaceDepositionPanel;
import com.apt.instrumentation.TestInstrumentation;
import com.apt.models.ISeabedModel;
import com.apt.models.ISettlingModel;
import com.apt.models.ITurbulenceModel;
import com.apt.models.ITidalModel;
import com.apt.models.bathymetry.FixedBathymetryModel;
import com.apt.models.bed.RegularGridBedLayoutModel;
import com.apt.models.cage.CageLayoutModel;
import com.apt.models.cage.RegularGridCagePositionManager;
import com.apt.models.chemical.NoChemicalDegradeModel;
import com.apt.models.defaecation.DefacationModel;
import com.apt.models.disruption.DefaultDisruptionModel;
import com.apt.models.disruption.ExponentialDisruptionModel;
import com.apt.models.feed.FeedModel;
import com.apt.models.flow.FixedFlowModel;
import com.apt.models.growth.GrowthModel;
import com.apt.models.mortality.MortalityModel;
import com.apt.models.seabed.SeabedModel;
import com.apt.models.seasonality.SeasonalityModel;
import com.apt.models.settling.DefaultSettlingModel;
import com.apt.models.stock.StockModel;
import com.apt.models.tidal.FixedTidalModel;
import com.apt.models.time.TimeModel;
import com.apt.models.transport.BedTransportModel;
import com.apt.models.transport.EulerIntegrator;
import com.apt.models.transport.Integrator;
import com.apt.models.transport.TransportModel;
import com.apt.models.turbulence.DefaultTurbulenceModel;


/**
 * @author SA05SF
 *
 */
public class RunTestEngine1Plot {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		ConfigurationProperties config = new ConfigurationProperties();
		config.setProperty("BedModel.Layers.minCount", "2");
		config.setProperty("BedModel.Layers.minCount", "5");
		config.setProperty("BedModel.Layers.consolidationTime", "30");
		config.setProperty("BedModel.Surface.h0z0", "1500");
		config.setProperty("BedModel.Layers.thresholdMass", "2.0");
		config.setProperty("BedModel.Layers.contractionTime", "900");
		config.setProperty("BedModel.Layers.expansionTime", "14400");
		config.setProperty("BedModel.CriticalStress.value", "0.2");
	
		
		NullInstrumentation noinst = new NullInstrumentation(null);
		
		// time model 0 to 120 days at 1 minute intervals

		TimeModel tm = new TimeModel(0L, 90* 86400 * 1000L, 60000L);
		
		Domain domain = new Domain(0,2000,0,2000);
		FixedBathymetryModel bm = new FixedBathymetryModel(domain);
		FixedFlowModel fm = new FixedFlowModel();
		ISeabedModel sea = new SeabedModel(SeabedType.MUD, 0.02, 0.0002, 0.0002);
		
		RegularGridBedLayoutModel bed = new RegularGridBedLayoutModel(bm, fm, sea, config);
	
		DefaultDisruptionModel dim = new DefaultDisruptionModel();
		ITurbulenceModel turb = new DefaultTurbulenceModel(0.02, 0.02, 0.01);
		ITidalModel tidm = new FixedTidalModel(0.0);
		ISettlingModel settling = new DefaultSettlingModel();
		Integrator euler = new EulerIntegrator(fm);
		IChemicalDegradeModel chm = new NoChemicalDegradeModel();
		TransportModel trans = new TransportModel(bm, fm, tidm, turb, settling, sea, euler, noinst);
		BedTransportModel btrans = new BedTransportModel(bm, fm, turb, sea, euler, noinst);
		GrowthModel gm = new GrowthModel();
		DefacationModel dm = new DefacationModel();
		MortalityModel mm = new MortalityModel(0.1);
		SeasonalityModel sem = new SeasonalityModel(0.0, 0.0);
		CageLayoutModel clm = new CageLayoutModel("cages");
		// some cages
		
		RegularGridCagePositionManager pm = new RegularGridCagePositionManager(980, 880, 40.0, 60.0, 0.0);
		for (int i = 0; i < 2; i++) {
			double x = 980.0 + i * 40.0;
			for (int j = 0; j < 4; j++) {
				double y = 880.0 + (j+2) * 40.0;
				StockModel s = new StockModel(100000, 0.5);
				FeedModel f = new FeedModel(12*3600*1000L, 100, 0.5/1000.0, 0.03, 0.00005, 0.002, 0.007);
				String name = String.format("C%d:%d", i, j);
				Cage c = new Cage(name, x, y, 20.0, 20.0, 16.0, 8.0, 0.0, s, f);
				pm.add(c, i, j);
			}
		}
		
		clm.setPositionManager(pm);

		// setup graphics instrument display
		ParticleTrackPanel panel = new ParticleTrackPanel(bm, clm);
		SurfaceDepositionPanel panel2 = new SurfaceDepositionPanel(bm, clm);
		FaecesDepositionRatePanel panel3 = new FaecesDepositionRatePanel();
		MassDepositionPanel panel4 = new MassDepositionPanel(bm, clm);
		FlowRatePanel panel5 = new FlowRatePanel();
		ParticleCountPanel panel6 = new ParticleCountPanel();
	 
		TestInstrumentation test = new TestInstrumentation(null, null, null, null, panel4, null, null, null, null, null);

		
		// INSTRUMENTS
		/*JFrame f = new JFrame("Final particles on seabed");
		f.getContentPane().setLayout(new BorderLayout());
		f.getContentPane().add(panel, BorderLayout.CENTER);
		f.pack();
		f.setSize(600, 600);
		f.setLocation(200, 200);
		f.setVisible(true);*/

		/*JFrame f2 = new JFrame("Deposition on seabed");
		f2.getContentPane().setLayout(new BorderLayout());
		f2.getContentPane().add(panel2, BorderLayout.CENTER);
		f2.pack();
		f2.setSize(600, 600);
		f2.setLocation(900, 200);
		f2.setVisible(true);
		
		JFrame f3 = new JFrame("Deposition rate");
		f3.getContentPane().setLayout(new BorderLayout());
		f3.getContentPane().add(panel3.createChartPanel(), BorderLayout.CENTER);
		f3.pack();
		f3.setSize(800, 400);
		f3.setLocation(200, 600);
		f3.setVisible(true);*/
		
		JFrame f4 = new JFrame("Mass deposition");
		f4.getContentPane().setLayout(new BorderLayout());
		f4.getContentPane().add(panel4, BorderLayout.CENTER);
		f4.pack();
		f4.setSize(600,600);
		f4.setLocation(700, 500);
		f4.setVisible(true);
		
		/*JFrame f5 = new JFrame("Flow rate");
		f5.getContentPane().setLayout(new BorderLayout());
		f5.getContentPane().add(panel5.createChartPanel(), BorderLayout.CENTER);
		f5.pack();
		f5.setSize(800, 400);
		f5.setLocation(200, 600);
		f5.setVisible(true);*/
		
		/*JFrame f6 = new JFrame("Particles being tracked");
		f6.getContentPane().setLayout(new BorderLayout());
		f6.getContentPane().add(panel6.createChartPanel(), BorderLayout.CENTER);
		f6.pack();
		f6.setSize(800, 400);
		f6.setLocation(500, 400);
		f6.setVisible(true);*/
		
		TestEngine engine = new TestEngine(tm, trans, btrans, dim, clm, gm, dm, mm, sem, bed, sea, chm, test);
		engine.run();

	}

}
