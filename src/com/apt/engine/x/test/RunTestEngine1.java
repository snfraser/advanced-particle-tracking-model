/**
 * 
 */
package com.apt.engine.x.test;

import java.io.File;
import java.io.PrintWriter;

import com.apt.models.Cage;
import com.apt.models.IChemicalDegradeModel;
import com.apt.cli.util.ConfigurationProperties;
import com.apt.data.Domain;
import com.apt.data.SeabedType;
import com.apt.engine.x.TestEngine;
import com.apt.instrumentation.NullInstrumentation;
import com.apt.models.IFlowModel;
import com.apt.models.ISeabedModel;
import com.apt.models.ISettlingModel;
import com.apt.models.ITurbulenceModel;
import com.apt.models.ITidalModel;
import com.apt.models.bathymetry.FixedBathymetryModel;
import com.apt.models.bed.RegularGridBedLayoutModel;
import com.apt.models.cage.CageLayoutModel;
import com.apt.models.cage.RegularGridCagePositionManager;
import com.apt.models.chemical.NoChemicalDegradeModel;
import com.apt.models.defaecation.DefacationModel;
import com.apt.models.disruption.DefaultDisruptionModel;
import com.apt.models.disruption.ExponentialDisruptionModel;
import com.apt.models.feed.FeedModel;
import com.apt.models.flow.FixedFlowModel;
import com.apt.models.flow.NetcdfFlowModel;
import com.apt.models.growth.GrowthModel;
import com.apt.models.mortality.MortalityModel;
import com.apt.models.seabed.SeabedModel;
import com.apt.models.seasonality.SeasonalityModel;
import com.apt.models.settling.DefaultSettlingModel;
import com.apt.models.stock.StockModel;
import com.apt.models.tidal.FixedTidalModel;
import com.apt.models.time.TimeModel;
import com.apt.models.transport.BedTransportModel;
import com.apt.models.transport.EulerIntegrator;
import com.apt.models.transport.Integrator;
import com.apt.models.transport.TransportModel;
import com.apt.models.turbulence.DefaultTurbulenceModel;

/**
 * @author SA05SF
 *
 */
public class RunTestEngine1 {

	static final String HOME = System.getProperty("user.home");
	static final File FF1 = new File(HOME, "mesh.nc");

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		ConfigurationProperties config = new ConfigurationProperties();
		config.setProperty("BedModel.Layers.minCount", "2");
		config.setProperty("BedModel.Layers.minCount", "5");
		config.setProperty("BedModel.Layers.consolidationTime", "30");
		config.setProperty("BedModel.Surface.h0z0", "1500");
		config.setProperty("BedModel.Layers.thresholdMass", "2.0");
		config.setProperty("BedModel.Layers.contractionTime", "900");
		config.setProperty("BedModel.Layers.expansionTime", "14400");
		config.setProperty("BedModel.CriticalStress.value", "0.2");
	
		
		try {
			
			NullInstrumentation noinst = new NullInstrumentation(null);
		
			
			// time model 0 to 360 days at 1 minute intervals
			TimeModel tm = new TimeModel(0L, 50 * 86400 * 1000L, 60000L);

			Domain domain = new Domain(0, 2000, 0, 2000);
			FixedBathymetryModel bm = new FixedBathymetryModel(domain);
			IFlowModel fm = new FixedFlowModel();
			ISeabedModel sea = new SeabedModel(SeabedType.MUD, 0.02, 0.0002, 0.0002);
			RegularGridBedLayoutModel bed = new RegularGridBedLayoutModel(bm, fm, sea, config);
			
			//IFlowModel fm = new NetcdfFlowModel(FF1);
			ITurbulenceModel turb = new DefaultTurbulenceModel(0.02, 0.02, 0.01);
			//ExponentialDisruptionModel dim = new ExponentialDisruptionModel();
			DefaultDisruptionModel dim = new DefaultDisruptionModel();
			IChemicalDegradeModel chm = new NoChemicalDegradeModel();
		
			ITidalModel tidm = new FixedTidalModel(0.0);
			//ParticleCellTracker tracker = new ParticleCellTracker();
			ISettlingModel settling = new DefaultSettlingModel();
			Integrator euler = new EulerIntegrator(fm);
			TransportModel trans = new TransportModel(bm, fm, tidm, turb, settling, sea, euler, noinst);
			BedTransportModel btrans = new BedTransportModel(bm, fm, turb, sea, euler, noinst);
			CageLayoutModel cm = new CageLayoutModel("Cages");
			DefacationModel dm = new DefacationModel();
			MortalityModel mm = new MortalityModel(0.1);
			SeasonalityModel sem = new SeasonalityModel(0.0, 0.0);

			// some cages 2x6 array, 12 cages total, 250 tonnes per cage = 3000 tonnes total
			RegularGridCagePositionManager pm = new RegularGridCagePositionManager(980, 780, 40.0, 40.0, 0.0);
			
			
			for (int i = 0; i < 2; i++) {
				double x = 980.0 + i * 40.0;;
				for (int j = 0; j < 6; j++) {
					double y = 780.0 + j * 40.0;
					StockModel stock = new StockModel(100000, 0.5); // 5*100000 = 500 tonnes per cage
					FeedModel feed = new FeedModel(6 * 3600 * 1000L, 100, 0.5/1000.0, 0.03, 0.00005, 0.002, 0.007); // 6 hours (ms)
					String name = String.format("C%d:%d", i, j);
					// xy are not needed the regulargridmgr will sort these out
					Cage c = new Cage(name, x, y, 20.0, 20.0, 16.0, 8.0, 0.0, stock, feed);
					System.err.printf("Add cage: %s at (%f,%f) contains: %d fish avg: %f kg biomass: %f kg \n",
							c.getName(), c.getX(), c.getY(), stock.getStockNumber(), stock.getAverageFishMass(),
							stock.getBiomass());
					pm.add(c, i, j);
				}
			}
			
			cm.setPositionManager(pm);
			//cm.addCage(c);
			
			GrowthModel gm = new GrowthModel();

		
			TestEngine engine = new TestEngine(tm, trans, btrans, dim, cm, gm, dm, mm, sem, bed, sea, chm, noinst);
			engine.run();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
