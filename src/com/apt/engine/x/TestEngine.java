/**
 * 
 */
package com.apt.engine.x;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.swing.JOptionPane;

import com.apt.models.Cage;
import com.apt.cli.test.TestRunModel;
import com.apt.data.ChemicalComponent;
import com.apt.data.ChemicalComponentType;
import com.apt.data.CompoundMass;
import com.apt.data.Domain;
import com.apt.data.Particle;
import com.apt.data.ParticleClass;
import com.apt.data.ParticlesState;
import com.apt.data.Season;
import com.apt.engine.IEngine;
import com.apt.engine.IEngineController;
import com.apt.instrumentation.Instrumentation;
import com.apt.models.IBedCellModel;
import com.apt.models.IBedLayoutModel;
import com.apt.models.IBedTransportModel;
import com.apt.models.ICageLayoutModel;
import com.apt.models.IChemicalDegradeModel;
import com.apt.models.IDefacationModel;
import com.apt.models.IDisruptionModel;
import com.apt.models.IFeedModel;
import com.apt.models.IGrowthModel;
import com.apt.models.IMortalityModel;
import com.apt.models.ISeabedModel;
import com.apt.models.ISeasonalityModel;
import com.apt.models.ITimeModel;
import com.apt.models.ITransportModel;
import com.apt.models.bed.BedCellModel;
import com.apt.models.bed.BedLayerModel;
import com.apt.models.bed.RegularGridBedLayoutModel;
import com.apt.models.cage.CageLayoutModel;
import com.apt.models.cage.ICagePositionManager;
import com.apt.models.defaecation.DefacationModel;
import com.apt.models.feed.FeedModel;
import com.apt.models.growth.GrowthModel;
import com.apt.models.mortality.MortalityModel;
import com.apt.models.seasonality.SeasonalityModel;
import com.apt.models.stock.StockModel;
import com.apt.models.time.TimeModel;
import com.apt.models.transport.BedTransportFactory;
import com.apt.models.transport.TransportFactory;
import com.apt.models.transport.TransportModel;
import com.apt.project.RunConfiguration;

/**
 * A simple engine to get things going.
 * 
 * @author SA05SF
 *
 */
public class TestEngine implements IEngine, IEngineController {

	static final long T_BREAK_1 = 0L;

	static final long T_BREAK_2 = 5 * 86400 * 1000L;
	
	/** How often we monitor the selected observed cell.*/
	static final int MON_INT = 5;

	ITimeModel tm;
	ITransportModel trans;
	IBedTransportModel btrans;
	IDisruptionModel dim;
	IChemicalDegradeModel chm;
	Instrumentation instruments;
	ICageLayoutModel cm;
	IGrowthModel gm;
	IMortalityModel mm;
	IDefacationModel dm;
	IBedLayoutModel bedLayoutModel;
	ISeasonalityModel sem;
	ISeabedModel sea;

	// TEST - move to instruments or logging ...

	long runEndTime;
	long runStartTime;

	private volatile boolean breakPoint;

	private volatile boolean paused = false;
	private volatile boolean aborted = false;

	/**
	 * 
	 */
	public TestEngine() {
		// TODO any setup
	}

	/**
	 * @param tm
	 */
	public TestEngine(TimeModel tm, TransportModel trans, IBedTransportModel btrans, IDisruptionModel dim,
			CageLayoutModel cm, IGrowthModel gm, DefacationModel dm, MortalityModel mm, SeasonalityModel sem,
			RegularGridBedLayoutModel bed, ISeabedModel sea, IChemicalDegradeModel chm, Instrumentation instruments) {
		this.tm = tm;
		this.trans = trans;
		this.btrans = btrans;
		this.dim = dim;
		this.cm = cm;
		this.gm = gm;
		this.dm = dm;
		this.mm = mm;
		this.sem = sem;
		this.bedLayoutModel = bed;
		this.instruments = instruments;
	}

	@Override
	/**
	 * Set a breakpoint - called externally eg by a GUI to pause execution of the
	 * model.
	 */
	public void setBreakPoint() {
		this.breakPoint = true;
	}

	public void run() {
		runOut(null, null);
	}

	/**
	 * Run model using supplied output streams.
	 * @param out
	 * @param out2
	 */
	public void runOut(PrintWriter out, PrintWriter out2) {

		if (out == null) {
			out = new PrintWriter(System.out);
		}

		int i = 0;
		runStartTime = System.currentTimeMillis();

		// all particles currently in suspension
		List<Particle> particles = new ArrayList<Particle>();

		double initialBiomass = 0.0;
		double totalFeedReleased = 0.0;
		double totalFaeces = 0.0;
		double totalFeedWaste = 0.0;
		
		int totalFeedParticles = 0;
		int totalFaecesParticles = 0;
		
		int totalMorts = 0;
		int lostCount = 0;
		double lostMass = 0.0;

		CompoundMass lastRelease = null;

		ICagePositionManager pm = cm.getPositionManager();

		// Work out initial biomass
		List<Cage> c1 = cm.cages();
		for (Cage cage : c1) {
			StockModel stock = cage.getStock();
			initialBiomass += stock.getBiomass();
		}

		long time = 0L;

		// TEMP:: specify a cell to monitor - use instruments.observeCellAt(x,y) in future
		BedCellModel obscell = null;
		try {
			// Cell os = instruments.getObservedCell();

			IBedCellModel im = bedLayoutModel.getBedModel(343796.000000,1013751.00); // 346343.000000, 1028138.00
			// highest deposition 271312.500000,7138212.500000 east 271612.5, 7138412.5
			obscell = (BedCellModel) im;
			obscell.setObserved(true);
		} catch (Exception e) {
			// no can find this cell
			System.err.printf("XTE: Exception finding cell...at %f,%f \n", 950.0, 875.0);
			e.printStackTrace();
		}
		
	
		tm.reset();
		while (!tm.end()) {

			if (aborted)
				break;

			time = tm.time();

			// breakpoint...
			if (breakPoint) {
				int reply = JOptionPane.showConfirmDialog(null,
						String.format("Breakpoint: T: %tD:%tH Resume", time, time), "Breakpoint", JOptionPane.OK_OPTION,
						JOptionPane.QUESTION_MESSAGE);
				if (reply == JOptionPane.CANCEL_OPTION)
					return;
				if (reply == JOptionPane.OK_OPTION) {
					this.breakPoint = false;
				}

			}

			if (paused) {
				int reply = JOptionPane.showConfirmDialog(null,
						String.format("Paused at: T: %tD:%tH Resume", time, time), "Breakpoint", JOptionPane.OK_OPTION,
						JOptionPane.QUESTION_MESSAGE);

				this.paused = false;
			}

			// System.err.println("TS: "+time);

			int progress = (int) (100.0 * tm.getProgress());
			// if (progress % 5 == 0)
			// out.printf("Progress: %d \n", progress);

			List<Cage> c2 = cm.cages();
		
			double totalBiomass = 0.0;

			int ncr = 0; // count number of feed releases

			// For each CAGE in layout...
			for (Cage cage : c2) {

				StockModel stock = cage.getStock();
				IFeedModel fm = cage.getFeed();

				// FEED release for cage this timestep
				// we may or may not release feed this time-step.
				CompoundMass feedRelease = fm.releaseFeed(cage, time);

				if (feedRelease != null) {
					// If we released, record the release for defac calculations
					lastRelease = feedRelease;
					totalFeedReleased += feedRelease.getComponentMass(ChemicalComponentType.SOLIDS);
					ncr++;

					// apply growth model and feed mass to stock.
					gm.applyFeed(stock, feedRelease, time);

					// check the stock weight
					double cageBiomass = stock.getBiomass();
					totalBiomass += cageBiomass;

					// record the last feed time and masses
					cage.getEnv().timeLastFeedCycle = time;
					cage.getEnv().lastFeedMass = feedRelease.getComponentMass(ChemicalComponentType.SOLIDS);

					List<Particle> wasteParticles = fm.generateWasteFeedParticles(cage, pm, feedRelease);
					particles.addAll(wasteParticles);
					totalFeedParticles += wasteParticles.size();
				
					// add all the feed waste masses to get total feedwaste released this cycle
					for (Particle p : wasteParticles) {
						totalFeedWaste += p.getMasses().getComponentMass(ChemicalComponentType.SOLIDS);
					}

					// instruments.updateRate(time, totalFeedWaste, ParticleClass.FEED);

					// WHY is this inside the feedrelase - happens anyway??
					double mortalityMass = mm.deathMass(cage, time);
					if (mortalityMass > 0.0) {
						int deads = (int) Math.floor(mortalityMass / stock.getAverageFishMass());
						stock.setStockNumber(stock.getStockNumber() - deads);
						// System.err.printf("TE:: some fish have died in cage: %s : %d \n",
						// cage.getName(), deads);
						totalMorts += deads;
					}

				}

				// FAECES release for cage this timestep
				CompoundMass faeces = dm.createFaecesMass(lastRelease, time - cage.getEnv().timeLastFeedCycle,
						tm.getStep());

				List<Particle> faecesParticles = dm.generateFaecesParticles(cage, pm, faeces);
				particles.addAll(faecesParticles); // may be none
				
				totalFaecesParticles += faecesParticles.size();
				//totalFaeces += faeces.getComponentMass(ChemicalComponentType.SOLIDS);
				
				for (Particle p : faecesParticles) {
					totalFaeces += p.getMasses().getComponentMass(ChemicalComponentType.SOLIDS);
				}
				// instruments.updateRate(time, totalFaeces, ParticleClass.FAECES);

			} // next cage in layout

			
			// START PARTICLE TRACKING
			List<Particle> landedParticles = new ArrayList<Particle>();
			List<Particle> lostParticles = new ArrayList<Particle>();
			// List<Particle> suspendParticles = new ArrayList<Particle>();

			// handle any particle disruption here
			// List<Particle> disruptedParticles = dim.degrade(particles, time);
			// particles = disruptedParticles;

			int dp = 0;

			for (Particle p : particles) {
				switch (p.getState()) {
				case SUSPEND:
					// If particle is SUSPEND or resuspended: move the particle.
					trans.move(p, time, tm.getStep());
					// it may now be LOST or ONBED, we will deal with it next time-step
					break;
				case LOST:
					// out of domain: track it and its mass.
					lostParticles.add(p);
					lostCount++;
					lostMass += p.getMasses().getComponentMass(ChemicalComponentType.SOLIDS);
					break;
				case ONBED:
					// landed: track it.
					landedParticles.add(p); 
					break;
				}
			}

			bedLayoutModel.deposit(landedParticles); // add to cell bedload
		
			// bed model processing may result in a load of new RESUSPENSION particles, these will be added to the main list
			bedLayoutModel.processBed(time, tm.getStep(), btrans, chm);
		
			// get the list of particles released
			List<Particle> resusParticles = bedLayoutModel.getResuspensionParticles();
			
		
			// Check for any particles which were lost while on the bed by bed-transport
			// these are particles which have either migrated out-of-domain or ? maybe beached??
			List<Particle> lostOnBedParticles = bedLayoutModel.getLostBedLoadParticles();
			
			if (lostOnBedParticles.size() > 0)
				System.err.printf("XTE:: after bed transport we have lost: %d additional particles\n", lostOnBedParticles.size());
			
			// account for any excess lost mass...is it still being counted in the cell where it landed??
			for (Particle p : lostOnBedParticles) {
				lostCount++;
				lostMass += p.getMasses().getComponentMass(ChemicalComponentType.SOLIDS);
			}
			
			// Particles onbed being transported can end up OOD or beached
			// add to lost list so we can later remove from the tracking list
			lostParticles.addAll(lostOnBedParticles);
			
			// these are now in suspension
			particles.addAll(resusParticles);

			// TEMP:: update time series for a specific marked cell.
			if (obscell != null) {
				monitorObservedCell(obscell, time, particles);
			} 

			// pass this info to any instrumentation
			instruments.addParticles(time, landedParticles);
			instruments.addParticles(time, lostParticles);
		
			// remove the finalized particles
			// landed particles are no longer tracked by the engine but by the individual cell models
			particles.removeAll(landedParticles);
			
			// lost particles are no longer tracked anywhere
			particles.removeAll(lostParticles);
		
			// pass info about all particles currently being tracked ie SUS and RESUS
			instruments.updateParticleCount(time, particles);

			// only print this if we released feed
			/*if (ncr != 0) {
				Season season = sem.getSeason(time);
				System.err.printf("%d :: %s Time: %tj %tT Total biomass %f kg \n", tm.getNStep(), season.getShortName(),
						time, time, totalBiomass);
			}*/

			// update deposition every ...50 steps
			if (tm.getNStep() % 500 == 0)
			// instruments.updateBed(bed, time);
				instruments.updateBed(bedLayoutModel, time);

			tm.step();
			i++;

		} // next timestep

		runEndTime = System.currentTimeMillis();
		long durationSec = (runEndTime - runStartTime) / 1000; // s

		// final plot of bed state
		instruments.updateBed(bedLayoutModel, time);

		// scan the bed, find the maximum deposition in any cell
		double bedCellArea = 0.0;
		List<IBedCellModel> cells = bedLayoutModel.getCells();
		double maxMassPerCell = -999.99;
		double maxFluxPerCell = -999.99;
		double retainedSolidsMass = 0.0;
		double bedloadMass = 0.0;
		//double allIncomingMass = 0.0;
		//double allOutgoingMass = 0.0;
		IBedCellModel maxcell = null;
		for (IBedCellModel c : cells) {

			bedCellArea += c.getArea();

			// ChemicalComponent solids =
			// c.getLayers().getSurfaceLayer().getMass().getComponent(ChemicalComponentType.SOLIDS);
			double solidsMass = c.getTotalMass().getComponentMass(ChemicalComponentType.SOLIDS);
			// if (solids != null) {
			if (solidsMass > maxMassPerCell) {
				maxMassPerCell = solidsMass;
				maxcell = c;
			}
			if (solidsMass / c.getArea() > maxFluxPerCell) {
				maxFluxPerCell = solidsMass / c.getArea();
			}
			retainedSolidsMass += solidsMass;
			//allIncomingMass += ((BedCellModel)c).getIncomingMass();
			//allOutgoingMass += ((BedCellModel)c).getOutgoingMass();
			bedloadMass += c.getBedLoad().getComponentMass(ChemicalComponentType.SOLIDS);
			
		}

		// particles still in the water-column in suspension
		double wcMass = 0.0;
		for (Particle p : particles) {
			if (p.getState() == ParticlesState.SUSPEND)
				wcMass += p.getMasses().getComponentMass(ChemicalComponentType.SOLIDS);
		}
		
		double finalBiomass = 0.0;
		int finalCount = 0;
		List<Cage> ci = cm.cages();
		for (Cage cage : ci) {
			StockModel stock = cage.getStock();
			finalBiomass += stock.getBiomass();
			finalCount += stock.getStockNumber();
		}

		double runDays = ((double) tm.getRunDuration() / 86400000.0);
		double maxDepositionConcentration = maxFluxPerCell * 365.25 / runDays;

		double biomassIncrease = finalBiomass - initialBiomass;
		double fcr = totalFeedReleased / biomassIncrease;
		double sgr = Math.log(finalBiomass / initialBiomass) * 100.0 / runDays;

		System.err.printf("Run duration: %f days \n", runDays);
		System.err.printf("Initial biomass: %f kg \n", initialBiomass);
		System.err.printf("Final biomass %f kg \n", finalBiomass);
		System.err.printf("Final stock count: %d, total mortality: %d fish \n", finalCount, totalMorts);
		System.err.printf("Feed conversion: Biomass increase: %f kg, total feed: %f kg, FCR: %f SGR: %f \n",
				biomassIncrease, totalFeedReleased, fcr, sgr);
		System.err.println();
		System.err.printf("Total feed waste %f kg \n", totalFeedWaste);
		System.err.printf("Total feed waste particles: %d \n", totalFeedParticles);

		System.err.printf("Total faeces dumped %f kg \n", totalFaeces);
		System.err.printf("Total faeces particles: %d \n", totalFaecesParticles);
		
		System.err.printf("Total waste mass %f kg \n", totalFaeces+totalFeedWaste);
		System.err.printf("Lost particles: %d \n", lostCount);
		System.err.println();
		System.err.printf("Exported mass: %f kg \n", lostMass);
		System.err.printf("Bedload mass: %f kg \n", bedloadMass);
		System.err.printf("Mass retained in domain: %f kg \n", retainedSolidsMass);
		System.err.printf("Mass in water column: %f kg \n", wcMass);
		System.err.printf("Total mass in system: %f kg \n", lostMass + retainedSolidsMass + bedloadMass + wcMass);
		System.err.println();
		double ucmass = totalFaeces + totalFeedWaste-( lostMass + retainedSolidsMass + bedloadMass + wcMass);
		System.err.printf("Unaccounted mass: %f kg \n", ucmass);
		System.err.println();
		//System.err.printf("All incoming mass: %f kg \n", allIncomingMass);
		//System.err.printf("All outgoing mass: %f kg \n", allOutgoingMass);
		System.err.println();System.err.println();
		System.err.printf("Maximum deposition of solids in a cell: %f kg flux: %f kg/m2/yr in cell: (%f,%f)\n",
				maxMassPerCell, maxDepositionConcentration, maxcell.getX(), maxcell.getY());
		System.err.printf("Total simulation time: %d s\n", durationSec);

		System.err.printf("Actual generated feed: %f \n", FeedModel.totalFeedMass);
		System.err.printf("Actual generated poo:  %f \n", DefacationModel.totalPooMass);
		
		
		// dump duplicates to log file....
		out.printf("Run duration: %f days \n", runDays);
		out.printf("Initial biomass: %f kg \n", initialBiomass);
		out.printf("Final biomass %f kg \n", finalBiomass);
		out.printf("Final stock count: %d, total mortality: %d fish \n", finalCount, totalMorts);
		out.printf("Feed conversion: Biomass increase: %f kg, total feed: %f kg, FCR: %f \n", biomassIncrease,
				totalFeedReleased, fcr);
		out.println();
		out.printf("Total feed waste %f kg \n", totalFeedWaste);
		out.printf("Total feed waste particles: %d \n", totalFeedParticles);
		System.err.printf("Total waste mass %f kg \n", totalFaeces+totalFeedWaste);
		out.printf("Total faeces dumped %f kg \n", totalFaeces);
		out.printf("Total faeces particles: %d \n", totalFaecesParticles);
		out.println();
		out.printf("Lost particles: %d \n", lostCount);
		out.printf("Exported mass: %f kg \n", lostMass);
		out.printf("Mass retained in domain: %f kg \n", retainedSolidsMass);
		out.printf("Bedload mass: %f kg \n", bedloadMass);
		out.printf("Mass in water column: %f kg \n", wcMass);
		out.printf("Total mass in system: %f kg \n", lostMass + retainedSolidsMass + bedloadMass + wcMass);
		out.println();
		out.printf("Unaccounted mass: %f kg \n", ucmass);
		out.printf("Maximum deposition of solids in a cell: %f kg flux: %f kg/m2/yr \n", maxMassPerCell,
				maxDepositionConcentration);
		out.printf("Total simulation time: %d s\n", durationSec);

		instruments.runLog("run.duration", "" + runDays);
		instruments.runLog("initial.biomass", "" + initialBiomass);
		instruments.runLog("final.biomass", "" + finalBiomass);
		instruments.runLog("growth.fcr", "" + fcr);
		instruments.runLog("growth.sgr", "" + sgr);
		instruments.runLog("final.stock.count", "" + finalCount);
		instruments.runLog("total.morts", "" + totalMorts);
		instruments.runLog("feed.waste.mass", "" + totalFeedWaste);
		instruments.runLog("faeces.mass", "" + totalFaeces);
		instruments.runLog("feed.particle.count", "" + totalFeedParticles);
		instruments.runLog("faeces.particle.count", "" + totalFaecesParticles);
		instruments.runLog("lost.particle.count", "" + lostCount);
		instruments.runLog("exported.mass", "" + lostMass);
		instruments.runLog("retained.mass", ""+ retainedSolidsMass);
		instruments.runLog("watercolumn.mass", "" + wcMass);
		instruments.runLog("bedload.mass", "" + bedloadMass);
		instruments.runLog("max.deposition.year", "" + maxDepositionConcentration);
		instruments.runLog("simulation.time", "" + durationSec);
		
		instruments.runLog("bed.consolidationTime", ""+BedCellModel.CONSOLIDATION_TIME_SEC);
		instruments.runLog("bed.expansionTime", ""+BedCellModel.LAYER_EXPANSION_TIME_SEC);
		instruments.runLog("bed.contractionTime", ""+BedCellModel.LAYER_CONTRACTION_TIME_SEC);
		instruments.runLog("bed.thresholdMassArea", ""+BedCellModel.THRESHOLD_MASS_PER_AREA);
		instruments.runLog("bed.minTauCritical", ""+BedCellModel.MIN_TAU_CRIT);
		instruments.runLog("bed.h0z0", ""+BedCellModel.H0_Z0);
		instruments.runLog("bed.nresus", ""+RegularGridBedLayoutModel.nResusParticlesPerCell);
		instruments.runLog("bed.releaseHeight", ""+RegularGridBedLayoutModel.resusMaxHeight);

		

		// this info should be passed to the logging or instruments so they can do whatever...
		// eg. inst.runLogStatus("total.deposition.feed", totalfeedWaste); // rlog-keys
		// as consts??

		// final plot of bed state
		instruments.updateBed(bedLayoutModel, time);
		
		out2.println();
		out2.println("-------------------------------------------------------");
		out2.println("  Cell mass fractions: ID   De   Co   Tx   Er  Lo  Ret");
		out2.println("-------------------------------------------------------");
		out2.println();
		for (IBedCellModel c : cells) {
			BedCellModel bcm = (BedCellModel)c;
			double[] f = bcm.getMassFractions();
			double retained = c.getTotalMass().getComponentMass(ChemicalComponentType.SOLIDS);
			double bedload = c.getBedLoad().getComponentMass(ChemicalComponentType.SOLIDS);
			out2.printf("[%6d]  De: %f Co: %f Tx: %f Er: %f Lo: %f Bed: %f Retained: %f \n", 
					c.getElementRef(), f[0], f[1], f[2], f[3], f[4], bedload, retained);
		}
		

	} // run

	@Override
	public void run(RunConfiguration runConfig) throws Exception {

		tm = runConfig.getTimeModel();
		
		instruments = runConfig.getInstruments();
		System.err.printf("XTE:run:: instruments = %s \n", instruments != null ? instruments.getClass().getName(): "NONE");

		// create the trans here once
		TransportFactory transf = runConfig.getTransportFactory();
		trans = transf.getTransportModel(instruments);

		BedTransportFactory btransf = runConfig.getBedTransportFactory();
		btrans = btransf.getBedTransportModel(instruments);

		gm = runConfig.getGrowthModel();
		dm = runConfig.getDefaecationModel();
		mm = runConfig.getMortalityModel();
		bedLayoutModel = runConfig.getBedLayoutModel();
		chm = runConfig.getChemicalModel();
		sem = runConfig.getSeasonalityModel();
		sea = runConfig.getSeabed();
		cm = runConfig.getCageLayoutModel();

		// controller opens a few files in the runfolder..
		File output = new File(runConfig.getRunFolder(), "run.log");
		FileWriter writer = new FileWriter(output);
		PrintWriter out = new PrintWriter(new BufferedWriter(writer));
		out.printf("Engine:: %s:: Starting model: %s run: %d \n", "TESTENGINE", runConfig.getName(),
				runConfig.getRun());
		
		// TEMP to log fractions of mass movement in cells
		File output2 = new File(runConfig.getRunFolder(), "bedmass.dat");
		FileWriter writer2 = new FileWriter(output2);
		PrintWriter out2 = new PrintWriter(new BufferedWriter(writer2));
		
		// start the instrumentation 
		instruments.startLogging(); 
		
		// add extra metadata from proj and model?? these should be in the model and passed to runconfig??
		instruments.metadata(runConfig.getProjectName(), runConfig.getModelName(), cm);
	
		
		// TODO define some methods in inst so we can pass these models...
		instruments.useBathymetry(runConfig.getBathymetry());
		//instruments.useFlowmetry(runConfig.getFlowmetry());
		
		instruments.runLog("engine", "TESTENGINE");

		// rename this as the generic method of IEngine??
		try {
			Thread.sleep(5000L);
		} catch (Exception e) {
		}
		runOut(out, out2);

		instruments.outputRunLog();

		runConfig.writePostrunMetadata(runStartTime, runEndTime, "Completed");

		out.close();
		out2.close();
	}

	@Override
	public void pauseEngine() {
		this.paused = true;

	}

	@Override
	public void resumeEngine() {
		this.paused = false;

	}

	@Override
	public void abortEngine() {
		this.aborted = true;

	}
	
	private void monitorObservedCell(BedCellModel obscell, long time, List<Particle> allparticles) {
		
		double cellmass = obscell.getTotalMass().getComponentMass(ChemicalComponentType.SOLIDS);
		double thresholdMass = obscell.getThresholdMass();

		double[] data = obscell.getMyData(); // bfv, tau, erosionmass

		if (tm.getNStep() % MON_INT == 0) {
			// System.err.printf("XTE::Cell@ (%f, %f) Mass: %f, bfv: %f m/s taub: %f Pa
			// eflux: %f kg \n",
			// cell.getX(), cell.getY(), cellmass, data[0], data[1], data[2]);
			instruments.updateFlow(time, -0.1, data[0]);
			instruments.updateFlow(time, -0.9, data[1]);
			instruments.updateMass(time, cellmass, thresholdMass);
			instruments.updateTauB(time, data[3]); // 1 taub
			// instrumenta.updateBfv(time, data[0]); // 0
			instruments.updateErosionFlux(time, data[4]); // 2
			double bedload = obscell.getBedLoad().getComponentMass(ChemicalComponentType.SOLIDS);
			// System.err.printf("XTE:: bedload: %f \n", bedload);
			instruments.updateBedLoad(time, bedload);
			
			//instruments.updateResusMass(time, resusMass);
			
			// TEMP::: special for telemetry instrument...
			instruments.updateBedCell(time, obscell);
			
		}

		if (obscell.getLayers().getNumberLayers() > 0) {
			double tc = obscell.getLayers().getLayer(0).getCurrentTauCrit();
			double etc = obscell.getLayers().getLayer(0).getEquilibriumTauCrit();					
			instruments.updateTauC(1, time, tc, etc);
		}
		instruments.updateBedLayerCount(time, obscell.getLayers().getNumberLayers());

		// work out how much mass is suspended above this specific cell...
		double resusMass = 0.0;
		for (Particle p : allparticles) {
			switch (p.getState()) {
			case SUSPEND:
				if (p.getCurrentCell() != null && p.getCurrentCell().ref == obscell.getElementRef()) {
					resusMass += p.getMasses().getComponentMass(ChemicalComponentType.SOLIDS);
				}
				break;
			}
		}
		instruments.updateResusMass(time, resusMass);
		
		// instruments.getBedModelListener().updateLayers(time, im, (BedLayerModel)
		// cell.getLayers());
		
		
	}

}
