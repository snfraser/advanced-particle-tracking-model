/**
 * 
 */
package com.apt.engine;

/**
 * @author SA05SF
 *
 */
public interface IEngineController {

	public void pauseEngine();
	
	public void resumeEngine();
	
	public void abortEngine();
	
}
