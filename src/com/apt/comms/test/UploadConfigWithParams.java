/**
 * 
 */
package com.apt.comms.test;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * @author SA05SF
 *
 */
public class UploadConfigWithParams {

	static final String CONTENT_URLFORM = "application/x-www-form-urlencoded";
	static final String CONTENT_JSON = "application/json";
	static final String NDM_USER_AGENT = "NewDepomod-MigClient-0.1";

	static final String BASE_URL = "https://www.depomod-analytics.net/aptm/";
	static final String CNAME = "Regime:CAN";
	static final String PNAME = "Project:WindyBaySE";
	static final String MNAME = "Model:HiStock4";
	static final String CGNAME = "Group:L3N";
	static final String RNAME = "Run:5";

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		try {
			JSONObject input = new JSONObject();
			input.put("name", CNAME);
			input.put("base", 1);

			JSONArray params = new JSONArray();
			params.put(createParam("Model.RunMode", "MULTITHREAD", "string"));
			params.put(createParam("Model.RunMode.MaxThreads", "16", "int"));
			params.put(createParam("Model.Start", "0", "int"));
			params.put(createParam("Model.End", "2160000", "int"));
			params.put(createParam("Model.Tags", "Local:Estuary", "string"));
			params.put(createParam("Model.Feed.WasteFraction", "0.12", "double"));
			params.put(createParam("Model.Feed.CarbonFraction", "0.3", "double"));
			params.put(createParam("Model.Feed.FaecesCarbonFraction", "0.28", "double"));
			params.put(createParam("Model.Feed.FaecesNitrogenFraction", "0.087", "double"));
			params.put(createParam("Physical.Sea.Bed.Temperature", "10.2", "double"));
			params.put(createParam("Physical.Sea.Salinity", "32.2", "double"));
			params.put(createParam("Physical.BedModel.consolidationTime", "345600", "double"));
			params.put(createParam("Physical.BedModel.relaxationTime50", "14400", "double"));
			params.put(createParam("Physical.BedModel.ContractionTime50", "1800", "double"));
			params.put(createParam("Physical.BedModel.ParticlesPerArea", "4", "int"));
			params.put(createParam("Physical.BedModel.ErosionCoeff", "0.031", "double"));
			params.put(createParam("Physical.BedModel.TauC.Base", "0.017", "double"));

			input.put("params", params);

			System.out.println("AptmTest: sending json payload:: " + input.toString(3));

			String url = BASE_URL + "createConfig.php";

			/*
			 * HttpResponse<String> res = Unirest.post(url) .header("Content-Type",
			 * "application/json").header("Accept", "application/json")
			 * .header("User-Agent", "AptmClient-v1.0").header("Xdepomod-User", "Tester01")
			 * .header("Xdepomod-Auth", "notokendefined").body(input) .asString();
			 * //.asJson();
			 */

			HttpClient httpClient = HttpClient.newBuilder().version(HttpClient.Version.HTTP_2)
					.connectTimeout(Duration.ofSeconds(60)).build();

			// HttpResponse<JsonNode> res = null;
			HttpResponse<String> res = null;

			HttpRequest request = HttpRequest.newBuilder().POST(HttpRequest.BodyPublishers.noBody())
					.uri(URI.create(url)).setHeader("User-Agent", NDM_USER_AGENT) // add request header
					.header("Content-Type", CONTENT_JSON).header("Accept", CONTENT_JSON)

					.build();

			res = httpClient.send(request, HttpResponse.BodyHandlers.ofString());

			int status = res.statusCode();

			System.err.printf("Status: %d \n", status);

			String body = res.body();
			JSONObject ob = new JSONObject(body);

			System.out.println("Aptmtest: result...");
			System.out.println(ob.toString(4));

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static JSONObject createParam(String key, String value, String type) throws Exception {

		JSONObject ob = new JSONObject();
		ob.put("key", key);
		ob.put("value", value);
		ob.put("type", type);

		return ob;

	}

}
