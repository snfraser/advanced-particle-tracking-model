/**
 * 
 */
package com.apt.comms.test;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;
import java.util.List;

import org.json.JSONObject;

import com.apt.comms.AptmClient;
import com.apt.project.Config;

/**
 * @author SA05SF
 *
 */
public class FetchRegimeConfigs {


	final String CONTENT_URLFORM = "application/x-www-form-urlencoded";
	final String CONTENT_JSON = "application/json";
	final String NDM_USER_AGENT = "NewDepomod-MigClient-0.1";
	static final String BASE_URL = "https://www.depomod-analytics.net/aptm/";

	/**
	 * 
	 */
	public FetchRegimeConfigs() {

	}

	private void fetchRegimesUsingClient() throws Exception {

		AptmClient client = new AptmClient();
		List<Config> configs = client.getRegimeConfigs();

		for (Config c : configs) {
			System.err.printf("retrieved regime: %s \n", c.getName());
		}

	}

	private void fetchRegimesManually() throws Exception {

		String url = BASE_URL + "getRegimeConfigs.php";

		/*HttpResponse<String> res = Unirest.get(url).header("Content-Type", "application/json")
				.header("Accept", "application/json").header("User-Agent", "AptmClient-v1.0")
				.header("Xdepomod-User", "TestUser01").header("Xdepomod-Auth", "notokeninuse").asString();*/

		  HttpClient httpClient = HttpClient.newBuilder()
	                .version(HttpClient.Version.HTTP_2)
	                .connectTimeout(Duration.ofSeconds(60))
	                .build();
		
		  HttpResponse<String> res = null;
		  
		  HttpRequest request = HttpRequest.newBuilder()
                  .GET()
                  .uri(URI.create(url))
                  .setHeader("User-Agent", NDM_USER_AGENT) // add request header
                  .header("Content-Type", CONTENT_JSON)
                  .header("Accept", CONTENT_JSON)
                  .build();
		  
		 res = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
		  
		int status = res.statusCode();
	

		System.out.printf("Aptmtest: Response status: %d\n", status);

		String body = res.body();
		System.err.printf("Response: %s \n", body);

	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		try {
			FetchRegimeConfigs f = new FetchRegimeConfigs();
			f.fetchRegimesUsingClient();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
