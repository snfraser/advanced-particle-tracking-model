/**
 * 
 */
package com.apt.comms.test;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;
import java.util.Enumeration;
import java.util.Properties;
import java.util.Set;
import java.util.Stack;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * @author SA05SF
 *
 */
public class FetchConfigWithParams {


	final String CONTENT_URLFORM = "application/x-www-form-urlencoded";
	final String CONTENT_JSON = "application/json";
	final String NDM_USER_AGENT = "NewDepomod-MigClient-0.1";
	
	static final String BASE_URL = "https://www.depomod-analytics.net/aptm/";
	static final int CID = 17;

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		try {
			// 1. fetch a single property entity
			FetchConfigWithParams test = new FetchConfigWithParams();
			// test.fetchConfig(CID);

			test.fetchChain(CID);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void fetchChain(int cid) throws Exception {

		//Stack<Properties> stack = new Stack<Properties>();
		Stack<Config> stack = new Stack<Config>();
		
		int ii = 0;
		Config config = fetchConfig(cid);
		//stack.push(config.props);
		stack.push(config);
		try {
			Thread.sleep(2000L);
		} catch (Exception ee) {
		}

		// Trace back through the config chain, pop entries on stack
		do {
			ii++;
			// System.err.printf("About to fetch config with CID: %d ....\n", config.cid);
			config = fetchConfig(config.xbase);
			//stack.push(config.props);
			stack.push(config);
			System.err.printf("Got config seq: %d \n", ii);
			try {
				Thread.sleep(2000L);
			} catch (Exception ee) {
			}
		} while (config.cid != 1);
		
		
		System.err.printf("All done: stack contains: %d property sets, unwinding...\n", stack.size());
		
		// Unwind stack, overwriting props
		Properties props = new Properties();
		StringBuffer label = new StringBuffer();
		while (!stack.isEmpty()) {
			
			//Properties next = stack.pop();
			Config nextconfig = stack.pop();
			//props.putAll(next); // override all entries
			props.putAll(nextconfig.props);
			label.append("/").append(nextconfig.name);
		}
		
		System.err.printf("Stack label: %s \n", label.toString());
		
		Enumeration e = props.keys();
		while (e.hasMoreElements()) {
			String key = (String)e.nextElement();
			String value = props.getProperty(key);
			System.err.printf("%s = %s \n", key, value);
		}

	}

	private Config fetchConfig(int cid) throws Exception {

		System.err.printf("fetch():: input::cid = %d \n", cid);

		String url = BASE_URL + "getConfig.php?cid="+cid;

		/*HttpResponse<JsonNode> res = Unirest.get(url)
				.header("Content-Type", "application/json")
				.header("Accept", "application/json")
				.header("User-Agent", "AptmClient-v1.0")
				.header("Xdepomod-User", "TestUser01")
				.header("Xdepomod-Auth", "notokeninuse")
				.queryString("cid", cid)
				.asJson();*/

		 HttpClient httpClient = HttpClient.newBuilder()
	                .version(HttpClient.Version.HTTP_2)
	                .connectTimeout(Duration.ofSeconds(60))
	                .build();

	        HttpResponse<String> res = null;

	        HttpRequest request = HttpRequest.newBuilder()
                    .GET()
                    .uri(URI.create(url))
                    .setHeader("User-Agent", NDM_USER_AGENT) 
                    .header("Content-Type", CONTENT_JSON)
                    .header("Accept", CONTENT_JSON)
                    .header("User-Agent", NDM_USER_AGENT)
                    .build();
		
	        res = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
	        		
		int status = res.statusCode();
	
		System.out.printf("Aptmtest: Response status: %d \n", status);

		String body = res.body();
	
		JSONObject ob = new JSONObject(body);
		
		System.out.printf("Response...");
		System.out.println(ob.toString(4));

		JSONObject all = ob.getJSONObject("config");
		JSONArray params = all.getJSONArray("params");
		Properties props = new Properties();
		for (int index = 0; index < params.length(); index++) {
			JSONObject pp = (JSONObject) params.get(index);
			String key = pp.getString("pkey");
			String value = pp.getString("pvalue");
			String type = pp.getString("type");
			props.setProperty(key, value);
			System.err.printf("Parameter: %s -> %s (%s)\n", key, value, type);
		}

		JSONObject cfg = all.getJSONObject("config");
		String name = cfg.getString("name");
		int base = cfg.getInt("xbase");
		int icid = cfg.getInt("cid");
		System.err.printf("fetch::() Config found:: name: %s, cid: %d, base is: %d \n", name, icid, base);

		Config config = new Config(name, props, cid, base);

		return config;

	}

	class Config {
		String name;
		Properties props;
		int cid;
		int xbase;

		/**
		 * @param name
		 * @param props
		 * @param cid
		 * @param xbase
		 */
		public Config(String name, Properties props, int cid, int xbase) {
			this.name = name;
			this.props = props;
			this.cid = cid;
			this.xbase = xbase;
		}

	}

}
