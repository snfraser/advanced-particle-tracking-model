/**
 * 
 */
package com.apt.comms;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.json.JSONArray;
import org.json.JSONObject;

import com.apt.project.Config;

/**
 * @author SA05SF
 *
 */
public class AptmClient {

	final String CONTENT_URLFORM = "application/x-www-form-urlencoded";
	final String CONTENT_JSON = "application/json";
	final String NDM_USER_AGENT = "NewDepomod-MigClient-0.1";
	static final String BASE_URL = "https://www.depomod-analytics.net/aptm/";

	static final String USER_AGENT = "AptmClient-v1.0";

	public List<Config> getRegimeConfigs() throws Exception {

		String url = BASE_URL + "getRegimeConfigs.php";

		/*
		 * HttpResponse<JsonNode> res = Unirest.get(url).header("Content-Type",
		 * "application/json") .header("Accept",
		 * "application/json").header("User-Agent", USER_AGENT) .header("Xdepomod-User",
		 * "TestUser01").header("Xdepomod-Auth", "notokeninuse").asJson();
		 */

		HttpClient httpClient = HttpClient.newBuilder().version(HttpClient.Version.HTTP_2)
				.connectTimeout(Duration.ofSeconds(60)).build();

		// HttpResponse<JsonNode> res = null;
		HttpResponse<String> res = null;

		HttpRequest request = HttpRequest.newBuilder().GET().uri(URI.create(url))
				.setHeader("User-Agent", NDM_USER_AGENT) // add request header
				.header("Content-Type", CONTENT_JSON).header("Accept", CONTENT_JSON).build();

		res = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
		
		int status = res.statusCode();

		System.out.printf("Aptmtest: Response status: %d \n", status);

		String body = res.body();
		JSONObject ob = new JSONObject(body);

		System.out.printf("Response...");
		System.out.println(ob.toString(4));

		List<Config> configs = new ArrayList<Config>();

		JSONArray confarray = ob.getJSONArray("configs");
		for (int index = 0; index < confarray.length(); index++) {
			JSONObject container = (JSONObject) confarray.get(index);
			JSONObject cfg = container.getJSONObject("config");

			String name = cfg.getString("name");
			int base = cfg.getInt("xbase");
			int icid = cfg.getInt("cid");

			JSONArray params = container.getJSONArray("params");
			Properties props = new Properties();
			for (int jindex = 0; jindex < params.length(); jindex++) {
				JSONObject pp = (JSONObject) params.get(jindex);
				String key = pp.getString("pkey");
				String value = pp.getString("pvalue");
				String type = pp.getString("type");
				props.setProperty(key, value);
				// System.err.printf("Parameter: %s -> %s (%s)\n", key, value, type);
			}
			Config config = new Config(name, props, icid, base);

			// System.err.printf("fetch::() Config found:: name: %s, cid: %d, base is: %d
			// \n", name, icid, base);
			configs.add(config);

		}

		return configs;

	}

	public Config getConfig(int cid) throws Exception {

		String url = BASE_URL + "getConfig.php?cid=" + cid;

		/*
		 * HttpResponse<JsonNode> res = Unirest.get(url).header("Content-Type",
		 * "application/json") .header("Accept",
		 * "application/json").header("User-Agent", USER_AGENT) .header("Xdepomod-User",
		 * "TestUser01").header("Xdepomod-Auth", "notokeninuse").queryString("cid", cid)
		 * .asJson();
		 */

		HttpClient httpClient = HttpClient.newBuilder().version(HttpClient.Version.HTTP_2)
				.connectTimeout(Duration.ofSeconds(60)).build();

		// HttpResponse<JsonNode> res = null;
		HttpResponse<String> res = null;

		HttpRequest request = HttpRequest.newBuilder().GET().uri(URI.create(url))
				.setHeader("User-Agent", NDM_USER_AGENT) // add request header
				.header("Content-Type", CONTENT_JSON).header("Accept", CONTENT_JSON).build();

		res = httpClient.send(request, HttpResponse.BodyHandlers.ofString());

		int status = res.statusCode();

		String body = res.body();
		JSONObject ob = new JSONObject(body);
		// System.out.printf("Response...");
		// System.out.println(ob.toString(4));

		JSONObject result = ob.getJSONObject("config");
		JSONArray params = result.getJSONArray("params");
		Properties props = new Properties();
		for (int index = 0; index < params.length(); index++) {
			JSONObject pp = (JSONObject) params.get(index);
			String key = pp.getString("pkey");
			String value = pp.getString("pvalue");
			String type = pp.getString("type");
			props.setProperty(key, value);
			// System.err.printf("Parameter: %s -> %s (%s)\n", key, value, type);
		}

		JSONObject cfg = result.getJSONObject("config");
		String name = cfg.getString("name");
		int base = cfg.getInt("xbase");
		int icid = cfg.getInt("cid");
		// System.err.printf("fetch::() Config found:: name: %s, cid: %d, base is: %d
		// \n", name, icid, base);

		Config config = new Config(name, props, cid, base);

		return config;
	}

}
