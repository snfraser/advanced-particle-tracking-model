package com.apt.instrumentation;

import java.util.HashMap;
import java.util.Map;

import com.apt.data.Cell;
import com.apt.data.Particle;

/**
 * Class to keep track of which particle is in which cell.
 * 
 * @author SA05SF
 *
 */
public class ParticleCellTracker {

	private Map<String, Cell> cellMap;
	
	
	/**
	 * 
	 */
	public ParticleCellTracker() {
		System.err.println("Creating particle tracker mappings...");
		cellMap = new HashMap<String, Cell>();
	}
	
	/**
	 *  Add or update a particle cell mapping entry.*
	 *
	 * @param p
	 * @param ccc
	 */
	public void addCellMappings(Particle p, Cell bathymetryCell) {
		cellMap.put(p.getId(), bathymetryCell);
	}
	
	/**
	 * Locate the bathymetry cell containing the particle on its latest update.
	 * @param p
	 * @return
	 */
	public Cell getCell(Particle p) {
		if (p == null)
			return null;
		return cellMap.get(p.getId());
	}
	
	public void setCellMapping(Particle p, Cell cell) {
		cellMap.put(p.getId(), cell);
	}
	
	
	public void removeMapping(Particle p) {
		cellMap.remove(p.getId());
	}
}
