/**
 * 
 */
package com.apt.instrumentation.implementation.telemetry;

import org.json.JSONArray;
import org.json.JSONObject;

import com.apt.data.ChemicalComponentType;
import com.apt.models.IBedCellModel;
import com.apt.models.IBedLayer;
import com.apt.models.IBedLayerModel;
import com.apt.models.bed.BedCellModel;

/**
 * 
 */
public class BedCellData extends TelemetryData {

	private static final String TYPE = "BEDCELL";

	private BedCellModel bedcell;

	public BedCellData(String muid, double time, BedCellModel bedcell) {
		super(muid, time);
		this.bedcell = bedcell;
	}

	@Override
	public String getType() {
		return TYPE;
	}

	@Override
	public JSONObject getPayload() throws Exception {

		JSONObject jcell = new JSONObject();
		jcell.put("ref", bedcell.getElementRef());
		jcell.put("x", bedcell.getX());
		jcell.put("y", bedcell.getY());
		
		double bedLoadSolids = bedcell.getBedLoad().getComponentMass(ChemicalComponentType.SOLIDS);
		jcell.put("bedload", bedLoadSolids);
		
		double threshmass = bedcell.getThresholdMass();
		jcell.put("threshold", threshmass);
		
		// some may not be set?? depending when this is called
		double[] mydata = bedcell.getMyData(); // sflow, bflow, bfv, taub, eflux
		jcell.put("sFlow", mydata[0]);
		jcell.put("bFlow", mydata[1]);
		jcell.put("bfv", mydata[2]);
		jcell.put("taub", mydata[3]);
		jcell.put("eflux", mydata[4]);

		JSONArray jlayers = new JSONArray();

		IBedLayerModel layers = bedcell.getLayers();

		int index = 0;
		
		// Surface layer
		IBedLayer surface = layers.getSurfaceLayer();
		JSONObject jslayer = new JSONObject();
		jslayer.put("level", 0);
		double tauc = surface.getCurrentTauCrit();
		jslayer.put("tauc", tauc);
		double tauce = surface.getEquilibriumTauCrit();
		jslayer.put("eqtauc", tauce);
		double solids = surface.getMass().getComponentMass(ChemicalComponentType.SOLIDS);
		jslayer.put("mass", solids);
		
		jlayers.put(index, jslayer); // INDEX=0

		// Other layers - start at 1
		
		for (IBedLayer layer : layers.getLayers()) {
			index++;
			JSONObject jlayer = new JSONObject();
			jlayer.put("level", index);

			tauc = layer.getCurrentTauCrit();
			jlayer.put("tauc", tauc);
			tauce = layer.getEquilibriumTauCrit();
			jlayer.put("eqtauc", tauce);
			solids = layer.getMass().getComponentMass(ChemicalComponentType.SOLIDS);
			jlayer.put("mass", solids);

			jlayers.put(index, jlayer); // INDEX = 1..N
		}
		index++;

		// Basement layer
		IBedLayer basement = layers.getBasement();
		JSONObject jblayer = new JSONObject();
		jblayer.put("level", layers.getNumberLayers() + 1);
		tauc = basement.getCurrentTauCrit();
		jblayer.put("tauc", tauc);
		tauce = basement.getEquilibriumTauCrit();
		jblayer.put("eqtauc", tauce);
		solids = basement.getMass().getComponentMass(ChemicalComponentType.SOLIDS);
		jblayer.put("mass", solids);
		
		jlayers.put(index, jblayer); // INDEX = N+1
		
		jcell.put("layers", jlayers);
		
		return jcell;
		
	}

}
