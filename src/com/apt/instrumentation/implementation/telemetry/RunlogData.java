/**
 * 
 */
package com.apt.instrumentation.implementation.telemetry;

import java.util.Enumeration;

import org.json.JSONArray;
import org.json.JSONObject;

import com.apt.cli.util.ConfigurationProperties;

/**
 * 
 */
public class RunlogData extends TelemetryData {
	
	private static final String TYPE = "RUNLOG";
	
	private ConfigurationProperties runlog;
	
	public RunlogData(String muid, double time, ConfigurationProperties runlog) {
		super(muid, time);
		this.runlog = runlog;
	}

	@Override
	public String getType() {
	return TYPE;
	}

	@Override
	public JSONObject getPayload() throws Exception {
		
		JSONArray jlist = new JSONArray();
		
		int index = 0;
		Enumeration keys = runlog.keys();
		while (keys.hasMoreElements()) {
			String key = (String)keys.nextElement();
			String value = runlog.getProperty(key);
			JSONObject job = new JSONObject();					
			job.put("key",key);
			job.put("value", value);
			jlist.put(index, job);
			index++;
		}
		
		JSONObject jdata = new JSONObject();
		jdata.put("runlog", jlist);
		return jdata;
		
	}

	

}
