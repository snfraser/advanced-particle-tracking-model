/**
 * 
 */
package com.apt.instrumentation.implementation.telemetry;

import org.json.JSONObject;

/**
 * 
 */
public abstract class TelemetryData {
	
	/** Unique ID attached to the associated model run.*/
	private String muid;
	
	/** Time since start of model run [ms].*/
	private double time;

	/**
	 * Create a TelemetryData with supplied params.
	 * 
	 * @param muid Model UUID - identifies the model run. 
	 * @param time Time since start of model run [ms].
	 */
	public TelemetryData(String muid, double time) {
		super();
		this.muid = muid;
		this.time = time;
	}

	/**
	 * @return the muid
	 */
	public String getMuid() {
		return muid;
	}

	/**
	 * @return the time
	 */
	public double getTime() {
		return time;
	}
	
	/**
	 * @return the type of telemetry data.
	 */
	public abstract String getType();

	/**
	 * @return the payload as a JSON object.
	 */
	public abstract JSONObject getPayload() throws Exception;

}
