/**
 * 
 */
package com.apt.instrumentation.implementation.telemetry;

import java.util.List;
import java.util.Queue;

import com.apt.cli.util.ConfigurationProperties;
import com.apt.data.Particle;
import com.apt.data.ParticleClass;
import com.apt.instrumentation.IBedModelListener;
import com.apt.instrumentation.Instrumentation;
import com.apt.models.IBathymetryModel;
import com.apt.models.IBedCellModel;
import com.apt.models.IBedLayoutModel;
import com.apt.models.ICageLayoutModel;
import com.apt.models.bed.BedCellModel;

/**
 * 
 */
public class TelemetryInstrumentation implements Instrumentation {

	private String muid;
	
	private TelemetryDespatcher despatcher;
	
	private Queue<TelemetryData> queue;
	
	private ConfigurationProperties runlog;
	
	
	/**
	 * @param queue
	 * @param despatcher
	 */
	public TelemetryInstrumentation(String muid, Queue<TelemetryData> queue, TelemetryDespatcher despatcher) {
		super();
		this.muid = muid;
		this.queue = queue;
		this.despatcher = despatcher;
		runlog = new ConfigurationProperties();
	}



	/**
	 * Start the despatcher. endLogging() should stop it after the queue is emptied.
	 */
	@Override
	public void startLogging() {
		
		System.err.printf("TelemetryInstruments::start logging...\n");
		
		final TelemetryDespatcher fdespatcher = despatcher;
		
		Thread t = new Thread(new Runnable() {
			
			@Override
			public void run() {
				fdespatcher.start();
			}
		});
		
		t.start();
		
	}
	


	@Override
	public void metadata(String projectName, String modelName, ICageLayoutModel layout) {
		
		// assume time = 0 as expect this as first message
		ModelMetadataData data = new ModelMetadataData(muid, 0.0, projectName, modelName, layout);
		boolean added = queue.offer(data); 
	}

	
	@Override
	public IBedModelListener getBedModelListener() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void observedCell(double x, double y) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addParticles(long time, List<Particle> particles) {
		if (particles.size() == 0)
			return;
		ParticleData data  = new ParticleData(muid, (double)time, particles);
		boolean added = queue.offer(data); 
		// we can use this return to indicate the item was actually added...
	}

	@Override
	public void updateRate(long time, double rate, ParticleClass type) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateBed(IBedLayoutModel bed, long time) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateFlow(long time, double sigma, double flow) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateMass(long time, double mass, double thresholdMass) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateParticleCount(long time, List<Particle> particles) {
		if (particles.size() == 0)
			return;
		ParticleData data  = new ParticleData(muid, (double)time, particles);
		boolean added = queue.offer(data); 
		
	}

	@Override
	public void runLog(String key, String value) {
		// TODO Auto-generated method stub
		runlog.setProperty(key, value);
	}

	@Override
	public void updateTauB(long time, double taub) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateBedLoad(long time, double bedload) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateErosionFlux(long time, double flux) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void outputRunLog() throws Exception {
		// make up time it is the end of the run
		RunlogData data = new RunlogData(muid, 0.0, runlog);
		boolean accepted = queue.offer(data);
	}

	@Override
	public void updateTauC(int level, long time, double tauc, double eqtauc) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateBedLayerCount(long time, int nlevels) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateResusMass(long time, double resusMass) {
		// TODO Auto-generated method stub
		
	}



	@Override
	public void useBathymetry(IBathymetryModel bm) {
		BathymetryData data = new BathymetryData(muid, 0, bm);
		boolean accepted = queue.offer(data);
	}

	@Override
	public void updateBedCell(long time, IBedCellModel cell) {
		BedCellData data = new BedCellData(muid, (double)time, (BedCellModel)cell);
		boolean accepted = queue.offer(data);
		
	}

}
