/**
 * 
 */
package com.apt.instrumentation.implementation.telemetry;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import com.apt.data.CageType;
import com.apt.models.Cage;
import com.apt.models.ICageLayoutModel;
import com.apt.models.cage.ICagePositionManager;

/**
 * 
 */
public class ModelMetadataData extends TelemetryData {

	private static final String TYPE = "MODELMETADATA";
	
	private String projectName;
	
	private String modelName;
	
	private ICageLayoutModel layout;
	


	/**
	 * @param muid
	 * @param time
	 * @param projectName
	 * @param modelName
	 * @param layout
	 */
	public ModelMetadataData(String muid, double time, String projectName, String modelName, ICageLayoutModel layout) {
		super(muid, time);
		this.projectName = projectName;
		this.modelName = modelName;
		this.layout = layout;
	}

	@Override
	public String getType() {
		return TYPE;
	}

	@Override
	public JSONObject getPayload() throws Exception {
		
		JSONObject job = new JSONObject();
		job.put("project", projectName);
		job.put("model", modelName);
		
		JSONObject jcages = new JSONObject();
		jcages.put("name", layout.getLayoutName());
		
		JSONArray jgrid = new JSONArray();
		ICagePositionManager pm = layout.getPositionManager();
	
		int index = 0;
		List<Cage> cages = pm.getCages();
		System.err.printf("MMD: getpayload: cageslist contains: %d cages \n", cages.size());
		
		for (Cage c : cages) {
			String id = c.getName();
			
			double x = pm.getPosition(id).getX();
			double y = pm.getPosition(id).getY();
			double l = c.getLength();
			double w = c.getWidth();
			CageType type = c.getType();
			double dd = c.getDepth();
			double hh = c.getHeight();
			double aa = c.getAngle();
			
			System.err.printf("MMD add cage: %s at (%f, %f)\n", c.getName(), x, y);
			
			JSONObject jcage = new JSONObject();
			jcage.put("x", x);
			jcage.put("y", y);
			jcage.put("length", l);
			jcage.put("width", w);
			jcage.put("type", type.name());
			jcage.put("depth", dd);
			jcage.put("height", hh);
			jcage.put("angle", aa);
			
			jgrid.put(index, jcage);
			index++;
		}
		
		jcages.put("grid", jgrid);
		job.put("cages", jcages);
		
		return job;
		
	}

}
