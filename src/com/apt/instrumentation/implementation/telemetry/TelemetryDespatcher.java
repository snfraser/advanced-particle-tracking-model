/**
 * 
 */
package com.apt.instrumentation.implementation.telemetry;

import java.io.PrintWriter;
import java.net.Socket;
import java.util.Queue;

import org.json.JSONObject;

/**
 * 
 */
public class TelemetryDespatcher {

	/** Time to wait of the queue is empty before re-polling. */
	private static long POLLING_DELAY = 1000L;

	/** Time to wait before attempting to re-connect to server. */
	private static long BACKOFF_DELAY = 1000L;

	/** Maximum time to backoff if fail to connect. */
	private static long MAX_BACKOFF_DELAY = 600 * 1000L;

	private String host;

	private int port;

	private volatile boolean forceStop = false;

	private Queue<TelemetryData> queue;

	private volatile boolean running = false;

	private Socket client;

	private PrintWriter pout;

	private long backoffDelay;

	/**
	 * @param queue
	 */
	public TelemetryDespatcher(Queue<TelemetryData> queue, String host, int port) {
		super();
		this.queue = queue;
		this.host = host;
		this.port = port;
		backoffDelay = BACKOFF_DELAY;
	}

	public void start() {

		if (running) {
			System.err.printf("TD: ATTEMPT TO START WHEN ALREADY RUNNING !!");
			return;
		}
		
		// ?? do we need some sort of timeout incase model dies and we are waiting forever ??

		boolean connected = false;
		running = true;
		while (!forceStop) {

			while (!connected) {
				try {
					client = new Socket(host, port);
					pout = new PrintWriter(client.getOutputStream());
					connected = true;
				} catch (Exception e) {
					e.printStackTrace();
					System.err.printf("TD: FAILED TO CONNECT TO BROKER on PORT %d !!\n", port);
					// BACKOFF then retry
					backoffDelay = backoffDelay > MAX_BACKOFF_DELAY ? MAX_BACKOFF_DELAY : backoffDelay * 2;
					try {
						Thread.sleep(backoffDelay);
					} catch (InterruptedException ix) {
					}
					continue;
				}
			}

			// pull something off the queue, what if connect breaks??
			TelemetryData data = null;
			while (data == null) {
				data = queue.poll();
				if (data != null)
					break;

				try {
					// System.err.printf("TD:: Q is empty, waiting...");
					Thread.sleep(POLLING_DELAY);
				} catch (InterruptedException ix) {
				}
			}

			// data was not null
			try {
				// build the packet
				JSONObject packet = new JSONObject();
				packet.put("uid", data.getMuid());
				packet.put("time", data.getTime());
				packet.put("type", data.getType());
				packet.put("payload", data.getPayload());

				// send it onwards...
				send(packet);

			} catch (Exception e) {
				// e.printStackTrace();
				// here we should notify to close and reconnect the client
				connected  = false; // ?? is this always true ?
			}

		}

	}

	/**
	 * Send the data packet onwards to the broker. Init impl: uses a PrintWriter and
	 * terminates with CR, this is OK for a java broker. May have to switch to
	 * DataWriter and add a packet length word at start for non-java brokers..
	 * 
	 * @param packet The data packet as a JSON object.
	 * @throws Exception
	 */
	private void send(JSONObject packet) throws Exception {

		String packetContent = packet.toString();
		// System.err.printf("TD:: packet to send is: %d bytes \n",
		// packetContent.length());

		// later we should keep the socket open to avoid opening vast nos of connections
		// !!
		// Socket client = new Socket("localhost", 8787);

		// keep open hopefully....
		pout.println(packet.toString());
		pout.flush();
		// pout.close();
	}

	/**
	 * Stop the despatcher.
	 * 
	 */
	public void stop() {
		forceStop = true;
	}

}
