/**
 * 
 */
package com.apt.instrumentation.implementation.telemetry;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import com.apt.data.Cell;
import com.apt.models.IBathymetryModel;

/**
 * 
 */
public class BathymetryData extends TelemetryData {

	private static final String TYPE = "BATHYMETRY";
	
	private IBathymetryModel bathymetry ;
	
	public BathymetryData(String muid, double time, IBathymetryModel bathymetry) {
		super(muid, time);
		this.bathymetry = bathymetry;
	}

	@Override
	public String getType() {
		return TYPE;
	}

	@Override
	public JSONObject getPayload() throws Exception {
		// build payload...
		double x0 = bathymetry.getDomainX0();
		double y0 = bathymetry.getDomainY0();
		double dx = bathymetry.getDomainWidth();
		double dy = bathymetry.getDomainHeight();
		List<Cell> cells = bathymetry.listCells();
		
		JSONObject jbathy = new JSONObject();
		jbathy.put("xmin", x0);
		jbathy.put("ymin", y0);
		jbathy.put("xmax", x0 + dx);
		jbathy.put("ymax", y0 + dy);
		
		int index = 0;
		JSONArray jgrid = new JSONArray();
		for (Cell c : cells) {
			JSONObject jcell = new JSONObject();
			jcell.put("x", c.xc);
			jcell.put("y", c.yc);
			jcell.put("z", c.zc);
			jcell.put("ref", c.ref);
			jgrid.put(index, jcell);
			index++;
		}
		
		jbathy.put("grid", jgrid);
		
		return jbathy;
		
	}

}
