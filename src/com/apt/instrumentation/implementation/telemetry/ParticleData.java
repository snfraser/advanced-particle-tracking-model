/**
 * 
 */
package com.apt.instrumentation.implementation.telemetry;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import com.apt.data.Particle;

/**
 * 
 */
public class ParticleData extends TelemetryData {

	private static final String TYPE = "PARTICLES";
	
	private List<Particle> particles;
	
	public ParticleData(String muid, double time, List<Particle> particles) {
		super(muid, time);
		this.particles = particles;
	}

	@Override
	public String getType() {
		return TYPE;
	}

	@Override
	public JSONObject getPayload() throws Exception {
		// build payload...
		
		int index = 0;
		JSONArray jlist = new JSONArray();
		
		for (Particle p : particles) {
			JSONObject jp = new JSONObject();
			jp.put("id", p.getId());
			jp.put("state", p.getState().name());
			jp.put("type", p.getType().name());
			jp.put("x", p.getX());
			jp.put("y", p.getY());
			jp.put("z", p.getZ());
			jlist.put(index, jp);
			index++;
		}
		
		JSONObject jo = new JSONObject();
		jo.put("particles", jlist);
		return jo;
	
	}
}
