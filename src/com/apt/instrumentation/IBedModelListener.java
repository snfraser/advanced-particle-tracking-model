/**
 * 
 */
package com.apt.instrumentation;

import com.apt.data.CompoundMass;
import com.apt.models.IBedCellModel;
import com.apt.models.bed.BedLayerModel;

import depomod.netcdf.data.Cell;

/**
 * @author SA05SF
 *
 */
public interface IBedModelListener {

	/**
	 * Update the mass deposited on the surface of a cell.
	 * @param time The time [ms].
	 * @param cell A Mesh cell.
	 * @param mass The compound mass on the surface.
	 */
	public void updateSurfaceMass(long time, IBedCellModel cell, CompoundMass mass);
	
	/**
	 * Update the state of the layers in a bed cell.
	 * @param time the time [ms].
	 * @param cell A mesh cell.
	 * @param layers A BedLayerModel covering the layers of the cell.
	 */
	public void updateLayers(long time, IBedCellModel cell, BedLayerModel layers);
	
	/**
	 * Update the set of erosion parameters appertaining to a cell.
	 * @param time The time [ms].
	 * @param cell A mesh cell.
	 * @param frictionVelocity Current friction velocity value.
	 * @param taub Tau_b the current stress on the bed surface due to flow [Pa].
	 * @param flux The calculated erosion flux over the surface [kg].
	 */
	public void updateErosionVariables(long time, IBedCellModel cell, double frictionVelocity, double taub, double flux);
	
	/**
	 * Update the set of erosion parameters.
	 * @param density Density of the deposited material [kg/m3].
	 * @param massErosionCoeff Erosion coefficient to calculate erosion flux based on friction velocity [rho.v^2].
	 * @param zeta Fraction of erosion mass put back into water-column.
	 * @param z0 Surface roughness [m].
	 * @param boundaryHeight Height of the boundary layer 9where bundary speed is obtaine d[m].
	 */
	public void updateErosionParameters(double density, double massErosionCoeff, double zeta, double z0, double boundaryHeight);
	
}
