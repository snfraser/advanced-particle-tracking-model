/**
 * 
 */
package com.apt.instrumentation;

import java.io.File;
import java.util.List;

import com.apt.data.Particle;
import com.apt.data.ParticleClass;
import com.apt.models.IBathymetryModel;
import com.apt.models.IBedCellModel;
import com.apt.models.IBedLayoutModel;
import com.apt.models.ICageLayoutModel;
import com.apt.models.bed.RegularGridBedLayoutModel;

/**
 * @author SA05SF
 *
 */
public interface Instrumentation {
	
	public void startLogging();
	
	public void metadata(String projectName, String modelName, ICageLayoutModel layout);
	
	public void useBathymetry(IBathymetryModel bm);
	
	public IBedModelListener getBedModelListener();
	
	public void observedCell(double x, double y);
	
	public void addParticles(long time, List<Particle> particles);
	
	public void updateRate(long time, double rate, ParticleClass type);
	
	public void updateBed(IBedLayoutModel bed, long time);
	
	public void updateFlow(long time, double sigma, double flow);
	
	public void updateMass(long time, double mass, double thresholdMass);
	
	public void updateParticleCount(long time, List<Particle> particles);
	
	public void runLog(String key, String value);
	
	public void updateTauB(long time, double taub);
	
	public void updateBedLoad(long time, double bedload);
	
	public void updateErosionFlux(long time, double flux);
	
	public void outputRunLog() throws Exception;
	
	public void updateTauC(int level, long time, double tauc, double eqtauc);
	
	public void updateBedLayerCount(long time, int nlevels);
	
	public void updateResusMass(long time, double resusMass);
	
	public void updateBedCell(long time, IBedCellModel cell);
	
}
