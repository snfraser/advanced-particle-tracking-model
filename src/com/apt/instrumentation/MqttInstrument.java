/**
 * 
 */
package com.apt.instrumentation;

import java.util.List;

import com.apt.data.Particle;
import com.apt.data.ParticleClass;
import com.apt.models.IBathymetryModel;
import com.apt.models.IBedCellModel;
import com.apt.models.IBedLayoutModel;
import com.apt.models.ICageLayoutModel;

/**
 * 
 */
public class MqttInstrument implements Instrumentation {

	@Override
	public IBedModelListener getBedModelListener() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void observedCell(double x, double y) {
		// TODO Auto-generated method stub

	}

	@Override
	public void addParticles(long time, List<Particle> particles) {
		// TODO Auto-generated method stub
		// pack id, time, list<p> into datagram
		//getDatagram().fill(nextid(), time, Type.PARTICLES, particles).send();

	}

	@Override
	public void updateRate(long time, double rate, ParticleClass type) {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateBed(IBedLayoutModel bed, long time) {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateFlow(long time, double sigma, double flow) {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateMass(long time, double mass, double thresholdMass) {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateParticleCount(long time, List<Particle> particles) {
		// TODO Auto-generated method stub

	}

	@Override
	public void runLog(String key, String value) {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateTauB(long time, double taub) {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateBedLoad(long time, double bedload) {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateErosionFlux(long time, double flux) {
		// TODO Auto-generated method stub

	}

	@Override
	public void outputRunLog() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateTauC(int level, long time, double tauc, double eqtauc) {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateBedLayerCount(long time, int nlevels) {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateResusMass(long time, double resusMass) {
		// TODO Auto-generated method stub

	}

	@Override
	public void startLogging() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void metadata(String projectName, String modelName, ICageLayoutModel layout) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void useBathymetry(IBathymetryModel bm) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateBedCell(long time, IBedCellModel cell) {
		// TODO Auto-generated method stub
		
	}

}
