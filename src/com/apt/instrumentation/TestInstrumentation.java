package com.apt.instrumentation;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;

import com.apt.cli.util.ConfigurationProperties;
import com.apt.data.ChemicalComponent;
import com.apt.data.ChemicalComponentType;
import com.apt.data.CompoundMass;
import com.apt.data.Particle;
import com.apt.data.ParticleClass;
import com.apt.gui.BedLevelsCountPlot;
import com.apt.gui.CombinedTimeSeries1;
import com.apt.gui.CombinedTimeSeries2;
import com.apt.gui.FaecesDepositionRatePanel;
import com.apt.gui.FlowRatePanel;
import com.apt.gui.MassDepositionPanel;
import com.apt.gui.MassTimeSeriesPanel;
import com.apt.gui.ParticleCountPanel;
import com.apt.gui.ParticleTrackPanel;
import com.apt.gui.SurfaceDepositionPanel;
import com.apt.models.IBathymetryModel;
import com.apt.models.IBedCellModel;
import com.apt.models.IBedLayoutModel;
import com.apt.models.ICageLayoutModel;
import com.apt.models.bed.BedCellModel;
import com.apt.models.bed.RegularGridBedLayoutModel;

public class TestInstrumentation implements Instrumentation {

	private IBedModelListener bedListener = new DefaultBedCellListener();

	ParticleTrackPanel trackingPanel;
	SurfaceDepositionPanel surfacePanel;
	FaecesDepositionRatePanel faecesPanel;
	MassDepositionPanel massPanel;
	FlowRatePanel flowPanel;
	ParticleCountPanel countPanel;
	CombinedTimeSeries1 cts;
	CombinedTimeSeries2 cts2;
	BedLevelsCountPlot blcPanel;
	File runFolder;

	ConfigurationProperties runlog;

	PrintWriter massTimeWriter;
	PrintWriter taubTimeWriter;
	PrintWriter erosionTimeWriter;

	/**
	 * @param trackingPanel
	 */
	public TestInstrumentation(File runFolder, ParticleTrackPanel trackingPanel, SurfaceDepositionPanel surfacePanel,
			FaecesDepositionRatePanel faecesPanel, MassDepositionPanel massPanel, FlowRatePanel flowPanel,
			ParticleCountPanel countPanel, CombinedTimeSeries1 cts, CombinedTimeSeries2 cts2,
			BedLevelsCountPlot blcPanel) {
		this(runFolder);
	
		this.trackingPanel = trackingPanel;
		this.surfacePanel = surfacePanel;
		this.faecesPanel = faecesPanel;
		this.massPanel = massPanel;
		this.flowPanel = flowPanel;
		this.countPanel = countPanel;
		this.cts = cts;
		this.cts2 = cts2;
		this.blcPanel = blcPanel;

	}

	public TestInstrumentation(File runFolder) {

		runlog = new ConfigurationProperties();

		try {
			File massTimefile = new File(runFolder, "mass_time.dat");
			massTimeWriter = new PrintWriter(new FileWriter(massTimefile));
			File taubTimefile = new File(runFolder, "taub_time.dat");
			taubTimeWriter = new PrintWriter(new FileWriter(taubTimefile));
			File erosionTimefile = new File(runFolder, "erosion_time.dat");
			erosionTimeWriter = new PrintWriter(new FileWriter(erosionTimefile));
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/*this.trackingPanel = trackingPanel;
	this.surfacePanel = surfacePanel;
	this.faecesPanel = faecesPanel;
	this.massPanel = massPanel;
	this.flowPanel = flowPanel;
	this.countPanel = countPanel;
	this.cts = cts;
	this.cts2 = cts2;
	this.blcPanel = blcPanel;*/
	
	
	
	
	public void observedCell(double x, double y) {
		if (trackingPanel != null)
			trackingPanel.observedCell(x, y);
	}

	/**
	 * @param trackingPanel the trackingPanel to set
	 */
	public void setTrackingPanel(ParticleTrackPanel trackingPanel) {
		this.trackingPanel = trackingPanel;
	}

	/**
	 * @param surfacePanel the surfacePanel to set
	 */
	public void setSurfacePanel(SurfaceDepositionPanel surfacePanel) {
		this.surfacePanel = surfacePanel;
	}

	/**
	 * @param faecesPanel the faecesPanel to set
	 */
	public void setFaecesPanel(FaecesDepositionRatePanel faecesPanel) {
		this.faecesPanel = faecesPanel;
	}

	/**
	 * @param massPanel the massPanel to set
	 */
	public void setMassPanel(MassDepositionPanel massPanel) {
		this.massPanel = massPanel;
	}

	/**
	 * @param flowPanel the flowPanel to set
	 */
	public void setFlowPanel(FlowRatePanel flowPanel) {
		this.flowPanel = flowPanel;
	}

	/**
	 * @param countPanel the countPanel to set
	 */
	public void setCountPanel(ParticleCountPanel countPanel) {
		this.countPanel = countPanel;
	}

	/**
	 * @param cts the cts to set
	 */
	public void setCts(CombinedTimeSeries1 cts) {
		this.cts = cts;
	}

	/**
	 * @param cts2 the cts2 to set
	 */
	public void setCts2(CombinedTimeSeries2 cts2) {
		this.cts2 = cts2;
	}

	/**
	 * @param blcPanel the blcPanel to set
	 */
	public void setBlcPanel(BedLevelsCountPlot blcPanel) {
		this.blcPanel = blcPanel;
	}

	@Override
	public void addParticles(long time, List<Particle> particles) {
		// System.err.printf("Inst:: add particles size: %d \n", particles.size());
		if (trackingPanel != null)
			trackingPanel.addParticles(time, particles);
		if (surfacePanel != null)
			surfacePanel.addParticles(time, particles);
	}

	public void updateRate(long time, double rate, ParticleClass type) {
		if (type == ParticleClass.FAECES) {
			if (faecesPanel != null)
				faecesPanel.updateFaecesDeposition(time, rate);
		} else {
			if (faecesPanel != null)
				faecesPanel.updateWasteFeedDeposition(time, rate);
		}
	}

	public void updateBed(IBedLayoutModel bed, long time) {
		System.err.println("TI:: update bed...");
		if (massPanel != null)
			massPanel.updateBed((RegularGridBedLayoutModel) bed, time);

		// write the bedmodel out to a file....
		try {
			File bedModelFile = new File(runFolder, "bedmodel.dat");
			PrintWriter pout = new PrintWriter(new FileWriter(bedModelFile));

			pout.printf("Bedmodel time: %f\n", (double) time / 1000.0);

			List<IBedCellModel> cells = bed.getCells();
			for (IBedCellModel c : cells) {
				double x = c.getX();
				double y = c.getY();
				pout.printf("%f %f ", x, y);

				CompoundMass mass = c.getTotalMass();
				Iterator<ChemicalComponentType> types = mass.types();
				while (types.hasNext()) {
					ChemicalComponentType itype = types.next();
					ChemicalComponent amass = mass.getComponent(itype);
					double cmass = amass.getMass();
					pout.printf(" %f ", cmass);
				}
				pout.println();
			}

			pout.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void updateFlow(long time, double sigma, double flow) {
		if (flowPanel != null)
			flowPanel.updateFlowRate(time, flow);
		if (cts != null) {
			if (sigma > -0.5)
				cts.addSurfaceFlowData(time, flow);
			else if (sigma < -0.5) {
				cts.addBoundaryFlowData(time, flow);
			}
		}
	}

	@Override
	public void updateParticleCount(long time, List<Particle> particles) {
		if (countPanel != null) {
			int feedCount = 0;
			int faecesCount = 0;
			// split the list into 2 sublists
			for (Particle p : particles) {
				if (p.getType() == ParticleClass.FAECES)
					faecesCount++;
				else
					feedCount++;
			}

			countPanel.updateFaecesParticleCount(time, faecesCount);
			countPanel.updateFeedParticleCount(time, feedCount);
			// System.err.printf("TI:: at: %d :: Fd: %d, Fae: %d \n", time, feedCount,
			// faecesCount);
		}

		if (massPanel != null) {
			massPanel.updateSuspensionParticles(time, particles);
		}

	}

	@Override
	public void runLog(String key, String value) {
		runlog.setProperty(key, value);
	}

	@Override
	public void outputRunLog() throws Exception {
		File logfile = new File(runFolder, "runlog.dat");
		FileWriter writer = new FileWriter(logfile);
		runlog.store(writer, "Model runlog");

		// close all other writer here - maybe we need a Inst.close() function which
		// does this??
		massTimeWriter.close();
		taubTimeWriter.close();
		erosionTimeWriter.close();
	}

	@Override
	public void updateMass(long time, double mass, double thresholdMass) {
		// System.err.println("TI:update mass:" +time/86400000+" "+mass);
		// write mass v time to file
		if (massTimeWriter != null) {
			massTimeWriter.printf("%f %f\n", (double) time, mass);
		}
		if (cts != null)
			cts.addSurfaceLayerMassData(time, mass, thresholdMass);
	}

	@Override
	public void updateTauB(long time, double taub) {
		// System.err.println("TI:update taub:" +time/86400000+" "+taub);
		if (taubTimeWriter != null) {
			taubTimeWriter.printf("%f %f\n", (double) time, taub);
		}

		if (cts != null) {
			cts.addTauBData(time, taub);
			cts.addBFVData(time, 0.0);
			cts.addErosionFluxData(time, 0.0);
			cts.addSurfaceFlowData(time, 0.0);
			cts.addBoundaryFlowData(time, 0.0);
		}
	}

	@Override
	public IBedModelListener getBedModelListener() {
		return bedListener;
	}

	@Override
	public void updateBedLoad(long time, double bedload) {
		if (cts != null)
			cts.addBedLoad(time, bedload);
	}

	@Override
	public void updateErosionFlux(long time, double flux) {
		if (erosionTimeWriter != null) {
			erosionTimeWriter.printf("%f %f\n", (double) time, flux);
		}
		if (cts != null)
			cts.addErosionFluxData(time, flux);
	}

	@Override
	public void updateTauC(int level, long time, double tauc, double eqtauc) {
		if (cts2 != null)
			cts2.addTauCData(time, tauc, eqtauc);
	}

	public void updateBedLayerCount(long time, int nlevels) {
		if (blcPanel != null)
			blcPanel.updateLayerCount(time, nlevels);

	}

	public void updateResusMass(long time, double resusMass) {
		if (cts != null) {
			cts.addResusMass(time, resusMass);
		}
	}

	@Override
	public void startLogging() {
		System.err.printf("InternalInstruments::start logging...\n");
	}

	@Override
	public void metadata(String projectName, String modelName, ICageLayoutModel layout) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void useBathymetry(IBathymetryModel bm) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateBedCell(long time, IBedCellModel cell) {
		// TODO Auto-generated method stub
		
	}
}
