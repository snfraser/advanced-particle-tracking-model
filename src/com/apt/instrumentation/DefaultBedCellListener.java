/**
 * 
 */
package com.apt.instrumentation;

import com.apt.data.ChemicalComponentType;
import com.apt.data.CompoundMass;
import com.apt.models.IBedCellModel;
import com.apt.models.bed.BedLayer;
import com.apt.models.bed.BedLayerModel;

import depomod.netcdf.data.Cell;

/**
 * @author SA05SF
 *
 */
public class DefaultBedCellListener implements IBedModelListener {

	private Cell observedCell = null;
	
	@Override
	public void updateSurfaceMass(long time, IBedCellModel cell, CompoundMass mass) {
		// TODO Auto-generated method stub
	}

	@Override
	public void updateLayers(long time, IBedCellModel cell, BedLayerModel layers) {
		BedLayer l1 = (BedLayer) layers.getLayer(1);
		double tauc = l1.getCurrentTauCrit();
		double mass = l1.getMass().getComponentMass(ChemicalComponentType.SOLIDS);
		System.err.printf("L1:: t: %f tauc: %f mass: %f\n", (double)time, tauc, mass);
	}

	
	@Override
	public void updateErosionVariables(long time, IBedCellModel cell, double frictionVelocity, double taub,
			double flux) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateErosionParameters(double density, double massErosionCoeff, double zeta, double z0,
			double boundaryHeight) {
		// TODO Auto-generated method stub
		
	}

}
