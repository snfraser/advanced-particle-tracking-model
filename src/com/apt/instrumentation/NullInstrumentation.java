package com.apt.instrumentation;

import java.io.File;
import java.io.FileWriter;
import java.util.List;

import com.apt.cli.util.ConfigurationProperties;
import com.apt.data.Particle;
import com.apt.data.ParticleClass;
import com.apt.models.IBathymetryModel;
import com.apt.models.IBedCellModel;
import com.apt.models.IBedLayoutModel;
import com.apt.models.ICageLayoutModel;


public class NullInstrumentation implements Instrumentation {

	ConfigurationProperties runlog;

	IBedModelListener bedListener = new NullBedListener();
	File runFolder;
	
	/**
	 * 
	 */
	public NullInstrumentation(File runFolder) {
		this.runFolder = runFolder;
		runlog = new ConfigurationProperties();
	}

	@Override
	public void addParticles(long time, List<Particle> particles) {
		// DO NOTHING
	}
	
	@Override
	public void updateRate(long time, double rate, ParticleClass type) {
		
	}

	@Override
	public void updateBed(IBedLayoutModel bed, long time) {
		
	}
	
	@Override
	public void updateFlow(long time, double sigma, double flow)  {}
	
	@Override
	public void updateParticleCount(long time, List<Particle> particles) {}
	
	@Override
	public void runLog(String key, String value) {
		runlog.setProperty(key, value);
	}

	@Override
	public void outputRunLog() throws Exception {
		File logfile = new File(runFolder, "runlog.dat");
		FileWriter writer = new FileWriter(logfile);
		runlog.store(writer, "Model runlog");
	}

	@Override
	public void updateMass(long time, double mass, double thresholdMass) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public IBedModelListener getBedModelListener() {
		// TODO Auto-generated method stub
		return bedListener;
	}

	@Override
	public void updateTauB(long time, double taub) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateBedLoad(long time, double bedload) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateErosionFlux(long time, double flux) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateTauC(int level, long time, double tauc, double eqtauc) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateBedLayerCount(long time, int nlevels) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateResusMass(long time, double resusMass) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void observedCell(double x, double y) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void startLogging() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void metadata(String projectName, String modelName, ICageLayoutModel layout) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void useBathymetry(IBathymetryModel bm) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateBedCell(long time, IBedCellModel cell) {
		// TODO Auto-generated method stub
		
	}
}
