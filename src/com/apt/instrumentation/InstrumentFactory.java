/**
 * 
 */
package com.apt.instrumentation;

import java.awt.BorderLayout;
import java.io.File;
import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;

import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JPanel;

import com.apt.cli.util.ConfigurationProperties;
import com.apt.gui.BedLevelsCountPlot;
import com.apt.gui.CombinedTimeSeries1;
import com.apt.gui.CombinedTimeSeries2;
import com.apt.gui.MassDepositionPanel;
import com.apt.gui.ParticleTrackPanel;
import com.apt.gui.SurfaceDepositionPanel;
import com.apt.instrumentation.implementation.telemetry.TelemetryData;
import com.apt.instrumentation.implementation.telemetry.TelemetryDespatcher;
import com.apt.instrumentation.implementation.telemetry.TelemetryInstrumentation;
import com.apt.models.IBathymetryModel;
import com.apt.models.ICageLayoutModel;
import com.apt.models.ISeabedModel;

/**
 * 
 */
public class InstrumentFactory {

	/**
	 * Create the instrumentation using the supplied models and configuration
	 * options.
	 * 
	 * @param runFolder  Folder where to store any generated output files.
	 * @param prefix     A prefix for any outputs.
	 * @param bathymetry Bathymetry model.
	 * @param layout     Cages layout.
	 * @param seabed     Seabed roughness and grainsize.
	 * @param config     Options to define which instruments are available.
	 * @return
	 */
	public static Instrumentation createInstruments(File runFolder, String prefix, IBathymetryModel bathymetry,
			ICageLayoutModel layout, ISeabedModel seabed, ConfigurationProperties config) throws Exception {

		// in here we should be deciding which instruments to create - needs more detail
		// we select both the specific listeners to apply and how to display the info

		// Instruments.BedCell.Monitor.enabled: true - select what to monitor
		// Instruments.BedCell.LayerCountDisplay.enabled: true - select how to monitor
		// it
		// Instruments.Flow.Monitor.enabled: true
		// Instruments.Flow.FlowStatsDisplay.enabled: true
		// Instruments.Flow.FlowHeatmapDisplay.enabled: true
		// Instruments.Particle.Tracking.enabled: true
		// Instruments.Particle.MultiTrackDisplay.enabled: true
		// Instruments.Particle.MultiTrackDisplay.bufferSize: 200
		// Instruments.Particle.MultiTrackDisplay.useTypeColors: true
		// etc etc etc...

		String instClass = config.getProperty("Instruments.class");

		Instrumentation theInstruments = null;

		if (instClass.equalsIgnoreCase("internal")) {

			System.err.printf("InstFactoryCreate BPM using: %s \n", bathymetry.getBCN());

			theInstruments = new TestInstrumentation(runFolder);

			boolean doTrackPanel = config.getBooleanValue("Instruments.particleTrackPanel");
			if (doTrackPanel) {
				// any other config options
				ParticleTrackPanel trackPanel = new ParticleTrackPanel(bathymetry, layout);
				createFrame(prefix, "Landed Particles", 50, 50, 400, 400, trackPanel);
				((TestInstrumentation) theInstruments).setTrackingPanel(trackPanel);
			}

			boolean doSurfacePanel = config.getBooleanValue("Instruments.surfaceDepositionPanel");
			if (doSurfacePanel) {
				SurfaceDepositionPanel surfacePanel = new SurfaceDepositionPanel(bathymetry, layout);
				createFrame(prefix, "Deposition", 475, 50, 400, 400, surfacePanel);
				((TestInstrumentation) theInstruments).setSurfacePanel(surfacePanel);
			}

			boolean doMassPanel = config.getBooleanValue("Instruments.massDepositionPanel");
			if (doMassPanel) {
				MassDepositionPanel massPanel = new MassDepositionPanel(bathymetry, layout);
				createFrame(prefix, "Mass/Resus", 50, 475, 400, 400, massPanel);
				((TestInstrumentation) theInstruments).setMassPanel(massPanel);
			}

			// SeabedStateDisplay seabedPanel = new SeabedStateDisplay(seabed, null);
			// BathymetryPanel bathyPanel = new BathymetryPanel(bathymetry);
			// MassTimeSeriesPanel massTimePanel = new MassTimeSeriesPanel();

			boolean doBedMonitoring = config.getBooleanValue("Instruments.bedMonitor");
			if (doBedMonitoring) {
				// CTS-1
				CombinedTimeSeries1 cts = new CombinedTimeSeries1();
				createFrame(prefix, "Mass time", 900, 50, 850, 850,
						cts.createChartPanel("Deposition/erosion", "Cell (300, 1150)"));
				((TestInstrumentation) theInstruments).setCts(cts);

				// CTS-2
				CombinedTimeSeries2 cts2 = new CombinedTimeSeries2();
				createFrame(prefix, "Critical Stress", 475, 475, 400, 400,
						cts2.createChartPanel("Tauc[L1]", "Cell (300, 1150)"));
				((TestInstrumentation) theInstruments).setCts2(cts2);

				// BLCP
				BedLevelsCountPlot bcp = new BedLevelsCountPlot();
				createFrame(prefix, "Bed layers", 700, 700, 400, 400, bcp.createChartPanel("Bed layers"));
				((TestInstrumentation) theInstruments).setBlcPanel(bcp);
			}

			boolean observeCell = config.getBooleanValue("Instruments.observeCell.enabled");
			if (observeCell) {
				double cellX = config.getDoubleValue("Instruments.observeCell.X");
				double cellY = config.getDoubleValue("Instruments.observeCell.Y");
				theInstruments.observedCell(cellX, cellY);
			}
		} else if (instClass.equalsIgnoreCase("telemetry")) {
			
			int qsize = config.getIntValue("Instruments.telemetryQueueSize", 1000);
			String host = config.getProperty("Instruments.telemetryHost", "localhost");
			int port = config.getIntValue("Instruments.telemetryPort", 8787);
			
			Queue<TelemetryData> queue = new ArrayBlockingQueue<>(1000);
			TelemetryDespatcher despatcher = new TelemetryDespatcher(queue, host, port);

			theInstruments = new TelemetryInstrumentation("Run#666", queue, despatcher);
			
		} else if (instClass.equalsIgnoreCase("NONE")) {
			
			theInstruments = new NullInstrumentation(runFolder);
			
		}
		return theInstruments;

	}

	private static void createFrame(String prefix, String title, int x, int y, int w, int h, JPanel content) {
		JFrame f = new JFrame("NDART: " + prefix + " " + title);
		f.getContentPane().setLayout(new BorderLayout());
		f.getContentPane().add(content, BorderLayout.CENTER);
		f.pack();
		f.setBounds(x, y, w, h);
		try {
			f.setIconImage(
					ImageIO.read(ClassLoader.getSystemResourceAsStream("com/apt/gui/resources/particles_icon.png")));
		} catch (Exception e) {
		}

		f.setVisible(true);
	}

}
