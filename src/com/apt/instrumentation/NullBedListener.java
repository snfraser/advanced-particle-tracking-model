/**
 * 
 */
package com.apt.instrumentation;

import com.apt.data.CompoundMass;
import com.apt.models.IBedCellModel;
import com.apt.models.bed.BedLayerModel;

import depomod.netcdf.data.Cell;

/**
 * @author SA05SF
 *
 */
public class NullBedListener implements IBedModelListener {

	@Override
	public void updateSurfaceMass(long time, IBedCellModel cell, CompoundMass mass) {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateLayers(long time, IBedCellModel cell, BedLayerModel layers) {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateErosionVariables(long time, IBedCellModel cell, double frictionVelocity, double taub,
			double flux) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateErosionParameters(double density, double massErosionCoeff, double zeta, double z0,
			double boundaryHeight) {
		// TODO Auto-generated method stub
		
	}

	
}
