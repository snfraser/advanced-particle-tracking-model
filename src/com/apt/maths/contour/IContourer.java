/**
 * 
 */
package com.apt.maths.contour;

import java.util.LinkedList;

/**
 * @author SA05SF
 *
 */
public interface IContourer {
	
	  /**
    *
    * @param d
    * @param ilb
    * @param iub
    * @param jlb
    * @param jub
    * @param x
    * @param y
    * @param z
    * @return
    */
   public LinkedList<ContourSegment> contour(double[][] d, int ilb, int iub, int jlb, int jub, double[] x, double[] y, double z);

}
