/**
 * 
 */
package com.apt.maths.contour;

/**
 * @author SA05SF
 *
 */
public class Vertex {
	 public final int dimensions;
	    private double[] location;
	    
	    
	    public Vertex(double xCoordinate,double yCoordinate){
	        this(new double[]{xCoordinate,yCoordinate});
	    }
	    
	    public Vertex(double[] location){
	        dimensions = location.length;
	        this.location=location;
	    }
	    
	    /**
	     * return the Side of a point for a directed line
	     * @param point
	     * @param line
	     * @return 
	     */
//	    public static Side isLeft(Vertex point,Edge line){
//	        Side side;
//	        return side;
//	    }


	    /**
	     * @return the location
	     */
	    public double[] getLocation() {
	        return location;
	    }

	    /**
	     * @return the location
	     */
	    public double getCoordinate(int index) throws Exception {
	        if(index>=this.dimensions || index<0){
	        	throw new Exception("Index out of range");
	        }
	        return location[index];
	    }

	    /**
	     * @param index
	     * @param coordinate
	     */
	    public void setLocation(int index,double coordinate) throws Exception {
	        if(index>=this.dimensions || index<0){
	        	throw new Exception("Index out of range");
	        }
	        location[index]=coordinate;
	    }

	    /**
	     * @set the x location
	     */
	    public void setX(double xCoordinate) {
	        location[0]=xCoordinate;
	    }
	    
	    /**
	     * @set the x location
	     */
	    public void setY(double yCoordinate) {
	        location[1]=yCoordinate;
	    }
	    /**
	     * @param location the location to set
	     */
	    public void setLocation(double[] location) throws Exception {
	        if(location.length!=this.dimensions){
	        	throw new Exception("Wrong dimensions for location");
	        }
	        this.location = location;
	    }
	    
	    public double distance(Vertex aPoint) throws Exception {
	        if(aPoint.dimensions!=this.dimensions){
	        	throw new Exception("Wrong dimensions for point");
	        }
	        double sum=0.0;
	        for (int i = 0; i < location.length; i++) {
	            double temp = location[i]-aPoint.getCoordinate(i);
	            sum+=temp*temp;
	        }
	        return Math.sqrt(sum);
	    }

	  
	    public double getX() {
	        return location[0];
	    }

	    public double getY() {
	        return location[1];
	    }
	    
	    @Override
	    public String toString(){
	        return "plot("+location[0]+","+location[1]+",'mx','LineWidth',3)";
	    }
}
