/**
 * 
 */
package com.apt.maths.contour;

/**
 * @author SA05SF
 *
 */
public class Edge {
	 public Edge(double startX,double startY,double endX,double endY){
	        this(new Vertex(startX,startY), new Vertex(endX,endY));
	    }
	    
	    public Edge(Vertex start,Vertex end){
	        this.start=start;
	        this.end=end;
	    }
	    
	    public Edge(Edge edge){
	        this.start=edge.start;
	        this.end=edge.end;
	    }
	    
	    
	    private Vertex start;
	    private Vertex end;

	    /**
	     * @return the start
	     */
	    public Vertex getStart() {
	        return start;
	    }

	    /**
	     * @return the end
	     */
	    public Vertex getEnd() {
	        return end;
	    }

	    public void reverse() {
	            Vertex temp;
	            //swap x
	            temp=start;
	            start=end;
	            end=temp;
	    }
	    
	    public boolean isDuplicate(Edge segment) {
	        return forwardDuplicate(segment) || reverseDuplicate(segment); 
	    }
	    
	    public boolean forwardDuplicate(Edge segment){
	        boolean retVal= start.getX() == segment.getStart().getX() && start.getY() == segment.getStart().getY();
	        retVal = retVal && end.getX() == segment.getEnd().getX() && end.getY() == segment.getEnd().getY();
	        return retVal;        
	    }

	    public boolean reverseDuplicate(Edge segment){
	        boolean retVal= end.getX() == segment.getStart().getX() && end.getY() == segment.getStart().getY();
	        retVal = retVal && start.getX() == segment.getEnd().getX() && start.getY() == segment.getEnd().getY();
	        return retVal;        
	    }


	    void setEnd(Vertex end) {
	        this.end=end;
	    }
	    
	    void setStart(Vertex start) {
	        this.start=start;
	    }
}
