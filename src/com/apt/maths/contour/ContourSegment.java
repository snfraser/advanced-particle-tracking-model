/**
 * 
 */
package com.apt.maths.contour;

/**
 * @author SA05SF
 *
 */
public class ContourSegment extends Edge implements Comparable<ContourSegment> {

	 private final double contourLevel;
     
     /**
     * ContourSegment line segment of contour
     *
     * Represents a line between the start and end coordinates.
     *
     * @param startX    - start coordinate for X
     * @param startY    - start coordinate for Y
     * @param endX      - end coordinate for X
     * @param endY      - end coordinate for Y
     * @param contourLevel - Contour level for line.
     */

     public ContourSegment(double startX, double startY, double endX, double endY, double contourLevel) {
//         this.startX = startX;
//         this.startY = startY;
//         this.endX = endX;
//         this.endY = endY;
         super(startX,startY,endX,endY);
         this.contourLevel = contourLevel;
     }

     public ContourSegment(Edge edge, double contourLevel) {
         super(edge);
         this.contourLevel = contourLevel;
     }

     
     @Override
     public int compareTo(ContourSegment o) {
         return Double.compare(this.getContourLevel(), o.getContourLevel());
     }


 /**
  * @return the startX
  */
 public double getStartX() {
     return getStart().getX();
 }

 /**
  * @return the startY
  */
 public double getStartY() {
     return getStart().getY();
 }

 /**
  * @return the endX
  */
 public double getEndX() {
     return getEnd().getX();
 }

 /**
  * @return the endY
  */
 public double getEndY() {
     return getEnd().getY();
 }

 /**
  * @return the contourLevel
  */
 public double getContourLevel() {
     return contourLevel;
 }
	
}
