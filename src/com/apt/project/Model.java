package com.apt.project;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Properties;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.json.JSONArray;
import org.json.JSONObject;

import com.apt.cli.util.ConfigurationProperties;
import com.apt.cli.util.FileUtils;
import com.apt.data.ChemicalTreatmentType;
import com.apt.data.Domain;
import com.apt.gui.FaecesDepositionRatePanel;
import com.apt.gui.FlowRatePanel;
import com.apt.gui.MassDepositionPanel;
import com.apt.gui.ParticleCountPanel;
import com.apt.gui.ParticleTrackPanel;
import com.apt.gui.SurfaceDepositionPanel;
import com.apt.instrumentation.InstrumentFactory;
import com.apt.instrumentation.Instrumentation;
import com.apt.instrumentation.NullInstrumentation;
import com.apt.instrumentation.ParticleCellTracker;
import com.apt.instrumentation.TestInstrumentation;
import com.apt.models.IBathymetryModel;
import com.apt.models.IBedLayoutModel;
import com.apt.models.ICageLayoutModel;
import com.apt.models.IChemicalDegradeModel;
import com.apt.models.IDefacationModel;
import com.apt.models.IDisruptionModel;
import com.apt.models.IFlowModel;
import com.apt.models.IGrowthModel;
import com.apt.models.IMortalityModel;
import com.apt.models.ISeabedModel;
import com.apt.models.ISeasonalityModel;
import com.apt.models.ISettlingModel;
import com.apt.models.ITidalModel;
import com.apt.models.ITransportModel;
import com.apt.models.ITurbulenceModel;
import com.apt.models.bed.BedModelFactory;
import com.apt.models.bed.RegularGridBedLayoutModel;
import com.apt.models.cage.CageLayoutModel;
import com.apt.models.chemical.ChemicalModelFactory;
import com.apt.models.chemical.DefaultChemicalDegradeModel;
import com.apt.models.defaecation.DefacationModel;
import com.apt.models.defaecation.DefacationModelFactory;
import com.apt.models.disruption.DisruptionModelFactory;
import com.apt.models.disruption.ExponentialDisruptionModel;
import com.apt.models.growth.FixedBiomassGrowthModel;
import com.apt.models.growth.GrowthModel;
import com.apt.models.growth.GrowthModelFactory;
import com.apt.models.mortality.MortalityModel;
import com.apt.models.mortality.MortalityModelFactory;
import com.apt.models.seasonality.SeasonalityModel;
import com.apt.models.settling.DefaultSettlingModel;
import com.apt.models.settling.SettlingModelFactory;
import com.apt.models.tidal.FixedTidalModel;
import com.apt.models.tidal.TidalModelFactory;
import com.apt.models.time.TimeModel;
import com.apt.models.transport.TransportModel;
import com.apt.models.turbulence.DefaultTurbulenceModel;
import com.apt.models.turbulence.TurbulenceModelFactory;
import com.apt.models.ITimeModel;

/**
 * Definitions of models and other configurations needed to run a simulation.
 * 
 * NOTE: should be able to get rid of MOST/ALL the public setter methods??
 * 
 * @author SA05SF
 *
 */
public class Model {

	private File modelFolder;

	String name;
	String description;
	String author;
	long creationDate;
	List<String> tags;
	
	ConfigurationProperties modelConfig;

	private ITimeModel timeModel;
	private ITransportModel transportModel;
	private IDisruptionModel disruptionModel;
	private ISettlingModel settlingModel;
	private IGrowthModel growthModel;
	private IMortalityModel mortalityModel;
	private IDefacationModel defacationModel;
	private ITurbulenceModel turbulenceModel;
	private IChemicalDegradeModel chemicalModel;
	private ITidalModel tidalModel;
	private IBedLayoutModel bedLayoutModel;
	private ISeasonalityModel seasonalityModel;
	private Instrumentation instrumentation;

	/**
	 * @param name
	 * @param description
	 * @param author
	 * @param creationDate
	 */
	public Model(String name, String description, String author, long creationDate) {
		super();
		this.name = name;
		this.description = description;
		this.author = author;
		this.creationDate = creationDate;
		tags = new ArrayList<String>();
	}

	public void load(IBathymetryModel bm, IFlowModel fm, ISeabedModel sea, ConfigurationProperties config) throws Exception {
		
		// TODO: load using info in the various config files under model...
		//File configsFolder = new File(modelFolder, "configs");
		//File modelFolder = new File(modelsFolder, name);
		
		File modelConfigFile = new File(modelFolder, "config.properties");
		
		modelConfig = new ConfigurationProperties();
		modelConfig.load(new FileReader(modelConfigFile));
		modelConfig.add(config); // add all the runtime configs
		
		
		timeModel = loadTimeModel(modelConfig);

		growthModel = GrowthModelFactory.createGrowthModel(modelConfig);
		
		mortalityModel = MortalityModelFactory.createMortalityModel(modelConfig);
		
		defacationModel = DefacationModelFactory.createDefacationModel(modelConfig);
	
		bedLayoutModel = BedModelFactory.createBedLayoutModel(bm, fm, sea, modelConfig);
		
		seasonalityModel = new SeasonalityModel(0.0, 0.0);
		
		tidalModel = TidalModelFactory.createTidalModel(modelConfig);
		
		turbulenceModel = TurbulenceModelFactory.createTurbulenceModel(modelConfig);
		
		settlingModel = SettlingModelFactory.createSettlingModel(modelConfig); // spec speed for fod, fae, mud, sand, etc
	
		disruptionModel = DisruptionModelFactory.createDisruptionModel(modelConfig);
		
		chemicalModel = ChemicalModelFactory.createChemicalModel(modelConfig);
		
		// define the instruments here, they will be notified of the runfolder later...
		//instrumentation = InstrumentFactory.createInstruments(modelConfig);
		

	}
	
	private ITimeModel loadTimeModel(ConfigurationProperties modelConfig) throws Exception {
		//File timeConfigFile = new File(modelFolder, "time.properties");
		//ConfigurationProperties timeProperties = FileUtils.loadConfig(timeConfigFile);
		System.err.println("Loading model timing...");
		// these should be in ms?
		double start = Double.parseDouble(modelConfig.getProperty("Model.start")); // ms
		double end = Double.parseDouble(modelConfig.getProperty("Model.end"));
		double step = Double.parseDouble(modelConfig.getProperty("Model.step"));
		
		ITimeModel tm = new TimeModel((long)start, (long)end, (long)step);
		System.err.printf("Timemodel is: st: %f dt: %f et: %f \n", start, step, end);
		return tm;
	}

	

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the author
	 */
	public String getAuthor() {
		return author;
	}

	/**
	 * @param author the author to set
	 */
	public void setAuthor(String author) {
		this.author = author;
	}

	/**
	 * @return the creationDate
	 */
	public long getCreationDate() {
		return creationDate;
	}

	/**
	 * @param creationDate the creationDate to set
	 */
	public void setCreationDate(long creationDate) {
		this.creationDate = creationDate;
	}
	
	

	/**
	 * @return the modelConfig
	 */
	public ConfigurationProperties getModelConfig() {
		return modelConfig;
	}

	/**
	 * @param modelConfig the modelConfig to set
	 */
	public void setModelConfig(ConfigurationProperties modelConfig) {
		this.modelConfig = modelConfig;
	}

	/**
	 * @param tag
	 * @return
	 * @see java.util.List#contains(java.lang.Object)
	 */
	public boolean containsTag(String tag) {
		return tags.contains(tag);
	}

	/**
	 * @param atags
	 * @return true if added successfully
	 * @see java.util.List#addAll(java.util.Collection)
	 */
	public boolean addTags(Collection<String> atags) {
		return tags.addAll(atags);
	}

	/**
	 * @param tag
	 * @see java.util.List#add(java.lang.Object)
	 */
	public void addTag(String tag) {
		tags.add(tag);
	}

	/**
	 * @return the tags
	 */
	public List<String> getTags() {
		return tags;
	}

	/**
	 * @return the timeModel
	 */
	public ITimeModel getTimeModel() {
		return timeModel;
	}

	/**
	 * @param timeModel the timeModel to set
	 */
	public void setTimeModel(ITimeModel timeModel) {
		this.timeModel = timeModel;
	}

	/**
	 * @return the transportModel
	 */
	public ITransportModel getTransportModel() {
		return transportModel;
	}

	/**
	 * @param transportModel the transportModel to set
	 */
	public void setTransportModel(ITransportModel transportModel) {
		this.transportModel = transportModel;
	}

	
	
	/**
	 * @return the settlingModel
	 */
	public ISettlingModel getSettlingModel() {
		return settlingModel;
	}

	/**
	 * @param settlingModel the settlingModel to set
	 */
	public void setSettlingModel(ISettlingModel settlingModel) {
		this.settlingModel = settlingModel;
	}

	/**
	 * @return the disruptionModel
	 */
	public IDisruptionModel getDisruptionModel() {
		return disruptionModel;
	}

	/**
	 * @param disruptionModel the disruptionModel to set
	 */
	public void setDisruptionModel(IDisruptionModel disruptionModel) {
		this.disruptionModel = disruptionModel;
	}

	/**
	 * @return the turbulenceModel
	 */
	public ITurbulenceModel getTurbulenceModel() {
		return turbulenceModel;
	}

	/**
	 * @param turbulenceModel the turbulenceModel to set
	 */
	public void setTurbulenceModel(ITurbulenceModel turbulenceModel) {
		this.turbulenceModel = turbulenceModel;
	}

	
	/**
	 * @return the tidalModel
	 */
	public ITidalModel getTidalModel() {
		return tidalModel;
	}

	/**
	 * @param tidalModel the tidalModel to set
	 */
	public void setTidalModel(ITidalModel tidalModel) {
		this.tidalModel = tidalModel;
	}

	/**
	 * @return the growthModel
	 */
	public IGrowthModel getGrowthModel() {
		return growthModel;
	}

	/**
	 * @param growthModel the growthModel to set
	 */
	public void setGrowthModel(IGrowthModel growthModel) {
		this.growthModel = growthModel;
	}
	
	

	/**
	 * @return the chemicalModel
	 */
	public IChemicalDegradeModel getChemicalModel() {
		return chemicalModel;
	}

	/**
	 * @param chemicalModel the chemicalModel to set
	 */
	public void setChemicalModel(IChemicalDegradeModel chemicalModel) {
		this.chemicalModel = chemicalModel;
	}

	/**
	 * @return the mortalityModel
	 */
	public IMortalityModel getMortalityModel() {
		return mortalityModel;
	}

	/**
	 * @param mortalityModel the mortalityModel to set
	 */
	public void setMortalityModel(IMortalityModel mortalityModel) {
		this.mortalityModel = mortalityModel;
	}

	/**
	 * @return the defaecationModel
	 */
	public IDefacationModel getDefaecationModel() {
		return defacationModel;
	}

	/**
	 * @param defacationModel the defaecationModel to set
	 */
	public void setDefaecationModel(IDefacationModel defacationModel) {
		this.defacationModel = defacationModel;
	}

	/**
	 * @return the bedLayoutModel
	 */
	public IBedLayoutModel getBedLayoutModel() {
		return bedLayoutModel;
	}

	/**
	 * @param bedLayoutModel the bedLayoutModel to set
	 */
	public void setBedLayoutModel(IBedLayoutModel bedLayoutModel) {
		this.bedLayoutModel = bedLayoutModel;
	}

	/**
	 * @return the seasonalityModel
	 */
	public ISeasonalityModel getSeasonalityModel() {
		return seasonalityModel;
	}

	/**
	 * @param seasonalityModel the seasonalityModel to set
	 */
	public void setSeasonalityModel(ISeasonalityModel seasonalityModel) {
		this.seasonalityModel = seasonalityModel;
	}

	/**
	 * @return the instrumentation
	 */
	public Instrumentation getInstrumentation() {
		return instrumentation;
	}

	/**
	 * @param instrumentation the instrumentation to set
	 */
	public void setInstrumentation(Instrumentation instrumentation) {
		this.instrumentation = instrumentation;
	}

	/**
	 * @return the modelFolder
	 */
	public File getModelFolder() {
		return modelFolder;
	}

	/**
	 * @param modelFolder the modelFolder to set
	 */
	public void setModelFolder(File modelFolder) {
		this.modelFolder = modelFolder;
	}

	
}
