/**
 * 
 */
package com.apt.project;

import java.util.Properties;

/**
 * Stores information about a project, model or run.
 * @author SA05SF
 *
 */
public class Config {
	
	private String name;
	private String description;
	private Properties properties;
	private int cid;
	private int xbase;

	/**
	 * @param name
	 * @param props
	 * @param cid
	 * @param xbase
	 */
	public Config(String name, Properties properties, int cid, int xbase) {
		this.name = name;
		this.properties = properties;
		this.cid = cid;
		this.xbase = xbase;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @return the props
	 */
	public Properties getProps() {
		return properties;
	}

	/**
	 * @return the cid
	 */
	public int getCid() {
		return cid;
	}

	/**
	 * @return the xbase
	 */
	public int getXbase() {
		return xbase;
	}


}
