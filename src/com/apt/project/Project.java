package com.apt.project;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.SimpleTimeZone;

import org.json.JSONArray;
import org.json.JSONObject;

import com.apt.cli.util.ConfigurationProperties;
import com.apt.models.Cage;
import com.apt.data.CageType;
import com.apt.data.Domain;
import com.apt.data.GridPosition;
import com.apt.data.Mesh;
import com.apt.data.PositionVector;
import com.apt.data.RegularSquareMesh;
import com.apt.data.VoronoiMesh;
import com.apt.instrumentation.Instrumentation;
import com.apt.models.IBathymetryModel;
import com.apt.models.ICageLayoutModel;
import com.apt.models.IFeedModel;
import com.apt.models.IFlowModel;
import com.apt.models.ISeabedModel;
import com.apt.models.ITurbulenceModel;
import com.apt.models.bathymetry.MeshBathymetry;
import com.apt.models.bed.RegularGridBedLayoutModel;
import com.apt.models.cage.CageLayoutModel;
import com.apt.models.cage.FreeFormCagePositionManager;
import com.apt.models.cage.ICagePositionManager;
import com.apt.models.cage.RegularGridCagePositionManager;
import com.apt.models.defaecation.DefacationModel;
import com.apt.models.feed.FeedModel;
import com.apt.models.feed.SpecialFeedModel;
import com.apt.models.flow.NetcdfFlowModel;
import com.apt.models.growth.GrowthModel;
import com.apt.models.mortality.MortalityModel;
import com.apt.models.readers.BathymetryReader;
import com.apt.models.readers.FlowmetryReader;
import com.apt.models.readers.MeshReader;
import com.apt.models.readers.MultipointSeabedModelReader;
import com.apt.models.readers.SeabedReader;
import com.apt.models.seabed.MultipointSeabedModel;
import com.apt.models.seabed.SeabedModel;
import com.apt.models.seasonality.SeasonalityModel;
import com.apt.models.stock.StockModel;
import com.apt.models.time.TimeModel;
import com.apt.models.transport.TransportModel;
import com.apt.readers.RegularGridBathymetryReader;
import com.apt.readers.SinglePointFlowmetryReader;

/**
 * Represents a farm site project.
 * 
 * usage example...... Project p = Project.load(/rootdir/, projname); Model m =
 * proj.loadModel(modelname); RunConfig config =
 * m.layout("cages2").chemical("embz").tag("some.extra.info"); // have a model
 *
 * Engine e = new SomeEngine(....);
 * 
 * e.run(config)
 *
 * 
 * 
 * 
 * @author SA05SF
 *
 */

public class Project {

	public static SimpleDateFormat SDF = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
	static {
		SDF.setTimeZone(new SimpleTimeZone(0, "UTC"));
	}

	private File projectFolder;

	private ConfigurationProperties config;

	private String name;
	private String description;
	private String owner;
	private String ccode;
	private double geoLat;
	private double geoLon;
	private String author;

	private List<String> tags;

	// Mesh variables...
	private IBathymetryModel bathymetry;
	private IFlowModel flowmetry;
    private ISeabedModel seabed;

	/**
	 * Create a new Project with the specified parameters.
	 * 
	 * @param name        The farm project name - typically this will be a shortened
	 *                    version in camel-case.
	 * @param description A short description of the project (basically about the
	 *                    site itself).
	 * @param owner       The company who owns the farm site.
	 * @param ccode       A 3 letter country code (ISO3166-1 alpha-3). J11+ use:
	 *                    Locale.getISO3Country()....
	 * @param geoLat      Latitude of the farm site (approximate centre of domain).
	 * @param geoLon      Longitude of the farm site (approximate centre of domain).
	 * @param author      Name of the author.
	 */
	public Project(String name, String description, String owner, String ccode, double geoLat, double geoLon,
			String author) {
		super();
		this.name = name;
		this.description = description;
		this.owner = owner;
		this.ccode = ccode;
		this.geoLat = geoLat;
		this.geoLon = geoLon;
		this.author = author;
		tags = new ArrayList<String>();
	}

	public void load() throws Exception {

		// load the project configuration (this should be in proj.props)
		config = new ConfigurationProperties();
		File configFile = new File(projectFolder, "project.properties");
		config.load(new FileReader(configFile));

		// Load the project-specific models: bathymetry, flow and seabed.
		// Each could have a different mesh or could all be the same mesh
		// for netcdf input the bathymetry mesh is a set of voronoi cells
		// dual to the flow mesh (usually a delauney triangulation)

		// APTM: definition of mesh: a set of cells with their surrounding nodes.

		// bathymetry is defined at the nodes so after identifying a particle's cell
		// the depth is interpolated from the surrounding nodes.

		// flowmetry is defined at the centroid of cell so after identifying a
		// particle's cell
		// the flow is interpolated from the cell and its nearest neighbours.

		// seabed status is defined for a cell so after identifying a particle's cell
		// the seabed status is determined from the cell alone.

		// alt1: load bathymetry mesh from file.
		// File bathymetryMeshFile = new File(projectFolder, "bathymetry.mesh");
		// BathymetryReader br = new BathymetryReader();
		// bathymetry = br.load(bathymetryMeshFile);

		// TODO: we should define a way of deciding which type of bathymetry we need
		// from the file-ext
		// or from some config value.

		// mesh descriptor will point to the bathymetry data file
		File meshDescriptionFile = new File(projectFolder, "mesh.json");
		MeshReader mr = new MeshReader();
		Mesh mesh = mr.readFile(projectFolder, meshDescriptionFile);
		
		//File bathyMeshFile = new File(projectFolder, "bathymetry/regulargrid.mesh");
		//RegularGridBathymetryReader rgr = new RegularGridBathymetryReader();
		//Mesh mesh = rgr.readFile(bathyMeshFile);
		// RegularGridBathymetry
		bathymetry = new MeshBathymetry(mesh); // any mesh: regular square, triangular, voronoi

		Domain d = mesh.getDomainBounds();
		double d0 = bathymetry.getDepth((d.x0 + d.x1)/2.0, (d.y0 + d.y1)/2.0);
		System.err.printf("Project: Bathy at centre is: %f \n", d0);
		
		d0 = bathymetry.getDepth((d.x0 + d.x1)/3.0, (d.y0 + d.y1)/3.0);
		System.err.printf("Project: Bathy at 1/3 is: %f \n", d0);
		
		// load flowmetry mesh from file.
		File flowmetryMeshFile = new File(projectFolder, "flowmetry/flowmetry_Q_B.flow");
		//FlowmetryReader fr = new FlowmetryReader();
		//flowmetry = fr.load(mesh, flowmetryMeshFile);
		
		SinglePointFlowmetryReader rdr = new SinglePointFlowmetryReader();// pass mesh in for depths...
		flowmetry = rdr.readFile(flowmetryMeshFile);
		
		// load netcdf flow file
		// File flowmetryMeshFile2 = new File(projectFolder, "flowmetry_netcdf.mesh");
		// flowmetry = new NetcdfFlowModel(flowmetryMeshFile2);
		// flowmetry.

		// load seabed mesh from file.
		File seabedMeshFile = new File(projectFolder, "seabed/seabed_Q_A.mesh");
		MultipointSeabedModelReader sr = new MultipointSeabedModelReader();
		seabed = sr.readFile(seabedMeshFile);


		// load turb model??

	}

	/**
	 * Create a cagelayout with the specified name and layout and stores it.
	 * 
	 * @param layout    The layout to create.
	 * @param overwrite If true, the layout file will be overwritten if it already
	 *                  exists.
	 * @throws Exception
	 */
	public void createLayout(ICageLayoutModel layout, boolean overwrite) throws Exception {

		CageLayoutModel layout2 = (CageLayoutModel) layout;

		if (!projectFolder.exists())
			throw new IOException(String.format("project folder %s does not exist \n", projectFolder.getPath()));

		File layoutsFolder = new File(projectFolder, "layouts");
		if (!layoutsFolder.exists())
			throw new IOException(String.format("layout folder %s does not exist \n", layoutsFolder.getPath()));

		File layoutFile = new File(layoutsFolder, layout.getLayoutName() + ".json");
		if (layoutFile.exists()) {
			if (!overwrite)
				throw new IOException(String.format("layout file %s already exists \n", layoutFile.getPath()));
		}

		// write the layout to json format .... TODO

		JSONObject jlayout = new JSONObject();
		jlayout.put("name", layout.getLayoutName());

		JSONArray jgroups = new JSONArray();

		ICagePositionManager pm = layout.getPositionManager();

		List<Cage> cages = pm.getCages();

		JSONObject jgroup1 = new JSONObject();
		JSONArray jgridArray = new JSONArray();
		
		if (pm instanceof RegularGridCagePositionManager) {
			// grid of ij offsets, cages
			RegularGridCagePositionManager rpm = (RegularGridCagePositionManager) pm;

			jgroup1.put("type", "REGULARGRID");

			JSONObject jgparams = new JSONObject();
			jgparams.put("xCorner", rpm.getxCorner());
			jgparams.put("yCorner", rpm.getxCorner());
			jgparams.put("iSpacing", rpm.getiSpacing());
			jgparams.put("jSpacing", rpm.getjSpacing());
			jgparams.put("bearing", rpm.getBearing());

			jgroup1.put("params", jgparams);


			for (Cage cage : cages) {
				// grid position
				GridPosition grid = rpm.getGridPosition(cage.getName());
				JSONObject jgriditem = new JSONObject();
				jgriditem.put("id", cage.getName());
				jgriditem.put("i", grid.getI());
				jgriditem.put("j", grid.getJ());
				jgridArray.put(jgriditem);
			}

		} else if (pm instanceof FreeFormCagePositionManager) {
			// grid of positions, cages
			FreeFormCagePositionManager ffm = (FreeFormCagePositionManager) pm;
		
			jgroup1.put("type", "FREEFORM");

			for (Cage cage : cages) {
				PositionVector pos = ffm.getPosition(cage.getName());
				JSONObject jgriditem = new JSONObject();
				jgriditem.put("id", cage.getName());
				jgriditem.put("x", pos.getX());
				jgriditem.put("y", pos.getY());
				jgridArray.put(jgriditem);
			}
		}

		JSONArray jcagesArray = new JSONArray();

		for (Cage cage : cages) {
			JSONObject jcage = new JSONObject();
			jcage.put("name", cage.getName());
			// jcage.put("x", cage.getX()); // notused
			// jcage.put("y", cage.getY()); // notused
			jcage.put("length", cage.getLength());
			jcage.put("width", cage.getWidth());
			jcage.put("depth", cage.getDepth());
			jcage.put("height", cage.getHeight());
			jcage.put("angle", cage.getAngle());
			jcage.put("form", cage.getType().name());
			jcage.put("params", "P01"); // link to the feed and stock data
			jcagesArray.put(jcage);

		}

		jgroup1.put("cages", jcagesArray);
		jgroup1.put("grid", jgridArray);
		jgroups.put(jgroup1);

		jlayout.put("groups", jgroups);

		// INPUTS: FEED and STOCK
		JSONArray jinputs = new JSONArray();

		JSONObject jinput1 = new JSONObject();
		jinput1.put("name", "P01");

		JSONObject jstock = new JSONObject();
		jstock.put("number", 100000);
		jstock.put("mass", 0.5);
		jinput1.put("stock", jstock);

		JSONObject jfeed = new JSONObject();
		jfeed.put("interval", 6 * 3600 * 1000);
		jfeed.put("numParticles", 100);
		jfeed.put("particleMass", 0.5 / 1000.0);
		jfeed.put("wasteFractionSolids", 0.03);
		jfeed.put("wasteFractionCarbon", 0.0005);
		jfeed.put("wasteFractionChemical", 0.002);
		jfeed.put("specificFeedRate", 0.007);
		jinput1.put("feed", jfeed);

		jinputs.put(jinput1);

		jlayout.put("inputs", jinputs);

		JSONObject wrapper = new JSONObject();
		wrapper.put("layout", jlayout);

		try (FileWriter fr = new FileWriter(layoutFile); BufferedWriter writer = new BufferedWriter(fr);) {
			writer.write(wrapper.toString(3));
		}

	}

	public ICageLayoutModel readLayout(String name) throws Exception {
		System.err.println("Loading cage layout: " + name);
		if (!projectFolder.exists())
			throw new IOException(String.format("project folder %s does not exist \n", projectFolder.getPath()));

		File layoutsFolder = new File(projectFolder, "layouts");
		if (!layoutsFolder.exists())
			throw new IOException(String.format("layout folder %s does not exist \n", layoutsFolder.getPath()));

		File layoutFile = new File(layoutsFolder, name + ".json");
		if (!layoutFile.exists())
			throw new IOException(String.format("layout file %s does not exist \n", layoutFile.getPath()));

		// read the cage layout from the json format .... TODO

		// {{layout: {name: "cages", cages: [{name: "C1:1", x: 2, y: 4, w: 20, l: 20, d:
		// 15, h: 5, type: "circ"...}, {}, {}...]} }

		StringBuffer buff = new StringBuffer();
		try (BufferedReader bin = new BufferedReader(new FileReader(layoutFile))) {
			String line = null;
			while ((line = bin.readLine()) != null) {
				buff.append(line);
			}
		} // automatically close file etc

		String json = buff.toString();

		// extract info

		JSONObject wrapper = new JSONObject(json);

		JSONObject jlayout = wrapper.getJSONObject("layout");
		String layoutName = jlayout.getString("name");

		// Layout: groups:[], inputs:[]

		// NOTE: using the mutable layout model not the generic interface
		CageLayoutModel layout = new CageLayoutModel(layoutName);

		// TODO: for now just create one of each we will mapo them later
		StockModel stock = null;
		IFeedModel feed = null;

		JSONArray jinputsArray = jlayout.getJSONArray("inputs");
		for (int ii = 0; ii < jinputsArray.length(); ii++) {

			JSONObject jinput1 = jinputsArray.getJSONObject(ii);
			String jinput1name = jinput1.getString("name");

			JSONObject jstock = jinput1.getJSONObject("stock");
			int nstock = jstock.getInt("number");
			double avmass = jstock.getDouble("mass");

			JSONObject jfeed = jinput1.getJSONObject("feed");
			long interval = jfeed.getLong("interval");
			double carbf = jfeed.getDouble("wasteFractionCarbon");
			double solidsf = jfeed.getDouble("wasteFractionSolids");
			double chemf = jfeed.getDouble("wasteFractionChemical");
			double sfr = jfeed.getDouble("specificFeedRate");
			double pmass = jfeed.getDouble("particleMass");
			int np = jfeed.getInt("numParticles");

			// these next are made up for now - they should be added from model configs
			stock = new StockModel(nstock, avmass); // 5*100000 = 500 tonnes per cage
			feed = new FeedModel(interval, np, pmass, solidsf, carbf, chemf, sfr); // 6 hours
			long HOURLY = 3600*1000L;
			long ONEDAY = 24*3600*1000L;
			//feed = new SpecialFeedModel(HOURLY, ONEDAY, 33750.0); // 5400kg per release

		}

		JSONArray jgroupArray = jlayout.getJSONArray("groups");
		for (int ig = 0; ig < jgroupArray.length(); ig++) {
			JSONObject jgroup = jgroupArray.getJSONObject(ig);
			String type = jgroup.getString("type");

			if (type.equalsIgnoreCase("REGULARGRID")) {

				JSONObject gparams = jgroup.getJSONObject("params");
				double xc = gparams.getDouble("xCorner");
				double yc = gparams.getDouble("yCorner");
				double dx = gparams.getDouble("iSpacing");
				double dy = gparams.getDouble("jSpacing");
				double brg = gparams.getDouble("bearing");
				// int nx = gparams.getInt("nx");
				// int ny = gparams.getInt("ny");

				RegularGridCagePositionManager rgm = new RegularGridCagePositionManager(xc, yc, dx, dy, brg);

				JSONArray jgridArray = jgroup.getJSONArray("grid");
				Map<String, GridPosition> gmap = new HashMap<String, GridPosition>();
				for (int igrid = 0; igrid < jgridArray.length(); igrid++) {
					JSONObject jgrid = jgridArray.getJSONObject(igrid);
					String cageID = jgrid.getString("id");
					int i = jgrid.getInt("i");
					int j = jgrid.getInt("j");
					gmap.put(cageID, new GridPosition(i, j));
				}

				JSONArray jcagesArray = jgroup.getJSONArray("cages");
				// sanity check: nx*ny == cages.length

				for (int ic = 0; ic < jcagesArray.length(); ic++) {
					JSONObject jcage = jcagesArray.getJSONObject(ic);
					String cid = jcage.getString("name");
					int ci = gmap.get(cid).getI();
					int cj = gmap.get(cid).getJ();

					double width = jcage.getDouble("width");
					double length = jcage.getDouble("length");
					double height = jcage.getDouble("height");
					double depth = jcage.getDouble("depth");
					double angle = jcage.getDouble("angle");
					CageType cageform = CageType.valueOf(jcage.getString("form"));
					// set xy to 0 for gridLayout
					// TODO:URGENT: use a clone of the stock and feed models!!!!
					StockModel cageStock = new StockModel(stock);
					Cage cage = new Cage(cid, 0, 0, width, length, height, depth, angle, cageStock, feed);
					cage.setType(cageform);
					rgm.add(cage, ci, cj);
				}

				layout.setPositionManager(rgm);

			} else if (type.equalsIgnoreCase("FREEFORM")) {

				FreeFormCagePositionManager ffm = new FreeFormCagePositionManager();
				
				JSONArray jgridArray = jgroup.getJSONArray("grid");
				Map<String, PositionVector> gmap = new HashMap<String, PositionVector>();
				for (int igrid = 0; igrid < jgridArray.length(); igrid++) {
					JSONObject jgrid = jgridArray.getJSONObject(igrid);
					String cageID = jgrid.getString("id");
					double x = jgrid.getDouble("x");
					double y = jgrid.getDouble("y");
					gmap.put(cageID, new PositionVector(x, y, 0.0));
				}

				JSONArray jcagesArray = jgroup.getJSONArray("cages");
				// sanity check: nx*ny == cages.length

				for (int ic = 0; ic < jcagesArray.length(); ic++) {
					JSONObject jcage = jcagesArray.getJSONObject(ic);
					String cid = jcage.getString("name");
					double cx = gmap.get(cid).getX();
					double cy = gmap.get(cid).getY();

					double width = jcage.getDouble("width");
					double length = jcage.getDouble("length");
					double height = jcage.getDouble("height");
					double depth = jcage.getDouble("depth");
					double angle = jcage.getDouble("angle");
					CageType cageform = CageType.valueOf(jcage.getString("form"));
					// set xy to 0 for gridLayout
					// TODO:URGENT: use a clone of the stock and feed models!!!!
					StockModel cageStock = new StockModel(stock);
					Cage cage = new Cage(cid, 0, 0, width, length, height, depth, angle, cageStock, feed);
					cage.setType(cageform);
					ffm.addCage(cage, new PositionVector(cx, cy, 0.0));
				}

				layout.setPositionManager(ffm);
				
			}

		}

		return layout;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the owner
	 */
	public String getOwner() {
		return owner;
	}

	/**
	 * @param owner the owner to set
	 */
	public void setOwner(String owner) {
		this.owner = owner;
	}

	/**
	 * @return the ccode
	 */
	public String getCcode() {
		return ccode;
	}

	/**
	 * @param ccode the ccode to set
	 */
	public void setCcode(String ccode) {
		this.ccode = ccode;
	}

	/**
	 * @return the geoLat
	 */
	public double getGeoLat() {
		return geoLat;
	}

	/**
	 * @param geoLat the geoLat to set
	 */
	public void setGeoLat(double geoLat) {
		this.geoLat = geoLat;
	}

	/**
	 * @return the geoLon
	 */
	public double getGeoLon() {
		return geoLon;
	}

	/**
	 * @param geoLon the geoLon to set
	 */
	public void setGeoLon(double geoLon) {
		this.geoLon = geoLon;
	}

	/**
	 * @return the author
	 */
	public String getAuthor() {
		return author;
	}

	/**
	 * @param author the author to set
	 */
	public void setAuthor(String author) {
		this.author = author;
	}

	/**
	 * @param tag
	 * @return
	 * @see java.util.List#contains(java.lang.Object)
	 */
	public boolean containsTag(String tag) {
		return tags.contains(tag);
	}

	/**
	 * @param atags
	 * @return true if added successfully
	 * @see java.util.List#addAll(java.util.Collection)
	 */
	public boolean addTags(Collection<String> atags) {
		return tags.addAll(atags);
	}

	/**
	 * @param tag
	 * @see java.util.List#add(java.lang.Object)
	 */
	public void addTag(String tag) {
		tags.add(tag);
	}

	/**
	 * @return the tags
	 */
	public List<String> getTags() {
		return tags;
	}

	/**
	 * @return the bathymetry
	 */
	public IBathymetryModel getBathymetry() {
		return bathymetry;
	}

	/**
	 * @param bathymetry the bathymetry to set
	 */
	public void setBathymetry(IBathymetryModel bathymetry) {
		this.bathymetry = bathymetry;
	}

	/**
	 * @return the flowmetry
	 */
	public IFlowModel getFlowmetry() {
		return flowmetry;
	}

	/**
	 * @param flowmetry the flowmetry to set
	 */
	public void setFlowmetry(IFlowModel flowmetry) {
		this.flowmetry = flowmetry;
	}

	/**
	 * @return the seabed
	 */
	public ISeabedModel getSeabed() {
		return seabed;
	}

	/**
	 * @param seabed the seabed to set
	 */
	public void setSeabed(ISeabedModel seabed) {
		this.seabed = seabed;
	}

	/**
	 * @return the projectFolder
	 */
	public File getProjectFolder() {
		return projectFolder;
	}

	/**
	 * @param projectFolder the projectFolder to set
	 */
	public void setProjectFolder(File projectFolder) {
		this.projectFolder = projectFolder;
	}

	@Override
	public String toString() {
		return String.format(
				"Project [name=%s, description=%s, owner=%s, ccode=%s, geoLat=%s, geoLon=%s, author=%s, tags=%s]", name,
				description, owner, ccode, geoLat, geoLon, author, tags);
	}

}
