package com.apt.project;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import com.apt.cli.util.ConfigurationProperties;
import com.apt.data.ChemicalTreatmentType;
import com.apt.instrumentation.InstrumentFactory;
import com.apt.instrumentation.Instrumentation;
import com.apt.models.IBathymetryModel;
import com.apt.models.IBedLayoutModel;
import com.apt.models.ICageLayoutModel;
import com.apt.models.IChemicalDegradeModel;
import com.apt.models.IDefacationModel;
import com.apt.models.IDisruptionModel;
import com.apt.models.IFlowModel;
import com.apt.models.IGrowthModel;
import com.apt.models.IMortalityModel;
import com.apt.models.ISeabedModel;
import com.apt.models.ISeasonalityModel;
import com.apt.models.ISettlingModel;
import com.apt.models.ITidalModel;
import com.apt.models.ITimeModel;
import com.apt.models.ITransportModel;
import com.apt.models.ITurbulenceModel;
import com.apt.models.cage.CageLayoutModel;
import com.apt.models.settling.DefaultSettlingModel;
import com.apt.models.transport.BedTransportFactory;
import com.apt.models.transport.Integrator;
import com.apt.models.transport.TransportFactory;
import com.apt.models.transport.TransportModel;


public class RunConfiguration {

	/** The outputs subfolder under the model folder.*/
	private File outputsFolder;
	
	/** The specific folder for this run.*/
	private File runFolder;
	
	/** Name for this run ??. */
	String name;
	
	/** Run number ??.*/
	int run;
	
	List<String> tags;
	
	ConfigurationProperties meta;
	
	private String projectName;
	private String modelName;
	
	private IBathymetryModel bathymetry;
	private IFlowModel flowmetry;
	private ITurbulenceModel turbulenceModel;
	private ISettlingModel settlingModel;
	private ITidalModel tidalModel;
	private ISeabedModel seabed;
	private ChemicalTreatmentType chemical;
	private IDisruptionModel disruptionModel;
	private ITimeModel timeModel;
	private TransportFactory transportFactory;
	private BedTransportFactory bedTransportFactory;
	private ICageLayoutModel cageLayoutModel;
	private IGrowthModel growthModel;
	private IMortalityModel mortalityModel;
	private IDefacationModel defacationModel;
	private IBedLayoutModel bedLayoutModel;
	private IChemicalDegradeModel chemicalModel;
	private ISeasonalityModel seasonalityModel;
	private Integrator integrator;
	private Instrumentation instruments;
	
	/** 
	 * Create a RunConfig using the name and tags list
	 * e.g. new RunConfiguration("LowCarbonDecay", "Run1", "SmallCages", "dd=30.0", "cx=15.5")
	 * @param name
	 * @param tags
	 */
	public RunConfiguration(String name, List<String> tags, ChemicalTreatmentType chemical, ICageLayoutModel layout) {
		this.name = name;
		this.tags = tags;
		this.chemical = chemical;
		this.cageLayoutModel = layout;
	}
	
	public void createNewRun(File modelFolder, String prefix, ConfigurationProperties config) throws Exception {

		// check we have the project folder and can access it.
		if (!modelFolder.exists())
			throw new IOException(
					String.format("model folder %s does not exist \n", modelFolder.getPath()));

		// check the model folder does NOT already exist.
		outputsFolder = new File(modelFolder, "outputs");
		if (!outputsFolder.exists())
			throw new IOException(String.format("outputs folder %s does not exists in %s \n",
					outputsFolder.getName(), modelFolder.getPath()));

		// read the outputs metadata
		ConfigurationProperties outputsMeta = readOutputsMetadata(outputsFolder);
		// inc run number and then save again
		run = outputsMeta.getIntValue("run", 0);
		run++;
		outputsMeta.setProperty("run", ""+run);
		
		runFolder = new File(outputsFolder, "run-"+run);
		// create the run-specific folder
		if (!runFolder.mkdir())
			throw new IOException(String.format("could not create run folder; %s", runFolder.getPath()));

		// rewrite the outputs metafile
		writeOutputsMetadata(outputsFolder, outputsMeta);
		
		// write our own metadata to the run folder
		writeInitialMetadata(runFolder);
		
	
		prepareInstrumentation(runFolder, String.format("%s(%d)",prefix, run), bathymetry, cageLayoutModel, seabed, config);
		

	}
	
	public void prepareInstrumentation(File runFolder, String prefix, IBathymetryModel bathymetry, ICageLayoutModel layout, ISeabedModel seabed, ConfigurationProperties config) throws Exception {
		instruments = InstrumentFactory.createInstruments(runFolder, prefix, bathymetry,layout, seabed, config);
	}
	
	private ConfigurationProperties readOutputsMetadata(File outputsFolder) throws IOException {
		ConfigurationProperties outputsMeta = new ConfigurationProperties();
		File outputsMetadataFile = new File(outputsFolder, "output.info");
		outputsMeta.load(new FileReader(outputsMetadataFile));
		return outputsMeta;
	}
	
	private void writeOutputsMetadata(File outputsFolder, ConfigurationProperties outputsMeta) throws IOException {
		File outputsMetadataFile = new File(outputsFolder, "output.info");
		outputsMeta.store(new FileWriter(outputsMetadataFile), "General outputs metadata");
	}
	
	/** Write out the initial metadata for this run. Called when the run is prepared.*/
	private void writeInitialMetadata(File runFolder) throws IOException {
		File runMetaFile = new File(runFolder, "run.info");
		meta = new ConfigurationProperties();

		// fill this with the run info - -whatever that is!
		meta.setProperty("name", name);
		meta.setProperty("run", ""+run);
		meta.setProperty("chemical", chemical.name());
		meta.setProperty("layout", cageLayoutModel.getLayoutName()); //layout.getName()
		meta.putList("tags", tags, ":");
		//meta.setProperty("duration", copied from model props);
		meta.store(new FileWriter(runMetaFile), "Run metadata");
	}
	
	
	
	/** Write out run metadata after the run completes.*/
	public void writePostrunMetadata(long start, long end, String status) throws Exception {
		File runMetaFile = new File(runFolder, "run.info");
		// fill this with the run info - -whatever that is!
		meta.setProperty("startTime", ""+start);
		meta.setProperty("endTime", ""+end);
		meta.setProperty("status", status);
		//meta.setProperty("duration", copied from model props);
		meta.store(new FileWriter(runMetaFile), "Run metadata");
	}
	
	/** Update and write out run metadata well after run to include any comments.*/
	private void updateMetadata(File runFolder, ConfigurationProperties meta, String comment) throws IOException {
		File runMetaFile = new File(runFolder, "run.info");
		meta.setProperty("comment", comment);
		meta.store(new FileWriter(runMetaFile), "Run metadata");	
	}
	
	/** 
	 * Load the run config from a file.
	 * @param file The file containing a run configuration in json format.
	 * @throws Exception If anything goes wrong during loading or the file does not exist.
	 */
	public void load(File file) throws Exception {
		
	}
	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	
	public int getRun() { return run;}
	
	/**
	 * @return the tags
	 */
	public List<String> getTags() {
		return tags;
	}

	
	
	/**
	 * @return the projectName
	 */
	public String getProjectName() {
		return projectName;
	}

	/**
	 * @param projectName the projectName to set
	 */
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	/**
	 * @return the modelName
	 */
	public String getModelName() {
		return modelName;
	}

	/**
	 * @param modelName the modelName to set
	 */
	public void setModelName(String modelName) {
		this.modelName = modelName;
	}

	/**
	 * @return the cageLayoutModel
	 */
	public ICageLayoutModel getCageLayoutModel() {
		return cageLayoutModel;
	}

	/**
	 * @param cageLayoutModel the cageLayoutModel to set
	 */
	public void setCageLayoutModel(ICageLayoutModel cageLayoutModel) {
		this.cageLayoutModel = cageLayoutModel;
	}

	
	/**
	 * @return the settlingModel
	 */
	public ISettlingModel getSettlingModel() {
		return settlingModel;
	}

	/**
	 * @param settlingModel the settlingModel to set
	 */
	public void setSettlingModel(ISettlingModel settlingModel) {
		this.settlingModel = settlingModel;
	}

	/**
	 * @return the disruption
	 */
	public IDisruptionModel getDisruptionModel() {
		return disruptionModel;
	}

	/**
	 * @param disruption the disruption to set
	 */
	public void setDisruptionModel(IDisruptionModel disruption) {
		this.disruptionModel = disruptionModel;
	}

	/**
	 * @return the chemical
	 */
	public ChemicalTreatmentType getChemical() {
		return chemical;
	}

	/**
	 * @param chemical the chemical to set
	 */
	public void setChemical(ChemicalTreatmentType chemical) {
		this.chemical = chemical;
	}

	/** Chained method to set the layout.*/
	public RunConfiguration layout(ICageLayoutModel cm) {
		this.setCageLayoutModel(cm);
		return this;
	}
	
	/** Chained method to set the chemical treatment type.*/
	public RunConfiguration chemical(ChemicalTreatmentType chemical) {
		this.setChemical(chemical);
		return this;
	}
	
	
	
	
	

	/**
	 * @return the bathymetry
	 */
	public IBathymetryModel getBathymetry() {
		return bathymetry;
	}

	/**
	 * @param bathymetry the bathymetry to set
	 */
	public void setBathymetry(IBathymetryModel bathymetry) {
		this.bathymetry = bathymetry;
	}

	/**
	 * @return the flowmetry
	 */
	public IFlowModel getFlowmetry() {
		return flowmetry;
	}

	/**
	 * @param flowmetry the flowmetry to set
	 */
	public void setFlowmetry(IFlowModel flowmetry) {
		this.flowmetry = flowmetry;
	}

	
	
	/**
	 * @return the turbulenceModel
	 */
	public ITurbulenceModel getTurbulenceModel() {
		return turbulenceModel;
	}

	/**
	 * @param turbulenceModel the turbulenceModel to set
	 */
	public void setTurbulenceModel(ITurbulenceModel turbulenceModel) {
		this.turbulenceModel = turbulenceModel;
	}


	
	/**
	 * @return the chemicalModel
	 */
	public IChemicalDegradeModel getChemicalModel() {
		return chemicalModel;
	}

	/**
	 * @param chemicalModel the chemicalModel to set
	 */
	public void setChemicalModel(IChemicalDegradeModel chemicalModel) {
		this.chemicalModel = chemicalModel;
	}
	
	

	/**
	 * @return the instruments
	 */
	public Instrumentation getInstruments() {
		return instruments;
	}

	/**
	 * @param instruments the instruments to set
	 */
	public void setInstruments(Instrumentation instruments) {
		this.instruments = instruments;
	}

	/**
	 * @return the tidalModel
	 */
	public ITidalModel getTidalModel() {
		return tidalModel;
	}

	/**
	 * @param tidalModel the tidalModel to set
	 */
	public void setTidalModel(ITidalModel tidalModel) {
		this.tidalModel = tidalModel;
	}

	/**
	 * @return the seabed
	 */
	public ISeabedModel getSeabed() {
		return seabed;
	}

	/**
	 * @param seabed the seabed to set
	 */
	public void setSeabed(ISeabedModel seabed) {
		this.seabed = seabed;
	}

	/**
	 * @return the timeModel
	 */
	public ITimeModel getTimeModel() {
		return timeModel;
	}

	/**
	 * @param timeModel the timeModel to set
	 */
	public void setTimeModel(ITimeModel timeModel) {
		this.timeModel = timeModel;
	}

	/**
	 * @return the transportModel
	 */
	public TransportFactory getTransportFactory() {
		return transportFactory;
	}
	
	/**
	 * @return the transportModel
	 */
	public BedTransportFactory getBedTransportFactory() {
		return bedTransportFactory;
	}

	
	/**
	 * @return the growthModel
	 */
	public IGrowthModel getGrowthModel() {
		return growthModel;
	}

	/**
	 * @param growthModel the growthModel to set
	 */
	public void setGrowthModel(IGrowthModel growthModel) {
		this.growthModel = growthModel;
	}

	/**
	 * @return the mortalityModel
	 */
	public IMortalityModel getMortalityModel() {
		return mortalityModel;
	}

	/**
	 * @param mortalityModel the mortalityModel to set
	 */
	public void setMortalityModel(IMortalityModel mortalityModel) {
		this.mortalityModel = mortalityModel;
	}

	/**
	 * @return the defaecationModel
	 */
	public IDefacationModel getDefaecationModel() {
		return defacationModel;
	}

	/**
	 * @param defacationModel the defaecationModel to set
	 */
	public void setDefaecationModel(IDefacationModel defacationModel) {
		this.defacationModel = defacationModel;
	}
	
	

	/**
	 * @return the integrator
	 */
	public Integrator getIntegrator() {
		return integrator;
	}

	/**
	 * @param integrator the integrator to set
	 */
	public void setIntegrator(Integrator integrator) {
		this.integrator = integrator;
	}

	/**
	 * @return the bedLayoutModel
	 */
	public IBedLayoutModel getBedLayoutModel() {
		return bedLayoutModel;
	}

	/**
	 * @param bedLayoutModel the bedLayoutModel to set
	 */
	public void setBedLayoutModel(IBedLayoutModel bedLayoutModel) {
		this.bedLayoutModel = bedLayoutModel;
	}

	/**
	 * @return the seasonalityModel
	 */
	public ISeasonalityModel getSeasonalityModel() {
		return seasonalityModel;
	}

	/**
	 * @param seasonalityModel the seasonalityModel to set
	 */
	public void setSeasonalityModel(ISeasonalityModel seasonalityModel) {
		this.seasonalityModel = seasonalityModel;
	}
	
	
	
	
	/**
	 * @return the outputsFolder
	 */
	public File getOutputsFolder() {
		return outputsFolder;
	}

	/**
	 * @return the runFolder
	 */
	public File getRunFolder() {
		return runFolder;
	}

	
	
	/**
	 *  Create a transportModel using the pre-configured bathy and flow models and a supplied tracker.
	 * 
	 * @param tracker
	 * @return
	 */
	public TransportFactory createTransportFactory() throws Exception {
		if (bathymetry == null)
			throw new Exception("bathymetry not defined for transport model");
		if (flowmetry == null)
			throw new Exception("flowmetry not defined for transport model");
		
		transportFactory = new TransportFactory(timeModel, bathymetry, flowmetry, tidalModel, turbulenceModel, settlingModel, seabed, integrator);
		return transportFactory;
	}
	
	public BedTransportFactory createBedTransportFactory() throws Exception {
		if (bathymetry == null)
			throw new Exception("bathymetry not defined for transport model");
		if (flowmetry == null)
			throw new Exception("flowmetry not defined for transport model");
		
		bedTransportFactory = new BedTransportFactory(timeModel, bathymetry, flowmetry, turbulenceModel, seabed, integrator);
		return bedTransportFactory;
	}
	

	@Override
	public String toString() {
		return String.format(
				"RunConfiguration [modelname=%s, tags=%s, bathymetry=%s, flowmetry=%s, chemical=%s, cageLayoutModel=%s, integrator=%s]",
				name, tags, bathymetry, flowmetry, chemical, cageLayoutModel, integrator.getName().toLowerCase());
	}
	
	
	
	
	
	
}
