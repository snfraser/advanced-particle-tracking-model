package com.apt.project;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Properties;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.apache.commons.io.FileUtils;

import com.apt.cli.util.ConfigurationProperties;
import com.apt.comms.AptmClient;
import com.apt.data.ChemicalTreatmentType;
import com.apt.data.PositionVector;
import com.apt.instrumentation.Instrumentation;
import com.apt.instrumentation.ParticleCellTracker;
import com.apt.instrumentation.TestInstrumentation;
import com.apt.engine.EngineFactory;
import com.apt.engine.IEngine;
import com.apt.models.Cage;
import com.apt.models.ICageLayoutModel;
import com.apt.models.transport.Integrator;
import com.apt.models.transport.IntegratorFactory;

public class Program {

	private File workingFolder;

	/**
	 * @param workingFolder
	 */
	public Program(File workingFolder) {
		this.workingFolder = workingFolder;
	}

	public Project readProject(String projectName) throws Exception {
		
		// check the project folder exists and contains a proj.info file
		File projectFolder = new File(workingFolder, projectName);
		if (!projectFolder.exists())
			throw new IOException(String.format("Project folder: %s not found in working dir: %s", projectName,
					workingFolder.getPath()));

		// read the metadata
		ConfigurationProperties config = readProjectMetadata(projectFolder);

		// construct project and load bathymetry, flowmetry and seabed info
		// String name = config.getProperty("name");
		String desc = config.getProperty("desc");
		String owner = config.getProperty("owner");
		String ccode = config.getProperty("cc");
		double geoLat = config.getDoubleValue("geolat");
		double geoLon = config.getDoubleValue("geolon");
		String author = config.getProperty("author");

		List<String> atags = config.getList("tags", ":");

		Project project = new Project(projectName, desc, owner, ccode, geoLat, geoLon, author);
		project.addTags(atags);

		project.setProjectFolder(projectFolder);
		return project;
	}

	/**
	 * Create the project specified in the config.
	 * 
	 * @param config Details of the project (metadata).
	 * @return The specified project.
	 * @throws Exception
	 */
	public Project createProject(ConfigurationProperties config) throws Exception {

		// Metadata
		String projectName = config.getProperty("name");
		System.err.println("We should be using project: "+ projectName);
		String description = config.getProperty("desc");
		String owner = config.getProperty("owner");
		String cc = config.getProperty("cc");
		double geoLat = config.getDoubleValue("geolat");
		double geoLon = config.getDoubleValue("geolon");
		String author = config.getProperty("author", System.getProperty("user.name"));
		// extract a list of tags here....
		List<String> tags = config.getList("tags", ":");

		String regime = config.getProperty("regime");
		if (regime == null) 
			throw new Exception("CreateProject:: regime not specified");
		
		Project project = new Project(projectName, description, owner, cc, geoLat, geoLon, author);
		project.addTags(tags);

		// check we have the root folder and can access it.
		if (!workingFolder.exists())
			throw new IOException(String.format("working folder %s does not exist \n", workingFolder));

		// check the project folder does NOT already exist.
		File projectFolder = new File(workingFolder,projectName);
		if (projectFolder.exists())
			throw new IOException(String.format("project folder %s already exists in %s \n", projectFolder.getName(),
					workingFolder.getPath()));

		// create the project folder
		if (!projectFolder.mkdir())
			throw new IOException("could not create project folder");

		// create the subdirs - layouts, models,
		File layouts = new File(projectFolder, "layouts");
		layouts.mkdir();
		
		// create models folder
		File models = new File(projectFolder, "models");
		models.mkdir();
		
		// create bathymetry folder
		File bathy = new File(projectFolder, "bathymetry");
		bathy.mkdir();
		
		// create flowmetry folder
		File flow = new File(projectFolder, "flowmetry");
		flow.mkdir();
		
		// create seabed folder
		File seabed = new File(projectFolder, "seabed");
		seabed.mkdir();
		
		writeProjectMetadata(projectFolder, project);
		
		// pull down the regime configs and create a project config file projname.properties
		AptmClient client = new AptmClient();
		List<Config> configs = client.getRegimeConfigs();
	
		String regimeHeader = "Regime:"+regime;
		for (Config c : configs) {
			System.err.printf("retrieved regime: %s\n", c.getName());
			if (c.getName().equalsIgnoreCase(regimeHeader)) {
				Properties p = c.getProps();
				writeProjectConfiguration(p, projectFolder);
			}
		}

		return project;

	}

	public ConfigurationProperties readProjectMetadata(File projectFolder) throws Exception {
		ConfigurationProperties meta = new ConfigurationProperties();
		File metaFile = new File(projectFolder, "project.info");
		meta.load(new FileReader(metaFile));
		return meta;
	}
	
	public void writeProjectMetadata(File projectFolder, Project project) throws Exception {
		ConfigurationProperties meta = new ConfigurationProperties();
		meta.setProperty("name", project.getName());
		meta.setProperty("desc", project.getDescription());
		//meta.setProperty("created", String.format("%tFT%tT", model.getCreationDate(), model.getCreationDate()));
		meta.setProperty("owner", project.getOwner());
		meta.setProperty("author", project.getAuthor());
		meta.setProperty("cc", project.getCcode());
		meta.setProperty("geolat", ""+project.getGeoLat());
		meta.setProperty("geolon", ""+project.getGeoLon());
		meta.putList("tags", project.getTags(), ":");

		File metaFile = new File(projectFolder, "project.info");
		meta.store(new FileWriter(metaFile), "Model metadata");
	}
	
	public void writeProjectConfiguration(Properties props, File projectFolder) throws Exception {
		File metaFile = new File(projectFolder, "project.properties");
		props.store(new FileWriter(metaFile), "Project configuration");
	}

	public Model readModel(Project project, String modelName) throws Exception {
		// check we have the project folder
		File projectFolder = new File(workingFolder, project.getName());
		if (!projectFolder.exists())
			throw new IOException(String.format("Project folder: %s not found in working dir: %s", projectFolder,
					workingFolder.getPath()));

		// check the model exists
		File modelsFolder = new File(projectFolder, "models");
		if (!modelsFolder.exists())
			throw new IOException(
					String.format("Models folder: models/ not found in project dir: %s", projectFolder.getPath()));
		
		File modelFolder = new File(modelsFolder, modelName);
		if (!modelFolder.exists())
			throw new IOException(
					String.format("Model folder: %s not found in models dir: %s", modelName, modelsFolder.getPath()));

		// load model metadata
		ConfigurationProperties modelMeta = readModelMetadata(modelFolder);
		// name/desc/created/author/tags
		String desc = modelMeta.getProperty("desc");
		String author = modelMeta.getProperty("author");
		String strcreate = modelMeta.getProperty("created");
		long created = (Project.SDF.parse(strcreate)).getTime();
		List<String> atags = modelMeta.getList("tags", ":");

		Model model = new Model(modelName, desc, author, created);
		model.addTags(atags);

		model.setModelFolder(modelFolder);

		return model;
	}

	/**
	 * Create the model specified in the config.
	 * 
	 * @param project The project containing the model
	 * @param config  Details of the model (metadata).
	 * @return The specified model.
	 * @throws Exception
	 */
	public Model createModel(Project project, ConfigurationProperties config) throws Exception {

		// Metadata
		String modelName = config.getProperty("name");
		String description = config.getProperty("desc");
		String author = config.getProperty("author", System.getProperty("user.name"));
		// extract a list of tags here....
		List<String> tags = config.getList("tags", ":");

		Model model = new Model(modelName, description, author, System.currentTimeMillis());
		model.addTags(tags);

		// check we have the root folder and can access it.
		if (!workingFolder.exists())
			throw new IOException(String.format("working folder %s does not exist \n", workingFolder));

		// check we have the project folder and can access it.
		File projectFolder = new File(workingFolder, project.getName());
		if (!projectFolder.exists())
			throw new IOException(String.format("project folder %s does not exist \n", projectFolder));

		File modelsFolder = new File(projectFolder, "models");
		if (!modelsFolder.exists())
			throw new IOException(String.format("models folder %s does not exist in %s \n", projectFolder.getName(),
					projectFolder.getPath()));

		
		// check the model folder does NOT already exist.
		File modelFolder = new File(modelsFolder, modelName);
		if (modelFolder.exists())
			throw new IOException(String.format("model folder %s already exists in %s \n", modelsFolder.getName(),
					projectFolder.getPath()));

		// create the model folder
		if (!modelFolder.mkdir())
			throw new IOException("could not create model folder");

		// create the model directory structure under the project folder.
		//copyModelTemplate(modelFolder); // the models subdir is now part of the template!!

		// write out the model's metadata
		writeModelMetadata(modelFolder, model);

		return model;
	}

	/**
	 * Write the model metadata.
	 * 
	 * @param modelFolder The root of the model in the file-system.
	 * @param model
	 * @throws IOException
	 */
	private void writeModelMetadata(File modelFolder, Model model) throws IOException {

		ConfigurationProperties meta = new ConfigurationProperties();
		meta.setProperty("name", model.getName());
		meta.setProperty("desc", model.getDescription());
		meta.setProperty("created", String.format("%tFT%tT", model.getCreationDate(), model.getCreationDate()));
		meta.setProperty("author", model.getAuthor());
		meta.putList("tags", model.getTags(), ":");

		File metaFile = new File(modelFolder, "model.info");
		meta.store(new FileWriter(metaFile), "Model metadata");

	}

	/**
	 * Copy the model layout from the inernal template to the file-system.
	 * 
	 * @param modelFolder The root of the model in the file-system.
	 * @throws IOException
	 */
	private void copyModelTemplate(File modelFolder) throws IOException {

		// this file should contain the full model file structure template.
		try (InputStream in = getClass().getResourceAsStream("/com/apt/project/resources/model.zip");
				ZipInputStream zip = new ZipInputStream(in)) {

			// Use resource

			ZipEntry entry;

			while ((entry = zip.getNextEntry()) != null) {
				System.err.printf("processing template zip entry: %s %d bytes\n", entry.getName(), entry.getSize());
				if (entry.isDirectory()) {
					// FileUtil.createFolder(projectRoot, entry.getName());
					File targetFolder = new File(entry.getName());
					// FileUtils.createParentDirectories(targetFolder);
					System.err.printf("expecting to create folder: %s \n", entry.getName());
				} else {
					writeFile(zip, modelFolder, entry.getName());
					// writeFile(zip, modelSubFolder, null);
				}
			}

		}

	}

	private void writeFile(ZipInputStream zip, File modelSubFolder, String filename) throws IOException {

		File targetFile = new File(modelSubFolder, filename);

		// FileUtils.copyInputStreamToFile(zip, targetFile);

		FileUtils.copyToFile(zip, targetFile);

	}

	private ConfigurationProperties readModelMetadata(File modelFolder) throws IOException {
		ConfigurationProperties meta = new ConfigurationProperties();
		File metaFile = new File(modelFolder, "model.info");
		meta.load(new FileReader(metaFile));
		return meta;
	}

	/**
	 * 
	 * @param project
	 * @param model
	 * @param config The supplied config from the command line or pre-launcher but not including any model config.
	 * @return
	 * @throws Exception
	 */
	public RunConfiguration createRunConfig(Project project, Model model, ConfigurationProperties config) throws Exception {
		
		String name = config.getProperty("name"); // what name is this??? comes from command line args ...= uuid??
		String chemicalName = config.getProperty("chemical");
		String layoutName = config.getProperty("layout");
		
		// TODO override the instruments defined in inst.json in the model/inst/ folder
		// format liek:  --inst ptrack{n=500}:bwatch{x=550.0, y=1234.0, w=500, h=200}:mcdep{r=16}
		
		IEngine engine = EngineFactory.createEngine(config.getProperty("engine"));
		
		//String instOverride = config.getProperty("inst");
	
		//String engineName = config.getProperty("engine");
		
		List<String> atags = config.getList("tags", ":");
		
		ChemicalTreatmentType chemical = ChemicalTreatmentType.valueOf(chemicalName.toUpperCase());
		
		// read the layout into the project and use
		ICageLayoutModel layout = project.readLayout(layoutName);
	
		System.err.printf("Loaded cage layout: nc: %d\n", layout.cages().size());
		for (Cage c : layout.cages()) {
			PositionVector v = layout.getPositionManager().getPosition(c.getName());
			System.err.printf("Cage: %s at: (%f, %f)", c.getName(), v.getX(), v.getY());
		}
		
		RunConfiguration runner = new RunConfiguration(name, atags, chemical, layout); // name??
		
		// TODO add these 2 methods to proj and model ....
		//project.getMetaData();
		//model.getMetaData();
		
		runner.setProjectName(project.getName());// various other params here also desc, author, etc
		runner.setModelName(model.getName()); // various other params here also desc, author etc
		
		
		
		// copy project level models
		runner.setBathymetry(project.getBathymetry());
		runner.setFlowmetry(project.getFlowmetry());
	
		runner.setSeabed(project.getSeabed());
		
		runner.setCageLayoutModel(layout);//??? already got this
		
		// generate the integrator
		String integratorName = config.getProperty("integrator", "EULER");
		Integrator integrator = IntegratorFactory.getIntegrator(integratorName, project.getFlowmetry());
		
		// copy model level models
		// TODO runner.useModel(model);
		runner.setSeasonalityModel(model.getSeasonalityModel());
		runner.setBedLayoutModel(model.getBedLayoutModel());
		runner.setDisruptionModel(model.getDisruptionModel());
		runner.setSettlingModel(model.getSettlingModel());
		runner.setDefaecationModel(model.getDefaecationModel());
		runner.setTurbulenceModel(model.getTurbulenceModel());
		runner.setChemicalModel(model.getChemicalModel());
		runner.setTidalModel(model.getTidalModel());
		runner.setGrowthModel(model.getGrowthModel());
		runner.setMortalityModel(model.getMortalityModel());
		runner.setTimeModel(model.getTimeModel());
		runner.setIntegrator(integrator);
		runner.setInstruments(model.getInstrumentation());
	
		
		runner.createTransportFactory(); 
		runner.createBedTransportFactory(); 
		
		// create the run-specific subfolder in the models output folder. /proj/model/outputs/run-xxx
		String prefix = config.getProperty("prefix", "Test01");
		String fullprefix = String.format("%s/%s-%s", model.getName(), layout.getLayoutName(), prefix);
		runner.createNewRun(model.getModelFolder(), fullprefix, model.getModelConfig()); 
	
		// setup instrumentation here - from config or inst.properties in model? for now from model
		//runner.prepareInstrumentation();// ie set the runfolder for it....

		//runner.setEngine(engine);
		
		return runner;
	}

}