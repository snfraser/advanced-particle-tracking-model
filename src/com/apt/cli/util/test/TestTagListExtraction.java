package com.apt.cli.util.test;

import java.util.List;
import java.util.Properties;

import com.apt.cli.util.ConfigurationProperties;

public class TestTagListExtraction {

	public static void main(String[] args) {
		try {
			ConfigurationProperties p = new ConfigurationProperties();
			String TAGS = "aa:bb:cc:dd:ee:ff";
			p.setProperty("tags", TAGS);
			
			List<String> tags = p.getList("tags", ":");
			System.err.printf("Extract tags from: %s \n", TAGS);
			for (String s : tags) {
				System.err.printf("Tag: %s \n", s);
			}
			
			String OTHERS = "ggg,hhh,iii,jjj,kkk";
			p.setProperty("others", OTHERS);
			
			List<String> others = p.getList("others", ",");
			System.err.printf("Extract tags from: %s \n", OTHERS);
			for (String s : others) {
				System.err.printf("Other: %s \n", s);
			}
			
			String MORE = "ll/mm/nn/oo/pp/qq";
			p.setProperty("more", MORE);
			
			List<String> more = p.getList("more", "/");
			System.err.printf("Extract tags from: %s \n", MORE);
			for (String s : more) {
				System.err.printf("More: %s \n", s);
			}
					
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
