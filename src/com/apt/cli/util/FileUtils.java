package com.apt.cli.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

public class FileUtils {

	static final String APTM_HOME = System.getProperty("user.home") + "/" + ".aptm";

	/**
	 * Get the working directory either suplied in the config as key dir or by
	 * reading the .working file.
	 * 
	 * @param config
	 * @return
	 * @throws Exception
	 */
	public static File getWorkingDir(ConfigurationProperties config) throws Exception {

		// the current working directory
		File wd = readWorkingDir();

		// check the config and default to wd if not specified
		File dir = config.getFile("dir", wd);
		return dir;

	}

	public static File readWorkingDir() throws Exception {

		File working = new File(APTM_HOME, ".working");
		ConfigurationProperties config = new ConfigurationProperties();
		config.load(new FileReader(working));
		File wd = config.getFile("working");

		return wd;
	}

	public static ConfigurationProperties loadConfig(File file) throws Exception {
		ConfigurationProperties config = new ConfigurationProperties();

		try (FileReader fr = new FileReader(file);) {
			config.load(fr);
		}
		return config;
	}

}
