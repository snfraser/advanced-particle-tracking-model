package com.apt.cli.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.Properties;
import java.util.SimpleTimeZone;

public class ConfigurationProperties extends Properties {
	
	/** Create a ConfigurationProperties. */
	public ConfigurationProperties() {
		super();
	}

	/** Create a ConfigurationProperties with specified defaults. */
	public ConfigurationProperties(Properties defaults) {
		super(defaults);
	}

	/** Returns the property - trimmed. */
	public String getProperty(String key) {
		String value = super.getProperty(key);
		if (value == null)
			return null;
		return value.trim();
	}

	/** Returns the property - trimmed. */
	public String getProperty(String key, String def) {
		String value = super.getProperty(key);
		if (value == null)
			return def;
		return value.trim();
	}
	
	/** Add a set of proeprties these can overwrite the existing ones.
	 * 
	 * @param other A set of properties to merge.
	 */
	public void add(ConfigurationProperties other) {
		Enumeration ek = other.keys();
		while (ek.hasMoreElements()) {
			String k = (String)ek.nextElement();
			String v = other.getProperty(k);
			setProperty(k, v);
		}
	}
	
	/** Extract a list of tags set out like: key=v1:v2:v3:v4..*/
	public List<String> getList(String key, String seperator) {
		String strval = super.getProperty(key);
		String[] parts = strval.split(seperator);
		if (parts.length < 1)
			return null;
		List<String> list = new ArrayList<String>();
		for (int i = 0; i < parts.length; i++) {
			list.add(parts[i]);
		}
		return list;
	}
	
	public void putList(String key, List<String> tags, String seperator) {
		StringBuffer b = new StringBuffer();
		int i = 0;
		for (String s : tags) {
			if (i != 0)
				b.append(seperator);
			b.append(s);
			i++;
		}
		setProperty(key, b.toString());
	}

	/** Returns the int value of a property for specified key. */
	public int getIntValue(String key) throws ParseException {
		try {
			return Integer.parseInt(getProperty(key));
		} catch (Exception e) {
			throw new ParseException("Parsing key: " + key + ":" + e.toString(), 0);
		}
	}

	/**
	 * Returns the int value of a property for specified key or the supplied
	 * default.
	 */
	public int getIntValue(String key, int def) {
		try {
			return Integer.parseInt(getProperty(key));
		} catch (Exception e) {
			return def;
		}
	}

	/** Returns the float value of a property for specified key. */
	public float getFloatValue(String key) throws ParseException {
		try {
			return Float.parseFloat(getProperty(key));
		} catch (Exception e) {
			throw new ParseException("Parsing key: " + key + ":" + e.toString(), 0);
		}
	}

	/**
	 * Returns the float value of a property for specified key or the supplied
	 * default.
	 */
	public float getFloatValue(String key, float def) {
		try {
			return Float.parseFloat(getProperty(key));
		} catch (Exception e) {
			return def;
		}
	}

	/** Returns the double value of a property for specified key. */
	public double getDoubleValue(String key) throws ParseException {
		try {
			return Double.parseDouble(getProperty(key));
		} catch (Exception e) {
			throw new ParseException("Parsing key: " + key + ":" + e.toString(), 0);
		}
	}

	/**
	 * Returns the double value of a property for specified key or the supplied
	 * default.
	 */
	public double getDoubleValue(String key, double def) {
		try {
			return Double.parseDouble(getProperty(key));
		} catch (Exception e) {
			return def;
		}
	}

	/** Returns the long value of a property for specified key. */
	public long getLongValue(String key) throws ParseException {
		try {
			return (long) Long.parseLong(getProperty(key));
		} catch (Exception e) {
			throw new ParseException("Parsing key: " + key + ":" + e.toString(), 0);
		}
	}

	/**
	 * Returns the long value of a property for specified key or the supplied
	 * default.
	 */
	public long getLongValue(String key, long def) {
		try {
			return (long) Long.parseLong(getProperty(key));
		} catch (Exception e) {
			return def;
		}
	}

	/** Returns the boolean value of a property for specified key. */
	public boolean getBooleanValue(String key, boolean def) {
		if (getProperty(key) == null)
			return def;
		return Boolean.valueOf(getProperty(key)).booleanValue();
	}

	/** Returns the boolean value of a property for specified key. */
	public boolean getBooleanValue(String key) {
		return getBooleanValue(key, false);
	}

	/** Returns a Date from a string version of a long. */
	public Date getDateValue(String key) throws ParseException {
		try {
			return new Date(Long.parseLong(getProperty(key)));
		} catch (Exception e) {
			throw new ParseException("Parsing key: " + key + ":" + e.toString(), 0);
		}
	}

	/** Returns a Date from a string version of a long or the supplied default. */
	public Date getDateValue(String key, Date def) {
		try {
			return new Date(Long.parseLong(getProperty(key)));
		} catch (Exception e) {
			return def;
		}
	}

	/** Returns a Date from a string with supplied format. */
	public Date getDateValue(String key, String fmt) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat(fmt);
		sdf.setTimeZone(new SimpleTimeZone(0, "UTC"));
		return sdf.parse(getProperty(key));
	}

	/** Returns an existing File from its path name in a property key. */
	public File getFile(String key) throws FileNotFoundException {
		if (key == null || getProperty(key) == null)
			throw new FileNotFoundException("Cannot locate file for key (" + key + ")");
		File file = new File(getProperty(key));
		if (!file.exists())
			throw new FileNotFoundException("No such file: " + file.getPath());
		return file;
	}

	/**
	 * Returns a File from its path name in a property key or the supplied default.
	 */
	public File getFile(String key, String def) throws FileNotFoundException {
		try {
			return getFile(key);
		} catch (FileNotFoundException e) {
			File file = new File(def);
			if (!file.exists())
				throw new FileNotFoundException("No such file: " + file.getPath());
			return file;
		}
	}

	/**
	 * Returns a File from its path name in a property key or the supplied default.
	 */
	public File getFile(String key, File file) throws FileNotFoundException {
		try {
			return getFile(key);
		} catch (FileNotFoundException e) {
			if (!file.exists())
				throw new FileNotFoundException("No such file: " + file.getPath());
			return file;
		}
	}
}
