package com.apt.cli.test;

import com.apt.cli.AptmCreateModel;
import com.apt.cli.AptmCreateProject;
import com.apt.cli.util.ConfigurationProperties;

public class TestCreateModel {

	public static void main(String[] args) {
		try {

			ConfigurationProperties config = new ConfigurationProperties();
			config.setProperty("project","NorthBayInner"); 
			config.setProperty("name","VarFeed3b");
			config.setProperty("desc","A farm in South corner of Narrow Sound near Inversprochy with high flow regime");
			config.setProperty("author","snf");
			config.setProperty("tags", "VarFeed:MedMortality:HighFlow:ChannelLocation");
			
			AptmCreateModel c = new AptmCreateModel();
			c.execute(config);
			
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
