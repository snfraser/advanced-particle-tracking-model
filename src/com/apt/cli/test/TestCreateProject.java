package com.apt.cli.test;

import com.apt.cli.AptmCreateProject;
import com.apt.cli.util.ConfigurationProperties;

public class TestCreateProject {

	public static void main(String[] args) {
		try {

			ConfigurationProperties config = new ConfigurationProperties();
			//config.setProperty("",""); NO DIR USE WD
			config.setProperty("name","SouthChannel3");
			config.setProperty("desc","A farm in South Channel near Big Island with turbulent flow");
			config.setProperty("geolat","55.8");
			config.setProperty("geolon","-5.8");
			config.setProperty("cc","GBR");
			config.setProperty("author","snf");
			config.setProperty("owner","Argyll Salmon");
			config.setProperty("tags", "MedFeed:MedMortality:FastFlow");
			config.setProperty("regime", "CHL");
			
			AptmCreateProject c = new AptmCreateProject();
			c.execute(config);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
