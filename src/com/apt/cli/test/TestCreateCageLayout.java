package com.apt.cli.test;

import java.io.File;

import com.apt.cli.AptmRunModel;
import com.apt.cli.util.CommandTokenizer;
import com.apt.cli.util.ConfigurationProperties;
import com.apt.cli.util.FileUtils;
import com.apt.models.Cage;
import com.apt.data.CageType;
import com.apt.models.cage.CageLayoutModel;
import com.apt.models.cage.RegularGridCagePositionManager;
import com.apt.models.feed.FeedModel;
import com.apt.models.stock.StockModel;
import com.apt.project.Program;
import com.apt.project.Project;

public class TestCreateCageLayout {

	
	
	/**
	 * 
	 */
	public TestCreateCageLayout() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		
		try {
			// get command line args...
			// expecting: --dir --project --model --layout --chem --tags
			ConfigurationProperties config = new ConfigurationProperties();
			config.setProperty("project", "WestInlet");
			config.setProperty("name", "TG02Reg");
			config.setProperty("nx", ""+2);
			config.setProperty("ny", ""+8);
			config.setProperty("angle", ""+20.0);
			config.setProperty("xc", ""+1000.0);
			config.setProperty("yc", ""+1000.0);
			config.setProperty("dx", ""+40.0);
			config.setProperty("dy", ""+40.0);
			config.setProperty("wx", ""+20.0);
			config.setProperty("wy", ""+20.0);
			config.setProperty("depth", ""+10.0);
			config.setProperty("height", ""+5.0);
			TestCreateCageLayout test = new TestCreateCageLayout();
			test.execute(config);
						
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	private void execute(ConfigurationProperties config) throws Exception {
	
		File workingFolder = FileUtils.getWorkingDir(config);
		
		Program program = new Program(workingFolder);
		
		String projectName = config.getProperty("project");
		Project project = program.readProject(projectName);
		
		System.err.printf("Project: %s \n", project);
		
		String name = config.getProperty("name");
		int nx = config.getIntValue("nx");
		int ny = config.getIntValue("ny");
		double xc = config.getDoubleValue("xc");
		double yc = config.getDoubleValue("yc");
		double dx = config.getDoubleValue("dx");
		double dy = config.getDoubleValue("dy");
		double ww = config.getDoubleValue("wx");
		double ll = config.getDoubleValue("wy");
		double hh = config.getDoubleValue("height");
		double dd = config.getDoubleValue("depth");
		double angleDegNorth = config.getDoubleValue("angle");
		
		// convert to angle cw from North to acw from East, thence to radians
		double angleDegEast = (angleDegNorth < 90.0 ? 90.0 - angleDegNorth : 450.0 - angleDegNorth);
		double angleRadEast = -Math.toRadians(angleDegNorth);
		System.err.printf("Angle conversion: Supplied: %f cwn -> %f ace -> %f ace \n", angleDegNorth, angleDegEast, angleRadEast);
		
		CageLayoutModel cm = new CageLayoutModel(name);
		RegularGridCagePositionManager pm = new RegularGridCagePositionManager(xc, yc, dx, dy, angleDegEast);
		
		// create a bunch of cages
		for (int i = 0; i < nx; i++) {
			double xa = i*dx;
			for (int j = 0; j < ny; j++) {
				double ya = j*dy;
				
				double x = xc + xa*Math.cos(angleRadEast) - ya*Math.sin(angleRadEast);
				double y = yc + xa*Math.sin(angleRadEast) + ya*Math.cos(angleRadEast);
				
				//System.err.printf("Position for: %d,%d is xa: %f ya: %f  ->  x: %f y: %f \n", i, j, xa, ya, x, y);
				StockModel stock = new StockModel(100000, 0.5); // 5*100000 = 500 tonnes per cage
				
				FeedModel feed = new FeedModel(24 * 3600 * 1000L, 100, 0.5/1000.0, 0.03, 0.35, 0.002, 0.007); // 6 hours (ms)
				
				String cname = String.format("C%d:%d", i, j);
				Cage c = new Cage(cname, 0, 0, ww, ll, hh, dd, 0.0, stock, feed);
				c.setType(CageType.CIRCULAR);
				System.err.printf("Add cage: %s at (%f,%f) contains: %d fish avg: %f kg biomass: %f kg \n",
						c.getName(), x, y, stock.getStockNumber(), stock.getAverageFishMass(),
						stock.getBiomass());
				//cm.addCage(c);
				pm.add(c, i, j);
			}
		}
		
		cm.setPositionManager(pm);
		
		// now save the layout 
		project.createLayout(cm, true);
		
	}
	

}
