package com.apt.cli.test;

import com.apt.cli.AptmCreateModel;
import com.apt.cli.AptmCreateProject;
import com.apt.cli.util.ConfigurationProperties;

public class TestCreateProjectAnd1Model {

	public static void main(String[] args) {
		// first create a newproject
		try {

			ConfigurationProperties config = new ConfigurationProperties();
			//config.setProperty("",""); NO DIR USE WD
			config.setProperty("name","RoughSoundNW");
			config.setProperty("desc","A farm in an inlet near Tromso with turbulent flow regime");
			config.setProperty("geolat","64.2");
			config.setProperty("geolon","24.5");
			config.setProperty("cc","NOR");
			config.setProperty("author","snf");
			config.setProperty("owner","TromsoLax");
			config.setProperty("tags", "StdFeed:MedMortality:SlowFlow:Constricted");
			config.setProperty("regime", "NOR");
			
			AptmCreateProject c = new AptmCreateProject();
			c.execute(config);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		// second create a model in the project
		try {

			ConfigurationProperties config = new ConfigurationProperties();
			config.setProperty("project","RoughSoundNW"); 
			config.setProperty("name","HighFaeces");
			config.setProperty("desc","A test of a high faeces environment");
			config.setProperty("author","snf");
			config.setProperty("tags", "LowStock:HighMortality:TurbFlow:InletLocation");
			
			AptmCreateModel c = new AptmCreateModel();
			c.execute(config);
			
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void execute(ConfigurationProperties config) {
		
	}
}
