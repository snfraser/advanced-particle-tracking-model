/**
 * 
 */
package com.apt.cli.test;

import java.io.File;

import com.apt.cli.util.ConfigurationProperties;
import com.apt.cli.util.FileUtils;
import com.apt.data.CageType;
import com.apt.data.PositionVector;
import com.apt.models.Cage;
import com.apt.models.cage.CageLayoutModel;
import com.apt.models.cage.FreeFormCagePositionManager;
import com.apt.models.cage.RegularGridCagePositionManager;
import com.apt.models.feed.FeedModel;
import com.apt.models.stock.StockModel;
import com.apt.project.Program;
import com.apt.project.Project;

/**
 * @author SA05SF
 *
 */
public class TestCreateFreeformCageLayout {

	/**
	 * 
	 */
	public TestCreateFreeformCageLayout() {

	}

	public static void main(String args[]) {

		try {
			// get command line args...
			// expecting: --dir --project --model --layout --chem --tags
			ConfigurationProperties config = new ConfigurationProperties();
			config.setProperty("project", "WestInlet");
			config.setProperty("name", "TG06Box");
			config.setProperty("nc", "" + 8);
			config.setProperty("angle", "" + 20.0);
			config.setProperty("xc", "" + 1000.0);
			config.setProperty("yc", "" + 1000.0);
			config.setProperty("wx", "" + 300.0);
			config.setProperty("wy", "" + 300.0);
			config.setProperty("width", "" + 20.0);
			config.setProperty("length", "" + 20.0);
			config.setProperty("depth", "" + 10.0);
			config.setProperty("height", "" + 5.0);
			TestCreateFreeformCageLayout test = new TestCreateFreeformCageLayout();
			test.execute(config);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void execute(ConfigurationProperties config) throws Exception {

		File workingFolder = FileUtils.getWorkingDir(config);

		Program program = new Program(workingFolder);

		String projectName = config.getProperty("project");
		Project project = program.readProject(projectName);

		System.err.printf("Project: %s \n", project);

		String name = config.getProperty("name");
		int nc = config.getIntValue("nc");
		double xc = config.getDoubleValue("xc");
		double yc = config.getDoubleValue("yc");
		double wx = config.getDoubleValue("wx");
		double wy = config.getDoubleValue("wy");
		double ww = config.getDoubleValue("width");
		double ll = config.getDoubleValue("length");
		double hh = config.getDoubleValue("height");
		double dd = config.getDoubleValue("depth");
		double angleDegNorth = config.getDoubleValue("angle");

		// convert to angle cw from North to acw from East, thence to radians
		double angleDegEast = (angleDegNorth < 90.0 ? 90.0 - angleDegNorth : 450.0 - angleDegNorth);
		double angleRadEast = -Math.toRadians(angleDegNorth);
		System.err.printf("Angle conversion: Supplied: %f cwn -> %f ace -> %f ace \n", angleDegNorth, angleDegEast,
				angleRadEast);

		CageLayoutModel cm = new CageLayoutModel(name);
		FreeFormCagePositionManager ffm = new FreeFormCagePositionManager();

		// create a 10x10 grid, we will use only nc of these cells
		boolean[][] used = new boolean[10][10]; // all false
		
		int ii = 0;
		for (int i = 0; i < 10; i++) {
			for (int j = 0; j < 10; j++) {
				if (Math.random() < nc/100.0 && !used[i][j]) {
					double xa = xc - 0.5*wx; // BL corner
					double ya = yc - 0.5*wy;
					
					double x = xa + i*(wx/10.0) + (Math.random() - 0.5)*2.0;
					double y = ya + j*(wy/10.0) + (Math.random() - 0.5)*2.0;
					
					used[i][j] = true;
					//System.err.printf("Create cage in box: (%d, %d) at: (%f, %f) \n", i, j, x, y);
					
					StockModel stock = new StockModel(100000, 0.5); // 5*100000 = 500 tonnes per cage

					FeedModel feed = new FeedModel(24 * 3600 * 1000L, 100, 0.5 / 1000.0, 0.03, 0.35, 0.002, 0.007); // 6 hours (ms)

					double ww2 = ww + Math.random()*10.0;
					
					String cname = String.format("C%d", ii);
					Cage c = new Cage(cname, 0, 0, ww2, ww2, hh, dd, 0.0, stock, feed);
					c.setType(CageType.CIRCULAR);
					double volume = 0.25*Math.PI*c.getWidth()*c.getWidth()*c.getHeight();
					double stockdensity = stock.getBiomass()/volume; // c.getVolume()
					System.err.printf("Add cage: %s (%d,%d) at (%f,%f) contains: %d fish avg: %f kg biomass: %f kg sd: %f kg/m3\n", 
							c.getName(), i, j, x, y, stock.getStockNumber(), stock.getAverageFishMass(), stock.getBiomass(), stockdensity);
					// cm.addCage(c);
					ffm.addCage(c, new PositionVector(x, y, 0.0));
					ii++;
				}
			}
		}
	
		cm.setPositionManager(ffm);

		// now save the layout
		project.createLayout(cm, true);

	}

}
