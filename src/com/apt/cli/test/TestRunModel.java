package com.apt.cli.test;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.SplashScreen;
import java.lang.Runtime.Version;

import com.apt.cli.AptmRunModel;
import com.apt.cli.util.ConfigurationProperties;
import com.apt.gui.MassDepositionPanel;
import com.apt.gui.SurfaceDepositionPanel;
import com.apt.instrumentation.TestInstrumentation;
import com.apt.project.Project;

public class TestRunModel {

	public static double totalLostmass = 0.0;

	public static void main(String[] args) {
		try {

			// run from command line as:
			// aptm-run
			// --name S1T34 --project BigBayN --model VarFeed3b
			// --layout T09 -- chem EMBZ --eng Test --int EULER
			// --tags "FirstRun:VarTau02:Other"

			// ID = T<username><hhmmss> eg Tsa05sf092235
			long now = System.currentTimeMillis();
			String ID = String.format("T%s%tH%tm%ts", System.getProperty("user.name"), now, now, now);
			ConfigurationProperties config = new ConfigurationProperties();
			// config.setProperty("dir",""); NO DIR USE WD
			config.setProperty("name", ID);
			config.setProperty("project", "WestInlet");
			config.setProperty("model", "LowStock2");
			config.setProperty("layout", "TG11QA");
			config.setProperty("chemical", "embz");
			config.setProperty("engine", "TEST");
			config.setProperty("integrator", "EULER");
			config.setProperty("tags", "ET_StepFlow:");
			config.setProperty("prefix", "X");

			Version v = Runtime.version();
			// Package pack = Runtime.getRuntime().getClass().getPackage();
			// String tit = pack.getSpecificationTitle();
			// String vendor = pack.getSpecificationVendor();
			// String ver = pack.getSpecificationVersion();

			System.err.println();
			System.err.printf("================================================\n");
			System.err.printf(" Starting APTM using: Java Version: %s\n", v);
			System.err.printf("================================================\n");
			System.err.println();

			totalLostmass = 0.0;

			AptmRunModel run = new AptmRunModel();
			splash(5000L);
			
			run.execute(config);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	static void splash(long delay) {
		// wait a while before dropping the splashscreen.
		try {
			Thread.sleep(delay);
		} catch (InterruptedException ix) {
		}

		// catch and kill the splashscreen
		SplashScreen screen = SplashScreen.getSplashScreen();
		if (screen != null) {
			// can add a countdown or messages here
			Graphics2D g = screen.createGraphics();
			g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
			g.setFont(new Font("Arial", Font.BOLD, 32));
			g.setPaintMode();
			g.setColor(Color.ORANGE);
			// string start xy in bottom left corner
			g.drawString("Launching NDart...", 50, 250);

			screen.update();

			try {
				Thread.sleep(delay);
			} catch (InterruptedException ix) {
			}

			screen.close();
		}
	}

}
