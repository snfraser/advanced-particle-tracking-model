package com.apt.cli.test;

import java.io.File;
import java.util.List;

import com.apt.cli.util.ConfigurationProperties;
import com.apt.cli.util.FileUtils;
import com.apt.data.GridPosition;
import com.apt.models.Cage;
import com.apt.models.ICageLayoutModel;
import com.apt.models.cage.ICagePositionManager;
import com.apt.models.cage.RegularGridCagePositionManager;
import com.apt.project.Program;
import com.apt.project.Project;

public class TestReadCageLayout {

	
	
	/**
	 * 
	 */
	public TestReadCageLayout() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		
		try {
			ConfigurationProperties config = new ConfigurationProperties();
			config.setProperty("project", "WestInlet");
			config.setProperty("layout", "TG09BovA");
			
			TestReadCageLayout t = new TestReadCageLayout();
			t.execute(config);
			
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	// TODO: This does not yet conform with PositionManager !!!!!
	public void execute(ConfigurationProperties config) throws Exception {
		
		File workingFolder = FileUtils.getWorkingDir(config);
		
		Program program = new Program(workingFolder);
		
		String projectName = config.getProperty("project");
		String layoutName = config.getProperty("layout");
		
		Project project = program.readProject(projectName);
		
		System.err.printf("Project: %s \n", project);
		
		ICageLayoutModel layout = project.readLayout(layoutName);
		
		System.err.printf("Layout: %s \n", layout);
		
		ICagePositionManager pm = layout.getPositionManager();
		
		List<Cage> cages = pm.getCages();
		for (Cage c : cages) {
			String cid = c.getName();
			GridPosition grid = ((RegularGridCagePositionManager)pm).getGridPosition(cid);
			double x = pm.getPosition(cid).getX();
			double y = pm.getPosition(cid).getY();
			System.err.printf("cage: (%d,%d) %s at: (%f, %f) \n", grid.getI(), grid.getJ(), cid, x, y);
			
			
			
		}
		
		
		
	}

}
