package com.apt.cli;

import java.awt.AlphaComposite;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.SplashScreen;
import java.io.File;
import java.util.List;

import javax.imageio.ImageIO;
import javax.print.attribute.standard.MediaSize.Engineering;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import org.jfree.chart.util.PaintAlpha;

import com.apt.cli.util.CommandTokenizer;
import com.apt.cli.util.ConfigurationProperties;
import com.apt.cli.util.FileUtils;
import com.apt.models.Cage;
import com.apt.data.ChemicalTreatmentType;
import com.apt.data.Domain;
import com.apt.instrumentation.ParticleCellTracker;
import com.apt.engine.EngineFactory;
import com.apt.engine.IEngine;
import com.apt.engine.IEngineController;
import com.apt.engine.multistream.MultistreamEngine;
import com.apt.engine.x.TestEngine;
import com.apt.gui.BathymetryPanel;
import com.apt.gui.BedLevelsCountPlot;
import com.apt.gui.CombinedTimeSeries1;
import com.apt.gui.CombinedTimeSeries2;
import com.apt.gui.ControlPanel;
import com.apt.gui.MassDepositionPanel;
import com.apt.gui.MassTimeSeriesPanel;
import com.apt.gui.ParticleTrackPanel;
import com.apt.gui.SeabedStateDisplay;
import com.apt.gui.SurfaceDepositionPanel;
import com.apt.instrumentation.Instrumentation;
import com.apt.instrumentation.NullInstrumentation;
import com.apt.instrumentation.TestInstrumentation;
import com.apt.models.IBathymetryModel;
import com.apt.models.ICageLayoutModel;
import com.apt.models.IChemicalDegradeModel;
import com.apt.models.IFlowModel;
import com.apt.models.IGrowthModel;
import com.apt.models.ISeabedModel;
import com.apt.models.ISettlingModel;
import com.apt.models.ITurbulenceModel;
import com.apt.models.ITidalModel;
import com.apt.models.bathymetry.FixedBathymetryModel;
import com.apt.models.bed.RegularGridBedLayoutModel;
import com.apt.models.cage.CageLayoutModel;
import com.apt.models.chemical.NoChemicalDegradeModel;
import com.apt.models.defaecation.DefacationModel;
import com.apt.models.disruption.ExponentialDisruptionModel;
import com.apt.models.feed.FeedModel;
import com.apt.models.flow.FixedFlowModel;
import com.apt.models.growth.FixedBiomassGrowthModel;
import com.apt.models.growth.GrowthModel;
import com.apt.models.mortality.MortalityModel;
import com.apt.models.seabed.SeabedModel;
import com.apt.models.seasonality.SeasonalityModel;
import com.apt.models.settling.DefaultSettlingModel;
import com.apt.models.stock.StockModel;
import com.apt.models.tidal.FixedTidalModel;
import com.apt.models.time.TimeModel;
import com.apt.models.transport.BedTransportModel;
import com.apt.models.transport.EulerIntegrator;
import com.apt.models.transport.Integrator;
import com.apt.models.transport.TransportModel;
import com.apt.models.turbulence.DefaultTurbulenceModel;
import com.apt.project.Model;
import com.apt.project.Program;
import com.apt.project.Project;
import com.apt.project.RunConfiguration;

public class AptmRunModel {

	/**
	 * 
	 */
	public AptmRunModel() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {

		try {
			// get command line args...
			ConfigurationProperties config = CommandTokenizer.use("--").parse(args);
			System.out.printf("Config contains: %d args\n", config.size());

			AptmRunModel m = new AptmRunModel();
			m.execute(config); // return immediately

			// catch and kill the splashscreen
			try {
				Thread.sleep(5000L);
			} catch (InterruptedException ix) {
			}

			SplashScreen screen = SplashScreen.getSplashScreen();
			if (screen != null) {

				Graphics2D g = screen.createGraphics();
				// Do some drawing on the graphics object
				// Now update to the splash screen
				g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
				g.setFont(new Font("Arial", Font.BOLD, 32));
				g.setPaintMode();
				g.setColor(Color.ORANGE);
				// string start xy in bottom left corner
				g.drawString("Launching NDart...", 50, 250);

				screen.update();

				try {
					Thread.sleep(1000L);
				} catch (InterruptedException ix) {
				}
			} // next countdown step

			screen.close();
		} catch (

		Exception e) {
			e.printStackTrace();
		}

	}

	public void execute(ConfigurationProperties config) throws Exception {

		ExecutionThread ethread = new ExecutionThread(config);
		ethread.start();

	}

	/*
	 * private TestEngine fakeModels() { // time model 0 to 360 days at 1 minute
	 * intervals TimeModel tm = new TimeModel(0L, 360 * 86400 * 1000L, 60000L);
	 * 
	 * Domain domain = new Domain(0, 2000, 0, 2000); FixedBathymetryModel bm = new
	 * FixedBathymetryModel(domain); IFlowModel fm = new FixedFlowModel();
	 * RegularGridBedLayoutModel bed = new RegularGridBedLayoutModel(bm, fm, null);
	 * 
	 * ITidalModel tidm = new FixedTidalModel(0.0);
	 * 
	 * ITurbulenceModel turb = new DefaultTurbulenceModel(0.02, 0.02, 0.01);
	 * ISettlingModel settling = new DefaultSettlingModel();
	 * ExponentialDisruptionModel dim = new ExponentialDisruptionModel(0.9, 4.0); //
	 * IFlowModel fm = new NetcdfFlowModel(FF1); ISeabedModel sea = null; Integrator
	 * integrator = new EulerIntegrator(fm); ParticleCellTracker tracker = new
	 * ParticleCellTracker();
	 * 
	 * IChemicalDegradeModel chm = new NoChemicalDegradeModel(); TransportModel
	 * trans = new TransportModel(bm, fm, tidm, turb, settling, sea, integrator,
	 * null); BedTransportModel btrans = new BedTransportModel(bm, fm, turb, sea,
	 * integrator, null); CageLayoutModel cm = new CageLayoutModel("cages1");
	 * DefacationModel dm = new DefacationModel(); MortalityModel mm = new
	 * MortalityModel(0.14); SeasonalityModel sem = new SeasonalityModel(0.0, 0.0);
	 * // midnight 1 jan/1 jun N/S Hemispheres
	 * 
	 * // some cages 2x6 array, 12 cages total, 250 tonnes per cage = 3000 tonnes
	 * total for (int i = 0; i < 2; i++) { double x = 980.0 + i * 40.0; for (int j =
	 * 0; j < 6; j++) { double y = 780.0 + j * 40.0; StockModel stock = new
	 * StockModel(100000, 0.5); // 5*100000 = 500 tonnes per cage FeedModel feed =
	 * new FeedModel(6 * 3600 * 1000L, 100, 0.5 / 1000.0, 0.03, 0.00005, 0.002,
	 * 0.007); // 6 // hours // (ms) String name = String.format("C%d:%d", i, j);
	 * Cage c = new Cage(name, x, y, 20.0, 20.0, 16.0, 8.0, 0.0, stock, feed);
	 * System.err.
	 * printf("Add cage: %s at (%f,%f) contains: %d fish avg: %f kg biomass: %f kg \n"
	 * , c.getName(), c.getX(), c.getY(), stock.getStockNumber(),
	 * stock.getAverageFishMass(), stock.getBiomass()); // cm.addCage(c); } } //
	 * GrowthModel gm = new GrowthModel(); IGrowthModel gm = new
	 * FixedBiomassGrowthModel(null); // temp to model according to SEPA rules.
	 * 
	 * NullInstrumentation none = new NullInstrumentation(null); TestEngine engine =
	 * new TestEngine(tm, trans, btrans, dim, cm, gm, dm, mm, sem, bed, sea, chm,
	 * none); return engine; }
	 */

	private class ExecutionThread extends Thread {

		ConfigurationProperties config;

		/**
		 * @param config
		 */
		public ExecutionThread(ConfigurationProperties config) {
			super();
			this.config = config;
		}

		public void run() {

			try {
				File workingFolder = FileUtils.getWorkingDir(config);

				IEngine engine = EngineFactory.createEngine(config.getProperty("engine"));

				Program program = new Program(workingFolder);

				Project project = program.readProject(config.getProperty("project"));
				project.load(); // load the various configs - {bathy, flow, seabed}

				Model model = program.readModel(project, config.getProperty("model"));
				model.load(project.getBathymetry(), project.getFlowmetry(), project.getSeabed(), config);

				// create a run-config using the project and model settings and params passed in
				RunConfiguration runner = program.createRunConfig(project, model, config);

				// TEMP:: create some instrumentation here to override what was specd in model
				// configs
				IBathymetryModel bathymetry = runner.getBathymetry();
				ICageLayoutModel layout = runner.getCageLayoutModel();
				ISeabedModel seabed = runner.getSeabed();

				// String prefix =
				// project.getName()+"/"+model.getName()+"/"+layout.getLayoutName();
				// String prefix = model.getName()+"/"+layout.getLayoutName();
				// Instrumentation instruments = createInstrumentation(runner.getRunFolder(),
				// prefix, bathymetry, layout, seabed, config);

				// TODO : add a MultilayerFrame here...
				// MultilayerFrame f = new MultilayerFrame();
				// f.addTab(); // various tabs
				// f.addtab("Deposition").addLayer(new BathyLayer(bathy)).addLayer(new
				// FlowLayer(flow));

				ControlPanel control = new ControlPanel();

				JFrame f = new JFrame();
				f.getContentPane().add(control.createPanel((IEngineController) engine));
				f.setBounds(30, 600, 300, 100);
				f.setVisible(true);

				// System.err.printf("USING Growth model: %s \n", );
				// JOptionPane.showConfirmDialog(null, "Start run...", "NDart Simulation
				// Launcher v1.0",
				// JOptionPane.YES_NO_OPTION);
				// Got all the configuration, now run it...
				engine.run(runner);

			} catch (Exception e) {
				e.printStackTrace();
			}

		}

	}

}
