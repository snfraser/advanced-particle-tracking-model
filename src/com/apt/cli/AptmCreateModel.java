package com.apt.cli;

import java.io.File;
import java.sql.ClientInfoStatus;
import java.util.List;

import com.apt.cli.util.CommandTokenizer;
import com.apt.cli.util.ConfigurationProperties;
import com.apt.cli.util.FileUtils;
import com.apt.project.Model;
import com.apt.project.Program;
import com.apt.project.Project;

public class AptmCreateModel {

	
	/**
	 * 
	 */
	public AptmCreateModel() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		try {
			// get command line args...
			// expecting:  --dir --project ---desc  [--author OR $user] --tags
			ConfigurationProperties config = CommandTokenizer.use("--").parse(args); 
			AptmCreateModel m = new AptmCreateModel();
			m.execute(config);
			
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	public void execute(ConfigurationProperties config) throws Exception {

		File workingFolder = FileUtils.getWorkingDir(config);
		String projName = config.getProperty("project");
		
		Program program = new Program(workingFolder);
		Project project = program.readProject(projName);
		
		program.createModel(project, config);
		
		
	}
}
