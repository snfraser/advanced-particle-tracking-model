package com.apt.cli;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import com.apt.cli.util.CommandTokenizer;
import com.apt.cli.util.ConfigurationProperties;
import com.apt.cli.util.FileUtils;
import com.apt.project.Program;
import com.apt.project.Project;

public class AptmCreateProject {

	/**
	 * 
	 */
	public AptmCreateProject() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		try {
			
			// get command line args...
			// expecting:  --dir --project ---desc --owner --lat --lon --cc [--author OR $user] --tags
			ConfigurationProperties config = CommandTokenizer.use("--").parse(args); 
			AptmCreateProject p = new AptmCreateProject();
			p.execute(config);
			
		} catch (Exception e) {
			e.printStackTrace();
			// TODO generate proper output
		}
	}
	
	/** TODO we should catch any exceptions and prefix them with: apt-create-model: */
	public void execute(ConfigurationProperties config) throws Exception {

		// this will fall over if the specified wd does not exist..
		File workingFolder = FileUtils.getWorkingDir(config);
		System.err.println("we should be using WD: "+workingFolder.getPath());
		Program program = new Program(workingFolder);
		program.createProject(config);
		
	}

}
